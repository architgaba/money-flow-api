package com.mfm.api;


import com.mfm.api.domain.payment.plan.Plan;
import com.mfm.api.repository.payment.plan.PlanRepository;
import com.mfm.api.repository.payment.product.ProductRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserReferenceTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserRoleRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.util.response.AppProperties;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Product;
import com.stripe.model.Sku;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableCaching
public class MoneyFlowManagerApplication implements CommandLineRunner {


    private static Logger logger = LogManager.getLogger(MoneyFlowManagerApplication.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRoleRepository roleRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private UserReferenceTypeRepository referenceTypeRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private AppProperties appProperties;

    @PostConstruct
    public void init() {
        Stripe.apiKey = appProperties.getStripeSecretKey();
    }

    public static void main(String[] args) {

        SpringApplication.run(MoneyFlowManagerApplication.class, args);

        logger.info("|------------------------------------------|");
        logger.info("|                                          |");
        logger.info("|     MONEY FLOW MANAGER SERVER START      |");
        logger.info("|                                          |");
        logger.info("|------------------------------------------|");
        // logger.info("System Property :::: "+System.getProperty("networkaddress.cache.ttl"));

    }

    @Override
    public void run(String... args) throws Exception {
//        List<User> users = new ArrayList<>();
//        User user = new User();
//        user.setUserName("ppceo");
//        user.setFirstName("Donna");
//        user.setLastName("Campbell");
//        user.setEmailAddress("support@p2pconnection.net");
//        user.setPassword(passwordEncoder.encode("Nolanap5+"));
//        user.setRole(roleRepository.findByRoleCode(5));
//        user.setUserStatus(userStatusRepository.findByStatusCode(1));
//        user.setReferenceType(referenceTypeRepository.findByReferenceCode(5));
//        users.add(user);
//        userRepository.saveAll(users);
//        List<Referrers> referrersList = new ArrayList<>();
//        Referrers referrers = new Referrers();
//        referrers.setReferrerCode(1000l);
//        referrers.setReferrerType("ADMIN");
//        referrersList.add(referrers);
//        referrers.setUser(userRepository.findByUserName("ppceo"));
//        referrers.setReferrerType("SUPER_ADMIN");
//        referrersList.add(referrers);
//        referrerRepository.saveAll(referrersList);
//        productCreation1();
//        productCreation2();
//        createInitialFirstPlan();
//        createInitialSecondPlan();
//        createInitialThirdPlan();
    }


    public void createInitialFirstPlan() {
        Map<String, Object> planParams = new HashMap<String, Object>();
        planParams.put("amount", amountConverter(13.75f));
        planParams.put("interval", "month");
        planParams.put("product", productRepository.findAll().stream().findFirst().get().getStripeProductId());
        planParams.put("nickname", "Customer");
        planParams.put("currency", "usd");
        try {
            com.stripe.model.Plan planEO = com.stripe.model.Plan.create(planParams);
            if (planEO != null) {
                Plan newPlan = new Plan();
                newPlan.setPlanName(planEO.getNickname());
                newPlan.setStripePlanId(planEO.getId());
                newPlan.setProduct(productRepository.findByProductType("Customer"));
                newPlan.setPlanAmount(13.75f);
                newPlan.setPlanDescription("It is designed to assist you in keeping more of your hard earned income. Get You Paycheck Checkup today and use the Money Flow Manager App to assist you in minimizing your taxes by tracking and recording your business related expenses and reduce your taxable income, therefore; reduce the amount of tax you owe");
                newPlan.setPlanType("Customer");
                newPlan.setPlanCurrency("usd");
                newPlan.setPlanInterval(WordUtils.capitalize(planEO.getInterval()) + "ly");
                newPlan.setPlanActiveStatus(true);
                newPlan = planRepository.save(newPlan);
                if (newPlan != null) {
                    logger.info("Plan successfully saved");
                } else
                    logger.info("error");
            } else {
                logger.info("server down");
            }
        } catch (StripeException e) {
            logger.info("Trace Exception ::: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public void createInitialSecondPlan() {
        Map<String, Object> planParams = new HashMap<String, Object>();
        planParams.put("amount", amountConverter(137.27f));
        planParams.put("interval", "year");
        planParams.put("product", productRepository.findAll().stream().findFirst().get().getStripeProductId());
        planParams.put("nickname", "Customer");
        planParams.put("currency", "usd");
        try {
            com.stripe.model.Plan planEO = com.stripe.model.Plan.create(planParams);
            if (planEO != null) {
                Plan newPlan = new Plan();
                newPlan.setPlanName(planEO.getNickname());
                newPlan.setStripePlanId(planEO.getId());
                newPlan.setProduct(productRepository.findByProductType("Customer"));
                newPlan.setPlanAmount(137.27f);
                newPlan.setPlanDescription("It is designed to assist you in keeping more of your hard earned income. Get You Paycheck Checkup today and use the Money Flow Manager App to assist you in minimizing your taxes by tracking and recording your business related expenses and reduce your taxable income, therefore; reduce the amount of tax you owe");
                newPlan.setPlanType("Customer");
                newPlan.setPlanCurrency("usd");
                newPlan.setPlanInterval(WordUtils.capitalize(planEO.getInterval()) + "ly");
                newPlan.setPlanActiveStatus(true);
                newPlan = planRepository.save(newPlan);
                if (newPlan != null) {
                    logger.info("Plan successfully saved");
                } else
                    logger.info("error");
            } else {
                logger.info("server down");
            }
        } catch (StripeException e) {
            logger.info("Trace Exception ::: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void createInitialThirdPlan() {
        Map<String, Object> planParams = new HashMap<String, Object>();
        planParams.put("price", amountConverter(42.34f));
        com.mfm.api.domain.payment.product.Product product = productRepository.findByProductType("Customer-Rep");
        planParams.put("product", product.getStripeProductId());
        planParams.put("currency", "usd");
        Map<String, Object> inventoryParams = new HashMap<>();
        inventoryParams.put("type", "infinite");
        planParams.put("inventory", inventoryParams);
        try {
            Sku product1 = Sku.create(planParams);
            if (product1 != null) {
                Plan newPlan = new Plan();
                newPlan.setPlanName("Customer Rep");
                newPlan.setStripePlanId(product1.getId());
                newPlan.setProduct(product);
                newPlan.setPlanAmount(42.34f);
                newPlan.setPlanDescription("You are automatically qualified to refer other customers and earn a referrer fee.  You will earn $5 for every customer each month for as long as they continue to pay their customer fee of $12.99 each month. Refer 100 customers and earn $500 per month, it’s just that simple. Once you set up your Customer Rep account, you will be sent a link to order your Pay Card which will be used to pay your referral fees.");
                newPlan.setPlanType("Customer-Rep");
                newPlan.setPlanCurrency("usd");
                newPlan.setPlanInterval("One Time Pay");
                newPlan.setPlanActiveStatus(true);
                newPlan = planRepository.save(newPlan);
                if (newPlan != null) {
                    logger.info("Plan successfully saved");
                } else
                    logger.info("error");
            } else {
                logger.info("server down");
            }
        } catch (StripeException e) {
            logger.info("Trace Exception ::: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public String amountConverter(float amount) {
        String stringAmount = String.valueOf((int) (amount * 100));
        return stringAmount;
    }

    private void productCreation1() throws Exception {
        Map<String, Object> productParams = new HashMap<>();
        productParams.put("name", "Money Flow Manager Customer Products DEVELOPMENT");
        productParams.put("type", "service");
        Product product = Product.create(productParams);
        productRepository.save(new com.mfm.api.domain.payment.product.Product(product.getId(), product.getName(),
                "Customer"));
    }

    private void productCreation2() throws Exception {
        Map<String, Object> productParams = new HashMap<>();
        productParams.put("name", "Money Flow Manager Customer-Rep Products DEVELOPMENT");
        productParams.put("type", "good");
        productParams.put("shippable", "false");
        Product product = Product.create(productParams);
        productRepository.save(new com.mfm.api.domain.payment.product.Product(product.getId(), product.getName(),
                "Customer-Rep"));
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }


}