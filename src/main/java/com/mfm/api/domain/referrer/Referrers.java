package com.mfm.api.domain.referrer;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "mfm_referrers")
@Data
@EqualsAndHashCode(callSuper = false)
public class Referrers extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "referer_gen")
    @SequenceGenerator(name = "referer_gen", sequenceName = "referer_seq", allocationSize = 1)
    private Long referrerId;

    @NotNull
    private String referrerType;

    @Column(updatable = false, unique = true, nullable = false)
    private Long referrerCode;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;


}
