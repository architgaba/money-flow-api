package com.mfm.api.domain.countries;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "mfm_states")
@Data
public class States implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer stateId;

    @NotNull
    private String stateName;

    @NotNull
    private String stateCode;


}
