package com.mfm.api.domain.countries;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="mfm_countries")
@EqualsAndHashCode(callSuper = false)
@Data
public class Countries implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long countryId;

    @NotNull
    private String countryCode;

    @NotNull
    private String countryName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "countryId")
    private List<States> statesList = new ArrayList<>(0);

}
