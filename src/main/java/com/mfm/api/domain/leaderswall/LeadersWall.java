package com.mfm.api.domain.leaderswall;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_leaders_wall")
@Data
@EqualsAndHashCode(callSuper = false)
public class LeadersWall extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "leaders_wall_gen")
    @SequenceGenerator(name = "leaders_wall_gen", sequenceName = "leaders_wall_gen", allocationSize = 1)
    private Long leadersWallId;

    @ManyToOne
    @JoinColumn(name = "leadersWallCategoriesId")
    @JsonIgnore
    private LeadersWallCategories leadersWallCategories;

    private String imageUrl;

    @Transient
    private String categoryName;
}
