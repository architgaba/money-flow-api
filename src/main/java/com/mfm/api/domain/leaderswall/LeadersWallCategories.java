package com.mfm.api.domain.leaderswall;

import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_leaders_wall_categories")
@Data
@EqualsAndHashCode(callSuper = false)
public class LeadersWallCategories extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "leaders_wall_categories_gen")
    @SequenceGenerator(name = "leaders_wall_categories_gen", sequenceName = "leaders_wall_categories_gen", allocationSize = 1)
    private Long leadersWallCategoriesId;

    private String categoryName;

    private Integer aapRequirement;
}
