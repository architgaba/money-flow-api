package com.mfm.api.domain.moneyflowmanager.expense;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.account.RecurrenceInterval;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;


@Data
@Entity
@Table(name = "money_flow_expenses")
@EqualsAndHashCode(callSuper = false)
public class Expense extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "expense")
    @SequenceGenerator(name = "expense", sequenceName = "expense_seq", allocationSize = 1)
    @Column(unique = true, updatable = false, nullable = false)
    private Long moneyFlowExpenseId;

    private String expenseType;

    @Transient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String type;

    @Column(name = "date")
    @JsonIgnore
    private LocalDate entryDate;

    @NotNull
    private float amount;

    @Column(columnDefinition = "TEXT")
    private String source;

    @Column(columnDefinition = "TEXT")
    private String notes;

    private boolean isRecurring;

    private boolean isUnassigned;

    @Column(name = "recurrence_date")
    @JsonIgnore
    private LocalDate recurringDate;

    private RecurrenceInterval recurrenceInterval;

    @Transient
    private String date;

    @Transient
    private String recurrenceDate;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moneyFlowAccountId", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MoneyFlowAccount moneyFlowAccount;

    private String receiptKey;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Transient
    private String recurringType;

    public Expense(Long moneyFlowExpenseId, String expenseType, String type, float amount, String source, String notes,
                   boolean isRecurring, String date, String recurrenceDate, MoneyFlowAccount moneyFlowAccount, String receiptKey, boolean isUnassigned, RecurrenceInterval recurrenceInterval) {
        this.moneyFlowExpenseId = moneyFlowExpenseId;
        this.expenseType = expenseType;
        this.type = type;
        this.amount = amount;
        this.source = source;
        this.notes = notes;
        this.isRecurring = isRecurring;
        this.date = date;
        this.recurrenceDate = recurrenceDate;
        this.moneyFlowAccount = moneyFlowAccount;
        this.receiptKey = receiptKey;
        this.recurringType = expenseType;
        this.isUnassigned = isUnassigned;
        this.recurrenceInterval = recurrenceInterval;
    }

    public Expense() {
    }
}
