package com.mfm.api.domain.moneyflowmanager.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
@Table(name = "money_flow_accounts", uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "moneyFlowName"}))
@EqualsAndHashCode(callSuper = false)
@ToString(exclude = {"user", "vehicles"})
public class MoneyFlowAccount extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "money_flows")
    @SequenceGenerator(name = "money_flows", sequenceName = "money_flows", allocationSize = 1)
    @Column(unique = true, updatable = false, nullable = false)
    private Long moneyFlowAccountId;

    @NotNull
    private String moneyFlowType;

    @NotNull
    private String moneyFlowName;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = true, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

}
