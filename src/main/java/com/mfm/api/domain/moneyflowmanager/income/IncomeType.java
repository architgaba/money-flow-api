package com.mfm.api.domain.moneyflowmanager.income;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "money_flow_income_types")
@Data
@EqualsAndHashCode(callSuper = false)
public class IncomeType extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "income_type_generator")
    @SequenceGenerator(name="income_type_generator", sequenceName = "income_type_seq",allocationSize = 1,initialValue = 5)
    private Long typeId;

    private String name;

    private boolean isCustom;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = true,updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
}
