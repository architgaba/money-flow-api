package com.mfm.api.domain.moneyflowmanager.budget;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "money_flow_budgets")
@EqualsAndHashCode(callSuper = false)
public class Budget extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "budget_seq")
    @SequenceGenerator(name = "budget_seq",sequenceName = "budget_seq",allocationSize = 1)
    @Column(unique = true,updatable = false,nullable = false)
    private Long moneyFlowBudgetId;

    @Transient
    private String fromDate;

    @Transient
    private String toDate;

    @JsonIgnore
    @Column(name = "from_date",nullable = false)
    private LocalDate startDate;

    @JsonIgnore
    @Column(name = "to_date",nullable = false)
    private LocalDate endDate;

    @NotNull
    private Float amount;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moneyFlowAccountId", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MoneyFlowAccount moneyFlowAccount;

    public Budget(Long moneyFlowBudgetId, String fromDate, String toDate, @NotNull Float amount) {
        this.moneyFlowBudgetId = moneyFlowBudgetId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.amount = amount;
    }

    public Budget() {
    }
}
