package com.mfm.api.domain.moneyflowmanager.mileagetracker;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.util.converter.CustomDateConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "money_flow_mileages")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(exclude = {"vehicle", "moneyFlowAccount"})
public class Mileage extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "money_flow_mileage")
    @SequenceGenerator(name = "money_flow_mileage", sequenceName = "money_flow_mileage", allocationSize = 1)
    @Column(name = "mileage_id", updatable = false, unique = true, nullable = false)
    private Long id;

    @Transient
    private String vehicleName;

    @Transient
    private String flowName;

    @Transient
    private Long vehicleId;

    @ManyToOne
    @JoinColumn(name = "vehicle_id", nullable = true, updatable = true)
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Vehicle vehicle;

    private String tripType;

    @Transient
    private String tripDate;

    @Column(name = "trip_date")
    @JsonIgnore
    private LocalDate date;

    private String fromAddress;

    private String toAddress;

    @Transient
    private String startTime;

    @Column(name = "start_time")
    @JsonIgnore
    private LocalTime startingTime;

    @Transient
    private String endTime;

    @Column(name = "end_time")
    @JsonIgnore
    private LocalTime endingTime;

    private float mileageRate;

    private float milesDriven;

    private float tripCost;

    private float tolls;

    private float parking;

    private float totalExpense;

    private boolean isManual;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "money_flow_id", nullable = true, updatable = true)
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MoneyFlowAccount moneyFlowAccount;

    private boolean isMerged;

    public Mileage() {
    }

    public Mileage(Long id, Long vehicleId, Vehicle vehicle, String vehicleName, String tripType, LocalDate date, String fromAddress, String toAddress, LocalTime startingTime, LocalTime endingTime,
                   float mileageRate, float milesDriven, float tripCost, float tolls, float parking, float totalExpense, boolean isManual, MoneyFlowAccount moneyFlowAccount, boolean isMerged) {
        this.id = id;
        this.vehicleId = vehicleId;
        this.vehicle = vehicle;
        this.vehicleName = vehicleName;
        this.tripType = tripType;
        this.flowName = moneyFlowAccount.getMoneyFlowName();
        this.tripDate = CustomDateConverter.localDateToString(date);
        this.date = date;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.startTime = startingTime.toString();
        this.startingTime = startingTime;
        this.endTime = endingTime.toString();
        this.endingTime = endingTime;
        this.mileageRate = mileageRate;
        this.milesDriven = milesDriven;
        this.tripCost = tripCost;
        this.tolls = tolls;
        this.parking = parking;
        this.totalExpense = totalExpense;
        this.isManual = isManual;
        this.moneyFlowAccount = moneyFlowAccount;
        this.isMerged = isMerged;
    }
}
