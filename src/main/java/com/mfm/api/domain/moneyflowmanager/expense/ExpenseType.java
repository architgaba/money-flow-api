package com.mfm.api.domain.moneyflowmanager.expense;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "money_flow_expense_types")
@EqualsAndHashCode(callSuper = false)
@Data
public class ExpenseType extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "expense_type_generator")
    @SequenceGenerator(name="expense_type_generator", sequenceName = "expense_type_seq",allocationSize = 1,initialValue = 54)
    private Long typeId;

    private String name;

    @JsonIgnore
    private boolean isCustom;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = true,updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
}