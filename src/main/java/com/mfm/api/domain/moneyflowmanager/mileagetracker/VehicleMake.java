package com.mfm.api.domain.moneyflowmanager.mileagetracker;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "money_flow_vehicle_makes")
@Data
public class VehicleMake {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
}
