package com.mfm.api.domain.moneyflowmanager.contactmanager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "mfm_contact_manger")
@EqualsAndHashCode(callSuper = false)
public class ContactManager extends Auditable<String> implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_seq")
    @SequenceGenerator(name = "contact_seq", sequenceName = "contact_seq", allocationSize = 1)
    @Column(unique = true, updatable = false, nullable = false)
    private Long contactId;

    private String contactType;

    private String name;
    private String contactNumber;
    private String emailAddress;

    private boolean status;

//    @JsonIgnore
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "moneyFlowAccountId", updatable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    private MoneyFlowAccount moneyFlowAccount;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "userRegistrationId")
    private User user;
}
