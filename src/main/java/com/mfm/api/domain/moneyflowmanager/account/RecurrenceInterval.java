package com.mfm.api.domain.moneyflowmanager.account;

public enum RecurrenceInterval {
    WEEKLY,
    BI_WEEKLY,
    MONTHLY
}
