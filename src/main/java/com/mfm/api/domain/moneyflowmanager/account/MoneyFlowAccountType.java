package com.mfm.api.domain.moneyflowmanager.account;

public enum MoneyFlowAccountType {
    BUSINESS,
    HOME
}
