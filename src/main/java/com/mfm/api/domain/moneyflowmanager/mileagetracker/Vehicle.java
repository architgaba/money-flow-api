package com.mfm.api.domain.moneyflowmanager.mileagetracker;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "money_flow_vehicles")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(exclude = "mileageList")
public class Vehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "money_flow_vehicle")
    @SequenceGenerator(name = "money_flow_vehicle", sequenceName = "money_flow_vehicle", allocationSize = 1)
    @Column(name = "vehicle_id", updatable = false, unique = true, nullable = false)
    private Long id;

    private String make;

    private String model;

    @Column(name = "odo_meter_reading")
    private Long odoReading;

    private String plate;

    private Integer year;

    @ManyToOne
    @JoinColumn(name = "money_flow_id", nullable = true, updatable = true)
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MoneyFlowAccount moneyFlowAccount;

}
