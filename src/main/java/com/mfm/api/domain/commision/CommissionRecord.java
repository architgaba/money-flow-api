package com.mfm.api.domain.commision;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.referrer.Referrers;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "mfm_commission_details")
@Data
@EqualsAndHashCode(callSuper = false)
public class CommissionRecord extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commission_record_gen")
    @SequenceGenerator(name = "commission_record_gen", sequenceName = "commission_record__seq", allocationSize = 1)
    private Long commissionRecordId;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Referrers referrers;

    private LocalDate commissionCalculateFromDate;

    private LocalDate commissionCalculateToDate;

    private Long referrerId;

    private String userType;

    private String referrerName;

    private String referrerStatus;

    private Integer monthlyUserCount;

    private Integer yearlyUserCount;

    private Boolean isP2PYEA;
}
