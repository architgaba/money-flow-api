package com.mfm.api.domain.commision;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_commission_per_customer")
@Data
@EqualsAndHashCode(callSuper = false)
public class CommissionPerCustomer extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commission_per_customer_gen")
    @SequenceGenerator(name = "commission_per_customer_gen", sequenceName = "commission_per_customer_gen", allocationSize = 1)
    private Long commissionPerReferrerId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "commissionRecordId", nullable = false)
    @JsonIgnore
/*
    @OnDelete(action = OnDeleteAction.CASCADE)
*/
    private CommissionRecord commissionRecord;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    private Integer firstLevelCommission;
    private Integer secondLevelCommission;
    private int commissionAmount;

    private String name;

    private String userType;

    private String planType;

    private Boolean isP2PYEA;

}
