package com.mfm.api.domain.commision;

import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "mfm_app_ambassador_bonuses")
@Data
@EqualsAndHashCode(callSuper = false)
public class AppAmbassadorBonuses extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_ambassador_bonuses_gen")
    @SequenceGenerator(name = "app_ambassador_bonuses_gen", sequenceName = "app_ambassador_bonuses_gen", allocationSize = 1)
    private Long appAmbassadorBonuseId;

    private Long userRegistrationId;

    private String firstName;

    private String lastName;

    private String bonusEarnedFrom;

    private Float bonuseAmount;

    private LocalDate disbursementDate;
}
