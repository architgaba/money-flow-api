package com.mfm.api.domain.commision;

import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "mfm_commission_setting")
@Data
@EqualsAndHashCode(callSuper = false)
public class CommissionSetting extends Auditable<String> implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commission_setting_gen")
    @SequenceGenerator(name = "commission_setting_gen", sequenceName = "commission_setting__seq", allocationSize = 1)
    private Long commissionSettingId;

    private int schedulerRunningDay;

    private int commission1stLevelAmount;
    private int commission2ndLevelAmount;
}
