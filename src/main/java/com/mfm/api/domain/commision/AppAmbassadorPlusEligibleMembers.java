package com.mfm.api.domain.commision;

import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_app_ambassador_plus_eligible_members")
@Data
@EqualsAndHashCode(callSuper = false)
public class AppAmbassadorPlusEligibleMembers extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_ambassador_plus_eligible_members_gen")
    @SequenceGenerator(name = "app_ambassador_plus_eligible_members_gen", sequenceName = "app_ambassador_plus_eligible_members_gen", allocationSize = 1)
    private Long appAmbassadorId;

    private Long userRegistrationId;

    private String firstName;

    private String lastName;

    @Transient
    private Integer level;
}
