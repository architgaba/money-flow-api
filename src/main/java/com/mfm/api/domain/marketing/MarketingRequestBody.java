package com.mfm.api.domain.marketing;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class MarketingRequestBody {

    private String[] email;

    private String message;

    private String subject;
}
