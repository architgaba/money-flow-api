package com.mfm.api.domain.user.information;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@ToString(exclude = "user")
@Table(name = "mfm_user_billing_address")
@EqualsAndHashCode(callSuper = false)
public class UserBillingAddress extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "user_billing")
    @SequenceGenerator(name ="user_billing" ,sequenceName = "user_billing_seq",allocationSize = 1)
    private Long userBillingAddressId;

    private String addressName;
    private String firstName;
    private String lastName;
    @Column(length = 15)
    private String contactNumber;
    private String faxNumber;

    private String billingAddress1;
    private String billingAddress2;
    private String billingAddressCity;
    private String billingAddressCountry;
    private String billingAddressState;
    private Integer billingAddressPinCode;

    private String companyName;
    private boolean primaryAddress;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "userRegistrationId")
    private User user;
}
