package com.mfm.api.domain.user.registration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "contact_enquiries")
@EqualsAndHashCode(callSuper = false)
public class ContactEnquiries implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_enquiries")
    @SequenceGenerator(name = "contact_enquiries", sequenceName = "contact_enquiries_seq", allocationSize = 1)
    @Column(unique = true, updatable = false, nullable = false)
    @JsonIgnore
    private Long contactEnquiriesId;

    private String name;

    private String emailId;

    private String subject;

    private LocalDate date;

    @Column(columnDefinition = "TEXT")
    private String message;

    private String referredBy;
}
