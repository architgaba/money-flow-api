package com.mfm.api.domain.user.registration;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_user_roles")
@Data
public class UserRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true,updatable = false)
    private Integer roleCode;

    private String roleDescription;
}
