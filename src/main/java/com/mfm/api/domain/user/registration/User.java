package com.mfm.api.domain.user.registration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "mfm_users")
@EqualsAndHashCode(callSuper = false)
public class User extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_gen")
    @SequenceGenerator(name = "user_gen", sequenceName = "user_gen", allocationSize = 1)
    private Long userRegistrationId;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String userName;

    @JsonIgnore
    private String password;

    private Long referrerCode;

    @Column(unique = true)
    private String emailAddress;

    private boolean emailNotifyStatus;

    @Column(length = 15)
    private String contactNumber;

    private LocalDateTime lastPasswordChangeDate;

    private LocalDateTime lastSignedInDate;

    @JsonIgnore
    @Column(length = 1024)
    private String accountActivationKey;

    @JsonIgnore
    @Column(length = 1024)
    private String passwordResetKey;

    @Transient
    private String status;

    @OneToOne
    @JoinColumn(name = "referred_as", nullable = false)
    @JsonIgnore
    private ReferenceType referenceType;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "status_id", nullable = false)
    @JsonIgnore
    private UserStatus userStatus;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    @JsonIgnore
    private UserRole role;

    @Column(unique = true)
    private String stripeCustomerId;

    private String p2pLink;

    private Boolean isP2PYEA;

    @Transient
    private Integer level;

    private Boolean isEligibleForPromotion;

}
