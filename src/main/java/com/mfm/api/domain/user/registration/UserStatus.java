package com.mfm.api.domain.user.registration;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mfm_user_status")
@Data
public class UserStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(updatable = false,unique = true)
    private Integer statusCode;

    @NotNull
    private String statusDescription;
}
