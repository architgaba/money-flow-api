package com.mfm.api.domain.user.information;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@ToString(exclude = "user")
@Table(name = "mfm_user_shipping_address")
@EqualsAndHashCode(callSuper = false)
public class UserShippingAddress extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_shipping")
    @SequenceGenerator(name = "user_shipping", sequenceName = "user_shipping_seq", allocationSize = 1)
    private Long userShippingAddressId;

    private String addressName;
    private String firstName;
    private String lastName;
    @Column(length = 15)
    private String contactNumber;
    private String faxNumber;

    private String shippingAddress1;
    private String shippingAddress2;
    private String shippingAddressCity;
    private String shippingAddressCountry;
    private String shippingAddressState;
    private Integer shippingAddressPinCode;

    private String companyName;
    private boolean primaryAddress;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "userRegistrationId")
    private User user;
}
