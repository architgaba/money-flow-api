package com.mfm.api.domain.user.registration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "mfm_guest_users")
@EqualsAndHashCode(callSuper = false)
public class GuestUser extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "money_guest_users")
    @SequenceGenerator(name ="money_guest_users" ,sequenceName = "money_guest_users_seq",allocationSize = 1)
    @Column(unique = true,updatable = false,nullable = false)
    @JsonIgnore
    private Long guestUserId;

    private String name;

    private String emailId;

}
