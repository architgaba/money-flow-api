package com.mfm.api.domain.user.registration;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "mfm_user_reference_type")
@Data
@EqualsAndHashCode(callSuper = false)
public class ReferenceType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long refTypeId;

    @Column(unique = true,nullable = false,updatable = false)
    private Integer referenceCode;

    private String description;
}

