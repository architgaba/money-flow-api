package com.mfm.api.domain.user.information;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@ToString(exclude = "user")
@Table(name = "mfm_user_physical_address")
@EqualsAndHashCode(callSuper = false)
public class UserPhysicalAddress extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_physical")
    @SequenceGenerator(name = "user_physical", sequenceName = "user_physical_seq", allocationSize = 1)
    private Long userPhysicalAddressId;

    private String physicalAddress1;
    private String physicalAddress2;
    private String physicalAddressCity;
    private String physicalAddressCountry;
    private String physicalAddressState;
    private Integer physicalAddressPinCode;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "userRegistrationId")
    private User user;

}
