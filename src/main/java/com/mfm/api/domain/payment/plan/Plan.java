package com.mfm.api.domain.payment.plan;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.payment.product.Product;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mfm_plan")
@Data
@EqualsAndHashCode(callSuper = false)
public class Plan extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plan_gen")
    @SequenceGenerator(name = "plan_gen", sequenceName = "plan_seq", allocationSize = 1)
    private Long planId;

    @Column(unique = true)
    @JsonIgnore
    private String stripePlanId;

    private String planName;

    private float planAmount;

    @Column(length = 1021)
    private String planDescription;

    private String planType;

    private String planInterval;

    @JsonIgnore
    private String planCurrency;

    private Boolean planActiveStatus;

    @Transient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String activeStatus;

    @JsonIgnore
    @JoinColumn(name = "product_id",nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Product product;

    public static Plan plan(Plan plan) {
        plan.setActiveStatus(plan.getPlanActiveStatus() ? "Active" : "Inactive");
        return plan;
    }

}
