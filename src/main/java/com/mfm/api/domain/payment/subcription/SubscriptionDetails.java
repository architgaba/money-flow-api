package com.mfm.api.domain.payment.subcription;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mfm.api.configuration.Auditable;
import com.mfm.api.domain.payment.plan.Plan;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "mfm_subscription")
@Data
@EqualsAndHashCode(callSuper = false)
public class SubscriptionDetails extends Auditable<String> implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscription_gen")
    @SequenceGenerator(name = "subscription_gen", sequenceName = "subscription_seq", allocationSize = 1)
    private Long subscriptionId;


    @Column(unique = true)
    private String  stripeSubscriptionId;

    private LocalDate subscriptionDate;

    private LocalDate subscriptionExpiryDate;

    @OneToOne
    @JoinColumn(name = "plan_id")
    private Plan plan;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    private String status;

}
