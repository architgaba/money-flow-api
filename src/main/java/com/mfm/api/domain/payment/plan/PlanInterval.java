package com.mfm.api.domain.payment.plan;

public enum PlanInterval {
    MONTH,
    YEAR
}
