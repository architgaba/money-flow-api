package com.mfm.api.domain.payment.product;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "mfm_products")
@Data
@EqualsAndHashCode(callSuper = false)
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_gen")
    @SequenceGenerator(name = "product_gen", sequenceName = "product_seq",  allocationSize = 1)
    private Integer productId;

    @Column(unique = true)
    private String stripeProductId;

    private String productName;

    private String productType;

    public Product(String stripeProductId, String productName,String productType) {
        this.stripeProductId = stripeProductId;
        this.productName = productName;
        this.productType=productType;
    }

    public Product() {
    }
}
