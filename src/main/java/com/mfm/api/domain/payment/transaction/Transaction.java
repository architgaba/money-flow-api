package com.mfm.api.domain.payment.transaction;

import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.user.registration.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mfm_transaction")
@Data
@EqualsAndHashCode(callSuper = false)
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_gen")
    @SequenceGenerator(name = "transaction_gen", sequenceName = "transaction_seq",allocationSize = 1)
    private Long transactionId;

    private String stripeTransactionId;

    private String type;

    private Double transactionAmount;

    private String planType;

    private LocalDateTime transactionDate;

    private String subscriptionId;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private User user;

}
