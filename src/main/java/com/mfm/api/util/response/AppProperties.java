package com.mfm.api.util.response;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
@Data
public class AppProperties {

    private Integer minSecretLength;
    private Integer maxSecretLength;
    private String stripeSecretKey;


    private String loginPageRedirect;
    private String loginPageRedirectError;
    private String userRedirect;

    private String resetPasswordPageRedirectError;
    private String resetPasswordRedirect;
}
