package com.mfm.api.util.response;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:message.properties")
@ConfigurationProperties(prefix = "user")
@Data
public class MessageProperties {

    private String invalidInput;
    private String accountNotVerified;
    private String emailAlreadyExists;
    private String usernameNotAvailable;
    private String registeredSuccessfully;
    private String notExist;
    private String emailAddressNotExists;
    private String invalidUsername;
    private String accountVerification;
    private String invalidUserType;
    private String accountInactive;
    private String badCredentials;

    private String invalidPassword;
    private String passwordChangeSuccess;
    private String passwordNotMatches;

    private String customerSubscriptionAlreadyTaken;
    private String invalidReferrerCode;
    private String customerSubscribedSuccessfully;
    private String customerRepSubscriptionAlreadyTaken;
    private String customerRepSubscribedSuccessfully;

    private String personalInfoUpdate;
    private String physicalAddressSave;
    private String physicalAddressUpdate;
    private String billingAddressSave;
    private String billingAddressUpdate;
    private String billingAddressNotExists;
    private String billingAddressDelete;
    private String shippingAddressSave;
    private String shippingAddressUpdate;
    private String shippingAddressDelete;
    private String shippingAddressNotExists;

    private String mfmTypeNotSelected;
    private String mfmAccountSave;
    private String mfmAccountUpdate;
    private String mfmAccountDelete;
    private String mfmAccountNotExist;
    private String mfmIncomeTypeSave;
    private String mfmIncomeSave;
    private String mfmIncomeUpdate;
    private String mfmIncomeDelete;
    private String mfmIncomeNotExists;
    private String recurrenceDateNotSelected;
    private String receiptDeleted;
    private String receiptNotFound;
    private String mfmExpenseSave;
    private String mfmExpenseUpdate;
    private String mfmExpenseTypeSave;
    private String mfmExpenseDelete;
    private String mfmBudgetSave;
    private String mfmBudgetUpdate;
    private String mfmBudgetDelete;
    private String mfmBudgetNotExist;
    private String contactManagerExists;
    private String contactManagerSave;
    private String contactManagerDelete;
    private String contactManagerNotExists;
    private String contactManagerActivated;
    private String contactManagerActivatedSuccess;
    private String contactManagerInactivated;
    private String contactManagerInactivatedSuccess;
    private String contactManagerUpdate;
    private String mfmExpenseNotExists;


    private String invalidDateFormat;


    private String vehicleSave;
    private String vehicleUpdate;
    private String vehicleDelete;
    private String vehicleNotExists;
    private String mileageSave;
    private String mileageUpdate;
    private String mileageDelete;
    private String mileageNotExists;
    private String mergeTrips;
    private String selectedTripsInvalid;


    private String notAuthorizedAsReferrer;
    private String customerAccountActivated;
    private String customerCreated;
    private String customerResendActivationMail;
    private String customerAccountUpdate;
    private String customerAlreadyInActive;
    private String customerAlreadyActive;
    private String customerAlreadyArchived;
    private String customerRepCreated;
    private String P2PDistributor;
    private String TestCustomer;
    private String TestCustomerRep;


    private String notAuthorized;
    private String customerRepAccountUpdate;
    private String adminAdded;
    private String adminUpdate;
    private String updateUserEmail;



    private String emailSendSuccess;
    private String guestUserAdded;
    private String guestUserEmailExists;

    private String invalidPlanType;
    private String invalidPlanInterval;
    private String planSuccess;
    private String planActivationSuccess;
    private String planAlreadyActivated;
    private String planInactivationSuccess;
    private String planAlreadyInactivated;
    private String planNotExists;
    private String planDeleteSuccess;
    private String selectCard;
    private String subscriptionSuccess;
    private String transactionLoggedSuccess;
    private String subscriptionCancelled;
    private String cardExists;
    private String resetPasswordLinkExpired;
    private String resetPasswordLinkGenerate;
    private String resetPasswordInvalidOTP;
    private String resetPasswordSuccess;
    private String mfmCustomerCreated;
    private String commissionSettingUpdate;

    private String notValidReferralCode;
}
