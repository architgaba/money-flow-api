package com.mfm.api.util.function;

import com.mfm.api.domain.moneyflowmanager.account.RecurrenceInterval;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.util.converter.CustomDateConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.MonthDay;
import java.util.*;

public class CommonUtilFunctions {

    private static Logger logger = LogManager.getLogger(CommonUtilFunctions.class);

    /**
     * Date Range Check
     *
     * @param fromDate
     * @param toDate
     * @param entryDate
     * @return
     */
    public static boolean dateRangeCheck(String fromDate, String toDate, LocalDate entryDate) {
        return (entryDate.isAfter(CustomDateConverter.stringToLocalDate(fromDate)) || entryDate.isEqual(CustomDateConverter.stringToLocalDate(fromDate))) &&
                (entryDate.isBefore(CustomDateConverter.stringToLocalDate(toDate)) || entryDate.isEqual(CustomDateConverter.stringToLocalDate(toDate)));
    }

    public static int filterRecurringIncome(String fromDate, String toDate, Income income) {
        if (!income.isRecurring()) {
            boolean isPresent = (income.getEntryDate().isAfter(CustomDateConverter.stringToLocalDate(fromDate)) || income.getEntryDate().isEqual(CustomDateConverter.stringToLocalDate(fromDate))) &&
                    (income.getEntryDate().isBefore(CustomDateConverter.stringToLocalDate(toDate)) || income.getEntryDate().isEqual(CustomDateConverter.stringToLocalDate(toDate)));
            if (isPresent) {
                return 1;
            }
            return 0;
        }

        MonthDay recurringDate = MonthDay.of(income.getEntryDate().getMonth(), income.getEntryDate().getDayOfMonth());
        if (income.getRecurrenceInterval() == RecurrenceInterval.MONTHLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = income.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusMonths(1);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusMonths(1);
            }
            return dayList.size();
        }
        if (income.getRecurrenceInterval() == RecurrenceInterval.BI_WEEKLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = income.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusWeeks(2);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusWeeks(2);
            }

            if (dayList.contains(recurrenceDay.getDayOfWeek())) {
                return dayList.size();
            }
            return 0;
        }
        if (income.getRecurrenceInterval() == RecurrenceInterval.WEEKLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = income.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusWeeks(1);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusWeeks(1);
            }

            if (dayList.contains(recurrenceDay.getDayOfWeek())) {
                return dayList.size();
            }
            return 0;
        }
        return 0;
    }

    public static int filterRecurringExpense(String fromDate, String toDate, Expense expense) {
        if (!expense.isRecurring()) {
            boolean isPresent = (expense.getEntryDate().isAfter(CustomDateConverter.stringToLocalDate(fromDate)) || expense.getEntryDate().isEqual(CustomDateConverter.stringToLocalDate(fromDate))) &&
                    (expense.getEntryDate().isBefore(CustomDateConverter.stringToLocalDate(toDate)) || expense.getEntryDate().isEqual(CustomDateConverter.stringToLocalDate(toDate)));
            if (isPresent) {
                return 1;
            }
            return 0;
        }
        MonthDay recurringDate = MonthDay.of(expense.getEntryDate().getMonth(), expense.getEntryDate().getDayOfMonth());
        if (expense.getRecurrenceInterval() == RecurrenceInterval.MONTHLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = expense.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusMonths(1);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusMonths(1);
            }
            return dayList.size();
        }
        if (expense.getRecurrenceInterval() == RecurrenceInterval.BI_WEEKLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = expense.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusWeeks(2);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusWeeks(2);
            }

            if (dayList.contains(recurrenceDay.getDayOfWeek())) {
                return dayList.size();
            }
            return 0;
        }
        if (expense.getRecurrenceInterval() == RecurrenceInterval.WEEKLY) {
            LocalDate startDate = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate endDate = CustomDateConverter.stringToLocalDate(toDate);
            LocalDate recurrenceDay = expense.getEntryDate();
            List<DayOfWeek> dayList = new ArrayList<>();
            if (startDate.isAfter(recurrenceDay)) {
                startDate = recurrenceDay.plusWeeks(1);
            } else {
                startDate = recurrenceDay;
            }


            while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
                if ((startDate.compareTo(CustomDateConverter.stringToLocalDate(fromDate)) > 0)) {
                    dayList.add(startDate.getDayOfWeek());
                }
                startDate = startDate.plusWeeks(1);
            }

            if (dayList.contains(recurrenceDay.getDayOfWeek())) {
                return dayList.size();
            }
            return 0;
        }
        return 0;
    }

    /**
     * Date Range Check
     *
     * @param fromDate
     * @param toDate
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean dateRangeCheck(String fromDate, String toDate, LocalDate startDate, LocalDate endDate) {
        return (startDate.isAfter(CustomDateConverter.stringToLocalDate(fromDate)) || startDate.isEqual(CustomDateConverter.stringToLocalDate(fromDate))) &&
                (endDate.isBefore(CustomDateConverter.stringToLocalDate(toDate)) || endDate.isEqual(CustomDateConverter.stringToLocalDate(toDate)));
    }


    public static float roundOf(float amount) {
        return (float) (Math.round(amount * 100.0) / 100.0);
    }

    public static boolean subscriptionExpiryDateCheck(LocalDate planDate) {
        planDate = planDate.plusMonths(1);
        return planDate.isAfter(LocalDate.now());
    }

    public static boolean subscriptionCheck(LocalDate expiryDate) {
        return expiryDate.isAfter(LocalDate.now());
    }


    public static ResponseEntity<String> subscribeUserP2PAcademy(User user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String createdBy = new String();
        if (user.getIsP2PYEA()) {
            createdBy = "P2P YEA";
        } else {
            createdBy = "P2PMEC";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("firstName", user.getFirstName());
        map.put("lastName", user.getLastName());
        map.put("emailAddress", user.getEmailAddress());
        map.put("contactNo", user.getContactNumber());
        map.put("createdBy", createdBy);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        try {
            return restTemplate.postForEntity("https://myp2pacademy.com/api/v1/myp2pacademy/p2p-mec/add-user", entity, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new ResponseEntity<String>("Server Down", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<String> unsubscribeUserP2PAcademy(String userEmail, String status) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        Map<String, Object> map = new HashMap<>();
        map.put("value", status);
        map.put("id", userEmail);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        try {
            return restTemplate.postForEntity("https://myp2pacademy.com/api/v1/myp2pacademy/p2p-mec/update-membership", entity, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new ResponseEntity<String>("Server Down", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
