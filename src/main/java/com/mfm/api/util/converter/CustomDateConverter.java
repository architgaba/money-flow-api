package com.mfm.api.util.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class CustomDateConverter {

    static DateFormat formatterDDMMYYYY = new SimpleDateFormat("MM-dd-yyyy");
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss",Locale.ENGLISH);
    static DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM-dd-yyyy", Locale.ENGLISH);

    public static String convertDateToString(Date date) {
        return formatterDDMMYYYY.format(date);
    }

    public static String localDateTimeToString(LocalDateTime localDateTime) {
        return localDateTime.format(formatter);
    }

    public static String dateToString(Date date) {
        String textDate = date.toString().substring(0, 10);
        LocalDate localDate = LocalDate.parse(textDate);
        return localDateToString(localDate);
    }

    public static String localDateToString(LocalDate localDate) {
        return formatter2.format(localDate);
    }

    public static String dateToTimestamp(Date date) {
        String textDate = date.toString().substring(0, date.toString().lastIndexOf("."));
        return localDateTimeToString(LocalDateTime.parse(textDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    public static LocalDate dateToLocalDate(Date date) {
        String textDate = date.toString().substring(0, date.toString().lastIndexOf(" "));
        return LocalDate.parse(textDate,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static LocalDate stringToLocalDate(String date) {
        return LocalDate.parse(date, formatter2);
    }
}
