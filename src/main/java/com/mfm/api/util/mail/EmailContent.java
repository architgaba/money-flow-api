package com.mfm.api.util.mail;

import org.springframework.stereotype.Component;

/**
 * Email Content Util
 * For Account Activation &
 * Password Reset
 */
@Component
public class EmailContent {


    /**
     * Mail Content For New
     * User Registration
     *
     * @param userName
     * @param url
     * @return registration email content
     */
    public static String registrationEmailContent(String userName, String url) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays) </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Congratulations on becoming a P2P MEC Customer. Listed below is the key element of the " +
                "transaction you have recently completed with us. Please be sure to save this email or make a note of your unique username.</p>" +

                "<p style=\"font-size: 14px;font-weight: 500; background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Your User Name for future visits is: " +
                "<span style=\"font-size: 14px;font-weight: 500;padding: 2px;\">" + userName + "</span></p>" +
//                "<p style=\" font-size: 14px;font-weight: 500;background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your account :</p>" +


//                "<div  style=\"margin-top: 20px;margin-bottom: 20px;text-align: left;\">" +
//                "<a style=\"background: #fff;padding: 8px 60px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\" href=\"" + url + "\" >" + "Activate" + "</a>" +
//                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\"></a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    /**
     * Mail Content For
     * Reset Password OTP
     *
     * @param userName
     * @param firstName
     * @param lastName
     * @param url
     * @return email content fo OTP
     */
    public static String resetPasswordEmailContent(String userName, String firstName, String lastName, String url) {
        lastName = lastName != null ? lastName : "";
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + firstName + " " + lastName + ",</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please click on the button below to reset your password</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Username: " +
                "<span style=\"padding: 2px;\">" + userName + "</span><br><br>" +
                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;min-width: 120px;\">" + "Reset Password" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions" +
                "regarding your P2P MEC Account " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    /**
     * Email Content For
     * Success Password
     *
     * @param firstName
     * @param lastName
     * @param userName
     * @param passwordUpdateDate
     * @return Success Password Mail Content
     */
    public static String successPasswordEmailContent(String firstName, String lastName, String userName, String passwordUpdateDate) {
        lastName = lastName != null ? lastName : "";

        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + firstName + " " + lastName + ", </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Your Password has been reset successfully on " + passwordUpdateDate + "</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions" +
                "regarding your P2P MEC Account " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String customerRegistrationEmailContent(String url, String referrerCode, String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays) </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your Customer account. " +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String testCustomerRegistrationEmailContent(String url, String referrerCode, String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays) </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your Test Customer account. " +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String customerRepRegistrationEmailContent(String url, String referrerCode, boolean isTest, String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays)</h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your" + (isTest ? " Test " : " ") + " Customer Representative account. " +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String P2PRegistrationEmailContent(String url, String referrerCode, String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays)</h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your account as P2P Distributor." +
                "Once you click activate, follow the instructions to set up your personal account.</p>" +

                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
                "<a href=\"" + url + "\">" +
                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
                "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String p2pDistributorSelfRegisterEmailContent(String url, String referrerCode, String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays)</h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", </p>" +
//                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Listed below is the activation button to be used to set up your account as P2P Distributor." +
//                "Once you click activate, follow the instructions to set up your personal account.</p>" +

//                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your Personal Account.</p>" +
                "<p style=\" background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referrer Code  " +
                "<span style=\"background-color:#add2b1;padding: 2px;\">" + referrerCode + "</span></p>" +

//                "<div  style=\"margin-top: 20px;margin-bottom: 20px;    width: 9em;\">" +
//                "<a href=\"" + url + "\">" +
////                "<p style=\"background: #fff;padding: 8px 25px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\">" + "Activate" + "</p>" +
//                "</a>" +
//                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String accountActivationEmailContent(String name, String userName) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + name + ", " + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Your account is now active with username: " + userName + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions " +
                "regarding your P2P MEC account  to " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a> or call us at 972-637-7766.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further Notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String prospectUserEmailContent(String name, String emailId) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi " + ", " + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Prospect User details are listed below:</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Name:" + name + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Email ID:" + emailId + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; Send any questions " +
                "regarding your P2P MEC account  to " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a> or call us at 972-637-7766.</p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further Notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }


    public static String adminAccountActivationEmailContent(String adminName, String url) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hi, " + adminName + ",</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Congratulations on becoming a P2P MEC Administrator.Please click on the button below to activate your Personal Account.</p>" +

                "<p style=\" font-size: 14px;font-weight: 500;background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your account :</p>" +
                "<div  style=\"margin-top: 20px;margin-bottom: 20px;text-align: left;\">" +
                "<a style=\"background: #fff;padding: 8px 60px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\" href=\"" + url + "\" >" + "Activate" + "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">The P2P Connection, LLC Team</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Please do not reply to this message; send any questions" +
                "regarding your P2P Connection, LLC moneyflowmanager to " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"javascript:void(0);\">support@p2pmec.com</a> or call us at 972-637-7766.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">This message is a service email related to your use of P2P Connection, LLC moneyflowmanager." +
                "For general inquiries or to request support with your P2P Connection, LLC moneyflowmanager, please visit us at " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"http://www.p2pconnection.net\">http://www.p2pconnection.net</a> or call us at 972-637-7766 or send an email to " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"support@p2pmec.com\">support@p2pmec.com</a></p>" +

                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">unsubscribe from receiving any further Notifications from P2P Connection, LLC - " +
                "<a style=\"color:#44a0b3; text-decoration:none;\" href=\"\"> click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String subscriptionCancellation(String name, String referredBy, Long referralCode) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hello " + name + "</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >We have received a notification that you have requested to cancel your access to the P2P MEC, you will not receive any further billing to your account. " +
                "Please contact us if you become interested in reinstating your account in the future. </p>" +

                "<p style=\"font-size: 14px;font-weight: 500; background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referred By: " +
                "<span style=\"font-size: 14px;font-weight: 500;padding: 2px;\">" + referredBy + "</span></p>" +
                "<p style=\" font-size: 14px;font-weight: 500;background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Referral Code: " +
                "<span style=\"font-size: 14px;font-weight: 500;padding: 2px;\">" + referralCode + "</span></p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    public static String newSubscription(String name) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hello " + name + ",</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Welcome to the P2P MEC.  \n" +
                "Your next step is to go to the App Store if you have an Iphone or Play Store if you are using an Android phone, search for “P2P MEC and download it on your phone.  " +
                "once it's downloaded log in using your same username and password you used when you created this account\n. </p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    public static String subscriptionPaymentFailed(String name, String cardNumber) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >Hello " + name + ",</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\" >We were unable to charge your Visa ending in " + cardNumber + " for your P2P MEC subscription. " +
                "Please login into the P2P MEC application and update your billing information.</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P MEC</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P MEC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    public static String guestUserEmailContent(String userName) {
        return "<html><head></head>" +
                "<body>" +
                "<div style=\" height: auto;  width: auto;background-position: center; background-repeat: no-repeat; background-size: cover;margin-top: 0em;\">" +
                "<div style=\"background: rgba(26, 167, 27, 0.58);  font-weight: bold;padding:1.1em\">" +
                "<div style=\"margin-right: 1em;margin-left: 1em;padding: 0px;margin-top: 0em;\">" +

                "<h2 style=\"  font-size: 25px;font-weight: 700;color: #333;text-align: center;margin-bottom: 1rem;\">Welcome To The P2P MEC (The App That Pays) </h2>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">Congratulations on becoming a P2P MEC Customer. Listed below is the key element of the " +
                "transaction you have recently completed with us. Please be sure to save this email or make a note of your unique username.</p>" +

                "<p style=\"font-size: 14px;font-weight: 500; background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Your Username for future visits is: " +
                "<span style=\"font-size: 14px;font-weight: 500;padding: 2px;\">" + userName + "</span></p>" +
                "<p style=\" font-size: 14px;font-weight: 500;background: #f8fdff; padding: 5px; color: #000 !important;background: #f8fdff;  width: 80%;\">Please click on the button below to activate your account :</p>" +


                "<div  style=\"margin-top: 20px;margin-bottom: 20px;text-align: left;\">" +
                "<a style=\"background: #fff;padding: 8px 60px;border-radius: 5px;color: #328338;font-size: 13px; font-weight: 700;letter-spacing: 1px; text-decoration: none;\" href=\"" + "\" >" + "Activate" + "</a>" +
                "</div>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;line-height: 1.5em;\">To set up your personal P2P MEC Account, go directly to the *App Store* for IPhone users " +
                "and *Play Store* for Android users.  Download the app (P2P MEC), once it's downloaded, log in using the same " +
                "username and password you used when you created your P2P MEC Account.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">If you have any issues or need any assistance, contact customer support at " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"javascript:void(0);\">support@p2pmec.com</a>.</p>" +

                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 17px;font-weight: 500;color: #000;\">Your Dedicated Support Team at P2P Connection</p>" +
                "<p  style=\"font-size: 17px;font-weight: 500;color: #000;\">To unsubscribe from receiving any further notifications from P2P Connection, LLC - " +
                "<a style=\"font-size: 16px;color: #4b4bff;\" href=\"\" > click here.</a>." +
                "</p>" +

                "</div>" +
                "</div>" +
                "</div>" +

                "</body>" +
                "</html>";
    }

    public static String marketingMail(String message, String name, String emailId) {
        return "<html><head></head>" +
                "<body>" + message +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Yours in Success,</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">" + name + "</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Please do not reply to this message; Send any questions regarding your P2P MEC account to " + emailId + ".</p>" +
                "</body>" +
                "</html>";
    }

    public static String bonusMail(String name, String amount) {
        return "<html><head></head>" +
                "<body>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Hello " + name + "</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Congratulations, your App Ambassador Plus Bonus Commission has been credited to your account in the amount of $" + amount + " and will post to your P2P branded pay card provided you have met the 3 customer compliance requirement. To verify a breakdown of this payment, log into your account at moneyflowapp.com and select Bonus to view the referrals this payment was created for. Please do not hesitate to contact us if you have any questions or need any assistance.</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">P2P Support</p>" +
                "<img style=\"height: 60px;\" src=\"https://moneyflowapp.com/assets/images/LOGOPNG.png\">" +
                "</body>" +
                "</html>";
    }

    public static String commissionMail(String name, String amount) {
        return "<html><head></head>" +
                "<body>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Hello " + name + "</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Congratulations, your Monthly Residual Commission has been credited to your account in the amount of $" + amount + " and will post to your P2P branded Pay Card provided you have met the 3 customer compliance requirement. To view a breakdown of this payment, log into your account at moneyflowapp.com, select commissions to view the referrals this payment was created for. Please do not hesitate to contact us if you have any questions or need any assistance.</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">P2P Support</p>" +
                "<img style=\"height: 60px;\" src=\"https://moneyflowapp.com/assets/images/LOGOPNG.png\">" +
                "</body>" +
                "</html>";
    }

    public static String complianceWednesday(String name) {
        return "<html><head></head>" +
                "<body>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Hello " + name + "</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Commissions will be paid in 2 days and we wanted to remind you to verify that you have 3 active customers no later than 5 PM CST on Thursday which qualifies you to meet the compliance requirement that's necessary to release any commission. Please don't hesitate to contact us if you have any questions or need any assistance.</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">P2P Support</p>" +
                "<img style=\"height: 60px;\" src=\"https://moneyflowapp.com/assets/images/LOGOPNG.png\">" +
                "</body>" +
                "</html>";
    }

    public static String compliance18(String name) {
        return "<html><head></head>" +
                "<body>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Hello " + name + "</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">Residual Commissions will be paid in 2 days and we wanted to remind you to verify that you have 3 active customers no later than 5 PM CST on the 19th which qualifies you to meet the compliance requirement that's necessary to release any commission. Please don't hesitate to contact us if you have any questions or need any assistance.</p>" +
                "<p style=\"font-size: 14px;font-weight: 500;color: #000;\">P2P Support</p>" +
                "<img style=\"height: 60px;\" src=\"https://moneyflowapp.com/assets/images/LOGOPNG.png\">" +
                "</body>" +
                "</html>";
    }
}

