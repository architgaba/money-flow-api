package com.mfm.api.util.mail;


import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.GuestUser;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.referrer.ReferrerRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Service
public class MailNotification {

    private static final Logger log = LogManager.getLogger(MailNotification.class);
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private ReferrerRepository referrerRepository;

    @Value("${mail.bcc}")
    private String bcc;

    @Value("${mail.from}")
    private String from;

    @Value("${mail.to}")
    private String to;


    public MailNotification() {
    }

    public boolean changePasswordConfirmationMail(User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("Password Changed Notification");
            helper.setText(EmailContent.successPasswordEmailContent(user.getFirstName(), user.getLastName(), user.getUserName(), user.getLastPasswordChangeDate().toString()), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMail");
        return isMailSend;
    }


    public boolean sendMail(MailModel mailModel) {
        MimeMessage message = null;
        boolean isMailSend = false;
        try {
            message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            if (mailModel.getTo() != null && !mailModel.getTo().equals("")) helper.setTo(mailModel.getTo());
            if (mailModel.getMultipleTo() != null && mailModel.getMultipleTo().length > 0)
                helper.setTo(mailModel.getMultipleTo());
            if (mailModel.getCc() != null && mailModel.getCc().length > 0) helper.setCc(mailModel.getCc());
            if (mailModel.getBcc() != null && mailModel.getBcc().length > 0) helper.setBcc(mailModel.getBcc());
            helper.setBcc(bcc);
            helper.setFrom(new InternetAddress(mailModel.getFrom(), "P2P MEC SUPPORT"));
            helper.setSubject(mailModel.getSubject());
            helper.setText(mailModel.getMessage(), true);
            emailSender.send(message);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMail");
        return isMailSend;
    }

    public boolean sendMailToProspectUser(GuestUser guestUser) {
        MimeMessage message = null;
        boolean isMailSend = false;
        try {
            message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setTo(to);
            helper.setSubject("Prospect User Notification");
            helper.setText(EmailContent.prospectUserEmailContent(guestUser.getName(), guestUser.getEmailId()), true);
            emailSender.send(message);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMail");
        return isMailSend;
    }

    public boolean sendBonusMail(User user, String bonus) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
//            Long referrerCode = user.getReferrerCode();
//            StringBuilder referrerEmailId = new StringBuilder();
//            if (referrerCode != null) {
//                Referrers referrers = referrerRepository.findByReferrerCode(referrerCode);
//                if (referrers != null) {
//                    referrerEmailId.append(referrers.getUser().getEmailAddress());
//                    helper.setCc(referrerEmailId.toString());
//                }
//            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("Bonus Commissions");
            String username = user.getFirstName() + " " + (user.getLastName() != null ? user.getLastName() : "");
            helper.setText(EmailContent.bonusMail(username, bonus), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendBonusMail");
        return isMailSend;
    }

    public boolean sendCommissionMail(User user, String amount) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
//            Long referrerCode = user.getReferrerCode();
//            StringBuilder referrerEmailId = new StringBuilder();
//            if (referrerCode != null) {
//                Referrers referrers = referrerRepository.findByReferrerCode(referrerCode);
//                if (referrers != null) {
//                    referrerEmailId.append(referrers.getUser().getEmailAddress());
//                    helper.setCc(referrerEmailId.toString());
//                }
//            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("Monthly Residuals");
            String username = user.getFirstName() + " " + (user.getLastName() != null ? user.getLastName() : "");
            helper.setText(EmailContent.commissionMail(username, amount), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendCommissionMail");
        return isMailSend;
    }


    public boolean sendMailForMarketing(MailModel mailModel, List<MultipartFile> attachments, User user) {
        MimeMessage message = null;
        boolean isMailSend = false;
        String name = "";

        try {
            message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            if (mailModel.getTo() != null && !mailModel.getTo().equals("")) helper.setBcc(mailModel.getTo());
            if (mailModel.getMultipleTo() != null && mailModel.getMultipleTo().length > 0)
                helper.setBcc(mailModel.getMultipleTo());
            if (mailModel.getCc() != null && mailModel.getCc().length > 0) helper.setCc(mailModel.getCc());
            if (mailModel.getBcc() != null && mailModel.getBcc().length > 0) helper.setBcc(mailModel.getBcc());
            for (MultipartFile attachment : attachments) {
//                File convFile = new File(attachment.getOriginalFilename());
//                convFile.createNewFile();
//                FileOutputStream fos = new FileOutputStream(convFile);
//                fos.write(attachment.getBytes());
//                fos.close();
//                FileSystemResource file = new FileSystemResource(convFile);
//                helper.addAttachment(file.getFilename(), file);


                String attachName = attachment.getOriginalFilename();
                if (!attachName.equals("")) {

                    helper.addAttachment(attachName, new InputStreamSource() {

                        @Override
                        public InputStream getInputStream() throws IOException {
                            return attachment.getInputStream();
                        }
                    });
                }
            }
            if (user.getRole().getRoleCode() == 2 || user.getReferenceType().getReferenceCode() == 6 || user.getReferenceType().getReferenceCode() == 7) {
                helper.setCc(user.getEmailAddress());
                name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            } else {
                name = "P2P MEC Support";
            }


            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setTo(bcc);
            helper.setSubject(mailModel.getSubject());
            helper.setText(EmailContent.marketingMail(mailModel.getMessage(), name, user.getEmailAddress()), true);
            emailSender.send(message);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMail");
        return isMailSend;
    }

    public boolean sendMailWithAttachment(MailModel mailModel, String path) throws Exception {
        log.info("Entering ::: class ::: MailNotification ::: method ::: sendMailWithAttachment");
        boolean isMailSend = false;
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(mailModel.getTo());
            helper.setFrom(new InternetAddress(mailModel.getFrom(), "P2P MEC SUPPORT"));
            helper.setSubject(mailModel.getSubject());
            helper.setText(mailModel.getMessage());
            helper.setBcc(bcc);
            FileSystemResource file = new FileSystemResource(new File(path));
            helper.addAttachment("Invoice", file);
            emailSender.send(message);
            isMailSend = true;
            log.info("Exit ::: class ::: MailNotification ::: method ::: sendMailWithAttachment");

        } catch (MessagingException e) {
            isMailSend = false;
            e.printStackTrace();
        }
        return isMailSend;
    }


    public boolean sendMailToCustomer(User user, String url, String referrerCode, boolean isTest) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            String name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            if (isTest) {
                helper.setText(EmailContent.testCustomerRegistrationEmailContent(url, referrerCode, name), true);
                helper.setSubject("Customer Test Account Creation Notification");
            } else {
                helper.setSubject("Customer Account Creation Notification");
                helper.setText(EmailContent.customerRegistrationEmailContent(url, referrerCode, name), true);
            }
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            log.error(e.getMessage(), e);
            log.error("method ::: accountActivationMailToCustomerRep error ::: " + e.getMessage());
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMailToCustomer");
        return isMailSend;
    }

    public boolean sendMailToNewlyCreateAdmin(User user, String url) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setBcc(bcc);
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("Admin Account Creation Notification");
//            String customerName = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            helper.setText(EmailContent.adminAccountActivationEmailContent(user.getFirstName(), url), true);
            emailSender.send(mail);
            log.info("Email Send successfully for Customer Registration");
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            log.error(e.getMessage(), e);
            log.error("method ::: accountActivationMailToCustomerRep error ::: " + e.getMessage());
        }
        log.info("Exit ::: class ::: MailNotification ::: method ::: sendMailToCustomer");
        return isMailSend;
    }

    public boolean sendAccountActivationMailToCustomer(User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setBcc(bcc);
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("P2P MEC Account Activated");
            String name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            helper.setText(EmailContent.accountActivationEmailContent(name, user.getUserName()), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            log.error(e.getMessage(), e);
            log.error("method ::: accountActivationMailToCustomerRep error ::: " + e.getMessage());
        }
        return isMailSend;
    }

    public boolean sendAccountActivationMailToAdmin(User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        boolean isMailSend = false;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setBcc(bcc);
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setSubject("Admin Account Activated");
            String name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            helper.setText(EmailContent.accountActivationEmailContent(name, user.getUserName()), true);
            emailSender.send(mail);
            isMailSend = true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            isMailSend = false;
            log.error(e.getMessage(), e);
            log.error("method ::: accountActivationMailToCustomerRep error ::: " + e.getMessage());
        }
        return isMailSend;
    }

    public void accountActivationMailToCustomerRep(User user, String url, String referrerCode, String type) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            String name = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            switch (type) {
                case "Paid":
                    helper.setSubject("Customer Representative P2P MEC Account Creation Notification");
                    helper.setText(EmailContent.customerRepRegistrationEmailContent(url, referrerCode, false, name), true);
                    break;
                case "Free":
                    helper.setSubject("Customer Representative P2P MEC Test Account Creation Notification");
                    helper.setText(EmailContent.customerRepRegistrationEmailContent(url, referrerCode, true, name), true);
                    break;
                case "P2P Distributor":
                    helper.setSubject("P2P Distributor's P2P MEC Account Creation Notification");
                    if (url.equals(""))
                        helper.setText(EmailContent.p2pDistributorSelfRegisterEmailContent(url, referrerCode, name), true);
                    else
                        helper.setText(EmailContent.P2PRegistrationEmailContent(url, referrerCode, name), true);
                    break;
                default:
                    break;

            }

            emailSender.send(mail);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: accountActivationMailToCustomerRep error ::: " + e.getMessage());
        }
    }

    public void subscriptionEmails(String operationType, User user, String cardNumber) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            String userName = user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : "");
            switch (operationType) {
                case "cancel":
                    Referrers referrerObj = referrerRepository.findByReferrerCode(user.getReferrerCode());
                    helper.setSubject("P2P MEC Account Subscription Cancelled Notification");
                    helper.setText(EmailContent.subscriptionCancellation(userName, referrerObj.getUser().getFirstName() + (referrerObj.getUser().getLastName() != null ? (" " + referrerObj.getUser().getLastName()) : ""), referrerObj.getReferrerCode()), true);
                    break;
                case "buy":
                    helper.setSubject("P2P MEC Account Subscription Purchase Successful");
                    helper.setText(EmailContent.newSubscription(userName), true);
                    break;
                case "Payment Failed":
                    helper.setSubject("P2P MEC Account Subscription Payment Failed");
                    helper.setText(EmailContent.subscriptionPaymentFailed(userName, cardNumber), true);
                    Long referrerCode = user.getReferrerCode();
                    if (referrerCode != null) {
                        Referrers referrers = referrerRepository.findByReferrerCode(referrerCode);
                        helper.setCc(referrers.getUser().getEmailAddress());
                    }
                default:
                    break;
            }
            emailSender.send(mail);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: subscriptionEmails ::: error ::: " + e.getMessage());
        }
    }

    public void userAccountVerification(String url, String operationType, User user) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailAddress());
            Long userReferrerCode = user.getReferrerCode();
            StringBuilder referrerEmailId = new StringBuilder();
            if (userReferrerCode != null) {
                Referrers referrers = referrerRepository.findByReferrerCode(userReferrerCode);
                if (referrers != null) {
                    referrerEmailId.append(referrers.getUser().getEmailAddress());
                    helper.setCc(referrerEmailId.toString());
                }
            }
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            switch (operationType) {
                case "registration":
                    helper.setSubject("Welcome To The P2P MEC");
                    helper.setText(EmailContent.registrationEmailContent(user.getUserName(), url), true);
                    break;
                default:
                    break;
            }
            emailSender.send(mail);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: userAccountVerification ::: error ::: " + e.getMessage());
        }
    }

    public void sendMailToGuestUser(GuestUser guestUser) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(guestUser.getEmailId());
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            helper.setSubject("Welcome To The P2P MEC");
            helper.setText(EmailContent.guestUserEmailContent(guestUser.getName()), true);
            emailSender.send(mail);

        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: sendMailToGuestUser ::: error ::: " + e.getMessage());
        }
    }


    public void sendComplianceWednesdayMailToAAP(String name, User aap) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(aap.getEmailAddress());
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            helper.setSubject("Commissions Compliance Reminder");
            helper.setText(EmailContent.complianceWednesday(name), true);
            emailSender.send(mail);

        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: sendMailToGuestUser ::: error ::: " + e.getMessage());
        }
    }

    public void sendCompliance18MailToAA(String name, User aap) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(aap.getEmailAddress());
            helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
            helper.setBcc(bcc);
            helper.setSubject("Commissions Compliance Reminder");
            helper.setText(EmailContent.compliance18(name), true);
            emailSender.send(mail);

        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            log.error("method ::: sendMailToGuestUser ::: error ::: " + e.getMessage());
        }
    }
}
