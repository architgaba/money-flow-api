package com.mfm.api.controller.leaderswall;

import com.mfm.api.domain.leaderswall.LeadersWall;
import com.mfm.api.service.leaderswall.LeadersWallService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@RequestMapping("/leaders-wall")
public class LeadersWallController {

    private static Logger logger = LogManager.getLogger(LeadersWallController.class);

    @Autowired
    private LeadersWallService leadersWallService;

    @GetMapping("/list")
    public ResponseEntity<?> getLeadersWallList() {
        logger.info("method ::: getLeadersWallList");
        return leadersWallService.getLeadersWallList();
    }

    @PostMapping("/add/{leadersWallCategoriesId}")
    public ResponseEntity<?> addLeadersWall(@RequestHeader("Authorization") String auth, LeadersWall leadersWall, @PathVariable String leadersWallCategoriesId, @RequestParam(required = false, name = "file") MultipartFile file) {
        logger.info("method ::: addLeadersWall");
        return leadersWallService.addLeadersWall(leadersWall, leadersWallCategoriesId, file);
    }

//    @PutMapping("/update/{leadersWallId}")
//    public ResponseEntity<?> updateLeadersWall(@RequestBody LeadersWall leadersWall, @PathVariable String leadersWallId) {
//        logger.info("method ::: updateLeadersWall");
//        return leadersWallService.updateLeadersWall(leadersWall, leadersWallId);
//    }

    @DeleteMapping("/delete/{leadersWallId}")
    public ResponseEntity<?> deleteLeadersWall(@RequestHeader("Authorization") String auth, @PathVariable String leadersWallId) {
        logger.info("method ::: deleteLeadersWall");
        return leadersWallService.deleteLeadersWall(leadersWallId);
    }


}
