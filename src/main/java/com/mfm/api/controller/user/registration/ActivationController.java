package com.mfm.api.controller.user.registration;

import com.mfm.api.dto.login.ForgotPassword;
import com.mfm.api.dto.request.CustomerActivationDTO;
import com.mfm.api.dto.request.UserPasswordChangeDTO;
import com.mfm.api.service.user.activation.UserActivationService;
import com.mfm.api.util.response.AppProperties;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * User Registration/Account
 * Verification Controller
 */
@Controller
@CrossOrigin
@RequestMapping("/")
public class ActivationController {

    private static Logger logger = LogManager.getLogger(ActivationController.class);
    @Autowired
    private UserActivationService userActivationService;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private MessageProperties messageProperties;

    /**
     * User Account Activation
     *
     * @param id (Activation Key)
     * @return redirect to Login Page
     */
    @GetMapping("user/activate/account/{id}")
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public RedirectView accountActivation(@PathVariable String id) {
        logger.info("method ::: accountActivation");
        try {
            boolean status = userActivationService.accountActivation(id);
            if (status)
                //return new RedirectView("https://appzbuild.com/login");
                return new RedirectView(appProperties.getLoginPageRedirect());
            else {
                //return new RedirectView("https://appzbuild.com/error?message=Account Activation Link Expired , Please contact to administrators&error=498");
                return new RedirectView(appProperties.getLoginPageRedirectError());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: accountActivation ::: error ::: " + e.getMessage());
            //return new RedirectView("https://appzbuild.com/error?message=Account Activation Link Expired , Please contact to administrators&error=498");
            return new RedirectView(appProperties.getLoginPageRedirectError());
        }
    }

    /**
     * Emailing link For
     * Resting Password
     *
     * @param emailAddress
     * @return Email with link
     * to reset password to mentioned
     * email address
     */
    @GetMapping("user/forgot-password/{emailAddress}")
    public ResponseEntity<?> forgotPassword(@PathVariable String emailAddress, HttpServletRequest request) {
        logger.info("method ::: forgotPassword");
        return userActivationService.forgotPassword(emailAddress, request);
    }


    /**
     * Validating reset password key
     * & setting a new password
     *
     * @param forgotPassword
     * @return
     */
    @PutMapping("user/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ForgotPassword forgotPassword) {
        logger.info("method ::: resetPassword");
        String status = userActivationService.passwordReset(forgotPassword);
        if (status.equalsIgnoreCase("Password Reset Successfully !!!")) {
            return ResponseDomain.successResponse(messageProperties.getResetPasswordSuccess());
        } else
            return ResponseDomain.badRequest(status);
    }

    /**
     * This method is called for change the Password
     *
     * @param auth
     * @param userPasswordChangeDTO
     * @return ResponseDomain for change password  success or failed
     */
    @PostMapping("user/change-password")
    public ResponseEntity<?> changePassword(@RequestHeader("Authorization") String auth,
                                            @RequestBody UserPasswordChangeDTO userPasswordChangeDTO) {
        logger.info("method ::: changePassword");
        return userActivationService.changePassword(auth, userPasswordChangeDTO);
    }

    /**
     * This method is called by customer from Email open the dialog for activation
     *
     * @return Return page which get user name and password
     */
    @GetMapping("customer/activate/account/{id}")
    @Caching(evict = {
            @CacheEvict(value = "admin.users-details", allEntries = true)
    })
    public RedirectView customerActivation(@PathVariable("id") String id) {
        logger.info("method ::: customerActivation");
        try {
            logger.info("method ::: customerAccountActivation");
            if (userActivationService.accountActivation(id))
                //return new RedirectView("https://appzbuild.com/activate-user/" + id);
                return new RedirectView(appProperties.getUserRedirect() + id);
            else
                // return new RedirectView("https://appzbuild.com/error?message=Account Activation Link Expired , Please contact to administrators&error=498");
                return new RedirectView(appProperties.getLoginPageRedirectError());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: customerActivation ::: error ::: " + e.getMessage());
            return new RedirectView(appProperties.getLoginPageRedirectError());
            //return new RedirectView("https://appzbuild.com/error?message=Account Activation Link Expired , Please contact to administrators&error=498");
        }
    }


    @PutMapping("customer/activate/account/{id}")
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> customerAccountActivate(@PathVariable("id") String activationKey,
                                                     @RequestBody CustomerActivationDTO customerActivationDTO) {
        logger.info("method ::: customerAccountActivation");
        return userActivationService.customerAccountActivate(activationKey, customerActivationDTO);
    }

    @GetMapping("reset-password/{id}")
    public RedirectView resetPasswordWithLink(@PathVariable("id") String id) {
        logger.info("method ::: resetPasswordWithLink");
        try {
            logger.info("method ::: resetPasswordWithLink");
            if (userActivationService.checkPasswordResetKey(id))
                return new RedirectView(appProperties.getResetPasswordRedirect() + id);
            else
                return new RedirectView(appProperties.getResetPasswordPageRedirectError());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: resetPasswordWithLink ::: error ::: " + e.getMessage());
            return new RedirectView(appProperties.getResetPasswordPageRedirectError());
        }
    }


}