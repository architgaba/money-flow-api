package com.mfm.api.controller.user.login;

import com.mfm.api.dto.login.Login;
import com.mfm.api.service.user.login.LoginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * User Authentication
 *  & Authorization Controller
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class AuthenticationController {

    private static final Logger logger = LogManager.getLogger(AuthenticationController.class);

    @Autowired
    private LoginService loginService;

    /**
     * User Authentication/Login
     *
     * @param login
     * @return authorization token
     */
    @PostMapping("/authenticate")
    public ResponseEntity<?> userAuthentication(@RequestBody Login login){
        logger.info("method ::: userAuthentication");
       return loginService.userAuthentication(login);
    }


    /**
     * User Authorization
     * (Fetching Roles & UserName)
     *
     * @param authKey
     * @return user roles & name
     */
    @GetMapping("/authorize")
    ResponseEntity<?> authorize(@RequestHeader("Authorization") String authKey){
        logger.info("method ::: authorize");
        return loginService.authorize(authKey);
    }

}
