package com.mfm.api.controller.user.information;

import com.mfm.api.domain.user.information.UserBillingAddress;
import com.mfm.api.domain.user.information.UserPhysicalAddress;
import com.mfm.api.domain.user.information.UserShippingAddress;
import com.mfm.api.dto.request.UserPersonalInfoDTO;
import com.mfm.api.service.user.information.UserProfileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/information")
@CrossOrigin
public class UserProfileController {

    private static Logger logger = LogManager.getLogger(UserProfileController.class);

    @Autowired
    private UserProfileService userProfileService;

    /**
     * Update User
     * Personal Information
     *
     * @param infoDTO
     * @return Success Response
     */
    @PutMapping("/update")
    private ResponseEntity<?> updatePersonalInfo(@RequestHeader("Authorization") String auth, @RequestBody UserPersonalInfoDTO infoDTO) {
        logger.info("method ::: updatePersonalInfo");
        return userProfileService.updatePersonalInfo(infoDTO, auth);
    }

    /**
     * This method is call to Get User Personal Information
     *
     * @param auth
     * @return ResponseEntity for User Information
     */
    @GetMapping("/personal")
    public ResponseEntity<?> getUserInformation(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserInformation");
        return userProfileService.getUserInformation(auth);
    }

    /**
     * This method is call to Get Save User Physical Address Information
     *
     * @param auth
     * @param userPhysicalAddress
     * @return save message Success or Not
     */
    @PostMapping("/address/physical")
    public ResponseEntity<?> saveUserPhysicalAddress(@RequestHeader("Authorization") String auth,
                                                     @RequestBody UserPhysicalAddress userPhysicalAddress) {
        logger.info("method ::: saveUserPhysicalAddress");
        return userProfileService.saveUserPhysicalAddress(auth, userPhysicalAddress);
    }

    /**
     * This method is call to Get Get User Physical Address Information
     *
     * @param auth
     * @return ResponseEntity for User Physical Address Information
     */
    @GetMapping("/address/physical")
    public ResponseEntity<?> getUserPhysicalAddress(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserPhysicalAddress");
        return userProfileService.getUserPhysicalAddress(auth);
    }


    /**
     * This method is call to Get User Billing Address Information
     *
     * @param auth
     * @return ResponseEntity for User Billing Address Information in the form of List
     */
    @GetMapping("/address/billing")
    public ResponseEntity<?> getUserBillingAddress(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserBillingAddress");
        return userProfileService.getUserBillingAddress(auth);
    }


    /**
     * This method is call to Get Save User Billing Address Information in the form of Object
     *
     * @param auth
     * @param userBillingAddress
     * @return save message Success or Not
     */
    @PostMapping("/address/billing")
    public ResponseEntity<?> saveUserBillingAddress(@RequestHeader("Authorization") String auth,
                                                    @RequestBody UserBillingAddress userBillingAddress) {
        logger.info("method ::: saveUserBillingAddress");
        return userProfileService.saveUserBillingAddress(auth, userBillingAddress);
    }


    /**
     * This method use to update the the User Billing Address By Id
     *
     * @param auth
     * @param userBillingAddressId
     * @param userBillingAddress
     * @return update message Success or Not
     */
    @PutMapping("/address/billing/{userBillingAddressId}")
    public ResponseEntity<?> updateUserBillingAddress(@RequestHeader("Authorization") String auth,
                                                      @PathVariable Long userBillingAddressId,
                                                      @RequestBody UserBillingAddress userBillingAddress) {
        logger.info("method ::: updateUserBillingAddress");
        return userProfileService.updateUserBillingAddress(auth, userBillingAddressId, userBillingAddress);
    }


    /**
     * This method use to delete the the User Billing Address By Id
     *
     * @param auth
     * @param userBillingAddressId
     * @return delete message Success or Not
     */
    @DeleteMapping("/address/billing/{userBillingAddressId}")
    public ResponseEntity<?> deleteUserBillingAddress(@RequestHeader("Authorization") String auth,
                                                      @PathVariable Long userBillingAddressId) {
        logger.info("method ::: deleteUserBillingAddress");
        return userProfileService.deleteUserBillingAddress(auth, userBillingAddressId);
    }


    /**
     * This method is call to Get Get User Shipping Address Information
     *
     * @param auth
     * @return ResponseEntity for User Shipping Address Information in the form of List
     */
    @GetMapping("/address/shipping")
    public ResponseEntity<?> getUserShippingAddress(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserShippingAddress");
        return userProfileService.getUserShippingAddress(auth);
    }

    /**
     * This method is call to Get Save User Billing Address Information in the form of Object
     *
     * @param auth
     * @param UserShippingAddress
     * @return save message Success or Not
     */
    @PostMapping("/address/shipping")
    public ResponseEntity<?> saveUserShippingAddress(@RequestHeader("Authorization") String auth,
                                                     @RequestBody UserShippingAddress UserShippingAddress) {
        logger.info("method ::: saveUserShippingAddress");
        return userProfileService.saveUserShippingAddress(auth, UserShippingAddress);
    }


    /**
     * This method is call to update User Shipping Address by Id
     *
     * @param auth
     * @param userShippingAddressId
     * @param UserShippingAddress
     * @return update message Success or Not
     */
    @PutMapping("/address/shipping/{userShippingAddressId}")
    public ResponseEntity<?> updateUserShippingAddress(@RequestHeader("Authorization") String auth,
                                                       @PathVariable Long userShippingAddressId,
                                                       @RequestBody UserShippingAddress UserShippingAddress) {
        logger.info("method ::: updateUserShippingAddress");
        return userProfileService.updateUserShippingAddress(auth, userShippingAddressId, UserShippingAddress);
    }

    /**
     * This method is call to delete the ser Shipping Address By Id
     *
     * @param auth
     * @param userShippingAddressId
     * @return delete message Success or Not
     */
    @DeleteMapping("/address/shipping/{userShippingAddressId}")
    public ResponseEntity<?> deleteUserShippingAddress(@RequestHeader("Authorization") String auth,
                                                       @PathVariable Long userShippingAddressId) {
        logger.info("method ::: deleteUserShippingAddress");
        return userProfileService.deleteUserShippingAddress(auth, userShippingAddressId);
    }

    /**
     * Fetching User
     * Reference Type
     *
     * @param auth
     * @return
     */
    @GetMapping("/reference-type")
    public ResponseEntity<?> getUserReferenceType(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUserReferenceType");
        return userProfileService.getUserReferenceType(auth);
    }

    @PutMapping("/update-p2pLink")
    public ResponseEntity<?> updatep2pLink(@RequestHeader("Authorization") String auth, @RequestBody String p2pLink) {
        logger.info("method ::: updatep2pLink");
        return userProfileService.updatep2pLink(auth, p2pLink);
    }


}
