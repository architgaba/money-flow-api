package com.mfm.api.controller.user.registration;

import com.mfm.api.domain.user.registration.ContactEnquiries;
import com.mfm.api.dto.user.UserRegistrationDto;
import com.mfm.api.service.user.registration.RegistrationService;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * User Registration/Sign-Up
 * Controller
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class RegistrationController {

    private static Logger logger = LogManager.getLogger(RegistrationController.class);
    @Autowired
    private RegistrationService registrationService;

    public static UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();

    /**
     * For New USer Registration
     *
     * @param user
     * @return Response message for registration Success or not
     */
    @PostMapping("/register")
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> userRegistration(@RequestBody UserRegistrationDto user,
                                              HttpServletRequest request) {

        logger.info("method ::: userRegistration");
        logger.info(request.getHeader("User-Agent"));
        UserAgentStringParser resourceModuleParser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent parse = resourceModuleParser.parse(request.getHeader("User-Agent"));
        logger.info(parse.getOperatingSystem().getName());
        logger.info(parse.getOperatingSystem().getFamilyName());
        logger.info(parse.getName());
        logger.info(parse.getTypeName());
        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
        return registrationService.userRegistration(user, url);
    }


    /**
     * Fetching Countries
     *
     * @return List Of Countries
     * along with their respective states
     */
    @GetMapping("/countries")
    public ResponseEntity<?> getCountries() {
        logger.info("method ::: getCountries");
        return registrationService.getCountries();
    }




}
