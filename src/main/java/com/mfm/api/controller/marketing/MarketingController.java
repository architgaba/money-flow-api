package com.mfm.api.controller.marketing;

import com.mfm.api.domain.marketing.MarketingRequestBody;
import com.mfm.api.domain.user.registration.GuestUser;
import com.mfm.api.service.marketing.MarketingService;
import com.mfm.api.service.storage.AmazonS3ClientService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/marketing")
public class MarketingController {

    private static Logger logger = LogManager.getLogger(MarketingController.class);

    @Autowired
    private MarketingService marketingService;

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;


    /**
     * Sending Mails
     *
     * @param auth
     * @param marketingRequestBody
     * @return
     */

    @PostMapping("/send-mail")
    public ResponseEntity<?> sendMail(@RequestHeader("Authorization") String auth, @RequestParam(required = false, name = "attachments") List<MultipartFile> attachments, @Valid MarketingRequestBody marketingRequestBody, Errors errors) {
        return marketingService.sendMail(marketingRequestBody, attachments,auth);
    }

    /**
     * Uploading Images
     *
     * @param file
     * @return
     */

    @PostMapping("/upload-image")
    public ResponseEntity<?> uploadImages(@RequestParam(name = "image") MultipartFile file) {
        logger.info("method ::: uploadImages");
        return amazonS3ClientService.uploadImage(file);
    }

    /**
     * Adding Guest Users
     *
     * @param guestUser
     * @return
     */

    @PostMapping("/guest-users")
    public ResponseEntity<?> addGuestUser(@RequestBody GuestUser guestUser) {
        logger.info("method ::: addGuestUser");
        return marketingService.addGuestUser(guestUser);
    }

}
