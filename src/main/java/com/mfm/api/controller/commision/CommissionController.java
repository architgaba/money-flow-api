package com.mfm.api.controller.commision;


import com.mfm.api.domain.commision.CommissionSetting;
import com.mfm.api.service.commision.CommissionSettingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commission")
@CrossOrigin
public class CommissionController {


    private static Logger logger = LogManager.getLogger(CommissionController.class);

    @Autowired
    private CommissionSettingService commissionSettingService;

    /**
     * This method is called for getting the Commission settings
     *
     * @param auth
     * @return
     */
    @GetMapping("/setting")
    public ResponseEntity<?> getCommissionSetting(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getCommissionSetting");
        return commissionSettingService.getCommissionSetting(auth);
    }


    /**
     * This method is called when Update the Commission Setting
     *
     * @param auth
     * @param commissionSettingId
     * @param commissionSetting
     * @return
     */
    @PutMapping("/setting/update/id/{commissionSettingId}")
    public ResponseEntity<?> updateCommissionSetting(@RequestHeader("Authorization") String auth,
                                                     @PathVariable Long commissionSettingId,
                                                     @RequestBody CommissionSetting commissionSetting) {
        logger.info("method ::: updateCommissionSetting");
        return commissionSettingService.updateCommissionSetting(auth, commissionSettingId, commissionSetting);
    }

    /**
     * This Method is called by Admin,Super_Admin and Customer_Reps to get Customers's commission record
     *
     * @param auth
     * @param month
     * @param year
     * @return
     */
    @GetMapping("/fetch/report/")
    public ResponseEntity<?> getCommissionRecord(@RequestHeader("Authorization") String auth,
                                                 @RequestParam(required = false) int month,
                                                 @RequestParam(required = false) int year) {
        logger.info("method ::: getCommissionRecord");
        return commissionSettingService.getCommissionRecord(auth, month - 1, year);
    }


    /**
     * This method is called for getting the commission record by Referrer Id
     *
     * @param auth
     * @param commissionRecordId
     * @return
     */
    @GetMapping("/report/customer-reps/{commissionRecordId}")
    public ResponseEntity<?> getCommissionRecordByReferrerId(@RequestHeader("Authorization") String auth,
                                                             @PathVariable Long commissionRecordId,
                                                             @RequestParam String month,
                                                             @RequestParam String year) {
        logger.info("method ::: getCommissionRecordByReferrerId");
        return commissionSettingService.getCommissionRecordByReferrerId(auth, commissionRecordId, month, year);
    }

}
