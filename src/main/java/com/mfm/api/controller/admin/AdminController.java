package com.mfm.api.controller.admin;

import com.mfm.api.domain.user.registration.ContactEnquiries;
import com.mfm.api.dto.request.ChangeReferralCodeDTO;
import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.UpdateEmail;
import com.mfm.api.service.admin.AdminService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {


    private static Logger logger = LogManager.getLogger(AdminController.class);
    @Autowired
    private AdminService adminService;


    /**
     * List Of Referrers
     * Along With Their Customers
     *
     * @param auth
     * @return
     */
    @GetMapping(value = "/customer-reps")
    private ResponseEntity<?> allReferrers(@RequestHeader("Authorization") String auth) {

        logger.info("method ::: allReferrers");
        return adminService.allReferrers(auth);
    }

    @GetMapping(value = "/app-ambassador")
    private ResponseEntity<?> allAppAmbassador(@RequestHeader("Authorization") String auth) {

        logger.info("method ::: allAppAmbassador");
        return adminService.allAppAmbassador();
    }

    @GetMapping(value = "/app-ambassador/token-free")
    private ResponseEntity<?> allAppAmbassadorTokenFree() {

        logger.info("method ::: allAppAmbassadorTokenFree");
        return adminService.allAppAmbassadorTokenFree();
    }

    /**
     * Fetching Customers
     *
     * @param auth
     * @return
     */
    @GetMapping("/my-customers")
    private ResponseEntity<?> myCustomers(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: myCustomers");
        return adminService.getMyCustomers(auth);
    }

    /**
     * Add Customers
     *
     * @param auth
     * @return
     */
    @PostMapping("/add-customer")
    private ResponseEntity<?> addCustomer(@RequestHeader("Authorization") String auth,
                                          @RequestBody CustomerRegistrationDTO customerRegistrationDTO,
                                          HttpServletRequest request) {
        logger.info("method ::: addCustomer");
        return adminService.addCustomer(auth, customerRegistrationDTO, request);
    }


    /**
     * Adding Referrer
     *
     * @param auth
     * @param customerRegistrationDTO
     * @param request
     * @return
     */
    @PostMapping("/add-customer-rep")
    private ResponseEntity<?> addReferrer(@RequestHeader("Authorization") String auth,
                                          @RequestBody CustomerRegistrationDTO customerRegistrationDTO,
                                          HttpServletRequest request) {
        logger.info("method ::: addReferrer");
        return adminService.addReferrer(auth, customerRegistrationDTO, request);
    }

    @PostMapping("/add-p2p-distributor/self-register")
    private ResponseEntity<?> p2pDistributorSelfRegister(@RequestBody CustomerRegistrationDTO customerRegistrationDTO,
                                                         HttpServletRequest request) {
        logger.info("method ::: p2pDistributorSelfRegister");
        return adminService.p2pDistributorSelfRegister(customerRegistrationDTO, request);
    }

    /**
     * Update Customer & Customer-Rep
     * Account Status
     *
     * @param auth
     * @param userId
     * @param action
     * @param request
     * @return
     */
    @PutMapping({"/my-customer/update-account", "/customer-reps/update-account"})
    private ResponseEntity<?> updateUserStatus(@RequestHeader("Authorization") String auth,
                                               @RequestParam(name = "id") Long userId,
                                               @RequestParam String action,
                                               HttpServletRequest request) {
        logger.info("method ::: updateUserStatus");
        return adminService.updateUserStatus(auth, userId, action, request);
    }


    /**
     * Re-Send Activation Mail to User
     *
     * @param auth
     * @param userId
     * @param request
     * @return
     */
    @PostMapping({"/resend-activation-mail/customer/{id}", "/resend-activation-mail/customer-rep/{id}"})
    public ResponseEntity<?> resendActivationMailToUser(@RequestHeader("Authorization") String auth,
                                                        @PathVariable(name = "id") Long userId,
                                                        HttpServletRequest request) {
        logger.info("method ::: resendActivationMailToUser");
        return adminService.resendActivationMailToUser(auth, userId, request);
    }

    /**
     * Re-Send Activation Mail to Admin
     *
     * @param auth
     * @param userId
     * @param request
     * @return
     */

    @PostMapping({"/resend-activation-mail/admin/{id}"})
    public ResponseEntity<?> resendActivationMailToAdmin(@RequestHeader("Authorization") String auth,
                                                         @PathVariable(name = "id") Long userId,
                                                         HttpServletRequest request) {
        logger.info("method ::: resendActivationMailToAdmin");
        return adminService.resendActivationMailToAdmin(auth, userId, request);
    }

    /**
     * Give the report
     *
     * @param auth
     * @return
     */
    @GetMapping("/report")
    public ResponseEntity<?> report(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: report");
        return adminService.report(auth);
    }

    /**
     * Fetching Admin Details
     *
     * @param auth
     * @return
     */

    @GetMapping("/get-admin")
    public ResponseEntity<?> getAllAdmins(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getAllAdmins");
        return adminService.getAllAdmins();
    }

    /**
     * Adding Admin
     *
     * @param auth
     * @param customerRegistrationDTO
     * @param request
     * @return
     */

    @PostMapping("/add-admin")
    public ResponseEntity<?> addAdmin(@RequestHeader("Authorization") String auth,
                                      @RequestBody CustomerRegistrationDTO customerRegistrationDTO,
                                      HttpServletRequest request) {
        logger.info("method ::: addAdmin");
        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
        return adminService.addAdmin(auth, customerRegistrationDTO, url);
    }

    /**
     * update Admin details
     *
     * @param auth
     * @param id
     * @param action
     * @return
     */

    @PutMapping("/update-admin")
    public ResponseEntity<?> updateAdmin(@RequestHeader("Authorization") String auth,
                                         @RequestParam("id") String id,
                                         @RequestParam("action") String action) {
        logger.info("method ::: updateAdmin");
        return adminService.updateAdmin(auth, id, action);
    }

    /**
     * Fetching Guest Users
     *
     * @param auth
     * @return
     */
    @GetMapping("/guest-users")
    public ResponseEntity<?> getAllGuestUsers(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getAllGuestUsers");
        return adminService.getAllGuestUsers();
    }

    /**
     * Update Email Ids'
     *
     * @param auth
     * @param updateEmail
     * @return
     */

    @PutMapping("/update-email")
    public ResponseEntity<?> updateEmailId(@RequestHeader("Authorization") String auth,
                                           @RequestBody UpdateEmail updateEmail) {
        logger.info("method ::: updateEmailId");
        return adminService.updateEmailId(updateEmail);
    }

    /**
     * Storing the Contacts
     *
     * @param contactEnquiries
     * @return
     */

    @PostMapping("/contact-enquiries")
    public ResponseEntity<?> saveContactEnquiry(@RequestBody ContactEnquiries contactEnquiries) {
        logger.info("method ::: contactEnquiries");
        return adminService.saveContactEnquiry(contactEnquiries);
    }

    /**
     * Fetching Contacts
     *
     * @param auth
     * @return
     */

    @GetMapping("/contact-enquiries")
    public ResponseEntity<?> getContactEnquiry(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: contactEnquiries");
        return adminService.getContactEnquiry();
    }

    @DeleteMapping(value = "/delete-user/{userId}")
    private ResponseEntity<?> deleteUser(@RequestHeader("Authorization") String auth, @PathVariable("userId") String userId) {
        logger.info("method ::: deleteUser");
        return adminService.deleteUser(auth, userId);
    }

    @GetMapping(value = "/all-users")
    private ResponseEntity<?> getAllUsers(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getAllUsers");
        return adminService.getAllUsers();
    }

    @GetMapping(value = "/customers/{userId}")
    private ResponseEntity<?> getAllCustomers(@RequestHeader("Authorization") String auth, @PathVariable String userId) {
        logger.info("method ::: getAllCustomers");
        return adminService.getAllCustomers(userId);
    }

    @PostMapping(value = "/change-referral-code")
    private ResponseEntity<?> changeReferrerCode(@RequestHeader("Authorization") String auth, @RequestBody ChangeReferralCodeDTO changeReferralCodeDTO) {
        logger.info("method ::: changeReferrerCode");
        return adminService.changeReferrerCode(auth, changeReferralCodeDTO);
    }

    @GetMapping(value = "/all-customers")
    private ResponseEntity<?> allCustomers(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: allCustomers");
        return adminService.allCustomers(auth);
    }

    @GetMapping(value = "/app-ambassador/eligible-for-bonuses")
    private ResponseEntity<?> allAppAmbassadorEligibleForBonuses(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: allAppAmbassadorEligibleForBonuses");
        return adminService.allAppAmbassadorEligibleForBonuses();
    }

    @GetMapping(value = "/app-ambassador/eligible-for-bonuses/token-free")
    private ResponseEntity<?> allAppAmbassadorEligibleForBonusesTokenFree() {
        logger.info("method ::: allAppAmbassadorEligibleForBonusesTokenFree");
        return adminService.allAppAmbassadorEligibleForBonusesTokenFree();
    }

    @DeleteMapping(value = "/app-ambassador/eligible-for-bonuses/{userRegistrationId}")
    private ResponseEntity<?> deleteAppAmbassadorEligibleForBonuses(@RequestHeader("Authorization") String auth, @PathVariable String userRegistrationId) {
        logger.info("method ::: deleteAppAmbassadorEligibleForBonuses");
        return adminService.deleteAppAmbassadorEligibleForBonuses(auth, userRegistrationId);
    }

    @PostMapping(value = "/app-ambassador/eligible-for-bonuses/{userRegistrationId}")
    private ResponseEntity<?> addAsAppAmbassadorEligibleForBonuses(@RequestHeader("Authorization") String auth, @PathVariable String userRegistrationId) {
        logger.info("method ::: addAsAppAmbassadorEligibleForBonuses");
        return adminService.addAsAppAmbassadorEligibleForBonuses(auth, userRegistrationId);
    }

    @GetMapping(value = "/p2p-distributor")
    private ResponseEntity<?> allP2pDistributor(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: allP2pDistributor");
        return adminService.allP2pDistributor(auth);
    }

    @GetMapping(value = "/bonus-list")
    private ResponseEntity<?> allBonus(@RequestHeader("Authorization") String auth, @RequestParam(required = false) String fromDate,
                                       @RequestParam(required = false) String toDate) {
        logger.info("method ::: allBonus");
        return adminService.allBonus(auth, fromDate, toDate);
    }

    @GetMapping(value = "/my-bonus")
    private ResponseEntity<?> bonusByUserRegistrationId(@RequestHeader("Authorization") String auth, @RequestParam(required = false) String fromDate,
                                                        @RequestParam(required = false) String toDate) {
        logger.info("method ::: bonusByUserRegistrationId");
        return adminService.bonusByUserRegistrationId(auth, fromDate, toDate);
    }

    @GetMapping("/team-members/{userId}")
    public ResponseEntity<?> getTeamMembersByUserId(@RequestHeader("Authorization") String auth,
                                                    @PathVariable String userId) {
        logger.info("method ::: getTeamMembersByUserId");
        return adminService.getTeamMembersByUserId(auth, userId);
    }

    @GetMapping("/direct-indirect-users/{userId}")
    public ResponseEntity<?> getAllDirectIndirectUsers(@RequestHeader("Authorization") String auth,
                                                       @PathVariable String userId) {
        logger.info("method ::: getAllDirectIndirectUsers");
        return adminService.getAllDirectIndirectUsers(auth, userId);
    }

    @PostMapping("/make-eligible-for-Promotion")
    public ResponseEntity<?> makeEligibleforPromotion(@RequestHeader("Authorization") String auth,
                                                      @RequestBody List<String> userIdList) {
        logger.info("method ::: makeEligibleforPromotion");
        return adminService.makeEligibleforPromotion(auth, userIdList);
    }


    @PutMapping("/make-ineligible-for-Promotion")
    public ResponseEntity<?> makeIneligibleforPromotion(@RequestHeader("Authorization") String auth,
                                                        @RequestBody List<String> userIdList) {
        logger.info("method ::: makeIneligibleforPromotion");
        return adminService.makeIneligibleforPromotion(auth, userIdList);
    }

    @GetMapping("/users-eligible-for-Promotion")
    public ResponseEntity<?> getUsersEligibleForPromotion(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getUsersEligibleForPromotion");
        return adminService.getUsersEligibleForPromotion(auth);
    }

    @GetMapping("/user-with-group")
    public ResponseEntity<?> getAllUsersWithGroup(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getAllUsersWithGroup");
        return adminService.getAllUsersWithGroup(auth);
    }
}
