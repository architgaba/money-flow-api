package com.mfm.api.controller.payment.subscription;

import com.mfm.api.dto.request.SubscriptionDTO;
import com.mfm.api.service.payment.subscription.SubscriptionService;
import com.stripe.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/subscription")
@CrossOrigin
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping("/create")
    public ResponseEntity<?> createSubscription(@RequestHeader("Authorization") String auth,
                                                @RequestBody SubscriptionDTO subscriptionDTO) {
        return subscriptionService.createSubscription(auth, subscriptionDTO);
    }

    @GetMapping("/fetch-transactions")
    public ResponseEntity<?> getAllTransactions(@RequestHeader("Authorization") String auth) {
        return subscriptionService.getAllTransactions(auth);
    }

    @PostMapping(consumes = "application/json", produces = "application/json", value = "/log-order-transaction")
    public ResponseEntity<?> logOrderTransaction(@RequestBody String stripeJsonEvent) {
        Event event = Event.GSON.fromJson(stripeJsonEvent, Event.class);
        return subscriptionService.logOrderTransaction(event);
    }

    @PostMapping(consumes = "application/json", produces = "application/json", value = "/log-invoice-transaction")
    public ResponseEntity<?> logInvoiceTransaction(@RequestBody String stripeJsonEvent) {
        Event event = Event.GSON.fromJson(stripeJsonEvent, Event.class);
        return subscriptionService.logInvoiceTransaction(event);
    }

    @PutMapping("/cancel")
    public ResponseEntity<?> cancelSubscription(@RequestHeader("Authorization") String auth,
                                                @RequestParam String subscriptionId) {
        return subscriptionService.cancelSubscription(auth, subscriptionId);
    }

    @GetMapping("/cards")
    public ResponseEntity<?> userCards(@RequestHeader("Authorization") String auth) {
        return subscriptionService.userCards(auth);
    }

    @PostMapping("/add-card")
    public ResponseEntity<?> addCard(@RequestHeader("Authorization") String auth,
                                     @RequestParam("token") String token) {
        return subscriptionService.addCard(auth, token);
    }

    @DeleteMapping("/delete-card")
    public ResponseEntity<?> deleteCard(@RequestHeader("Authorization") String auth,
                                        @RequestParam("cardId") String cardId) {
        return subscriptionService.deleteCard(auth, cardId);
    }
}
