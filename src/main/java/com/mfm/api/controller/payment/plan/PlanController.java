package com.mfm.api.controller.payment.plan;

import com.mfm.api.domain.payment.plan.Plan;
import com.mfm.api.service.payment.plan.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/plan")
@CrossOrigin
public class PlanController {


    @Autowired
    private PlanService planService;

    @PostMapping("/create")
    public ResponseEntity<?> createPlan(@RequestHeader("Authorization") String auth,
                                        @RequestBody Plan plan) {
        return planService.createPlan(auth, plan);
    }


    @GetMapping("/fetch")
    public ResponseEntity<?> fetchAllPlan(@RequestHeader("Authorization") String auth) {
        return planService.fetchAllPlan(auth);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updatePlan(@RequestHeader("Authorization") String auth,
                                        @RequestParam Long planId,
                                        @RequestParam String status) {
        return planService.updatePlan(auth, planId, status);
    }

    @DeleteMapping("/delete/{planId}")
    public ResponseEntity<?> deletePlan(@RequestHeader("Authorization") String auth,
                                        @PathVariable Long planId) {
        return planService.deletePlan(auth, planId);
    }

}
