package com.mfm.api.controller.referrer;

import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.UpdateEmail;
import com.mfm.api.service.referrer.ReferrerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/referrer/")
@CrossOrigin
public class ReferrerController {

    private static Logger logger = LogManager.getLogger(ReferrerController.class);

    @Autowired
    private ReferrerService referrerService;

    @GetMapping("/my-customers")
    public ResponseEntity<?> getMyCustomers(@RequestHeader("Authorization") String auth) {
        logger.info("method ::: getMyCustomers");
        return referrerService.getMyCustomers(auth);
    }

    @PostMapping("/register/customer")
    public ResponseEntity<?> registerCustomer(@RequestHeader("Authorization") String auth,
                                              @RequestBody CustomerRegistrationDTO customerRegistrationDTO,
                                              HttpServletRequest request) {
        logger.info("method ::: registerCustomer");
        return referrerService.registerCustomer(auth, customerRegistrationDTO, request);
    }


    @PostMapping("/resend-mail/customer/{customerId}")
    public ResponseEntity<?> resendMailToCustomer(@RequestHeader("Authorization") String auth,
                                                  @PathVariable Long customerId,
                                                  HttpServletRequest request) {
        logger.info("method ::: resendMailToCustomer");
        return referrerService.resendMailToCustomer(auth, customerId, request);
    }

    @PutMapping("/my-customer/update-account")
    public ResponseEntity<?> updateCustomerAccount(@RequestHeader("Authorization") String auth,
                                                   @RequestParam(name = "id") Long userId,
                                                   @RequestParam String action,
                                                   HttpServletRequest request) {
        logger.info("method ::: updateCustomerAccount");
        return referrerService.updateCustomerAccount(auth, userId, action, request);
    }

    @PutMapping("/update-email")
    public ResponseEntity<?> updateEmail(@RequestHeader("Authorization") String auth,
                                         @RequestBody UpdateEmail updateEmail) {
        logger.info("method ::: updateEmail");
        return referrerService.updateEmail(updateEmail);
    }


    @GetMapping("/team-members/{userId}")
    public ResponseEntity<?> getTeamMembersByUserId(@RequestHeader("Authorization") String auth,
                                                    @PathVariable String userId) {
        logger.info("method ::: getTeamMembersByUserId");
        return referrerService.getTeamMembersByUserId(auth, userId);
    }

    @GetMapping("/direct-indirect-users/{userId}")
    public ResponseEntity<?> getAllDirectIndirectUsers(@RequestHeader("Authorization") String auth,
                                                       @PathVariable String userId) {
        logger.info("method ::: getAllDirectIndirectUsers");
        return referrerService.getAllDirectIndirectUsers(auth, userId);
    }

}
