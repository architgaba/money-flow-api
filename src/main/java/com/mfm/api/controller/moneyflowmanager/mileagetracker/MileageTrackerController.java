package com.mfm.api.controller.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import com.mfm.api.service.moneyflowmanager.mileagetracker.MileageTrackerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money-flow/mileage-tracker")
@CrossOrigin
public class MileageTrackerController {

    @Autowired
    private MileageTrackerService mileageTrackerService;

    /**
     * @param authKey
     * @return Return Vehicle Make List
     */
    @GetMapping("/make-list")
    public ResponseEntity<?> makeList(@RequestHeader("Authorization") String authKey) {
        return mileageTrackerService.makeList();
    }

    @GetMapping("/get-first")
    public ResponseEntity<?> getFirstFlowAndVehicle(@RequestHeader("Authorization") String authKey) {
        return mileageTrackerService.getFirstFlowAndVehicle(authKey);
    }

    /**
     * For Adding A New Vehicle
     *
     * @param authKey
     * @param flowId
     * @param vehicle
     * @return success/error message
     */
    @PostMapping("/add-vehicle")
    public ResponseEntity<?> addVehicle(@RequestHeader("Authorization") String authKey,
                                        @RequestParam Long flowId,
                                        @RequestBody Vehicle vehicle) {
        return mileageTrackerService.addVehicle(vehicle, flowId);
    }

    /**
     * Vehicle List Of A Money Flow Account
     *
     * @param authKey
     * @param flowId
     * @return list of vehicles
     */
    @GetMapping("/vehicle-list")
    public ResponseEntity<?> vehicleList(@RequestHeader("Authorization") String authKey,
                                         @RequestParam Long flowId) {
        return mileageTrackerService.vehicleList(flowId);
    }

    /**
     * For updating a vehicle
     *
     * @param authKey
     * @param vehicleId
     * @param vehicle
     * @return message & status code
     */
    @PutMapping("/update-vehicle")
    public ResponseEntity<?> updateVehicle(@RequestHeader("Authorization") String authKey,
                                           @RequestParam Long vehicleId,
                                           @RequestBody Vehicle vehicle) {
        return mileageTrackerService.updateVehicle(vehicleId, vehicle);
    }

    /**
     * For deleting a vehicle
     *
     * @param authKey
     * @param vehicleId
     * @return message & status code
     */
    @DeleteMapping("/delete-vehicle")
    public ResponseEntity<?> deleteVehicle(@RequestHeader("Authorization") String authKey,
                                           @RequestParam Long vehicleId) {
        return mileageTrackerService.deleteVehicle(vehicleId);
    }

    /**
     * Add new mileage
     *
     * @param authKey
     * @param flowId
     * @return message & status code
     */
    @PostMapping("/add-mileage")
    public ResponseEntity<?> addMileage(@RequestHeader("Authorization") String authKey,
                                        @RequestParam Long flowId,
                                        @RequestBody Mileage mileage) {
        return mileageTrackerService.addMileage(flowId, mileage);
    }

    /**
     * Update an existing mileage
     *
     * @param authKey
     * @param mileageId
     * @param mileage
     * @return message & status code
     */
    @PutMapping("/update-mileage")
    public ResponseEntity<?> updateMileage(@RequestHeader("Authorization") String authKey,
                                           @RequestParam Long mileageId,
                                           @RequestParam Long newFlowId,
                                           @RequestBody Mileage mileage) {
        return mileageTrackerService.updateMileage(mileageId, newFlowId, mileage);
    }

    /**
     * For deleting an existing mileage
     *
     * @param authKey
     * @param mileageId
     * @return message & status code
     */
    @DeleteMapping("/delete-mileage")
    public ResponseEntity<?> deleteMileage(@RequestHeader("Authorization") String authKey,
                                           @RequestParam Long mileageId) {
        return mileageTrackerService.deleteMileage(mileageId);
    }

    /**
     * For Fetching Mileage List of A Money Flow Account
     *
     * @param authKey
     * @param flowId
     * @param date
     * @return list of mileages
     */
    @GetMapping("/mileage-list")
    public ResponseEntity<?> mileageList(@RequestHeader("Authorization") String authKey,
                                         @RequestParam Long flowId,
                                         @RequestParam String date,
                                         @RequestParam(required = false) String toDate
    ) {
        return mileageTrackerService.mileageList(flowId, date,toDate);
    }

    /**
     * For Merging Two trips of a Money Flow Account
     *
     * @param authKey
     * @param tripId1
     * @param tripId2
     * @return message & status code
     */
    @PutMapping("/merge-mileages")
    public ResponseEntity<?> mergeMileage(@RequestHeader("Authorization") String authKey,
                                          @RequestParam Long tripId1,
                                          @RequestParam Long tripId2) {
        return mileageTrackerService.mergeMileage(tripId1, tripId2);
    }
}
