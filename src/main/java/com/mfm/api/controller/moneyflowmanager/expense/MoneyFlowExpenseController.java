package com.mfm.api.controller.moneyflowmanager.expense;

import com.mfm.api.domain.moneyflowmanager.expense.ExpenseType;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import com.mfm.api.service.moneyflowmanager.expense.MoneyFlowExpenseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * Money Flow Expense
 * Controller(CRUD For Expense)
 */
@CrossOrigin
@RestController
@RequestMapping("/money-flow/expense")
public class MoneyFlowExpenseController {

    @Autowired
    private MoneyFlowExpenseService moneyFlowExpenseService;

    /**
     * Creating a Expense Type
     * For a User
     *
     * @param authKey
     * @param expenseType
     * @return Response Message with Status Code
     */
    @PostMapping("/create/type")
    public ResponseEntity<?> createExpenseType(@RequestHeader("Authorization") String authKey, @RequestBody ExpenseType expenseType) {
        return moneyFlowExpenseService.createExpenseType(authKey, expenseType);
    }

    /**
     * Fetch All Expense Types
     * Of a User
     *
     * @param authKey
     * @return List Of Expense Type
     */
    @GetMapping("/fetch/expense-types")
    public ResponseEntity<?> fetchExpenseTypes(@RequestHeader("Authorization") String authKey) {
        return moneyFlowExpenseService.fetchExpenseTypes(authKey);
    }

    /**
     * Create a Expense
     * For a Money Flow Account
     *
     * @param authKey
     * @param moneyFlowId
     * @param expense
     * @return Response Message with Status Code
     */
    @PostMapping("/create/{moneyFlowId}")
    public ResponseEntity<?> createExpense(@RequestHeader("Authorization") String authKey,
                                           @PathVariable String moneyFlowId,
                                           @RequestParam(required = false, name = "file") MultipartFile file,
                                           @Valid Income_Expense_Dto expense) {
        return moneyFlowExpenseService.createExpense(expense, moneyFlowId, file);
    }

    /**
     * Fetch All The Expenses
     * of a Money Flow Account
     *
     * @param auth
     * @param moneyFlowId
     * @return List of Expenses
     */
    @GetMapping("/details/{moneyFlowId}")
    public ResponseEntity<?> fetchAllExpenseOfAMoneyFlowAccount(@RequestHeader("Authorization") String auth,
                                                                @PathVariable String moneyFlowId,
                                                                @RequestParam(required = false) String fromDate,
                                                                @RequestParam(required = false) String toDate) {
        return moneyFlowExpenseService.fetchAllExpenseOfAMoneyFlowAccount(moneyFlowId,fromDate,toDate);
    }

    /**
     * Updating an Existing Expense
     * of a Money Flow Account
     *
     * @param auth
     * @param expenseDto
     * @param moneyFlowExpenseId
     * @return Response Message with Status Code
     */
    @PutMapping("/update/{moneyFlowExpenseId}")
    public ResponseEntity<?> updateExpense(@RequestHeader("Authorization") String auth, @PathVariable Long moneyFlowExpenseId,
                                           @RequestParam(required = false, name = "file") MultipartFile file,
                                           @Valid Income_Expense_Dto expenseDto) {
        return moneyFlowExpenseService.updateExpense(moneyFlowExpenseId, expenseDto,file);
    }

    /**
     * Deleting a Income
     * of a Money Flow Account
     *
     * @param auth
     * @param moneyFlowExpenseId
     * @return Response Message with Status Code
     */
    @DeleteMapping("/delete/{moneyFlowExpenseId}")
    public ResponseEntity<?> deleteExpense(@RequestHeader("Authorization") String auth, @PathVariable Long moneyFlowExpenseId) {
        return moneyFlowExpenseService.deleteExpense(moneyFlowExpenseId);
    }
}
