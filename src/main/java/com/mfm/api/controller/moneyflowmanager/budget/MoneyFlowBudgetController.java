package com.mfm.api.controller.moneyflowmanager.budget;

import com.mfm.api.domain.moneyflowmanager.budget.Budget;
import com.mfm.api.service.moneyflowmanager.budget.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Money Flow
 * Budget Controller
 * (CRUD For Budget)
 */
@CrossOrigin
@RestController
@RequestMapping("/money-flow/budget")
public class MoneyFlowBudgetController {

    @Autowired
    private BudgetService budgetService;

    /**
     * Create a New Budget
     * For a Money Flow Account
     * @param budget
     * @return Response Message with Status Code
     */
    @PostMapping("/create/{moneyFlowAccountId}")
    ResponseEntity<?> createBudget(@RequestHeader("Authorization") String auth,
                                   @RequestBody Budget budget,
                                   @PathVariable Long moneyFlowAccountId) {
        return budgetService.createBudget(budget,moneyFlowAccountId);
    }

    /**
     * Fetching All Budgets
     * Of a Money Flow Account
     * @param auth
     * @param moneyFlowAccountId
     * @return List Of Budgets
     */
    @GetMapping("/details/{moneyFlowAccountId}")
    ResponseEntity<?> fetchBudgets(@RequestHeader("Authorization") String auth,
                                   @PathVariable Long moneyFlowAccountId,
                                   @RequestParam(required = false) String fromDate,
                                   @RequestParam(required = false) String toDate) {
        return budgetService.fetchBudgets(moneyFlowAccountId,fromDate,toDate);
    }

    /**
     * Deleting a Budget
     * Of a Money Flow Account
     * @param moneyFlowBudgetId
     * @return
     */
    @DeleteMapping("/delete/{moneyFlowBudgetId}")
    ResponseEntity<?> deleteBudget(@RequestHeader("Authorization") String auth,@PathVariable Long moneyFlowBudgetId){
        return budgetService.deleteBudget(moneyFlowBudgetId);
    }

    /**
     * Updating a Budget
     * Of a Money Flow Account
     * @param auth
     * @param moneyFlowBudgetId
     * @param budget
     * @return
     */
    @PutMapping("/update/{moneyFlowBudgetId}")
    ResponseEntity<?> updateBudget(@RequestHeader("Authorization") String auth,@PathVariable Long moneyFlowBudgetId,@RequestBody Budget budget){
        return budgetService.updateBudget(moneyFlowBudgetId,budget);
    }
}
