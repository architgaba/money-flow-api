package com.mfm.api.controller.moneyflowmanager.contactmanager;


import com.mfm.api.domain.moneyflowmanager.contactmanager.ContactManager;
import com.mfm.api.service.moneyflowmanager.contactmanager.ContactManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/money-flow/contact-manager")
public class ContactManagerController {

    @Autowired
    private ContactManagerService contactManagerService;

    @PostMapping("/create")
    public ResponseEntity<?> addContact(@RequestHeader("Authorization") String auth,
                                        @RequestBody ContactManager contactManager) {
        return contactManagerService.addContact(auth, contactManager);
    }

    @GetMapping("/details")
    public ResponseEntity<?> fetchAllContactByUser(@RequestHeader("Authorization") String auth) {
        return contactManagerService.fetchAllContactByUser(auth);
    }

    @DeleteMapping("/delete/{contactId}")
    public ResponseEntity<?> deleteContactById(@RequestHeader("Authorization") String auth,
                                               @PathVariable Long contactId) {
        return contactManagerService.deleteContactById(auth, contactId);
    }

    @PutMapping("/update/{contactId}/")
    public ResponseEntity<?> updateContact(@RequestHeader("Authorization") String auth,
                                           @PathVariable Long contactId,
                                           @RequestParam(required = false) String status,
                                           @RequestBody ContactManager contactManager) {
        return contactManagerService.updateContact(auth, contactId, status, contactManager);
    }
}
