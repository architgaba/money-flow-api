package com.mfm.api.controller.moneyflowmanager.account;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.service.moneyflowmanager.account.MoneyFlowAccountService;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/money-flow")
@CrossOrigin
public class MoneyFlowAccountController {

    @Autowired
    private MoneyFlowAccountService accountListService;


    /**
     * This method is call to get the List of all Money moneyflowmanager account
     *
     * @param auth
     * @return ResponseEntity
     */
    @GetMapping("/details")
    public ResponseEntity<?> getMoneyFlowAccountList(@RequestHeader("Authorization") String auth,
                                                     @RequestParam(required = false) String fromDate,
                                                     @RequestParam(required = false) String toDate) {
        return accountListService.getMoneyFlowAccountList(auth, fromDate, toDate);
    }

    /**
     * This method is called for save new Money Flow Account
     *
     * @param auth
     * @param moneyFlowAccount
     * @return ResponseDomain for success or failed
     */
    @PostMapping("/create")
    public ResponseEntity<?> saveMoneyFlowAccount(@RequestHeader("Authorization") String auth,
                                                  @RequestBody MoneyFlowAccount moneyFlowAccount, HttpServletRequest request) {
//        UserAgentStringParser resourceModuleParser = UADetectorServiceFactory.getResourceModuleParser();
//        ReadableUserAgent parse = resourceModuleParser.parse(request.getHeader("User-Agent"));
//        String deviceType = parse.getOperatingSystem().getFamilyName();
        return accountListService.saveMoneyFlowAccount(auth, moneyFlowAccount);
    }

    /**
     * This method is called for update the existing Money Flow Account by Id
     *
     * @param auth
     * @param moneyFlowAccountId
     * @param moneyFlowAccount
     * @return ResponseDomain for update successful or not
     */
    @PutMapping("/update/{moneyFlowAccountId}")
    public ResponseEntity<?> updateMoneyFlowAccount(@RequestHeader("Authorization") String auth,
                                                    @PathVariable Long moneyFlowAccountId,
                                                    @RequestBody MoneyFlowAccount moneyFlowAccount) {
        return accountListService.updateMoneyFlowAccount(auth, moneyFlowAccountId, moneyFlowAccount);
    }

    /**
     * This method is called for delete Money flow account by moneyFlowAccountId
     *
     * @param auth
     * @param moneyFlowAccountId
     * @return ResponseDomain for Success or failed
     */
    @DeleteMapping("/delete/{moneyFlowAccountId}")
    public ResponseEntity<?> deleteMoneyFlowAccount(@RequestHeader("Authorization") String auth,
                                                    @PathVariable Long moneyFlowAccountId) {
        return accountListService.deleteMoneyFlowAccount(auth, moneyFlowAccountId);
    }

    /**
     * Fetch Recurring(Income/Expense)
     * of a Money Flow Account
     *
     * @param auth
     * @param moneyFlowId
     * @return
     */
    @GetMapping("/recurring-entries/{moneyFlowId}")
    public ResponseEntity<?> fetchRecurringEntries(@RequestHeader("Authorization") String auth,
                                                   @PathVariable String moneyFlowId,
                                                   @RequestParam(required = false) String fromDate,
                                                   @RequestParam(required = false) String toDate) {
        return accountListService.fetchRecurringEntries(moneyFlowId, fromDate, toDate);
    }

    /**
     * Fetching Sum of Budget, Income, Expense,
     * Profit/Loss of a Money Flow Account
     *
     * @return Dashboard Data
     */
    @GetMapping("/dashboard/{moneyFlowAccountId}")
    public ResponseEntity<?> fetchDashboardData(@RequestHeader("Authorization") String auth,
                                                @PathVariable Long moneyFlowAccountId,
                                                @RequestParam String fromDate,
                                                @RequestParam String toDate) {
        return accountListService.fetchDashboardData(moneyFlowAccountId, fromDate, toDate);
    }

    /**
     * Fetch Un-Assigned Receipts(Income/Expense)
     * of a Money Flow Account
     *
     * @param auth
     * @param moneyFlowId
     * @return
     */
    @GetMapping("/unassigned-receipts/{moneyFlowId}")
    public ResponseEntity<?> fetchUnassignedReceipts(@RequestHeader("Authorization") String auth,
                                                     @PathVariable Long moneyFlowId,
                                                     @RequestParam(required = false) String fromDate,
                                                     @RequestParam(required = false) String toDate) {
        return accountListService.fetchUnassignedReceipts(moneyFlowId, fromDate, toDate);
    }

    /**
     * Deleting Receipt Image
     *
     * @param moneyFlowIncomeId
     * @param moneyFlowExpenseId
     * @return
     */
    @DeleteMapping("/receipts/delete-receipts")
    public ResponseEntity<?> deleteReceiptsImage(@RequestHeader("Authorization") String auth,
                                                 @RequestParam(value = "moneyFlowIncomeId", required = false) Long moneyFlowIncomeId,
                                                 @RequestParam(value = "moneyFlowExpenseId", required = false) Long moneyFlowExpenseId) {
        return accountListService.deleteReceipts(moneyFlowIncomeId != null ? moneyFlowIncomeId : moneyFlowExpenseId, moneyFlowIncomeId != null ? "income" : "expense");
    }
}
