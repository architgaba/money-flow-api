package com.mfm.api.controller.moneyflowmanager.income;

import com.mfm.api.domain.moneyflowmanager.income.IncomeType;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import com.mfm.api.service.moneyflowmanager.income.MoneyFlowIncomeService;
import com.mfm.api.util.response.ResponseDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * Money Flow
 * Income Controller
 * (CRUD FOR INCOME of a Money Flow Account)
 */
@CrossOrigin
@RestController
@RequestMapping("/money-flow/income")
public class MoneyFlowIncomeController {


    @Autowired
    private MoneyFlowIncomeService moneyFlowIncomeService;

    /**
     * Create a New Income
     * For a Money Flow Account
     *
     * @param authKey
     * @param flowId
     * @param income
     * @return Response Message with Status Code
     */
    @PostMapping("/create/{flowId}")
    public ResponseEntity<?> createIncome(@RequestHeader("Authorization") String authKey,
                                          @PathVariable String flowId,
                                          @RequestParam(required = false, name = "file") MultipartFile file,
                                          @Valid Income_Expense_Dto income, Errors errors) {
        if (errors.hasErrors())
            return ResponseDomain.badRequest(errors.getFieldError().getDefaultMessage());
        return moneyFlowIncomeService.createIncome(income, flowId, file);
    }

    /**
     * Fetching All Incomes
     * Of A Money Flow Account
     *
     * @param auth
     * @param flowId
     * @return List of income
     * of a money flow account
     */
    @GetMapping("/details/{flowId}")
    public ResponseEntity<?> fetchAllIncomeOfAMoneyFlowAccount(@RequestHeader("Authorization") String auth, @PathVariable String flowId, @RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate) {
        return moneyFlowIncomeService.fetchAllIncomeOfAMoneyFlowAccount(flowId, fromDate, toDate);
    }

    /**
     * Updating an Existing Income
     * of a Money Flow Account
     *
     * @param auth
     * @param income
     * @param moneyFlowIncomeId
     * @return Response Message with Status Code
     */
    @PutMapping("/update/{moneyFlowIncomeId}")
    public ResponseEntity<?> updateIncome(@RequestHeader("Authorization") String auth,
                                          @PathVariable Long moneyFlowIncomeId,
                                          @RequestParam(required = false, name = "file") MultipartFile file,
                                          @Valid Income_Expense_Dto income) {
        return moneyFlowIncomeService.updateIncome(moneyFlowIncomeId, income, file);
    }

    /**
     * Deleting a Income
     * of a Money Flow Account
     *
     * @param auth
     * @param moneyFlowIncomeId
     * @return Response Message with Status Code
     */
    @DeleteMapping("/delete/{moneyFlowIncomeId}")
    public ResponseEntity<?> deleteIncome(@RequestHeader("Authorization") String auth, @PathVariable Long moneyFlowIncomeId) {
        return moneyFlowIncomeService.deleteIncome(moneyFlowIncomeId);
    }

    /**
     * Creating Income Type
     * For a User
     *
     * @param authKey
     * @param income
     * @return Response Message with Status Code
     */
    @PostMapping("/create/type")
    public ResponseEntity<?> createIncomeType(@RequestHeader("Authorization") String authKey, @RequestBody IncomeType income) {
        return moneyFlowIncomeService.createIncomeType(authKey, income);
    }

    /**
     * Fetching ALl the Income
     * Types of a User
     *
     * @param authKey
     * @return List of Income Types
     */
    @GetMapping("/fetch/income-types")
    public ResponseEntity<?> fetchAllIncomeTypes(@RequestHeader("Authorization") String authKey) {
        return moneyFlowIncomeService.fetchAllIncomeTypes(authKey);
    }

}
