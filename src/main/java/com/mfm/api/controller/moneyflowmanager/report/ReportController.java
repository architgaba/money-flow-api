package com.mfm.api.controller.moneyflowmanager.report;

import com.mfm.api.service.moneyflowmanager.report.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money-flow/reports")
@CrossOrigin
public class ReportController {

    @Autowired
    private ReportService reportService;

    /**
     * Generating Profit/Loss
     * Report For a Money Flow Account
     *
     * @param auth
     * @param MoneyFlowAccountId
     * @param year
     * @return
     */
    @GetMapping("/profit-loss")
    public ResponseEntity<?> profitLossReport(@RequestHeader("Authorization") String auth,
                                              @RequestParam Long MoneyFlowAccountId,
                                              @RequestParam Integer year) {
        return reportService.profitLossReport(MoneyFlowAccountId, year);
    }

    @GetMapping("/mileage")
    public ResponseEntity<?> mileageReport(@RequestHeader("Authorization") String auth,
                                           @RequestParam(name = "flowId") Long MoneyFlowAccountId,
                                           @RequestParam String vehicle,
                                           @RequestParam Integer year) {
        return reportService.mileageReport(MoneyFlowAccountId, year,vehicle);
    }
}
