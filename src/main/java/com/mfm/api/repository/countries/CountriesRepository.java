package com.mfm.api.repository.countries;

import com.mfm.api.domain.countries.Countries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountriesRepository extends JpaRepository<Countries,Long> {
}
