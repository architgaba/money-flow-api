package com.mfm.api.repository.referrer;

import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReferrerRepository extends JpaRepository<Referrers, Long> {
    Referrers findByReferrerCode(Long referrerCode);

    Referrers findByUser(User user);

    List<Referrers> findAllByOrderByReferrerCodeAsc();

    Boolean existsByReferrerCode(Long referrerCode);
}
