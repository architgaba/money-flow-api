package com.mfm.api.repository.customer;

import com.mfm.api.domain.customer.Customers;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Long> {
    List<Customers> findAllByReferrers(Referrers referrers);
    Customers findByUser(User user);
}
