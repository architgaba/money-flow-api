package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.UserStatus;
import org.springframework.data.repository.CrudRepository;

public interface UserStatusRepository extends CrudRepository<UserStatus, Long> {

    UserStatus findByStatusCode(Integer statusCode);
}
