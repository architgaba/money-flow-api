package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole,Long> {

    UserRole findByRoleCode(Integer roleCode);
}
