package com.mfm.api.repository.user.information;

import com.mfm.api.domain.user.information.UserBillingAddress;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserBillingAddressRepository extends JpaRepository<UserBillingAddress, Long> {
    List<UserBillingAddress> findAllByUser(User user);

    UserBillingAddress findByUserBillingAddressId(Long userBillingAddressId);
}
