package com.mfm.api.repository.user.information;

import com.mfm.api.domain.user.information.UserPhysicalAddress;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserPhysicalAddressRepository extends JpaRepository<UserPhysicalAddress, Long> {

    UserPhysicalAddress findByUser(User user);
}
