package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.ContactEnquiries;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactEnquiriesRepository extends JpaRepository<ContactEnquiries, Long> {
}
