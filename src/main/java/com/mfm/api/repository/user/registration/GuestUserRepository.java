package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.GuestUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuestUserRepository extends JpaRepository<GuestUser,Long> {
}
