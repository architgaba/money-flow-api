package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.ReferenceType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReferenceTypeRepository extends JpaRepository<ReferenceType,Long> {

    ReferenceType findByReferenceCode(Integer referenceCode);
}
