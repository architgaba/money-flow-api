package com.mfm.api.repository.user.information;

import com.mfm.api.domain.user.information.UserShippingAddress;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserShippingAddressRepository extends JpaRepository<UserShippingAddress, Long> {

    List<UserShippingAddress> findAllByUser(User user);
    UserShippingAddress findByUserShippingAddressId(Long userShippingAddressId);
}
