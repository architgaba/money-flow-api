package com.mfm.api.repository.user.registration;

import com.mfm.api.domain.user.registration.User;
import com.mfm.api.domain.user.registration.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

//    User findByUserName(String username);

    User findByUserNameIgnoreCase(String username);

//    User findByEmailAddress(String emailAddress);

    User findByEmailAddressIgnoreCase(String emailAddress);

//    boolean existsByUserName(String userName);

    boolean existsByUserNameIgnoreCase(String userName);

    boolean existsByEmailAddress(String emailAddress);

    User findByUserRegistrationId(Long userRegistrationId);

    List<User> findAllByReferrerCode(Long referrerCode);

    List<User> findAllByRole(UserRole userRole);

    User findByStripeCustomerId(String stripeCustomerId);

    User findByPasswordResetKey(String key);

}
