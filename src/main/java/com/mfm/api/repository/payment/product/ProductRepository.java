package com.mfm.api.repository.payment.product;

import com.mfm.api.domain.payment.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {

    Product findByProductType(String productType);

}

