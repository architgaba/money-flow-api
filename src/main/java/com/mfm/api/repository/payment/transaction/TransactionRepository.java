package com.mfm.api.repository.payment.transaction;

import com.mfm.api.domain.payment.transaction.Transaction;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    boolean existsByUser(User user);

    List<Transaction> findAllByUser(User user);

    Optional<List<Transaction>> findAllBySubscriptionId(String subscriptionId);

}