package com.mfm.api.repository.payment.subscription;

import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends JpaRepository<SubscriptionDetails, String> {
    SubscriptionDetails findByUser(User user);
    List<SubscriptionDetails> findAllByUser(User user);
    List<SubscriptionDetails> findAllByUserOrderBySubscriptionIdDesc(User user);
    SubscriptionDetails findByStripeSubscriptionId(String stripeSubscriptionId);
}
