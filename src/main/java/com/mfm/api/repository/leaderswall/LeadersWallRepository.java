package com.mfm.api.repository.leaderswall;

import com.mfm.api.domain.leaderswall.LeadersWall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeadersWallRepository extends JpaRepository<LeadersWall, Long> {
}
