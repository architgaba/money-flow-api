package com.mfm.api.repository.leaderswall;

import com.mfm.api.domain.leaderswall.LeadersWallCategories;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeadersWallCategoriesRepository extends JpaRepository<LeadersWallCategories, Long> {
    public List<LeadersWallCategories> findByAapRequirementLessThanEqual(Integer aapRequirement, Sort leadersWallCategoriesId);
}
