package com.mfm.api.repository.commision;

import com.mfm.api.domain.commision.CommissionSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionSettingRepository extends JpaRepository<CommissionSetting, Long> {
}
