package com.mfm.api.repository.commision;

import com.mfm.api.domain.commision.AppAmbassadorPlusEligibleMembers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppAmbassadorPlusEligibleMembersRepository extends JpaRepository<AppAmbassadorPlusEligibleMembers, Long> {
    AppAmbassadorPlusEligibleMembers findByUserRegistrationId(Long userRegistrationId);
}
