package com.mfm.api.repository.commision;

import com.mfm.api.domain.commision.AppAmbassadorBonuses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface AppAmbassadorBonusesRepository extends JpaRepository<AppAmbassadorBonuses, Long> {
    List<AppAmbassadorBonuses> findAllByUserRegistrationId(Long userRegistrationId);

    List<AppAmbassadorBonuses> findAllByUserRegistrationIdAndDisbursementDateBetween(Long userRegistrationId, LocalDate fromDate, LocalDate toDate);

    List<AppAmbassadorBonuses> findAllByDisbursementDateBetween(LocalDate fromDate, LocalDate toDate);


}
