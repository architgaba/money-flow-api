package com.mfm.api.repository.commision;

import com.mfm.api.domain.commision.CommissionPerCustomer;
import com.mfm.api.domain.commision.CommissionRecord;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommissionPerCustomerRepository extends JpaRepository<CommissionPerCustomer, Long> {

    List<CommissionPerCustomer> findByCommissionRecord(CommissionRecord commissionRecord);

    List<CommissionPerCustomer> findAllByCommissionRecord(CommissionRecord record);

    List<CommissionPerCustomer> findAllByCommissionRecordIn(List<CommissionRecord> records);

    List<CommissionPerCustomer> findAllByUser(User user);
}

