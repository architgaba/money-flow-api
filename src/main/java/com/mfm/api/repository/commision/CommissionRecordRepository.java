package com.mfm.api.repository.commision;

import com.mfm.api.domain.commision.CommissionRecord;
import com.mfm.api.domain.referrer.Referrers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CommissionRecordRepository extends JpaRepository<CommissionRecord, Long> {

    List<CommissionRecord> findAllByReferrersOrderByCommissionCalculateFromDateAsc(Referrers referrers);

    List<CommissionRecord> findAllByReferrersOrderByCommissionCalculateFromDateDesc(Referrers referrers);

    CommissionRecord findByReferrersAndCommissionCalculateToDate(Referrers referrers, LocalDate localDateNew3);

    List<CommissionRecord> findAllByReferrersAndCommissionCalculateToDateBetween(Referrers referrers, LocalDate localDatePrevFrom, LocalDate localDatePrevTo);

    List<CommissionRecord> findAllByCommissionCalculateFromDateAndCommissionCalculateToDate(LocalDate localDateFrom, LocalDate localDateTo);

    List<CommissionRecord> findAllByReferrers(Referrers referrers);

//    List<CommissionRecord> findAllOrderByCommissionCalculateFromDateDesc();
}
