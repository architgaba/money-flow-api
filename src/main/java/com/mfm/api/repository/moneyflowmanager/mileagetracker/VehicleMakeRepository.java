package com.mfm.api.repository.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.mileagetracker.VehicleMake;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VehicleMakeRepository extends CrudRepository<VehicleMake, Long> {
    List<VehicleMake> findAllByOrderByNameAsc();
}
