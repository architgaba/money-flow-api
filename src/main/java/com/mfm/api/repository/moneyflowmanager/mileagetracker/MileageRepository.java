package com.mfm.api.repository.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface MileageRepository extends JpaRepository<Mileage, Long> {

    List<Mileage> findByMoneyFlowAccountAndDateOrderByDateDescStartingTimeAsc(MoneyFlowAccount moneyFlowAccount, LocalDate date);

    List<Mileage> findAllByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);

    List<Mileage> findAllByVehicle(Vehicle vehicle);

    List<Mileage> findByDateBeforeAndDateAfterAndMoneyFlowAccountOrderByDateDescStartingTimeAsc(LocalDate startDate, LocalDate endDate, MoneyFlowAccount moneyFlowAccount);

}
