package com.mfm.api.repository.moneyflowmanager.income;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IncomeRepository extends JpaRepository<Income,Long> {

    List<Income> findAllByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);
}
