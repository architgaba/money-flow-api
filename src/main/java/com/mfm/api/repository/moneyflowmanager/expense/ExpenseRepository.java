package com.mfm.api.repository.moneyflowmanager.expense;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExpenseRepository extends JpaRepository<Expense,Long> {

    List<Expense> findAllByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);
}
