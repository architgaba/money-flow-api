package com.mfm.api.repository.moneyflowmanager.budget;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.budget.Budget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BudgetRepository extends JpaRepository<Budget,Long> {

    List<Budget> findAllByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);
}
