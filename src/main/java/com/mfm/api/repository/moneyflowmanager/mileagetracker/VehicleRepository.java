package com.mfm.api.repository.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);

    List<Vehicle> findAllByMoneyFlowAccountOrderById(MoneyFlowAccount moneyFlowAccount);
}
