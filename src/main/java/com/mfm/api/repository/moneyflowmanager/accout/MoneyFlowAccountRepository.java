package com.mfm.api.repository.moneyflowmanager.accout;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoneyFlowAccountRepository extends JpaRepository<MoneyFlowAccount, Long> {
    List<MoneyFlowAccount> findAllByUser(User user);
    List<MoneyFlowAccount> findAllByUserOrderByMoneyFlowAccountId(User user);
    MoneyFlowAccount findByMoneyFlowAccountId(Long moneyFlowAccountId);
}
