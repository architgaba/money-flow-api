package com.mfm.api.repository.moneyflowmanager.contactmanager;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.contactmanager.ContactManager;
import com.mfm.api.domain.user.registration.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactManagerRepository extends JpaRepository<ContactManager, Long> {

    boolean existsByEmailAddress(String emailAddress);

    List<ContactManager> findAllByUser(User user);

//    List<ContactManager> findAllByMoneyFlowAccount(MoneyFlowAccount moneyFlowAccount);
}
