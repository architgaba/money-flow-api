package com.mfm.api.repository.moneyflowmanager.expense;

import com.mfm.api.domain.moneyflowmanager.expense.ExpenseType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpenseTypeRepository extends JpaRepository<ExpenseType,Long> {
}
