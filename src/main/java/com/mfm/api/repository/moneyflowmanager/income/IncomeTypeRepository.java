package com.mfm.api.repository.moneyflowmanager.income;

import com.mfm.api.domain.moneyflowmanager.income.IncomeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IncomeTypeRepository extends JpaRepository<IncomeType,Long> {

}
