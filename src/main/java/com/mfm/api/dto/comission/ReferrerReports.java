package com.mfm.api.dto.comission;


import lombok.Data;

@Data
public class ReferrerReports {

    private Long commissionRecordId;
    private Long referrerId;
    private String referrerName;
    private int monthlyCustomerCount;
    private String status;
    private int yearlyCustomerCount;
    private int totalCommissions;
    private String userType;
    private String startPeriod;
    private String endPeriod;
    private int commissionFromLevel1;
    private int commissionFromLevel2;
    private boolean isP2PYEA;

}
