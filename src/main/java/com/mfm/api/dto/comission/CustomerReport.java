package com.mfm.api.dto.comission;

import lombok.Data;

@Data
public class CustomerReport {
    private String startPeriod;
    private String finishPeriod;
    private int monthlyCustomerCount;
    private int yearlyCustomerCount;
    private float amount;
    private float total;
}
