package com.mfm.api.dto.request;


import lombok.Data;

@Data
public class UserPasswordChangeDTO {
    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;
}
