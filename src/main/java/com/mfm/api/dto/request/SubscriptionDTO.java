package com.mfm.api.dto.request;

import lombok.Data;

@Data
public class SubscriptionDTO {
    private Long planId;
    private String token;
    private Long optionalPlanId;
}
