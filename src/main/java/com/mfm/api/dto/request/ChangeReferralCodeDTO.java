package com.mfm.api.dto.request;

import lombok.Data;

@Data
public class ChangeReferralCodeDTO {

    private String currentReferralCode;

    private String newReferralCode;

    private String userRegistrationId;
}
