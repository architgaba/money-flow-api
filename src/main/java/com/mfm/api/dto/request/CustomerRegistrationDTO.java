package com.mfm.api.dto.request;

import com.mfm.api.domain.user.registration.User;
import lombok.Data;

@Data
public class CustomerRegistrationDTO {

    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String contactNumber;
    private String accountType;
    private Long referralCode;
    /*private boolean p2pDistributor;*/

    public User customerRegistrationDTOMapper(CustomerRegistrationDTO customerRegistrationDTO) {
        User user = new User();
        user.setFirstName(customerRegistrationDTO.getFirstName());
        user.setLastName(customerRegistrationDTO.getLastName());
        user.setEmailAddress(customerRegistrationDTO.getEmail());
        user.setContactNumber(customerRegistrationDTO.getContactNumber());
        return user;
    }

}
