package com.mfm.api.dto.request;

import lombok.Data;

@Data
public class CustomerActivationDTO {
    private String userName;
    private String password;
}
