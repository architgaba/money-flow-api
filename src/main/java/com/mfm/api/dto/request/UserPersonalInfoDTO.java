package com.mfm.api.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserPersonalInfoDTO implements Serializable {
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String contactNumber;
    private String userName;
}
