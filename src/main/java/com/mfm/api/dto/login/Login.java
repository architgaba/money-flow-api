package com.mfm.api.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class Login implements Serializable {

    @NotNull
    @Size(min = 1, max = 50)
    private String userName;

    @NotNull
    private String password;

    @JsonIgnore
    private Boolean rememberMe;

}
