package com.mfm.api.dto.login;


import lombok.Data;

import java.io.Serializable;

@Data
public class ForgotPassword implements Serializable {

    private String password;
    private String confirmPassword;
    private String passwordResetKey;

}
