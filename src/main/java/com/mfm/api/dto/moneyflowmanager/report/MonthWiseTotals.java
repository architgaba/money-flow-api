package com.mfm.api.dto.moneyflowmanager.report;

import lombok.Data;

@Data
public class MonthWiseTotals {
    private String monthName;
    private float amount;

    public MonthWiseTotals() {
    }
}
