package com.mfm.api.dto.moneyflowmanager;

import com.mfm.api.domain.moneyflowmanager.account.RecurrenceInterval;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import com.mfm.api.util.function.CommonUtilFunctions;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeParseException;

@Data
@Component
public class Income_Expense_Dto {

    private String type;
    private String date;
    private float amount;
    private String source;
    private String notes;
    private boolean isRecurring;
    private String recurrenceDate;
    private boolean isUnassigned;
    private String recurrenceInterval;


    public Income DtoToIncome(Income_Expense_Dto incomeDto) throws DateTimeParseException {
        Income income = new Income();
        income.setIncomeType(incomeDto.getType());
        income.setAmount(CommonUtilFunctions.roundOf(incomeDto.getAmount()));
        income.setType("income");
        income.setNotes(incomeDto.getNotes());
        income.setSource(incomeDto.getSource());
        income.setRecurring(incomeDto.isRecurring());
        income.setUnassigned(incomeDto.isUnassigned);
        return income;
    }

    public Expense DtoToExpense(Income_Expense_Dto expenseDto) throws DateTimeParseException {
        Expense expense = new Expense();
        expense.setExpenseType(expenseDto.getType());
        expense.setAmount(CommonUtilFunctions.roundOf(expenseDto.getAmount()));
        expense.setType("expense");
        expense.setNotes(expenseDto.getNotes());
        expense.setSource(expenseDto.getSource());
        expense.setRecurring(expenseDto.isRecurring());
        expense.setUnassigned(expenseDto.isUnassigned());
        return expense;
    }

}
