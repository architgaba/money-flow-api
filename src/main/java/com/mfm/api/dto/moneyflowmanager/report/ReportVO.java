package com.mfm.api.dto.moneyflowmanager.report;

import lombok.Data;

import java.util.List;

@Data
public class ReportVO {
    private String typeName;
    private List<MonthWiseTotals> amount;
    private float total;

    public ReportVO() {
    }
}
