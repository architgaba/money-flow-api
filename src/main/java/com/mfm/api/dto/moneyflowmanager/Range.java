package com.mfm.api.dto.moneyflowmanager;

import lombok.Data;

import java.io.Serializable;

@Data
public class Range implements Serializable {
     private String fromDate;
     private String toDate;
}
