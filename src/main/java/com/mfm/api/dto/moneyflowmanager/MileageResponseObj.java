package com.mfm.api.dto.moneyflowmanager;

import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import lombok.Data;

import java.util.List;

@Data
public class MileageResponseObj {
    private String vehicleName;
    private List<Mileage> trips;
    private float parking;
    private float tolls;
    private float tripCost;
}
