package com.mfm.api.dto.moneyflowmanager.report;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MonthWiseVO {

    private List<MonthWiseTotals> amount;
    private float total;

    public MonthWiseVO() {
        this.amount = new ArrayList<>();
        this.total = 0.0f;
    }
}
