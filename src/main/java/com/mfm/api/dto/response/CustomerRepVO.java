package com.mfm.api.dto.response;

import lombok.Data;

@Data
public class CustomerRepVO {
    private Long referrerId;
    private String name;
    private String email;
    private String contactNumber;
    private String joiningDate;
    private String status;

    public CustomerRepVO(Long referrerId, String name, String email, String contactNumber, String joiningDate, String status) {
        this.referrerId = referrerId;
        this.name = name;
        this.email = email;
        this.contactNumber = contactNumber;
        this.joiningDate = joiningDate;
        this.status = status;
    }
}
