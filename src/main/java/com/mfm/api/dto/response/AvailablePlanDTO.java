package com.mfm.api.dto.response;

import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.payment.transaction.Transaction;
import com.mfm.api.repository.payment.transaction.TransactionRepository;
import com.mfm.api.util.converter.CustomDateConverter;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Data
public class AvailablePlanDTO {

    @Autowired
    private TransactionRepository transactionRepository;

    private String subscriptionId;
    private String transactionId;
    private String purchaseDate;
    private String validity;
    private String planName;
    private float planAmount;
    private String planDescription;
    private String planType;
    private String planInterval;
    private Long planId;
    private String activeStatus;

    public List<AvailablePlanDTO> availablePlans(List<SubscriptionDetails> subscriptionDetails) {
        return subscriptionDetails.stream().map(subscriptionDetails1 -> mapper(subscriptionDetails1)).collect(Collectors.toList());

    }

    public final AvailablePlanDTO mapper(SubscriptionDetails subscriptionDetails) {
        AvailablePlanDTO availablePlanDTO = new AvailablePlanDTO();
        availablePlanDTO.setSubscriptionId(subscriptionDetails.getStripeSubscriptionId());
        Optional<List<Transaction>> transaction = transactionRepository.findAllBySubscriptionId(subscriptionDetails.getStripeSubscriptionId());
        availablePlanDTO.setTransactionId(transaction.isPresent() ? transaction.get().get(transaction.get().size()-1).getStripeTransactionId() : "");
        availablePlanDTO.setPurchaseDate(CustomDateConverter.localDateToString(subscriptionDetails.getSubscriptionDate()));
        availablePlanDTO.setValidity(subscriptionDetails.getSubscriptionExpiryDate() != null ? CustomDateConverter.localDateToString(subscriptionDetails.getSubscriptionExpiryDate()) : "LifeTime");
        availablePlanDTO.setPlanName(subscriptionDetails.getPlan().getPlanName());
        availablePlanDTO.setPlanAmount(subscriptionDetails.getPlan().getPlanAmount());
        availablePlanDTO.setPlanDescription(subscriptionDetails.getPlan().getPlanDescription());
        availablePlanDTO.setPlanType(subscriptionDetails.getPlan().getPlanType());
        availablePlanDTO.setPlanInterval(subscriptionDetails.getPlan().getPlanInterval());
        availablePlanDTO.setPlanId(subscriptionDetails.getPlan().getPlanId());
        availablePlanDTO.setActiveStatus(subscriptionDetails.getPlan().getActiveStatus());
        return availablePlanDTO;
    }

    public AvailablePlanDTO() {
    }
}
