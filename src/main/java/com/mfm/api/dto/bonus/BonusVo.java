package com.mfm.api.dto.bonus;

import lombok.Data;

import java.util.List;

@Data
public class BonusVo {
    public String bonuseAmount;
    public String name;
    public Long userRegistrationId;
    private Long customerCount;
    public List<BonusBreakDownVo> bonusBreakDownVoList;
}
