package com.mfm.api.dto.bonus;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BonusBreakDownVo {
    private Long appAmbassadorBonuseId;

    private String bonusEarnedFrom;

    private Float bonuseAmount;

    private LocalDate disbursementDate;

}
