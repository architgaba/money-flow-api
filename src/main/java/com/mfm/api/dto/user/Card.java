package com.mfm.api.dto.user;

import lombok.Data;

@Data
public class Card {
    public String creditCard;
    public String expiryDate;
    public String name;
    public String cardId;
}
