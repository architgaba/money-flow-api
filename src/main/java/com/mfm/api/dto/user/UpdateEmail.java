package com.mfm.api.dto.user;

import lombok.Data;

@Data
public class UpdateEmail {
    private String emailId;
    private String userId;
}
