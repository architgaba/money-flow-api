package com.mfm.api.dto.user;

import lombok.Data;

@Data
public class TeamMemberVo {
    private Long appAmbassadorId;

    private Long userRegistrationId;

    private String firstName;

    private String lastName;

    private Integer level;

    private String emailAddress;

    private String contactNumber;

    private String sponsorName;

    private String sponsorCode;

    public TeamMemberVo(Long appAmbassadorId, Long userRegistrationId, String firstName, String lastName, Integer level, String emailAddress, String contactNumber, String sponsorName, String sponsorCode) {
        this.appAmbassadorId = appAmbassadorId;
        this.userRegistrationId = userRegistrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.level = level;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
        this.sponsorName = sponsorName;
        this.sponsorCode = sponsorCode;
    }

    public TeamMemberVo() {

    }
}
