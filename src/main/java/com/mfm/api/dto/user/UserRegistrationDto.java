package com.mfm.api.dto.user;

import com.mfm.api.domain.user.registration.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserRegistrationDto implements Serializable {

    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private String password;
    private boolean emailUpdates;
    private Long customerRepCode;

    public User userRegistrationDTOMapper(UserRegistrationDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUserName(userDto.getUserName().trim());
        user.setEmailAddress(userDto.getEmail());
        user.setEmailNotifyStatus(userDto.isEmailUpdates());
        user.setPassword(userDto.getPassword());
        user.setReferrerCode(userDto.getCustomerRepCode());
        return user;
    }

}
