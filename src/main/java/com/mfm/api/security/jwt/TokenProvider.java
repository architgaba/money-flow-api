package com.mfm.api.security.jwt;


import com.mfm.api.domain.user.registration.User;
import com.mfm.api.domain.user.registration.UserStatus;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.security.DomainUserDetailsService;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.MessageProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * JWT Token Provider
 */
@Component
public class TokenProvider {

    @Autowired
    private DomainUserDetailsService domainUserDetailsService;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private MailNotification mailNotification;

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    private static final String AUTHORITIES_KEY = "auth";
    private String privateKey = "test";
    private long tokenValidityInMilliseconds = 2147483647;
    //    private long tokenValidityInMilliseconds = 30000;
    private long accountActivationTokenValidityInMilliseconds = 172800000;
    private long resetKeyValidity = 600000;


    /**
     * Authorization Token Provider
     *
     * @param authentication
     * @param userName
     * @return JWT Token
     */
    public String createToken(Authentication authentication, String userName, Long id) {

        String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, 1);

//        long now = (new Date()).getTime();
//        Date validity = new Date(now + this.tokenValidityInMilliseconds);
        return Jwts.builder()
                .setSubject(id.toString())
                .setAudience(userName)
                .setIssuer("MoneyFlowManager")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .setExpiration(c.getTime())
                .compact();
    }


    /**
     * @param request
     * @return token
     */
    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    /**
     * UserName Form Token
     *
     * @param token
     * @return
     */
    public String getUserName(String token) {
        if (StringUtils.hasText(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(7);
        }
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getAudience();
    }

    /**
     * User ID From Token
     *
     * @param token
     * @return
     */
    public String getUserId(String token) {
        if (StringUtils.hasText(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(7);
        }
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Authentication
     *
     * @param token
     * @return
     */
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(getUserName(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    /**
     * Validating Token
     *
     * @param token
     * @return
     */
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Account Activation
     * Key Generation
     *
     * @param user
     * @return
     */
    public String accountActivationToken(User user) {
        long now = (new Date()).getTime();
        Date validity = new Date(now + this.accountActivationTokenValidityInMilliseconds);
        Claims claims = Jwts.claims().setSubject(user.getUserName() == null ? "" : user.getUserName());
        claims.put("role", user.getRole().getRoleDescription());
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer("MoneyFlowManager")
                .setAudience(user.getEmailAddress())
                .setId(user.getUserRegistrationId() != null ? "" + user.getUserRegistrationId() : "")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .compact();
        return token;
    }

    /**
     * Reset Password
     * Key Generation
     *
     * @param user
     * @return
     */
    public String resetPasswordKey(User user, Integer OTP) {
        Claims claims = Jwts.claims().setSubject(user.getUserName() == null ? "" : user.getUserName());
        claims.put("role", user.getRole().getRoleDescription());
        claims.put("OTP", OTP);
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer("MoneyFlowManager")
                .setAudience(OTP.toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + this.resetKeyValidity))
                .signWith(SignatureAlgorithm.HS256, privateKey)
                .compact();
        return token;
    }

    /**
     * Account Activation &
     * Password Reset Key Expiry Check
     *
     * @param token
     * @return
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * Fetching Expiry Date
     * From Account Activation &
     * Password Reset Key
     *
     * @param token
     * @return
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * FETCHING CLAIMS(PAYLOAD) FROM TOKEN
     **/
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * FETCHING CLAIMS FROM TOKEN
     **/
    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * Fetching UserName
     * From Account Activation &
     * Password Reset Key
     *
     * @param token
     * @return
     */
    public String getUsernameAccountActivationFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

    /**
     * User Id From
     * Account Activation Token
     *
     * @param token
     * @return
     */
    public String getUserIdFromAccountActivationToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .getBody();
        return claims.getId();
    }


    /**
     * Validating User
     * Account Activation Key
     **/
    @CacheEvict(value = "super-admin.admins", allEntries = true)
    public Boolean validateAccountActivationToken(String token) {
        if (isTokenExpired(token))
            return false;
        final String username = getUsernameAccountActivationFromToken(token);
        User user;
        UserStatus userStatus = null;
        if (!username.equalsIgnoreCase("")) {
            user = userRepository.findByUserNameIgnoreCase(username);
            userStatus = (user.getRole().getRoleCode() == 4 ?
                    (userStatusRepository.findByStatusCode(1))
                    : (userStatusRepository.findByStatusCode(5)));
        } else {
            user = userRepository.findByUserRegistrationId(Long.parseLong(getUserIdFromAccountActivationToken(token)));
            userStatus = userStatusRepository.findByStatusCode(4);
        }
        if (user.getAccountActivationKey().equals(token)) {
            user.setAccountActivationKey(null);
            if (!username.equalsIgnoreCase(""))
                mailNotification.sendAccountActivationMailToCustomer(user);
            user.setUserStatus(userStatus);
            userRepository.save(user);
            return true;
        } else
            return false;
    }


    /**
     * Fetching OTP
     * From Token
     *
     * @param token
     * @return
     */
    public String getOTP(String token) {
        return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token).getBody().getAudience();
    }

    /**
     * Validating Password
     * Reset Token
     *
     * @param user
     * @param OTP
     * @return
     */
    public String validateResetKey(User user, String OTP) {
        String token = user.getPasswordResetKey();
        if (token == null)
            return messageProperties.getResetPasswordLinkGenerate();
        if (isTokenExpired(token))
            return messageProperties.getResetPasswordLinkExpired();
        else if (!getOTP(token).equals(OTP))
            return messageProperties.getResetPasswordInvalidOTP();
        else
            return "success";
    }

    public boolean validateResetToken(User user) {
        String token = user.getPasswordResetKey();
        if (token == null) {
            return false;
        }

        if (isTokenExpired(token)) {
            return false;
        }
        if (token.equals(user.getPasswordResetKey())) {
            return true;
        }
        return false;
    }
}
