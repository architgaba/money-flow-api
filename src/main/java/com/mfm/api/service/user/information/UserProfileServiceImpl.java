package com.mfm.api.service.user.information;

import com.mfm.api.domain.user.information.UserBillingAddress;
import com.mfm.api.domain.user.information.UserPhysicalAddress;
import com.mfm.api.domain.user.information.UserShippingAddress;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.request.UserPersonalInfoDTO;
import com.mfm.api.repository.user.information.UserBillingAddressRepository;
import com.mfm.api.repository.user.information.UserPhysicalAddressRepository;
import com.mfm.api.repository.user.information.UserShippingAddressRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProfileServiceImpl implements UserProfileService {

    private static Logger logger = LogManager.getLogger(UserProfileServiceImpl.class);

    @Autowired
    private UserPhysicalAddressRepository userPhysicalAddressRepository;
    @Autowired
    private UserBillingAddressRepository userBillingAddressRepository;
    @Autowired
    private UserShippingAddressRepository userShippingAddressRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private TokenProvider tokenProvider;


    @Override
    @Cacheable(value = "money.flow.user.personalInfo", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getUserInformation(String auth) {
        logger.info("method ::: getUserInformation");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        JSONObject res = new JSONObject();
        if (user != null) {
            res.put("userRegistrationId", user.getUserRegistrationId());
            res.put("firstName", user.getFirstName());
            res.put("lastName", user.getLastName());
            res.put("userName", user.getUserName());
            res.put("emailAddress", user.getEmailAddress());
            res.put("creationDate", CustomDateConverter.dateToTimestamp(user.getCreationDate()));
            res.put("lastModifiedDate", CustomDateConverter.dateToTimestamp(user.getLastModifiedDate()));
            res.put("userStatus", user.getUserStatus().getStatusDescription());
            res.put("contactNumber", user.getContactNumber());
            res.put("lastPasswordChangeDate", user.getLastPasswordChangeDate() != null ? CustomDateConverter.localDateTimeToString(user.getLastPasswordChangeDate()) : "");
            res.put("lastSignedInDate", CustomDateConverter.localDateTimeToString(user.getLastSignedInDate()));
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }


    @Override
    @CacheEvict(value = "money.flow.user.physicalAddress", allEntries = true)
    public ResponseEntity<?> saveUserPhysicalAddress(String auth, UserPhysicalAddress userPhysicalAddress) {
        logger.info("method ::: saveUserPhysicalAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            UserPhysicalAddress userPhysicalAddressEO = userPhysicalAddressRepository.findByUser(user);
            if (userPhysicalAddressEO != null) {
                userPhysicalAddressEO.setPhysicalAddress1(userPhysicalAddress.getPhysicalAddress1());
                userPhysicalAddressEO.setPhysicalAddress2(userPhysicalAddress.getPhysicalAddress2());
                userPhysicalAddressEO.setPhysicalAddressCountry(userPhysicalAddress.getPhysicalAddressCountry());
                userPhysicalAddressEO.setPhysicalAddressState(userPhysicalAddress.getPhysicalAddressState());
                userPhysicalAddressEO.setPhysicalAddressCity(userPhysicalAddress.getPhysicalAddressCity());
                userPhysicalAddressEO.setPhysicalAddressPinCode(userPhysicalAddress.getPhysicalAddressPinCode());
                userPhysicalAddressEO.setUser(user);
                userPhysicalAddressRepository.save(userPhysicalAddressEO);
                return ResponseDomain.postResponse(messageProperties.getPhysicalAddressUpdate());
            } else {
                userPhysicalAddress.setUser(user);
                userPhysicalAddressRepository.save(userPhysicalAddress);
                return ResponseDomain.postResponse(messageProperties.getPhysicalAddressSave());
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }


    @Override
    @Cacheable(value = "money.flow.user.physicalAddress", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getUserPhysicalAddress(String auth) {
        logger.info("method ::: getUserPhysicalAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            UserPhysicalAddress userPhysicalAddressEO = userPhysicalAddressRepository.findByUser(user);
            if (userPhysicalAddressEO != null) {
                return new ResponseEntity<>(userPhysicalAddressEO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new UserPhysicalAddress(), HttpStatus.OK);
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }


    @Override
    @Cacheable(value = "money.flow.user.billingAddress", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getUserBillingAddress(String auth) {
        logger.info("method ::: getUserBillingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<UserBillingAddress> userBillingAddressList = userBillingAddressRepository.findAllByUser(user);
            if (!userBillingAddressList.isEmpty()) {
                return new ResponseEntity<>(userBillingAddressList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(userBillingAddressList, HttpStatus.OK);
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }


    @Override
    @CacheEvict(value = "money.flow.user.billingAddress", allEntries = true)
    public ResponseEntity<?> saveUserBillingAddress(String auth, UserBillingAddress userBillingAddress) {
        logger.info("method ::: saveUserBillingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            if (userBillingAddress != null) {
                List<UserBillingAddress> userBillingAddressList = userBillingAddressRepository.findAllByUser(user);
                if (!userBillingAddressList.isEmpty()) {
                    if (userBillingAddress.isPrimaryAddress() == true) {
                        userBillingAddressList.stream().map(userBillingAddress1 -> {
                            userBillingAddress1.setPrimaryAddress(false);
                            return userBillingAddress1;
                        }).collect(Collectors.toList());
                        Iterable<UserBillingAddress> userBillingAddressesItr = userBillingAddressList;
                        userBillingAddressRepository.saveAll(userBillingAddressesItr);
                        saveUserBillingAddress(user, userBillingAddress);
                        return ResponseDomain.postResponse(messageProperties.getBillingAddressSave());
                    } else {
                        saveUserBillingAddress(user, userBillingAddress);
                        return ResponseDomain.postResponse(messageProperties.getBillingAddressSave());
                    }
                } else {
                    saveUserBillingAddress(user, userBillingAddress);
                    return ResponseDomain.postResponse(messageProperties.getBillingAddressSave());
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getInvalidInput());
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }

    public boolean saveUserBillingAddress(User user, UserBillingAddress userBillingAddress) {
        logger.info("method ::: saveUserBillingAddress");
        boolean isUserBillingAddressSave = false;
        if (userBillingAddress != null) {
            userBillingAddress.setUser(user);
            userBillingAddressRepository.save(userBillingAddress);
            isUserBillingAddressSave = true;
        } else {
            isUserBillingAddressSave = false;
        }
        return isUserBillingAddressSave;
    }

    @Override
    @CacheEvict(value = "money.flow.user.billingAddress", allEntries = true)
    public ResponseEntity<?> updateUserBillingAddress(String auth, Long userBillingAddressId, UserBillingAddress userBillingAddress) {
        logger.info("method ::: updateUserBillingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<UserBillingAddress> userBillingAddressList = userBillingAddressRepository.findAllByUser(user);
            if (!userBillingAddressList.isEmpty()) {
                if (userBillingAddress.isPrimaryAddress() == true) {
                    userBillingAddressList.stream().map(userBillingAddressEO -> {
                        if (userBillingAddressEO.getUserBillingAddressId() == userBillingAddressId) {
                            userBillingAddressEO.setAddressName(userBillingAddress.getAddressName());
                            userBillingAddressEO.setFirstName(userBillingAddress.getFirstName());
                            userBillingAddressEO.setLastName(userBillingAddress.getLastName());
                            userBillingAddressEO.setContactNumber(userBillingAddress.getContactNumber());
                            userBillingAddressEO.setFaxNumber(userBillingAddress.getFaxNumber());

                            userBillingAddressEO.setBillingAddress1(userBillingAddress.getBillingAddress1());
                            userBillingAddressEO.setBillingAddress2(userBillingAddress.getBillingAddress2());
                            userBillingAddressEO.setBillingAddressCountry(userBillingAddress.getBillingAddressCountry());
                            userBillingAddressEO.setBillingAddressState(userBillingAddress.getBillingAddressState());
                            userBillingAddressEO.setBillingAddressCity(userBillingAddress.getBillingAddressCity());
                            userBillingAddressEO.setBillingAddressPinCode(userBillingAddress.getBillingAddressPinCode());

                            userBillingAddressEO.setCompanyName(userBillingAddress.getCompanyName());
                            userBillingAddressEO.setPrimaryAddress(userBillingAddress.isPrimaryAddress());

                            userBillingAddressEO.setUser(user);
                            return userBillingAddressEO;
                        } else {
                            userBillingAddressEO.setPrimaryAddress(false);
                            return userBillingAddressEO;
                        }
                    }).collect(Collectors.toList());

                    Iterable<UserBillingAddress> userBillingAddresses = userBillingAddressList;
                    userBillingAddressRepository.saveAll(userBillingAddresses);
                    return ResponseDomain.putResponse(messageProperties.getBillingAddressUpdate());

                } else {
                    userBillingAddressList.stream().map(userBillingAddressEO -> {
                        if (userBillingAddressEO.getUserBillingAddressId() == userBillingAddressId) {
                            userBillingAddressEO.setAddressName(userBillingAddress.getAddressName());
                            userBillingAddressEO.setFirstName(userBillingAddress.getFirstName());
                            userBillingAddressEO.setLastName(userBillingAddress.getLastName());
                            userBillingAddressEO.setContactNumber(userBillingAddress.getContactNumber());
                            userBillingAddressEO.setFaxNumber(userBillingAddress.getFaxNumber());

                            userBillingAddressEO.setBillingAddress1(userBillingAddress.getBillingAddress1());
                            userBillingAddressEO.setBillingAddress2(userBillingAddress.getBillingAddress2());
                            userBillingAddressEO.setBillingAddressCountry(userBillingAddress.getBillingAddressCountry());
                            userBillingAddressEO.setBillingAddressState(userBillingAddress.getBillingAddressState());
                            userBillingAddressEO.setBillingAddressCity(userBillingAddress.getBillingAddressCity());
                            userBillingAddressEO.setBillingAddressPinCode(userBillingAddress.getBillingAddressPinCode());

                            userBillingAddressEO.setCompanyName(userBillingAddress.getCompanyName());
                            userBillingAddressEO.setPrimaryAddress(userBillingAddress.isPrimaryAddress());

                            userBillingAddressEO.setUser(user);
                            return userBillingAddressEO;
                        } else return false;
                    }).collect(Collectors.toList()).get(0);

                    Iterable<UserBillingAddress> userBillingAddresses = userBillingAddressList;
                    userBillingAddressRepository.saveAll(userBillingAddresses);
                    return ResponseDomain.putResponse(messageProperties.getBillingAddressUpdate());
                }

            } else {
                return ResponseDomain.badRequest(messageProperties.getBillingAddressNotExists());
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }

    @Override
    @CacheEvict(value = "money.flow.user.billingAddress", allEntries = true)
    public ResponseEntity<?> deleteUserBillingAddress(String auth, Long userBillingAddressId) {
        logger.info("method ::: deleteUserBillingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            UserBillingAddress userBillingAddressEO = userBillingAddressRepository.findByUserBillingAddressId(userBillingAddressId);
            if (userBillingAddressEO != null) {
                userBillingAddressRepository.deleteById(userBillingAddressId);
                return ResponseDomain.deleteResponse(messageProperties.getBillingAddressDelete());
            } else {
                return ResponseDomain.badRequest(messageProperties.getBillingAddressNotExists());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }


    @Override
    @Cacheable(value = "money.flow.user.shippingAddress", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getUserShippingAddress(String auth) {
        logger.info("method ::: getUserShippingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<UserShippingAddress> userShippingAddressList = userShippingAddressRepository.findAllByUser(user);
            if (!userShippingAddressList.isEmpty()) {
                return new ResponseEntity<>(userShippingAddressList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(userShippingAddressList, HttpStatus.OK);
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }


    @Override
    @CacheEvict(value = "money.flow.user.shippingAddress", allEntries = true)
    public ResponseEntity<?> saveUserShippingAddress(String auth, UserShippingAddress userShippingAddress) {
        logger.info("method ::: saveUserShippingAddressS");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            if (userShippingAddress != null) {
                List<UserShippingAddress> userShippingAddressList = userShippingAddressRepository.findAllByUser(user);

                if (!userShippingAddressList.isEmpty()) {
                    if (userShippingAddress.isPrimaryAddress() == true) {
                        userShippingAddressList.stream().map(userShippingAddressEO -> {
                            userShippingAddressEO.setPrimaryAddress(false);
                            return userShippingAddressEO;
                        }).collect(Collectors.toList());
                        Iterable<UserShippingAddress> userShippingAddressesItr = userShippingAddressList;
                        userShippingAddressRepository.saveAll(userShippingAddressesItr);
                        saveUserShippingAddress(user, userShippingAddress);
                        return ResponseDomain.postResponse(messageProperties.getShippingAddressSave());
                    } else {
                        saveUserShippingAddress(user, userShippingAddress);
                        return ResponseDomain.postResponse(messageProperties.getShippingAddressSave());
                    }
                } else {
                    saveUserShippingAddress(user, userShippingAddress);
                    return ResponseDomain.postResponse(messageProperties.getShippingAddressSave());
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getInvalidInput());
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }

    public boolean saveUserShippingAddress(User user, UserShippingAddress userShippingAddress) {
        logger.info("method ::: saveUserShippingAddress");
        boolean isUserShippingAddressSave = false;
        if (userShippingAddress != null) {
            userShippingAddress.setUser(user);
            userShippingAddressRepository.save(userShippingAddress);
            isUserShippingAddressSave = true;
        } else {
            isUserShippingAddressSave = false;
        }
        return isUserShippingAddressSave;
    }

    @Override
    @CacheEvict(value = "money.flow.user.shippingAddress", allEntries = true)
    public ResponseEntity<?> updateUserShippingAddress(String auth, Long userShippingAddressId, UserShippingAddress userShippingAddress) {
        logger.info("method ::: updateUserShippingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<UserShippingAddress> userShippingAddressList = userShippingAddressRepository.findAllByUser(user);
            if (!userShippingAddressList.isEmpty()) {
                if (userShippingAddress.isPrimaryAddress() == true) {
                    userShippingAddressList.stream().map(userShippingAddressEO -> {
                        if (userShippingAddressEO.getUserShippingAddressId() == userShippingAddressId) {
                            userShippingAddressEO.setAddressName(userShippingAddress.getAddressName());
                            userShippingAddressEO.setFirstName(userShippingAddress.getFirstName());
                            userShippingAddressEO.setLastName(userShippingAddress.getLastName());
                            userShippingAddressEO.setContactNumber(userShippingAddress.getContactNumber());
                            userShippingAddressEO.setFaxNumber(userShippingAddress.getFaxNumber());

                            userShippingAddressEO.setShippingAddress1(userShippingAddress.getShippingAddress1());
                            userShippingAddressEO.setShippingAddress2(userShippingAddress.getShippingAddress2());
                            userShippingAddressEO.setShippingAddressCountry(userShippingAddress.getShippingAddressCountry());
                            userShippingAddressEO.setShippingAddressState(userShippingAddress.getShippingAddressState());
                            userShippingAddressEO.setShippingAddressCity(userShippingAddress.getShippingAddressCity());
                            userShippingAddressEO.setShippingAddressPinCode(userShippingAddress.getShippingAddressPinCode());

                            userShippingAddressEO.setCompanyName(userShippingAddress.getCompanyName());
                            userShippingAddressEO.setPrimaryAddress(userShippingAddress.isPrimaryAddress());

                            userShippingAddressEO.setUser(user);
                            return userShippingAddressEO;
                        } else {
                            userShippingAddressEO.setPrimaryAddress(false);
                            return userShippingAddressEO;
                        }
                    }).collect(Collectors.toList());

                    Iterable<UserShippingAddress> userShippingAddresses = userShippingAddressList;
                    userShippingAddressRepository.saveAll(userShippingAddresses);
                    return ResponseDomain.putResponse(messageProperties.getShippingAddressUpdate());

                } else {
                    userShippingAddressList.stream().map(userShippingAddressEO -> {
                        if (userShippingAddressEO.getUserShippingAddressId() == userShippingAddressId) {
                            userShippingAddressEO.setAddressName(userShippingAddress.getAddressName());
                            userShippingAddressEO.setFirstName(userShippingAddress.getFirstName());
                            userShippingAddressEO.setLastName(userShippingAddress.getLastName());
                            userShippingAddressEO.setContactNumber(userShippingAddress.getContactNumber());
                            userShippingAddressEO.setFaxNumber(userShippingAddress.getFaxNumber());

                            userShippingAddressEO.setShippingAddress1(userShippingAddress.getShippingAddress1());
                            userShippingAddressEO.setShippingAddress2(userShippingAddress.getShippingAddress2());
                            userShippingAddressEO.setShippingAddressCountry(userShippingAddress.getShippingAddressCountry());
                            userShippingAddressEO.setShippingAddressState(userShippingAddress.getShippingAddressState());
                            userShippingAddressEO.setShippingAddressCity(userShippingAddress.getShippingAddressCity());
                            userShippingAddressEO.setShippingAddressPinCode(userShippingAddress.getShippingAddressPinCode());

                            userShippingAddressEO.setCompanyName(userShippingAddress.getCompanyName());
                            userShippingAddressEO.setPrimaryAddress(userShippingAddress.isPrimaryAddress());

                            userShippingAddressEO.setUser(user);
                            return userShippingAddressEO;
                        } else return false;
                    }).collect(Collectors.toList()).get(0);
                    Iterable<UserShippingAddress> userShippingAddressesItr = userShippingAddressList;
                    userShippingAddressRepository.saveAll(userShippingAddressesItr);
                    return ResponseDomain.putResponse(messageProperties.getShippingAddressUpdate());
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getShippingAddressNotExists());
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getNotExist());
        }
    }

    @Override
    @CacheEvict(value = "money.flow.user.shippingAddress", allEntries = true)
    public ResponseEntity<?> deleteUserShippingAddress(String auth, Long userShippingAddressId) {
        logger.info("method ::: deleteUserShippingAddress");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            UserShippingAddress userShippingAddressEO = userShippingAddressRepository.findByUserShippingAddressId(userShippingAddressId);
            if (userShippingAddressEO != null) {
                userShippingAddressRepository.delete(userShippingAddressEO);
                return ResponseDomain.deleteResponse(messageProperties.getShippingAddressDelete());
            } else {
                return ResponseDomain.badRequest(messageProperties.getShippingAddressNotExists());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.user.personalInfo", allEntries = true),
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)


    })
    public ResponseEntity<?> updatePersonalInfo(UserPersonalInfoDTO infoDTO, String auth) {
        logger.info("method ::: updatePersonalInfo");
        try {
            if (infoDTO.getUserName() == null || infoDTO.getEmailAddress() == null)
                return ResponseDomain.badRequest(messageProperties.getInvalidInput());
            else {
                User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
                user.setUserName(infoDTO.getUserName());
                user.setEmailAddress(infoDTO.getEmailAddress());
                user.setFirstName(infoDTO.getFirstName());
                user.setContactNumber(infoDTO.getContactNumber());
                user.setLastName(infoDTO.getLastName());
                userRepository.save(user);
                return ResponseDomain.putResponse(messageProperties.getPersonalInfoUpdate());
            }
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
            return ResponseDomain.badRequest(messageProperties.getInvalidInput());
        }
    }

    /**
     * Fetching User
     * Referred Type
     *
     * @param auth
     * @return
     */
    @Override
    @Cacheable(value = "money.flow.user.refType", key = "#root.methodName.concat(#auth)")
    public ResponseEntity<?> getUserReferenceType(String auth) {
        logger.info("method ::: getUserReferenceType");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            JSONObject object = new JSONObject();
            object.put("referredAs", user.getReferenceType().getDescription());
            object.put("p2pLink", user.getP2pLink());
            return new ResponseEntity<>(object, HttpStatus.OK);
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @CacheEvict(value = "money.flow.user.refType", allEntries = true)
    public ResponseEntity<?> updatep2pLink(String auth, String p2pLink) {
        logger.info("method ::: updatep2pLink");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            user.setP2pLink(p2pLink);
            userRepository.save(user);
            return ResponseDomain.successResponse("Your P2P link has been updated successfully.");
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }
}
