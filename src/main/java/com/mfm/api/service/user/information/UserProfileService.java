package com.mfm.api.service.user.information;

import com.mfm.api.domain.user.information.UserBillingAddress;
import com.mfm.api.domain.user.information.UserPhysicalAddress;
import com.mfm.api.domain.user.information.UserShippingAddress;
import com.mfm.api.dto.request.UserPersonalInfoDTO;
import org.springframework.http.ResponseEntity;

public interface UserProfileService {

    ResponseEntity<?> updatePersonalInfo(UserPersonalInfoDTO infoDTO, String auth);

    ResponseEntity<?> getUserInformation(String auth);

    ResponseEntity<?> saveUserPhysicalAddress(String auth, UserPhysicalAddress userPhysicalAddress);

    ResponseEntity<?> getUserPhysicalAddress(String auth);

    ResponseEntity<?> getUserBillingAddress(String auth);

    ResponseEntity<?> saveUserBillingAddress(String auth, UserBillingAddress userBillingAddress);

    ResponseEntity<?> updateUserBillingAddress(String auth, Long userBillingAddressId, UserBillingAddress userBillingAddress);

    ResponseEntity<?> deleteUserBillingAddress(String auth, Long userBillingAddressId);

    ResponseEntity<?> getUserShippingAddress(String auth);

    ResponseEntity<?> saveUserShippingAddress(String auth, UserShippingAddress userShippingAddress);

    ResponseEntity<?> updateUserShippingAddress(String auth, Long userShippingAddressId, UserShippingAddress userShippingAddress);

    ResponseEntity<?> deleteUserShippingAddress(String auth, Long userShippingAddressId);

    ResponseEntity<?> getUserReferenceType(String auth);

    ResponseEntity<?> updatep2pLink(String auth, String p2pLink);
}
