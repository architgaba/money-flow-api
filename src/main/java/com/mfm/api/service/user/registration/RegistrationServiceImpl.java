package com.mfm.api.service.user.registration;

import com.mfm.api.domain.countries.Countries;
import com.mfm.api.domain.customer.Customers;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.domain.user.registration.UserStatus;
import com.mfm.api.dto.user.UserRegistrationDto;
import com.mfm.api.repository.countries.CountriesRepository;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserReferenceTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserRoleRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.payment.subscription.SubscriptionService;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User Registration
 * Service Implementation
 */
@Service
@CacheConfig(cacheNames = "money-flow-user-registration")
public class RegistrationServiceImpl implements RegistrationService {

    private static Logger logger = LogManager.getLogger(RegistrationServiceImpl.class);
    @Autowired
    private CountriesRepository countriesRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private UserRoleRepository roleRepository;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private UserReferenceTypeRepository referenceTypeRepository;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private RegistrationServiceImpl RegistrationServiceImpl;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private MailNotification mailNotification;


    /**
     * @return List Countries
     * with their respective States
     */
    @Override
    @Cacheable(value = "money.flow.countries", key = "#root.methodName", sync = true)
    public ResponseEntity<?> getCountries() {
        logger.info("method ::: getCountries");
        try {
            List<Countries> list = countriesRepository.findAll();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: getCountries ::: Error ::: " + e);
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * @param userDto
     * @return Email to the
     * registered user for
     * account activation/verification
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> userRegistration(UserRegistrationDto userDto, String url) {
        logger.info("method ::: userRegistration");
        if (userDto.getPassword() == null || userDto.getUserName() == null || userDto.getEmail() == null) {
            logger.warn("method ::: userRegistration ::: In-Valid Input");
            return ResponseDomain.badRequest("" + messageProperties.getInvalidInput());
        }
        User user = userDto.userRegistrationDTOMapper(userDto);
        user.setIsP2PYEA(Boolean.FALSE);
        user.setIsEligibleForPromotion(Boolean.FALSE);
        user.setRole(roleRepository.findByRoleCode(1));
        user.setUserStatus(userStatusRepository.findByStatusCode(4));
        User userTest = userRepository.findByEmailAddressIgnoreCase(user.getEmailAddress());
        if (userTest != null) {
            if (userTest.getAccountActivationKey() != null) {
                logger.warn("method ::: userRegistration ::: User Already Exists");
                return ResponseDomain.badRequest("" + messageProperties.getAccountNotVerified());
            }
            return ResponseDomain.badRequest("" + messageProperties.getEmailAlreadyExists());
        } else if (userRepository.existsByUserNameIgnoreCase(user.getUserName())) {
            logger.warn("method ::: userRegistration ::: Already Exists");
            return ResponseDomain.badRequest("" + messageProperties.getUsernameNotAvailable());
        } else {
            try {
                user.setPassword(passwordEncoder.encode(user.getPassword()));
                user.setReferenceType(referenceTypeRepository.findByReferenceCode(5));
//                String jwt = tokenProvider.accountActivationToken(user);
//                user.setAccountActivationKey(jwt);

                user.setUserStatus(userStatusRepository.findByStatusCode(5));
                Referrers referrersEO = null;
                if (user.getReferrerCode() != null) {
                    referrersEO = referrerRepository.findByReferrerCode(user.getReferrerCode());
                    if (referrersEO == null) {
                        return ResponseDomain.badRequest(messageProperties.getNotValidReferralCode());
                    }
                }
                user.setReferrerCode(referrersEO != null ? referrersEO.getReferrerCode() : 1000l);
                userRepository.save(user);
//                url = url + "/user/activate/account/" + jwt;
                mailNotification.userAccountVerification("", "registration", user);
                mailNotification.sendAccountActivationMailToCustomer(user);
                return ResponseDomain.postResponse("" + messageProperties.getRegisteredSuccessfully());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                logger.error("method ::: userRegistration ::: Error ::: " + e);
                return ResponseDomain.internalServerError();
            }
        }
    }


    /**
     * @param auth
     * @param userType
     * @return message
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "money.flow.user.role", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> userSubscription(String auth, String userType) {
        logger.info("method ::: userSubscription");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("refresh", "300");
            if (userType.equalsIgnoreCase("Customer")) {
                Customers customerEO = customersRepository.findByUser(user);
                if (customerEO != null) {
                    return ResponseDomain.badRequest(messageProperties.getCustomerSubscriptionAlreadyTaken());
                } else {
                    Customers customers = new Customers();
                    customers.setReferrers(user.getReferrerCode() == null ? (referrerRepository.findByReferrerCode(1000l))
                            : referrerRepository.findByReferrerCode(user.getReferrerCode()));
                    if (customers.getReferrers() == null)
                        return ResponseDomain.badRequest(messageProperties.getInvalidReferrerCode());
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    if (!user.getRole().getRoleCode().equals(2))
                        user.setRole(roleRepository.findByRoleCode(3));
                    userRepository.save(user);
                    customers.setUser(user);
                    customersRepository.save(customers);
                    return ResponseDomain.postResponse(messageProperties.getCustomerSubscribedSuccessfully(), httpHeaders);
                }
            } else if (userType.equalsIgnoreCase("Customer-Rep")) {
                Referrers referrersEO = referrerRepository.findByUser(user);
                if (referrersEO != null) {
                    return ResponseDomain.badRequest(messageProperties.getCustomerSubscriptionAlreadyTaken());
                } else {
                    Referrers referrers = new Referrers();
                    List<Referrers> referrersListEO = referrerRepository.findAllByOrderByReferrerCodeAsc();
                    Referrers referrersLastIndex = referrersListEO.isEmpty() ? null : referrersListEO.get(referrersListEO.size() - 1);
                    referrers.setReferrerCode(referrersLastIndex == null ? 1000l : referrersLastIndex.getReferrerCode() + 1);
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    user.setRole(roleRepository.findByRoleCode(2));
                    userRepository.save(user);
                    referrers.setUser(user);
                    referrers.setReferrerType(user.getRole().getRoleDescription().replace("ROLE_", ""));
                    referrerRepository.save(referrers);
                    return ResponseDomain.postResponse(messageProperties.getCustomerRepSubscribedSuccessfully(), httpHeaders);
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getInvalidUserType());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }

}