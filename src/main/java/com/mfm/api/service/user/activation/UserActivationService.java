package com.mfm.api.service.user.activation;

import com.mfm.api.dto.login.ForgotPassword;
import com.mfm.api.dto.request.CustomerActivationDTO;
import com.mfm.api.dto.request.UserPasswordChangeDTO;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface UserActivationService {
    boolean accountActivation(String id);

    boolean checkPasswordResetKey(String id);

    ResponseEntity<?> forgotPassword(String email, HttpServletRequest request);

    String passwordReset(ForgotPassword forgotPassword);

    ResponseEntity<?> changePassword(String auth, UserPasswordChangeDTO userPasswordChangeDTO);

    ResponseEntity<?> customerAccountActivate(String activationKey, CustomerActivationDTO customerActivationDTO);

}

