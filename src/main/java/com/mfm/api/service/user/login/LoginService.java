package com.mfm.api.service.user.login;

import com.mfm.api.dto.login.Login;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface LoginService {

    ResponseEntity<?> userAuthentication(Login login);

    ResponseEntity<?> authorize(String authKey);

}
