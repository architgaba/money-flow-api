package com.mfm.api.service.user.login;

import com.mfm.api.domain.commision.AppAmbassadorPlusEligibleMembers;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.login.Login;
import com.mfm.api.repository.commision.AppAmbassadorPlusEligibleMembersRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.admin.AdminServiceImpl;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger logger = LogManager.getLogger(LoginServiceImpl.class);
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private AppAmbassadorPlusEligibleMembersRepository appAmbassadorPlusEligibleMembersRepository;
    @Autowired
    private AdminServiceImpl adminService;

    /**
     * User Authentication
     *
     * @param login
     * @return Authenticated Token
     */
    @Override
    public ResponseEntity<?> userAuthentication(Login login) {
        logger.info("method ::: userAuthentication");
        if (login.getUserName() == null || login.getPassword() == null) {
            logger.warn("method ::: userAuthentication ::: error ::: " + messageProperties.getInvalidInput());
        }
        try {
            User user = userRepository.findByUserNameIgnoreCase(login.getUserName().trim());
            if (user == null)
                return ResponseDomain.badRequest(messageProperties.getInvalidUsername());
            if (user.getAccountActivationKey() != null)
                return ResponseDomain.badRequest(messageProperties.getAccountVerification());
            if (user.getUserStatus().getStatusDescription().contains("INACTIVE"))
                return ResponseDomain.badRequest(messageProperties.getAccountInactive());
            JSONObject object = new JSONObject();
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(login.getUserName().trim(), login.getPassword());
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.createToken(authentication, login.getUserName().trim(), user.getUserRegistrationId());
            object.put("token", jwt);
            object.put("status", "true");
            user.setLastSignedInDate(LocalDateTime.now());
            userRepository.save(user);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(TokenProvider.AUTHORIZATION_HEADER, "Bearer " + jwt);
            return new ResponseEntity<>(object, httpHeaders, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  userAuthentication ::: error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getBadCredentials());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  userAuthentication ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError(e.getMessage());
        }
    }

    @Override
    @Cacheable(value = "money.flow.user.role", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> authorize(String auth) {
        logger.info("method ::: authorize");
        User currentUser;
        List<String> role = new ArrayList<>();
        try {
            currentUser = userRepository.findByUserNameIgnoreCase(tokenProvider.getUserName(auth));
            role.add(currentUser.getRole().getRoleDescription());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: authorize ::: error ::: " + e.getLocalizedMessage());
            return ResponseDomain.badRequest(e.getMessage());
        }
        JSONObject res = new JSONObject();
        res.put("permission", currentUser.getUserStatus().getStatusDescription());
        res.put("name", currentUser.getUserName());
        res.put("role", role);
        res.put("p2plink", currentUser.getP2pLink());
        res.put("refType", currentUser.getReferenceType().getDescription());
        res.put("status", currentUser.getUserStatus().getId().toString());
        res.put("customerCount", "0");
        res.put("isEligibleForPromotion", currentUser.getIsEligibleForPromotion());
        Referrers referrer = referrerRepository.findByUser(currentUser);
        if (referrer != null) {
            List<User> users = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
            long customerCount = users.stream()
                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count();
            res.put("customerCount", String.valueOf(customerCount));
        }
        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(currentUser.getUserRegistrationId());
        if (appAmbassadorPlusEligibleMembers != null) {
            res.put("myTeamSize", String.valueOf(adminService.countTeamSize(appAmbassadorPlusEligibleMembers)));
        } else {
            res.put("myTeamSize", "N.A");
        }
        res.put("refCode", ((currentUser.getRole().getRoleCode() == 2 || currentUser.getRole().getRoleCode() == 4 || currentUser.getRole().getRoleCode() == 5) ? (referrerRepository.findByUser(currentUser).getReferrerCode()) : ""));
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

}
