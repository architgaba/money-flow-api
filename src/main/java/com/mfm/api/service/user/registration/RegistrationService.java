package com.mfm.api.service.user.registration;

import com.mfm.api.domain.user.registration.ContactEnquiries;
import com.mfm.api.dto.user.UserRegistrationDto;
import org.springframework.http.ResponseEntity;

public interface RegistrationService {
    ResponseEntity<?> getCountries();

    ResponseEntity<?> userRegistration(UserRegistrationDto user, String url);

    ResponseEntity<?> userSubscription(String auth, String userType);

}
