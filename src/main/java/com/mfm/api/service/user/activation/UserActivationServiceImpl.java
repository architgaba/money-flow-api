package com.mfm.api.service.user.activation;

import com.mfm.api.domain.customer.Customers;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.login.ForgotPassword;
import com.mfm.api.dto.request.CustomerActivationDTO;
import com.mfm.api.dto.request.UserPasswordChangeDTO;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserRoleRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.user.registration.RegistrationService;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.mail.EmailContent;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.AppProperties;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * User Account
 * Verification Service
 * Implementation
 */
@Service
public class UserActivationServiceImpl implements UserActivationService {

    private static Logger logger = LogManager.getLogger(UserActivationServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    private EmailContent emailContent;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private MailNotification mailNotification;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private UserRoleRepository roleRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private CustomersRepository customersRepository;

    @Value("${mail.from}")
    private String from;

    /**
     * Validate User Account
     *
     * @param id
     * @return
     */
    @Override
    public boolean accountActivation(String id) {
        logger.info("method ::: accountActivation");
        return tokenProvider.validateAccountActivationToken(id);
    }

    @Override
    public boolean checkPasswordResetKey(String id) {
        logger.info("method ::: checkPasswordResetKey");
        User user = userRepository.findByPasswordResetKey(id);
        if (user != null) {
            return tokenProvider.validateResetToken(user);
        }
        return false;
    }

    @Override
    public ResponseEntity<?> forgotPassword(String email, HttpServletRequest request) {
        logger.info("method ::: forgotPassword");
        User user = userRepository.findByEmailAddressIgnoreCase(email);

        Integer otp = generateRandomIntIntRange(appProperties.getMinSecretLength(), appProperties.getMaxSecretLength());
        try {
            if (user != null) {
                if (user.getUserStatus().getStatusCode() == 4)
                    return ResponseDomain.badRequest("" + messageProperties.getAccountVerification());
                String passwordResetToken = tokenProvider.resetPasswordKey(user, otp);
                String url = (request.getScheme() + "://" + request.getServerName() + ":" + request
                        .getServerPort() + request.getContextPath());
                url = url + "/reset-password/" + passwordResetToken;
                logger.info("method ::: forgotPassword :::  user found");
                user.setPasswordResetKey(passwordResetToken);
                userRepository.save(user);

                logger.info("method ::: forgotPassword :::  reset password key generated");
                MimeMessage mail = emailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mail, true);
                helper.setTo(user.getEmailAddress());
                helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
                helper.setSubject("Password Reset Link");
                helper.setText(emailContent.resetPasswordEmailContent(user.getUserName(), user.getFirstName(), user.getLastName(), url), true);
                emailSender.send(mail);
                logger.info("method ::: forgotPassword :::  reset password link sent");
                return ResponseDomain.successResponse("Password reset link sent to the registered e-mail address");
            } else {
                logger.error("method ::: forgotPassword ::: Error ::: " + messageProperties.getEmailAddressNotExists());
                return ResponseDomain.badRequest("" + messageProperties.getEmailAddressNotExists());
            }
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: forgotPassword ::: error ::: " + e.getMessage());
            return ResponseDomain.badRequest("" + messageProperties.getEmailAddressNotExists());
        } catch (MessagingException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: forgotPassword ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError("Mail Server Down");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: forgotPassword ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError("Server Error");
        }
    }

    @Override
    public String passwordReset(ForgotPassword forgotPassword) {
        logger.info("method ::: passwordReset");
        User user = userRepository.findByPasswordResetKey(forgotPassword.getPasswordResetKey());
        try {
            if (user == null)
                return "Invalid Password Reset Key";
            if (forgotPassword.getPasswordResetKey().equals(user.getPasswordResetKey())) {
                if (!forgotPassword.getPassword().equals(forgotPassword.getConfirmPassword())) {
                    return "Password did not match";
                }
                user.setPassword(passwordEncoder.encode(forgotPassword.getPassword()));
                user.setPasswordResetKey(null);
                user.setLastPasswordChangeDate(LocalDateTime.now());
                userRepository.save(user);
                MimeMessage mail = emailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mail, true);
                helper.setTo(user.getEmailAddress());
                helper.setFrom(new InternetAddress(from, "P2P MEC SUPPORT"));
                helper.setSubject("Password Reset Successfully");
                helper.setText(emailContent.successPasswordEmailContent(user.getFirstName(), user.getLastName(), user.getUserName(), CustomDateConverter.localDateTimeToString(user.getLastPasswordChangeDate())), true);
                emailSender.send(mail);
                return "Password Reset Successfully !!!";
            } else
                return "Password Reset Key Expired";
        } catch (MessagingException | UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: passwordReset ::: error ::: " + e.getMessage());
            return "Mail Server Down";
        }
    }

    @Override
    @CacheEvict(value = "money.flow.user.personalInfo", allEntries = true)
    public ResponseEntity<?> changePassword(String auth, UserPasswordChangeDTO userPasswordChangeDTO) {
        logger.info("method ::: changePassword");
        if (userPasswordChangeDTO.getNewPassword().equals(userPasswordChangeDTO.getConfirmNewPassword())) {
            User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
            if (user != null) {
                boolean matchPasswordStatus = passwordEncoder.matches(userPasswordChangeDTO.getOldPassword(), user.getPassword());
                if (matchPasswordStatus) {
                    user.setPassword(passwordEncoder.encode(userPasswordChangeDTO.getConfirmNewPassword()));
                    user.setPasswordResetKey(null);
                    user.setLastPasswordChangeDate(LocalDateTime.now());
                    userRepository.save(user);
                    mailNotification.changePasswordConfirmationMail(user);
                    return ResponseDomain.postResponse(messageProperties.getPasswordChangeSuccess());
                } else {
                    return ResponseDomain.badRequest(messageProperties.getInvalidPassword());
                }
            } else {
                logger.error("method :::  deleteExpense ::: Error ::: " + messageProperties.getEmailAddressNotExists());
                return ResponseDomain.responseNotFound(messageProperties.getNotExist());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getPasswordNotMatches());
        }
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> customerAccountActivate(String activationKey, CustomerActivationDTO customerActivationDTO) {
        logger.info("method ::: customerAccountActivate");
        User user = null;
        try {
            user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserIdFromAccountActivationToken(activationKey)));
        } catch (Exception e) {
            return ResponseDomain.badRequest("Invalid Authorization Token");
        }
        if (user != null) {
            User userEO = userRepository.findByUserNameIgnoreCase(customerActivationDTO.getUserName());
            if (userEO != null) {
                return ResponseDomain.badRequest(messageProperties.getUsernameNotAvailable());
            } else {
                user.setUserName(customerActivationDTO.getUserName().trim());
                user.setPassword(passwordEncoder.encode(customerActivationDTO.getPassword()));
                user.setAccountActivationKey(null);
                if (user.getReferenceType().getReferenceCode() != 3 && user.getReferenceType().getReferenceCode() != 4 && user.getReferenceType().getReferenceCode() != 6 && user.getReferenceType().getReferenceCode() != 7)
                    user.setUserStatus(userStatusRepository.findByStatusCode(5));
                else {
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    Claims claims = tokenProvider.getAllClaimsFromToken(activationKey);
                    if (claims.get("role").equals("ROLE_ADMIN")) {
                        Referrers referrer = new Referrers();
                        referrer.setUser(user);
                        referrer.setReferrerType("ADMIN");
                        List<Referrers> referrersListEO = referrerRepository.findAllByOrderByReferrerCodeAsc();
                        Referrers referrersLastIndex = referrersListEO.isEmpty() ? null : referrersListEO.get(referrersListEO.size() - 1);
                        referrer.setReferrerCode(referrersLastIndex == null ? 1000l : referrersLastIndex.getReferrerCode() + 1);
                        user.setUserStatus(userStatusRepository.findByStatusCode(1));
                        referrerRepository.save(referrer);
                        user = userRepository.save(user);
                        if (user.getUserRegistrationId() != null) {
                            mailNotification.sendAccountActivationMailToAdmin(user);
                            return ResponseDomain.putResponse("You are Successfully registered with P2P MEC");
                        } else {
                            return ResponseDomain.internalServerError("Your account currently not activated, Please try again");
                        }
                    }
                    if (user.getReferenceType().getReferenceCode() == 4) {
                        Customers customer = new Customers();
                        customer.setReferrers(referrerRepository.findByReferrerCode(user.getReferrerCode()));
                        customer.setUser(user);
                        customersRepository.save(customer);
                        user.setRole(roleRepository.findByRoleCode(3));
                        user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    } else {
                        Referrers referrer = new Referrers();
                        referrer.setUser(user);
                        referrer.setReferrerType("REFERRER");
                        List<Referrers> referrersListEO = referrerRepository.findAllByOrderByReferrerCodeAsc();
                        Referrers referrersLastIndex = referrersListEO.isEmpty() ? null : referrersListEO.get(referrersListEO.size() - 1);
                        referrer.setReferrerCode(referrersLastIndex == null ? 1000l : referrersLastIndex.getReferrerCode() + 1);
                        user.setRole(roleRepository.findByRoleCode(2));
                        user.setUserStatus(userStatusRepository.findByStatusCode(1));
                        referrerRepository.save(referrer);
                    }
                }
                user = userRepository.save(user);
                if (user.getUserRegistrationId() != null) {
                    mailNotification.sendAccountActivationMailToCustomer(user);
                    return ResponseDomain.putResponse("You are Successfully registered with P2P MEC");
                } else {
                    return ResponseDomain.internalServerError("Your account currently not activated, Please try again");
                }
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }


    /**
     * OTP for Password Reset
     *
     * @param min
     * @param max
     * @return OTP
     */
    public static int generateRandomIntIntRange(int min, int max) {
        logger.info("method ::: generateRandomIntIntRange");
        double x = (Math.random() * ((max - min) + 1)) + min;
        if (x > 999999)
            x = x / 10;
        return (int) x;
    }
}
