package com.mfm.api.service.marketing;

import com.mfm.api.domain.marketing.MarketingRequestBody;
import com.mfm.api.domain.user.registration.GuestUser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MarketingService {

    ResponseEntity<?> sendMail(MarketingRequestBody marketingRequestBody, List<MultipartFile> attachments, String auth);

    ResponseEntity<?> addGuestUser(GuestUser guestUser);

}
