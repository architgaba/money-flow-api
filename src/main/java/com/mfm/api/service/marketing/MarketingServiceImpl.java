package com.mfm.api.service.marketing;

import com.mfm.api.domain.marketing.MarketingRequestBody;
import com.mfm.api.domain.user.registration.GuestUser;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.user.registration.GuestUserRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.mail.MailModel;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class MarketingServiceImpl implements MarketingService {

    private static Logger logger = LogManager.getLogger(MarketingServiceImpl.class);

    @Autowired
    private MailNotification mailNotification;
    @Autowired
    private MessageProperties successProperties;
    @Autowired
    private GuestUserRepository guestUserRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private TokenProvider tokenProvider;

    @Override
    public ResponseEntity<?> sendMail(MarketingRequestBody marketingRequestBody, List<MultipartFile> attachments, String auth) {
        logger.info("method ::: sendMail");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        MailModel mailModel = new MailModel(marketingRequestBody.getEmail(), marketingRequestBody.getSubject(), marketingRequestBody.getMessage());
        try {
            mailNotification.sendMailForMarketing(mailModel,attachments,user);
            return ResponseDomain.postResponse(messageProperties.getEmailSendSuccess());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: sendMail ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    @CacheEvict(value = "money.flow.guest.users", allEntries = true)
    public ResponseEntity<?> addGuestUser(GuestUser guestUser) {
        logger.info("method ::: addGuestUser");
        try {
            guestUserRepository.save(guestUser);
            mailNotification.sendMailToProspectUser(guestUser);
            return ResponseDomain.postResponse(messageProperties.getGuestUserAdded());

        } catch (DataIntegrityViolationException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  addGuestUser ::: error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getGuestUserEmailExists());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  addGuestUser ::: error ::: " + e.getMessage());
            return ResponseDomain.badRequest(e.getLocalizedMessage());
        }
    }
}
