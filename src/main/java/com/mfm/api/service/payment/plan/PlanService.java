package com.mfm.api.service.payment.plan;

import com.mfm.api.domain.payment.plan.Plan;
import org.springframework.http.ResponseEntity;

public interface PlanService {
    ResponseEntity<?> createPlan(String auth, Plan plan);

    ResponseEntity<?> fetchAllPlan(String auth);

    ResponseEntity<?> updatePlan(String auth, Long planId, String plan);

    ResponseEntity<?> deletePlan(String auth, Long planId);
}
