package com.mfm.api.service.payment.subscription;

import com.mfm.api.dto.request.SubscriptionDTO;
import com.stripe.model.Event;
import org.springframework.http.ResponseEntity;

public interface SubscriptionService {

    ResponseEntity<?> createSubscription(String auth, SubscriptionDTO subscriptionDTO);

    ResponseEntity<?> getAllTransactions(String auth);

    ResponseEntity<?> logOrderTransaction(Event event);

    ResponseEntity<?> logInvoiceTransaction(Event event);

    ResponseEntity<?> cancelSubscription(String auth,String subscriptionId);

    ResponseEntity<?> userCards(String auth);

    ResponseEntity<?> addCard(String auth, String token);

    ResponseEntity<?> deleteCard(String auth, String cardId);
}
