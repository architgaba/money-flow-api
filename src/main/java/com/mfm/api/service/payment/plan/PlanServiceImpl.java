package com.mfm.api.service.payment.plan;

import com.mfm.api.domain.payment.plan.Plan;
import com.mfm.api.domain.payment.product.Product;
import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.response.AvailablePlanDTO;
import com.mfm.api.repository.payment.plan.PlanRepository;
import com.mfm.api.repository.payment.product.ProductRepository;
import com.mfm.api.repository.payment.subscription.SubscriptionRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.response.AppProperties;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Sku;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class PlanServiceImpl implements PlanService {

    private static Logger logger = LogManager.getLogger(PlanServiceImpl.class);

    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private AvailablePlanDTO availablePlanDTO;
    @Autowired
    private ReferrerRepository referrerRepository;

    @PostConstruct
    public void init() {
        Stripe.apiKey = appProperties.getStripeSecretKey();
    }


    @Override
    public ResponseEntity<?> createPlan(String auth, Plan plan) {
        logger.info("method ::: createPlan");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if ((user != null && user.getRole().getRoleCode() == 5)) {

            if (!plan.getPlanType().equalsIgnoreCase("Customer") && !plan.getPlanType().equalsIgnoreCase("Customer-Rep"))
                return ResponseDomain.badRequest(messageProperties.getInvalidPlanType());
            if (!plan.getPlanInterval().equalsIgnoreCase("month") && !plan.getPlanInterval().equalsIgnoreCase("year") && !plan.getPlanInterval().equalsIgnoreCase("One Time"))
                return ResponseDomain.badRequest(messageProperties.getInvalidPlanInterval());
            List<Product> productList = productRepository.findAll();
            String stripePlanAmount = amountConverter(plan.getPlanAmount());
            Map<String, Object> planParams = new HashMap<String, Object>();
            if (plan.getPlanType().equalsIgnoreCase("Customer")) {
                planParams.put("amount", stripePlanAmount);
                planParams.put("interval", plan.getPlanInterval());
                planParams.put("product", productList.stream().filter(product -> product.getProductType().equalsIgnoreCase("Customer"))
                        .findFirst().get().getStripeProductId());
                planParams.put("nickname", plan.getPlanName());
                planParams.put("currency", "usd");
                try {
                    com.stripe.model.Plan planEO = com.stripe.model.Plan.create(planParams);
                    Plan newPlan = new Plan();
                    newPlan.setPlanName(planEO.getNickname());
                    newPlan.setStripePlanId(planEO.getId());
                    newPlan.setPlanAmount(plan.getPlanAmount());
                    newPlan.setProduct(productList.stream().filter(product -> product.getProductType().equalsIgnoreCase("Customer"))
                            .findFirst().get());
                    newPlan.setPlanType(plan.getPlanType());
                    newPlan.setPlanDescription(plan.getPlanDescription());
                    newPlan.setPlanCurrency("usd");
                    newPlan.setPlanInterval(WordUtils.capitalize(planEO.getInterval()) + "ly");
                    newPlan.setPlanActiveStatus(true);
                    plan = planRepository.save(newPlan);
                    if (plan != null) {
                        return ResponseDomain.postResponse(messageProperties.getPlanSuccess());
                    } else
                        return ResponseDomain.internalServerError();
                } catch (StripeException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("method :::  addAdmin ::: error ::: " + e.getMessage());
                    return ResponseDomain.internalServerError(e.getLocalizedMessage());
                }
            } else if (plan.getPlanType().equalsIgnoreCase("Customer-Rep")) {
                planParams.put("price", stripePlanAmount);
                planParams.put("product", productList.stream().filter(product -> product.getProductType().equalsIgnoreCase("Customer-Rep"))
                        .findFirst().get().getStripeProductId());
                planParams.put("currency", "usd");
                Map<String, Object> inventoryParams = new HashMap<>();
                inventoryParams.put("type", "infinite");
                planParams.put("inventory", inventoryParams);
                try {
                    Sku product1 = Sku.create(planParams);
                    Plan newPlan = new Plan();
                    newPlan.setPlanName(plan.getPlanName());
                    newPlan.setStripePlanId(product1.getId());
                    newPlan.setPlanAmount(plan.getPlanAmount());
                    newPlan.setPlanType(plan.getPlanType());
                    newPlan.setPlanDescription(plan.getPlanDescription());
                    newPlan.setPlanCurrency("usd");
                    newPlan.setPlanInterval(plan.getPlanInterval());
                    newPlan.setPlanActiveStatus(true);
                    newPlan.setProduct(productList.stream().filter(product -> product.getProductType().equalsIgnoreCase("Customer-Rep"))
                            .findFirst().get());
                    plan = planRepository.save(newPlan);
                    if (plan != null) {
                        return ResponseDomain.postResponse(messageProperties.getPlanSuccess());
                    } else
                        return ResponseDomain.internalServerError();
                } catch (StripeException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("method :::  addAdmin ::: error ::: " + e.getMessage());
                    return ResponseDomain.internalServerError(e.getLocalizedMessage());
                }
            } else
                return ResponseDomain.badRequest(messageProperties.getInvalidPlanType());
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        }
    }

    @Override
    public ResponseEntity<?> fetchAllPlan(String auth) {
        logger.info("method ::: fetchAllPlan");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Boolean isSponsorEligibleForPromotion = Boolean.FALSE;
            Long referrerCode = user.getReferrerCode();
            Referrers sponsor = referrerRepository.findByReferrerCode(referrerCode);
            if (sponsor != null) {
                isSponsorEligibleForPromotion = sponsor.getUser().getIsEligibleForPromotion();
            }

            List<Plan> planList = planRepository.findAll().stream().map(plan -> Plan.plan(plan)).collect(Collectors.toList());
            if (user.getIsP2PYEA()) {
                planList = planList.stream().filter(p2pYEAPlan -> p2pYEAPlan.getPlanId() == 4).collect(Collectors.toList());
            }
            if (isSponsorEligibleForPromotion && sponsor != null) {
                planList = planList.stream().filter(p2pYEAPlan -> p2pYEAPlan.getPlanId() != 3).collect(Collectors.toList());
            }
            if (!isSponsorEligibleForPromotion && sponsor != null) {
                planList = planList.stream().filter(p2pYEAPlan -> p2pYEAPlan.getPlanId() != 6).collect(Collectors.toList());
            }
            JSONObject jsonObject = new JSONObject();
            List<SubscriptionDetails> activePlan = new ArrayList<>();
            if (!(user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5)) {
                activePlan = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream()
                        .filter(distinctByKey(subscriptionDetails -> subscriptionDetails.getPlan()))
                        .filter(subscriptionDetails -> subscriptionDetails.getStatus().equalsIgnoreCase("Active"))
                        .collect(Collectors.toList());
                if (user.getIsP2PYEA()) {
                    activePlan = activePlan.stream().filter(p2pYEASubscription -> p2pYEASubscription.getPlan().getPlanId() == 4).collect(Collectors.toList());
                } else if (activePlan.size() > 0) {
                    activePlan = activePlan.stream().filter(p2pYEAPlan -> p2pYEAPlan.getPlan().getPlanId() != 4).collect(Collectors.toList());
                    planList = planList.stream().filter(p2pYEAPlan -> p2pYEAPlan.getPlanId() != 4).collect(Collectors.toList());
                }

//                activePlan = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream()
//                        .filter(distinctByKey(subscriptionDetails -> subscriptionDetails.getPlan()))
//                        .filter(subscriptionDetails -> ((subscriptionDetails.getSubscriptionExpiryDate() == null || CommonUtilFunctions.subscriptionCheck(subscriptionDetails.getSubscriptionExpiryDate())) && subscriptionDetails.getStatus().equalsIgnoreCase("Active")))
//                        .collect(Collectors.toList());
                jsonObject.put("activePlan", availablePlanDTO.availablePlans(activePlan));
            }
            if (user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
                return new ResponseEntity<>(planList, HttpStatus.OK);
            } else if (user.getReferenceType().getReferenceCode() == 1) {
                final Set<Long> userPlans = activePlan.stream()
                        .map(subscriptionDetails -> subscriptionDetails.getPlan().getPlanId())
                        .collect(Collectors.toSet());
                jsonObject.put("availablePlans", activePlan.isEmpty() ? (planList.stream().filter(plan -> (plan.getPlanType().equalsIgnoreCase("Customer-Rep") && plan.getPlanActiveStatus()))) :
                        (planList.stream().filter(plan -> plan.getPlanActiveStatus() && !userPlans.contains(plan.getPlanId()))));
                jsonObject.put("optionalPlans", activePlan.isEmpty() ? planList.stream().filter(plan -> (plan.getPlanType().equalsIgnoreCase("Customer")) && plan.getPlanActiveStatus())
                        : new ArrayList<>());
                jsonObject.put("message", null);
                jsonObject.put("test", false);
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else if (user.getReferenceType().getReferenceCode() == 2 || user.getReferenceType().getReferenceCode() == 5) {
                final Set<Long> userPlans = activePlan.stream()
                        .map(subscriptionDetails -> subscriptionDetails.getPlan().getPlanId())
                        .collect(Collectors.toSet());
                jsonObject.put("availablePlans", activePlan.isEmpty() ? (user.getReferenceType().getReferenceCode() == 2 ? planList.stream().filter(plan -> (plan.getPlanType().equalsIgnoreCase("Customer") && plan.getPlanActiveStatus()))
                        : planList.stream().filter(plan -> plan.getPlanActiveStatus())) : (planList.stream().filter(plan -> plan.getPlanActiveStatus() && !userPlans.contains(plan.getPlanId()))));
                if (user.getReferenceType().getReferenceCode() == 2)
                    jsonObject.put("optionalPlans", new ArrayList<>());
                else
                    jsonObject.put("optionalPlans", activePlan.isEmpty() ? planList.stream().filter(plan -> (plan.getPlanType().equalsIgnoreCase("Customer") && plan.getPlanActiveStatus()))
                            : new ArrayList<>());
                jsonObject.put("message", null);
                jsonObject.put("test", false);
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else if (user.getReferenceType().getReferenceCode() == 3 || user.getReferenceType().getReferenceCode() == 4) {
                jsonObject.put("availablePlans", planList.stream().filter(plan -> plan.getPlanActiveStatus()));
                jsonObject.put("optionalPlans", new ArrayList<>());
                jsonObject.put("message", "You have been given  a Test " + (user.getReferenceType().getReferenceCode() == 3 ? "Customer-Rep" : "Customer") + " Account and it will expire on "
                        + CustomDateConverter.localDateToString(CustomDateConverter.dateToLocalDate(user.getCreationDate()).plusDays(7)) + ". Below are the available plans of application");
                jsonObject.put("test", true);
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else if (user.getReferenceType().getReferenceCode() == 6) {
                jsonObject.put("availablePlans", planList.stream().filter(plan -> plan.getPlanActiveStatus()));
                jsonObject.put("optionalPlans", new ArrayList<>());
                jsonObject.put("message", "You have been given  a P2P Distributor Account. Below are the available plans of application");
                jsonObject.put("test", true);
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else {
                jsonObject.put("availablePlans", planList.stream().filter(plan -> plan.getPlanActiveStatus()));
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }

    }

    @Override
    public ResponseEntity<?> updatePlan(String auth, Long planId, String status) {
        logger.info("method ::: updatePlan");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if ((user != null && user.getRole().getRoleCode() == 5)) {
            Optional<Plan> subscriptionPlanEO = planRepository.findById(planId);
            if (subscriptionPlanEO.isPresent()) {
                if (subscriptionPlanEO.get().getPlanType().equalsIgnoreCase("Customer")) {
                    try {
                        com.stripe.model.Plan planStripeEO = com.stripe.model.Plan.retrieve(subscriptionPlanEO.get().getStripePlanId());
                        if (planStripeEO != null) {
                            Map<String, Object> updateParams = new HashMap<String, Object>();
                            if (status.equalsIgnoreCase("ACTIVE")) {
                                if (!(subscriptionPlanEO.get().getPlanActiveStatus())) {
                                    updateParams.put("active", true);
                                    planStripeEO = planStripeEO.update(updateParams);
                                    subscriptionPlanEO.get().setPlanActiveStatus(planStripeEO.getActive());
                                    planRepository.save(subscriptionPlanEO.get());
                                    return ResponseDomain.putResponse(messageProperties.getPlanActivationSuccess());
                                } else
                                    return ResponseDomain.badRequest(messageProperties.getPlanAlreadyActivated());
                            } else if (status.equalsIgnoreCase("INACTIVE")) {
                                if (subscriptionPlanEO.get().getPlanActiveStatus()) {
                                    updateParams.put("active", false);
                                    planStripeEO = planStripeEO.update(updateParams);
                                    subscriptionPlanEO.get().setPlanActiveStatus(planStripeEO.getActive());
                                    planRepository.save(subscriptionPlanEO.get());
                                    return ResponseDomain.putResponse(messageProperties.getPlanInactivationSuccess());
                                } else {
                                    return ResponseDomain.badRequest(messageProperties.getPlanAlreadyInactivated());
                                }
                            } else
                                return ResponseDomain.putResponse(messageProperties.getInvalidInput());
                        } else {
                            return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
                        }
                    } catch (StripeException e) {
                        logger.error(e.getMessage(), e);
                        logger.error("method :::  updatePlan ::: error ::: " + e.getMessage());
                        return ResponseDomain.internalServerError(e.getLocalizedMessage());
                    }
                } else {
                    try {
                        Sku planStripeEO = Sku.retrieve(subscriptionPlanEO.get().getStripePlanId());
                        if (planStripeEO != null) {
                            Map<String, Object> updateParams = new HashMap<String, Object>();
                            if (status.equalsIgnoreCase("ACTIVE")) {
                                if (!(subscriptionPlanEO.get().getPlanActiveStatus())) {
                                    updateParams.put("active", true);
                                    planStripeEO = planStripeEO.update(updateParams);
                                    subscriptionPlanEO.get().setPlanActiveStatus(planStripeEO.getActive());
                                    planRepository.save(subscriptionPlanEO.get());
                                    return ResponseDomain.putResponse(messageProperties.getPlanActivationSuccess());
                                } else
                                    return ResponseDomain.badRequest(messageProperties.getPlanAlreadyActivated());
                            } else if (status.equalsIgnoreCase("INACTIVE")) {
                                if (subscriptionPlanEO.get().getPlanActiveStatus()) {
                                    updateParams.put("active", false);
                                    planStripeEO = planStripeEO.update(updateParams);
                                    subscriptionPlanEO.get().setPlanActiveStatus(planStripeEO.getActive());
                                    planRepository.save(subscriptionPlanEO.get());
                                    return ResponseDomain.putResponse(messageProperties.getPlanInactivationSuccess());
                                } else {
                                    return ResponseDomain.badRequest(messageProperties.getPlanAlreadyInactivated());
                                }
                            } else
                                return ResponseDomain.putResponse(messageProperties.getInvalidInput());
                        } else {
                            return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
                        }
                    } catch (StripeException e) {
                        logger.error(e.getMessage(), e);
                        logger.error("method :::  updatePlan ::: error ::: " + e.getMessage());
                        return ResponseDomain.internalServerError(e.getLocalizedMessage());
                    }
                }
            } else {
                return ResponseDomain.responseNotFound(messageProperties.getPlanNotExists());
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotAuthorized());
        }
    }

    @Override
    public ResponseEntity<?> deletePlan(String auth, Long planId) {
        logger.info("method ::: deletePlan");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if ((user != null && user.getRole().getRoleCode() == 5)) {
            Optional<Plan> subscriptionPlanEO = planRepository.findById(planId);
            if (subscriptionPlanEO.isPresent()) {
                try {
                    com.stripe.model.Plan planStripeEO = com.stripe.model.Plan.retrieve(subscriptionPlanEO.get().getStripePlanId());
                    if (planStripeEO.getObject() != null) {
                        planStripeEO.delete();
                        planRepository.delete(subscriptionPlanEO.get());
                        return ResponseDomain.deleteResponse(messageProperties.getPlanDeleteSuccess());
                    } else
                        return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
                } catch (StripeException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("method :::  deletePlan ::: error ::: " + e.getMessage());
                    return ResponseDomain.internalServerError(e.getLocalizedMessage());
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        }
    }

    public static String amountConverter(float amount) {
        String stringAmount = String.valueOf((int) (amount * 100));
        return stringAmount;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

}
