package com.mfm.api.service.payment.subscription;

import com.mfm.api.domain.commision.AppAmbassadorBonuses;
import com.mfm.api.domain.commision.AppAmbassadorPlusEligibleMembers;
import com.mfm.api.domain.payment.plan.Plan;
import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.payment.transaction.Transaction;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.request.SubscriptionDTO;
import com.mfm.api.dto.user.Card;
import com.mfm.api.repository.commision.AppAmbassadorBonusesRepository;
import com.mfm.api.repository.commision.AppAmbassadorPlusEligibleMembersRepository;
import com.mfm.api.repository.payment.plan.PlanRepository;
import com.mfm.api.repository.payment.subscription.SubscriptionRepository;
import com.mfm.api.repository.payment.transaction.TransactionRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserReferenceTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.user.registration.RegistrationServiceImpl;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.AppProperties;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private static Logger logger = LogManager.getLogger(SubscriptionServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private RegistrationServiceImpl registrationService;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private UserReferenceTypeRepository referenceTypeRepository;
    @Autowired
    private MailNotification mailNotification;
    @Autowired
    private AppAmbassadorPlusEligibleMembersRepository appAmbassadorPlusEligibleMembersRepository;
    @Autowired
    private AppAmbassadorBonusesRepository appAmbassadorBonusesRepository;

    @PostConstruct
    public void init() {
        Stripe.apiKey = appProperties.getStripeSecretKey();
    }


    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "admin.customers", allEntries = true),
                    @CacheEvict(value = "admin.customer-reps", allEntries = true),
                    @CacheEvict(value = "customer-rep.customers", allEntries = true),
                    @CacheEvict(value = "money.flow.user.role", allEntries = true),
                    @CacheEvict(value = "admin.users-details", allEntries = true),
                    @CacheEvict(value = "admin.all-customer", allEntries = true)
            }
    )
    public ResponseEntity<?> createSubscription(String auth,
                                                SubscriptionDTO subscriptionDTO) {
        logger.info("method ::: createSubscription");
        if (subscriptionDTO.getToken() == null || subscriptionDTO.getToken().length() == 0) {
            return ResponseDomain.badRequest(messageProperties.getSelectCard());
        }
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user.getStripeCustomerId() == null) {
            ResponseEntity<?> entity = createNewStripeCustomer(user, subscriptionDTO.getToken());
            if (entity.getStatusCode().is2xxSuccessful())
                user.setStripeCustomerId(entity.getBody().toString());
            else
                return ResponseDomain.badRequest(entity.getBody().toString());
            subscriptionDTO.setToken(null);
            user.setIsP2PYEA(Boolean.FALSE);
            user = userRepository.save(user);
        }
        Optional<Plan> planEO = planRepository.findById(subscriptionDTO.getPlanId());
        SubscriptionDetails subscriptionDetails = new SubscriptionDetails();
        List<SubscriptionDetails> userSubscriptionHistory = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream()
                .filter(subscriptionDetails1 -> (subscriptionDetails1.getStatus().equalsIgnoreCase("Active") && (subscriptionDetails1.getSubscriptionExpiryDate() == null || CommonUtilFunctions.subscriptionCheck(subscriptionDetails1.getSubscriptionExpiryDate()))))
                .collect(Collectors.toList());
        List<Plan> userSubscribedPlans = (userSubscriptionHistory.isEmpty() ? new ArrayList<>() : userSubscriptionHistory.stream()
                .map(subscriptionDetails1 -> Plan.plan(subscriptionDetails1.getPlan())).collect(Collectors.toList()));
        if ((subscriptionDetails == null || !userSubscribedPlans.contains(planEO)) && planEO.get().getPlanType().equalsIgnoreCase("Customer-Rep")) {
            Map<String, Object> orderParams = new HashMap<String, Object>();
            orderParams.put("currency", "usd");
            orderParams.put("email", user.getEmailAddress());
            List<Object> itemsParams = new LinkedList<Object>();
            Map<String, String> item1 = new HashMap<String, String>();
            item1.put("type", "sku");
            item1.put("parent", planEO.get().getStripePlanId());
            itemsParams.add(item1);
            orderParams.put("items", itemsParams);
            try {
                Order order = Order.create(orderParams);
                Map<String, Object> orderPayParams = new HashMap<>();
                if (subscriptionDTO.getToken() != null) {
                    if (subscriptionDTO.getToken().startsWith("tok")) {
                        ResponseEntity<?> entity = addCard(auth, subscriptionDTO.getToken());
                        if (entity.getStatusCode().is2xxSuccessful()) {
                            orderPayParams.put("source", entity.getBody().toString());
                            orderPayParams.put("customer", user.getStripeCustomerId());
                        } else
                            return ResponseDomain.badRequest(entity.getBody().toString());

                    } else {
                        orderPayParams.put("customer", user.getStripeCustomerId());
                        orderPayParams.put("source", subscriptionDTO.getToken());
                    }
                } else
                    orderPayParams.put("customer", user.getStripeCustomerId());
                order.pay(orderPayParams);
                subscriptionDetails.setStripeSubscriptionId(order.getId());
                subscriptionDetails.setSubscriptionDate(LocalDateTime.ofEpochSecond(order.getCreated(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate());
                subscriptionDetails.setUser(user);
                subscriptionDetails.setStatus("Active");
                subscriptionDetails.setPlan(planEO.get());
                subscriptionRepository.save(subscriptionDetails);
                registrationService.userSubscription(auth, planEO.get().getPlanType());
                if (user.getUserStatus().getStatusCode() == 7) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    userRepository.save(user);
                }
                if (subscriptionDTO.getOptionalPlanId() != null) {
                    Optional<Plan> optionalPlan = planRepository.findById(subscriptionDTO.getOptionalPlanId());
                    if (optionalPlan.isPresent()) {
//                        if (optionalPlan.get().getPlanInterval().equals("Yearly")) {
//                            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersOptional = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
//                            if (appAmbassadorPlusEligibleMembersOptional == null) {
//                                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
//                                appAmbassadorPlusEligibleMembers.setUserRegistrationId(user.getUserRegistrationId());
//                                appAmbassadorPlusEligibleMembers.setFirstName(user.getFirstName());
//                                appAmbassadorPlusEligibleMembers.setLastName(user.getLastName());
//                                appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
//                                Long referrerCode = user.getReferrerCode();
//                                Referrers referrersEligibleForBonus = referrerRepository.findByReferrerCode(referrerCode);
//                                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers1 = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
//                                if (appAmbassadorPlusEligibleMembers1 != null) {
//                                    AppAmbassadorBonuses appAmbassadorBonuses = new AppAmbassadorBonuses();
//                                    appAmbassadorBonuses.setFirstName(referrersEligibleForBonus.getUser().getFirstName());
//                                    appAmbassadorBonuses.setLastName(referrersEligibleForBonus.getUser().getLastName());
//                                    appAmbassadorBonuses.setBonusEarnedFrom(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
//                                    appAmbassadorBonuses.setBonuseAmount(50f);
//                                    appAmbassadorBonuses.setDisbursementDate(LocalDate.now());
//                                    appAmbassadorBonuses.setUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
//                                    appAmbassadorBonusesRepository.save(appAmbassadorBonuses);
//                                }
//
//                            }
//                        }
                        Map<String, Object> item = new HashMap<>();
                        item.put("plan", optionalPlan.get().getStripePlanId());
                        Map<String, Object> items = new HashMap<>();
                        items.put("0", item);
                        Map<String, Object> params = new HashMap<>();
                        params.put("customer", user.getStripeCustomerId());
                        params.put("items", items);
                        com.stripe.model.Subscription subscriptionStripeEO = null;
                        SubscriptionDetails subscriptionDetails1 = new SubscriptionDetails();
                        try {
                            subscriptionStripeEO = com.stripe.model.Subscription.create(params);
                            subscriptionDetails1.setStripeSubscriptionId(subscriptionStripeEO.getId());
                            subscriptionDetails1.setUser(user);
                            subscriptionDetails1.setPlan(optionalPlan.get());
                            subscriptionDetails1.setStatus("Active");
                            subscriptionDetails1.setSubscriptionDate(LocalDateTime.ofEpochSecond(subscriptionStripeEO.getCurrentPeriodStart(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate());
                            subscriptionDetails1.setSubscriptionExpiryDate(LocalDateTime.ofEpochSecond(subscriptionStripeEO.getCurrentPeriodEnd(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate());
                            registrationService.userSubscription(auth, optionalPlan.get().getPlanType());
                            subscriptionDetails1 = subscriptionRepository.save(subscriptionDetails1);
                            if (subscriptionDetails1.getStripeSubscriptionId() != null) {
                                if (user.getUserStatus().getStatusCode() == 7) {
                                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                                    userRepository.save(user);
                                }
//                                mailNotification.subscriptionEmails("buy", user, null);
                                return ResponseDomain.postResponse(messageProperties.getSubscriptionSuccess());
                            } else
                                return ResponseDomain.internalServerError();
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                            logger.error("method :::  createSubscription ::: error ::: " + e.getMessage());
                            return ResponseDomain.internalServerError(e.getLocalizedMessage());
                        }
                    } else {
                        return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
                    }
                } else {
                    if (subscriptionDTO.getPlanId() == 4) {
                        registrationService.userSubscription(auth, "Customer");
                        user.setIsP2PYEA(Boolean.TRUE);
                        user = userRepository.save(user);
                    }

//                    List<SubscriptionDetails> activeYearlyPlan = userSubscriptionHistory.stream().filter(subscriptionDetail -> subscriptionDetail.getPlan().getPlanInterval().equals("Yearly")).collect(Collectors.toList());
//                    if (activeYearlyPlan.size() > 0) {
//                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersOptional = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
//                        if (appAmbassadorPlusEligibleMembersOptional == null) {
//                            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
//                            appAmbassadorPlusEligibleMembers.setUserRegistrationId(user.getUserRegistrationId());
//                            appAmbassadorPlusEligibleMembers.setFirstName(user.getFirstName());
//                            appAmbassadorPlusEligibleMembers.setLastName(user.getLastName());
//                            appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
//                        }
//                    }

                }
                mailNotification.subscriptionEmails("buy", user, null);
                return ResponseDomain.postResponse(messageProperties.getSubscriptionSuccess());
            } catch (StripeException e) {
                logger.error(e.getMessage(), e);
                logger.error("method ::: createSubscription ::: error ::: " + e.getMessage());
                return ResponseDomain.badRequest(e.getMessage());

            }
        } else if ((subscriptionDetails == null || !userSubscribedPlans.contains(planEO)) && planEO.get().getPlanType().equalsIgnoreCase("Customer")) {
            if (!userSubscribedPlans.stream().filter(plan -> plan.getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList()).isEmpty()) {
                Optional<SubscriptionDetails> activeSubscription = userSubscriptionHistory.stream().filter(subscriptionDetails1 -> subscriptionDetails1.getPlan().getPlanType().equalsIgnoreCase("Customer"))
                        .findFirst();
                Subscription subscription = null;
                if (activeSubscription.isPresent()) {
                    try {
                        subscription = Subscription.retrieve(activeSubscription.get().getStripeSubscriptionId());
                        subscription = subscription.cancel(null);
                        if (subscription.getStatus().equalsIgnoreCase("canceled")) {
                            activeSubscription.get().setStatus("Cancelled");
                            subscriptionRepository.save(activeSubscription.get());
                        }
                    } catch (StripeException e) {
                        logger.error(e.getMessage(), e);
                        logger.error("method :::  deleteExpense ::: Error ::: " + e.getMessage());
                        return ResponseDomain.internalServerError("Subscription can't be cancelled due to " + e.getMessage());
                    }
                    AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                    if (appAmbassadorPlusEligibleMembers != null) {
                        appAmbassadorPlusEligibleMembersRepository.deleteById(appAmbassadorPlusEligibleMembers.getAppAmbassadorId());
                    }
                }


            }
            List<SubscriptionDetails> customerSubscriptionsList = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream().filter(subscriptionDetails1 -> subscriptionDetails1.getPlan().getPlanType().equalsIgnoreCase("Customer"))
                    .filter(subscriptionDetails1 -> subscriptionDetails1.getStatus().equalsIgnoreCase("In-Active")).collect(Collectors.toList());
            customerSubscriptionsList.forEach(customerSubscription -> {
                try {
                    Subscription inActiveSubscription = Subscription.retrieve(customerSubscription.getStripeSubscriptionId());
                    inActiveSubscription = inActiveSubscription.cancel(null);
                    if (inActiveSubscription.getStatus().equalsIgnoreCase("canceled")) {
                        customerSubscription.setStatus("Cancelled");
                        subscriptionRepository.save(customerSubscription);
                    }
                } catch (StripeException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("method :::  deleteExpense ::: Error ::: " + e.getMessage());
                }

            });
//            if (planEO.get().getPlanInterval().equals("Yearly")) {
//                if ((user.getRole().getRoleCode() == 5 || user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 2)) {
//                    AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersOptional = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
//                    if (appAmbassadorPlusEligibleMembersOptional == null) {
//                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
//                        appAmbassadorPlusEligibleMembers.setUserRegistrationId(user.getUserRegistrationId());
//                        appAmbassadorPlusEligibleMembers.setFirstName(user.getFirstName());
//                        appAmbassadorPlusEligibleMembers.setLastName(user.getLastName());
//                        appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
//                    }
//                }
//                Long referrerCode = user.getReferrerCode();
//                Referrers referrersEligibleForBonus = referrerRepository.findByReferrerCode(referrerCode);
//                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers1 = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
//                if (appAmbassadorPlusEligibleMembers1 != null) {
//                    AppAmbassadorBonuses appAmbassadorBonuses = new AppAmbassadorBonuses();
//                    appAmbassadorBonuses.setFirstName(referrersEligibleForBonus.getUser().getFirstName());
//                    appAmbassadorBonuses.setLastName(referrersEligibleForBonus.getUser().getLastName());
//                    appAmbassadorBonuses.setBonusEarnedFrom(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
//                    appAmbassadorBonuses.setBonuseAmount(50f);
//                    appAmbassadorBonuses.setDisbursementDate(LocalDate.now());
//                    appAmbassadorBonuses.setUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
//                    appAmbassadorBonusesRepository.save(appAmbassadorBonuses);
//                }
//            }
            Map<String, Object> item = new HashMap<>();
            item.put("plan", planEO.get().getStripePlanId());
            Map<String, Object> items = new HashMap<>();
            items.put("0", item);
            Map<String, Object> params = new HashMap<>();
            params.put("customer", user.getStripeCustomerId());
            params.put("items", items);
            if (subscriptionDTO.getToken() != null) {
                if (subscriptionDTO.getToken().startsWith("tok")) {
                    ResponseEntity<?> entity = addCard(auth, subscriptionDTO.getToken());
                    if (entity.getStatusCode().is2xxSuccessful()) {
                        params.put("default_source", entity.getBody().toString());
                    } else
                        return ResponseDomain.badRequest(entity.getBody().toString());

                } else
                    params.put("default_source", subscriptionDTO.getToken());
            }
            com.stripe.model.Subscription subscriptionStripeEO = null;
            try {
                subscriptionStripeEO = com.stripe.model.Subscription.create(params);
                subscriptionDetails.setStripeSubscriptionId(subscriptionStripeEO.getId());
                subscriptionDetails.setUser(user);
                subscriptionDetails.setPlan(planEO.get());
                subscriptionDetails.setStatus("Active");
                subscriptionDetails.setSubscriptionDate(LocalDateTime.ofEpochSecond(subscriptionStripeEO.getCurrentPeriodStart(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate());
                subscriptionDetails.setSubscriptionExpiryDate(LocalDateTime.ofEpochSecond(subscriptionStripeEO.getCurrentPeriodEnd(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate());
                registrationService.userSubscription(auth, planEO.get().getPlanType());
                subscriptionDetails = subscriptionRepository.save(subscriptionDetails);
                if (subscriptionDetails.getStripeSubscriptionId() != null) {
                    if (user.getUserStatus().getStatusCode() == 7) {
                        user.setUserStatus(userStatusRepository.findByStatusCode(1));
                        userRepository.save(user);
                    }
                    mailNotification.subscriptionEmails("buy", user, null);
                    return ResponseDomain.postResponse(messageProperties.getSubscriptionSuccess());
                } else
                    return ResponseDomain.internalServerError();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                logger.error("method :::  createSubscription ::: Error ::: " + e.getMessage());
                return ResponseDomain.badRequest(e.getMessage());
            }
        } else
            return ResponseDomain.badRequest(messageProperties.getPlanNotExists());
    }

    public ResponseEntity<?> createNewStripeCustomer(User user, String stripeTokenId) {
        logger.info("method ::: createNewStripeCustomer");
        String customerId = null;
        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put("email", user.getEmailAddress());
        customerParams.put("description", user.getEmailAddress());
        customerParams.put("source", stripeTokenId);
        try {
            Customer customerStripeEO = Customer.create(customerParams);
            customerId = customerStripeEO.getId();
            return new ResponseEntity<>(customerId, HttpStatus.OK);
        } catch (StripeException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: createNewStripeCustomer ::: error" + e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<?> getAllTransactions(String auth) {
        logger.info("method ::: getAllTransactions");
        User userObj = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        Referrers referrer = referrerRepository.findByUser(userObj);
        List<User> customerList = null;
        List transactionsListObj = new ArrayList<>();
        if (referrer != null) {
            customerList = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
        }

        if (userObj.getRole().getRoleCode() == 5) {
            List<Transaction> transactionsList;
            transactionsList = transactionRepository.findAll();
            if (transactionsList != null) {
                transactionsList.forEach(transaction -> {
                    JSONObject transactionVo = new JSONObject();
                    transactionVo.put("transactionId", transaction.getStripeTransactionId());
                    transactionVo.put("transactionType", transaction.getType());
                    transactionVo.put("referrerCode", transaction.getUser().getReferrerCode());
                    Referrers referrerObj = referrerRepository.findByReferrerCode(transaction.getUser().getReferrerCode());
                    transactionVo.put("referredBy", referrerObj.getUser().getFirstName() + (referrerObj.getUser().getLastName() != null ? (" " + referrerObj.getUser().getLastName()) : ""));
                    transactionVo.put("planType", transaction.getPlanType());
                    transactionVo.put("transactionAmount", transaction.getTransactionAmount());
                    transactionVo.put("transactionDate", CustomDateConverter.localDateTimeToString(transaction.getTransactionDate()));
                    transactionVo.put("name", WordUtils.capitalize(transaction.getUser().getFirstName() + " " + (transaction.getUser()
                            .getLastName() == null ? "" : transaction.getUser().getLastName())));
                    transactionsListObj.add(transactionVo);
                });
            }
            return new ResponseEntity<>(transactionsListObj, HttpStatus.OK);
        }

        if (customerList != null) {
            customerList.forEach(customer -> {
                if (transactionRepository.existsByUser(customer)) {
                    List<Transaction> transactionsList = transactionRepository.findAllByUser(customer);
                    transactionsList.forEach(transaction -> {
                        JSONObject transactionVo = new JSONObject();
                        transactionVo.put("transactionId", transaction.getStripeTransactionId());
                        transactionVo.put("transactionType", transaction.getType());
                        transactionVo.put("referrerCode", transaction.getUser().getReferrerCode());
                        Referrers referrerObj = referrerRepository.findByReferrerCode(transaction.getUser().getReferrerCode());
                        transactionVo.put("referredBy", referrerObj.getUser().getFirstName() + (referrerObj.getUser().getLastName() != null ? (" " + referrerObj.getUser().getLastName()) : ""));
                        transactionVo.put("planType", transaction.getPlanType());
                        transactionVo.put("transactionAmount", transaction.getTransactionAmount());
                        transactionVo.put("transactionDate", CustomDateConverter.localDateTimeToString(transaction.getTransactionDate()));
                        transactionVo.put("name", WordUtils.capitalize(transaction.getUser().getFirstName() + " " + (transaction.getUser()
                                .getLastName() == null ? "" : transaction.getUser().getLastName())));
                        transactionsListObj.add(transactionVo);
                    });

                }

            });
            List<Transaction> transactionsList;
            transactionsList = transactionRepository.findAllByUser(userObj);
            if (transactionsList != null) {
                transactionsList.forEach(transaction -> {
                    JSONObject transactionVo = new JSONObject();
                    transactionVo.put("transactionId", transaction.getStripeTransactionId());
                    transactionVo.put("transactionType", transaction.getType());
                    transactionVo.put("referrerCode", transaction.getUser().getReferrerCode());
                    Referrers referrerObj = referrerRepository.findByReferrerCode(transaction.getUser().getReferrerCode());
                    transactionVo.put("referredBy", referrerObj.getUser().getFirstName() + (referrerObj.getUser().getLastName() != null ? (" " + referrerObj.getUser().getLastName()) : ""));
                    transactionVo.put("planType", transaction.getPlanType());
                    transactionVo.put("transactionAmount", transaction.getTransactionAmount());
                    transactionVo.put("transactionDate", CustomDateConverter.localDateTimeToString(transaction.getTransactionDate()));
                    transactionVo.put("name", WordUtils.capitalize(transaction.getUser().getFirstName() + " " + (transaction.getUser()
                            .getLastName() == null ? "" : transaction.getUser().getLastName())));
                    transactionsListObj.add(transactionVo);
                });
            }
        } else {
            if (transactionRepository.existsByUser(userObj)) {
                List<Transaction> transactionsList = transactionRepository.findAllByUser(userObj);
                transactionsList.forEach(transaction -> {
                    JSONObject transactionVo = new JSONObject();
                    transactionVo.put("transactionId", transaction.getStripeTransactionId());
                    transactionVo.put("transactionType", transaction.getType());
                    transactionVo.put("referrerCode", transaction.getUser().getReferrerCode());
                    Referrers referrerObj = referrerRepository.findByReferrerCode(transaction.getUser().getReferrerCode());
                    transactionVo.put("referredBy", referrerObj.getUser().getFirstName() + (referrerObj.getUser().getLastName() != null ? (" " + referrerObj.getUser().getLastName()) : ""));
                    transactionVo.put("planType", transaction.getPlanType());
                    transactionVo.put("transactionAmount", transaction.getTransactionAmount());
                    transactionVo.put("transactionDate", CustomDateConverter.localDateTimeToString(transaction.getTransactionDate()));
                    transactionVo.put("name", WordUtils.capitalize(transaction.getUser().getFirstName() + " " + (transaction.getUser()
                            .getLastName() == null ? "" : transaction.getUser().getLastName())));
                    transactionsListObj.add(transactionVo);
                });

            }
        }
        return new ResponseEntity<>(transactionsListObj, HttpStatus.OK);
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.users-details", allEntries = true),
    })
    public ResponseEntity<?> logOrderTransaction(Event event) {
        logger.info("method ::: logOrderTransaction");
        logger.info(event);
        StripeObject stripeObj = event.getData().getObject();
        try {
            String amountPaid = ((JSONObject) (new JSONParser().parse(stripeObj.toJson()))).get("amount")
                    .toString();
            String txId = "";
            try {
                txId = ((JSONObject) (new JSONParser().parse(stripeObj.toJson()))).get("charge").toString();
            } catch (Exception e) {
                txId = "Promotional offer";
            }

            String userEmailId = ((JSONObject) (new JSONParser().parse(stripeObj.toJson()))).get("email")
                    .toString();
            User user = userRepository.findByEmailAddressIgnoreCase(userEmailId);
            if (user != null) {
                Transaction transaction = new Transaction();
                transaction.setStripeTransactionId(txId);
                LocalDateTime date = LocalDateTime.ofEpochSecond(event.getCreated(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                transaction.setTransactionDate(date);
                transaction.setSubscriptionId(((JSONObject) (new JSONParser().parse(stripeObj.toJson()))).get("id").toString());
                transaction.setType(event.getType());
                Optional<Plan> p2PYEAPlan = planRepository.findById(4l);
                transaction.setPlanType("Customer Rep");
                try {
                    if (p2PYEAPlan.isPresent()) {
                        String stripePlanId = ((JSONObject) ((JSONArray) (((JSONObject) (new JSONParser().parse(stripeObj.toJson()))).get("items"))).get(0)).get("parent").toString();
                        if (p2PYEAPlan.get().getStripePlanId().equals(stripePlanId)) {
                            transaction.setPlanType("P2P YEA");
                            user.setIsP2PYEA(Boolean.TRUE);
                            userRepository.save(user);
                            CommonUtilFunctions.subscribeUserP2PAcademy(user);
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                transaction.setUser(user);
                transaction.setTransactionAmount(Double.parseDouble(amountPaid) / 100);
                transactionRepository.saveAndFlush(transaction);
                if (transaction.getType().equals("order.payment_succeeded")) {
                    List<SubscriptionDetails> userSubscriptionHistory = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream()
                            .filter(subscriptionDetails1 -> (subscriptionDetails1.getStatus().equalsIgnoreCase("Active") && (subscriptionDetails1.getSubscriptionExpiryDate() == null || CommonUtilFunctions.subscriptionCheck(subscriptionDetails1.getSubscriptionExpiryDate()))))
                            .collect(Collectors.toList());
                    List<SubscriptionDetails> activeYearlyPlan = userSubscriptionHistory.stream().filter(subscriptionDetail -> subscriptionDetail.getPlan().getPlanInterval().equals("Yearly")).collect(Collectors.toList());
                    if (activeYearlyPlan.size() > 0) {
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersOptional = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                        if (appAmbassadorPlusEligibleMembersOptional == null) {
                            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
                            appAmbassadorPlusEligibleMembers.setUserRegistrationId(user.getUserRegistrationId());
                            appAmbassadorPlusEligibleMembers.setFirstName(user.getFirstName());
                            appAmbassadorPlusEligibleMembers.setLastName(user.getLastName());
                            appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
                            CommonUtilFunctions.subscribeUserP2PAcademy(user);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ResponseDomain.internalServerError();
        }
        return ResponseDomain.successResponse(messageProperties.getTransactionLoggedSuccess());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.users-details", allEntries = true)
    })
    public ResponseEntity<?> logInvoiceTransaction(Event event) {
        logger.info("method ::: logInvoiceTransaction");
        logger.info(event);
        StripeObject stripeObj = event.getData().getObject();
        try {
            String txId = null;
            String stripeCustomerId = null;
            String amountPaid = null;
            String subscriptionId = null;
            User user = null;
            JSONObject jsonObject = ((JSONObject) (new JSONParser().parse(stripeObj.toJson())));
            //for subscription renewal failure
            if (event.getType().equals("invoice.payment_failed")) {
                txId = jsonObject.get("charge").toString();
                stripeCustomerId = jsonObject.get("customer")
                        .toString();
                amountPaid = jsonObject.get("amount_paid")
                        .toString();
                subscriptionId = jsonObject.get("subscription")
                        .toString();
                user = userRepository.findByStripeCustomerId(stripeCustomerId);
                SubscriptionDetails subscriptionDetails = subscriptionRepository.findByStripeSubscriptionId(subscriptionId);
                if (subscriptionDetails != null && subscriptionDetails.getStatus().equalsIgnoreCase("active")) {
                    subscriptionDetails.setStatus("In-Active");
                    subscriptionRepository.save(subscriptionDetails);
                }
                user.setUserStatus(userStatusRepository.findByStatusCode(7));
                userRepository.save(user);
                String last4 = "";
                try {
                    ExternalAccount charge = Charge.retrieve(jsonObject.get("charge").toString()).getSource();
                    JSONObject chargeJsonObject = ((JSONObject) (new JSONParser().parse(charge.toJson())));
                    last4 = chargeJsonObject.get("last4").toString();
                } catch (StripeException e) {
                    e.printStackTrace();
                }
                mailNotification.subscriptionEmails("Payment Failed", user, last4);
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    appAmbassadorPlusEligibleMembersRepository.deleteById(appAmbassadorPlusEligibleMembers.getAppAmbassadorId());
                    CommonUtilFunctions.unsubscribeUserP2PAcademy(user.getEmailAddress(), "INACTIVE");
                }
            }
            //for successful subscription
            else if (event.getType().equals("invoice.payment_succeeded")) {
                txId = jsonObject.get("charge").toString();
                stripeCustomerId = jsonObject.get("customer")
                        .toString();
                amountPaid = jsonObject.get("amount_paid")
                        .toString();
                subscriptionId = jsonObject.get("subscription")
                        .toString();
                user = userRepository.findByStripeCustomerId(stripeCustomerId);
            }
            if (user != null) {
                Transaction transaction = new Transaction();
                transaction.setStripeTransactionId(txId);
                LocalDateTime date = LocalDateTime.ofEpochSecond(event.getCreated(), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                transaction.setTransactionDate(date);
                transaction.setSubscriptionId(subscriptionId);
                transaction.setType(event.getType());
                transaction.setUser(user);
                transaction.setPlanType("Customer");
                transaction.setTransactionAmount(Double.parseDouble(amountPaid) / 100);
                transactionRepository.saveAndFlush(transaction);
                if (event.getType().equals("invoice.payment_succeeded")) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    userRepository.save(user);
                    JSONObject lines = (JSONObject) new JSONParser().parse(jsonObject.get("lines").toString());
                    JSONArray data = (JSONArray) new JSONParser().parse(lines.get("data").toString());
                    JSONObject zeroIndex = (JSONObject) new JSONParser().parse(data.get(0).toString());
                    JSONObject period = (JSONObject) new JSONParser().parse(zeroIndex.get("period").toString());
                    String subscription_id = jsonObject.get("subscription").toString();
                    SubscriptionDetails renewedSubscriptionDetails = subscriptionRepository.findByStripeSubscriptionId(subscription_id);
                    if (renewedSubscriptionDetails != null) {
                        LocalDate subscriptionExpiryDate = LocalDateTime.ofEpochSecond(Long.valueOf(period.get("end").toString()), 0, OffsetDateTime.now(ZoneId.systemDefault()).getOffset()).atZone(ZoneId.systemDefault()).toLocalDate();
                        renewedSubscriptionDetails.setSubscriptionExpiryDate(subscriptionExpiryDate);
                        renewedSubscriptionDetails.setStatus("Active");
                        subscriptionRepository.save(renewedSubscriptionDetails);
                    }
                    String billing_reason = jsonObject.get("billing_reason").toString();
                    String interval = ((JSONObject) ((JSONObject) ((JSONArray) ((JSONObject) jsonObject.get("lines")).get("data")).get(0)).get("plan")).get("interval").toString();
                    Long referrerCode = user.getReferrerCode();
                    Referrers referrersEligibleForBonus = referrerRepository.findByReferrerCode(referrerCode);
                    AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers1 = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
                    if (interval.equals("year")) {
//                        billing_reason.equals("subscription_cycle")
//                        Long referrerCode = user.getReferrerCode();
//                        Referrers referrersEligibleForBonus = referrerRepository.findByReferrerCode(referrerCode);
                        //
                        if ((user.getRole().getRoleCode() == 5 || user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 2)) {
                            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersOptional = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                            if (appAmbassadorPlusEligibleMembersOptional == null) {
                                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
                                appAmbassadorPlusEligibleMembers.setUserRegistrationId(user.getUserRegistrationId());
                                appAmbassadorPlusEligibleMembers.setFirstName(user.getFirstName());
                                appAmbassadorPlusEligibleMembers.setLastName(user.getLastName());
                                appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
                                CommonUtilFunctions.subscribeUserP2PAcademy(user);
                            }
                        }
                        if (appAmbassadorPlusEligibleMembers1 != null) {
                            AppAmbassadorBonuses appAmbassadorBonuses = new AppAmbassadorBonuses();
                            appAmbassadorBonuses.setFirstName(referrersEligibleForBonus.getUser().getFirstName());
                            appAmbassadorBonuses.setLastName(referrersEligibleForBonus.getUser().getLastName());
                            appAmbassadorBonuses.setBonusEarnedFrom(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
                            if (referrersEligibleForBonus.getUser().getIsEligibleForPromotion()) {
                                appAmbassadorBonuses.setBonuseAmount(20f);
                            } else {
                                appAmbassadorBonuses.setBonuseAmount(50f);
                            }
                            appAmbassadorBonuses.setDisbursementDate(LocalDate.now());
                            appAmbassadorBonuses.setUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
                            appAmbassadorBonusesRepository.save(appAmbassadorBonuses);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ResponseDomain.internalServerError(e.getLocalizedMessage());
        }
        return ResponseDomain.successResponse(messageProperties.getTransactionLoggedSuccess());
    }


    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "admin.customers", allEntries = true),
                    @CacheEvict(value = "admin.customer-reps", allEntries = true),
                    @CacheEvict(value = "customer-rep.customers", allEntries = true),
                    @CacheEvict(value = "money.flow.user.role", allEntries = true),
                    @CacheEvict(value = "admin.users-details", allEntries = true),
                    @CacheEvict(value = "admin.all-customer", allEntries = true)
            }
    )
    public ResponseEntity<?> cancelSubscription(String auth, String subscriptionId) {
        logger.info("method ::: cancelSubscription");
        Subscription sub = null;
        try {
            sub = Subscription.retrieve(subscriptionId);
            sub = sub.cancel(null);
            if (sub.getStatus().equalsIgnoreCase("canceled")) {
                String userId = tokenProvider.getUserId(auth);
                SubscriptionDetails subscriptionDetails = subscriptionRepository.findByStripeSubscriptionId(subscriptionId);
                subscriptionDetails.setStatus("Cancelled");
                subscriptionRepository.save(subscriptionDetails);
                User user = userRepository.findByUserRegistrationId(Long.parseLong(userId));
                user.setReferenceType(referenceTypeRepository.findByReferenceCode(5));
                user.setUserStatus(userStatusRepository.findByStatusCode(7));
                userRepository.save(user);
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    appAmbassadorPlusEligibleMembersRepository.deleteById(appAmbassadorPlusEligibleMembers.getAppAmbassadorId());
                    CommonUtilFunctions.unsubscribeUserP2PAcademy(user.getEmailAddress(), "INACTIVE");
                }
                mailNotification.subscriptionEmails("cancel", user, null);
                return ResponseDomain.putResponse(messageProperties.getTransactionLoggedSuccess());
            } else
                return ResponseDomain.internalServerError();
        } catch (StripeException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            logger.error("method :::  cancelSubscription ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError("Subscription can't be cancelled due to " + e.getLocalizedMessage());
        }
    }


    @Override
    public ResponseEntity<?> userCards(String auth) {
        logger.info("method ::: userCards");
        Customer customer = null;
        String apiUrl = "https://api.stripe.com/v1/customers/";
        String stripeId = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth))).getStripeCustomerId();
        if (stripeId == null)
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        apiUrl = apiUrl + stripeId + "/sources";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + Stripe.apiKey);
        headers.add("Accept", "application/x-www-form-urlencoded");
        HttpEntity<?> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
            JSONObject json = (JSONObject) new JSONParser().parse(response.getBody());
            JSONArray jsonArray = (JSONArray) json.get("data");
            List<Card> cards = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                json.clear();
                json = (JSONObject) jsonArray.get(i);
                Card card = new Card();
                card.setCreditCard(json.get("brand").toString() + " " + WordUtils.capitalize(json.get("object").toString()) + " ending in " + json.get("last4").toString());
                card.setName(WordUtils.capitalize((json.get("name") == null) ? "" : json.get("name").toString()));
                card.setCardId(json.get("id").toString());
                card.setExpiryDate((json.get("exp_month").toString().length() > 1 ? json.get("exp_month").toString() : "0" + json.get("exp_month").toString()) + "/" + json.get("exp_year").toString());
                cards.add(card);
                json.clear();
            }
            return new ResponseEntity<>(cards, HttpStatus.OK);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
            return ResponseDomain.badRequest(e.getLocalizedMessage());
        }
    }

    @Override
    public ResponseEntity<?> addCard(String auth, String token) {
        logger.info("method ::: addCard");
        Customer customer = null;
        try {
            Token retrieve = Token.retrieve(token);
            String stripeCustomerId = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth))).getStripeCustomerId();
            String cardCheck = retrieve.getCard().getFingerprint();
            customer = Customer.retrieve(stripeCustomerId);
            String apiUrl = "https://api.stripe.com/v1/customers/";
            apiUrl = apiUrl + stripeCustomerId + "/sources";
            RestTemplate restTemplate = new RestTemplate();
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("Authorization", "Bearer " + Stripe.apiKey);
            headers.add("Accept", "application/x-www-form-urlencoded");
            HttpEntity<?> entity = new HttpEntity<>(headers);
            try {
                ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
                JSONObject json = (JSONObject) new JSONParser().parse(response.getBody());
                JSONArray jsonArray = (JSONArray) json.get("data");
                for (int i = 0; i < jsonArray.size(); i++) {
                    json.clear();
                    json = (JSONObject) jsonArray.get(i);
                    String s = json.get("fingerprint").toString();
                    if (s.equals(cardCheck))
                        return new ResponseEntity<>(messageProperties.getCardExists(), HttpStatus.BAD_REQUEST);
                }
            } catch (ParseException e) {
                logger.error(e.getMessage(), e);
                return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
            }
            Map<String, Object> params = new HashMap<>();
            params.put("source", token);
            ExternalAccount account = customer.getSources().create(params);
            params.clear();
            params.put("default_source", account.getId());
            customer.update(params);
            return new ResponseEntity<>(account.getId(), HttpStatus.OK);
        } catch (StripeException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  addCard ::: Error ::: " + e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<?> deleteCard(String auth, String cardId) {
        logger.info("method ::: deleteCard");
        Customer customer = null;
        try {
            String stripeCustomerId = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth))).getStripeCustomerId();
            customer = Customer.retrieve(stripeCustomerId);
            customer.getSources().retrieve(cardId).delete();
            return ResponseDomain.successResponse("Card Deleted Successfully");
        } catch (StripeException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  deleteCard ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest(e.getLocalizedMessage());
        }
    }
}
