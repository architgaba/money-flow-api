package com.mfm.api.service.leaderswall;

import com.mfm.api.domain.leaderswall.LeadersWall;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface LeadersWallService {

    ResponseEntity<?> getLeadersWallList();

//    ResponseEntity<?> updateLeadersWall(LeadersWall leadersWall, String leadersWallId);

    ResponseEntity<?> deleteLeadersWall(String leadersWallId);

    ResponseEntity<?> addLeadersWall(LeadersWall leadersWall, String leadersWallCategoriesId, MultipartFile file);
}








