package com.mfm.api.service.leaderswall;

import com.mfm.api.domain.leaderswall.LeadersWall;
import com.mfm.api.domain.leaderswall.LeadersWallCategories;
import com.mfm.api.repository.leaderswall.LeadersWallCategoriesRepository;
import com.mfm.api.repository.leaderswall.LeadersWallRepository;
import com.mfm.api.service.storage.AmazonS3ClientService;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LeadersWallServiceImpl implements LeadersWallService {

    private static Logger logger = LogManager.getLogger(LeadersWallServiceImpl.class);

    @Autowired
    private LeadersWallRepository leadersWallRepository;
    @Autowired
    private LeadersWallCategoriesRepository leadersWallCategoriesRepository;
    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    @Override
    public ResponseEntity<?> getLeadersWallList() {
        logger.info("method ::: getLeadersWallList");
        List<LeadersWall> leadersWallList = leadersWallRepository.findAll(Sort.by("leadersWallId").ascending());
        Map<String, List<LeadersWall>> leadersWallMap = new HashMap<>();
        List<LeadersWallCategories> leadersWallCategoriesList = leadersWallCategoriesRepository.findAll();
        leadersWallCategoriesList.forEach(leadersWallCategory -> {
            List<LeadersWall> selectedLeadersWallList = leadersWallList.stream().filter(leadersWall -> leadersWall.getLeadersWallCategories().getCategoryName().equals(leadersWallCategory.getCategoryName())).collect(Collectors.toList());
            selectedLeadersWallList.stream().forEach(selectedLeadersWall -> selectedLeadersWall.setCategoryName(leadersWallCategory.getCategoryName()));
            leadersWallMap.put(leadersWallCategory.getCategoryName(), selectedLeadersWallList);

        });
        return new ResponseEntity<>(leadersWallMap, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addLeadersWall(LeadersWall leadersWall, String leadersWallCategoriesId, MultipartFile file) {
        logger.info("method ::: addLeadersWall");
        Optional<LeadersWallCategories> leadersWallCategoriesOptional = leadersWallCategoriesRepository.findById(Long.parseLong(leadersWallCategoriesId));
        if (leadersWallCategoriesOptional.isPresent()) {
            if (file != null) {
                try {
                    boolean fileDeleteStatus;
                    ResponseEntity entity = null;
                    fileDeleteStatus = leadersWall.getImageUrl() != null ? amazonS3ClientService.deleteReceipt(leadersWall.getImageUrl()) : false;
                    entity = amazonS3ClientService.uploadLeadersWall(file);
                    leadersWall.setImageUrl(entity.getBody().toString());
                    leadersWall.setLeadersWallCategories(leadersWallCategoriesOptional.get());
                    leadersWallRepository.save(leadersWall);
                    return ResponseDomain.postResponse("Team Leader added successfully");
                } catch (Exception e) {
                    logger.error("method :::  addLeadersWall ::: Error ::: " + e.getMessage());
                    return ResponseDomain.internalServerError();
                }

            }
            return ResponseDomain.badRequest("There’s some problem with the image you are trying to upload");
        }
        return ResponseDomain.badRequest("Please provide a correct category for Leaders Team");
    }

//    @Override
//    public ResponseEntity<?> updateLeadersWall(LeadersWall leadersWall, String leadersWallId) {
//        Optional<LeadersWallCategories> leadersWallCategoriesOptional = leadersWallCategoriesRepository.findById(Long.parseLong(leadersWallId));
//        if (leadersWallCategoriesOptional.isPresent()) {
//            leadersWall.setLeadersWallCategories(leadersWallCategoriesOptional.get());
//            leadersWallRepository.save(leadersWall);
//            return ResponseDomain.postResponse("Leaders Wall added Successfully.");
//        }
//        return ResponseDomain.badRequest("Invalid Leaders Wall Category.");
//    }

    @Override
    public ResponseEntity<?> deleteLeadersWall(String leadersWallId) {
        Optional<LeadersWall> leadersWallOptional = leadersWallRepository.findById(Long.parseLong(leadersWallId));
        if (leadersWallOptional.isPresent()) {
            boolean fileDeleteStatus;
            fileDeleteStatus = leadersWallOptional.get().getImageUrl() != null ? amazonS3ClientService.deleteReceipt(leadersWallOptional.get().getImageUrl()) : false;
            leadersWallRepository.delete(leadersWallOptional.get());
            return ResponseDomain.postResponse("Team Leader deleted successfully");
        }
        return ResponseDomain.badRequest("Team Leader you are trying to delete doesn’t exist");
    }
}
