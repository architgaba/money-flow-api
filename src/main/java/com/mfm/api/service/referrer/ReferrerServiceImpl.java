package com.mfm.api.service.referrer;

import com.mfm.api.domain.commision.AppAmbassadorPlusEligibleMembers;
import com.mfm.api.domain.customer.Customers;
import com.mfm.api.domain.leaderswall.LeadersWallCategories;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.TeamMemberVo;
import com.mfm.api.dto.user.UpdateEmail;
import com.mfm.api.repository.commision.AppAmbassadorPlusEligibleMembersRepository;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.leaderswall.LeadersWallCategoriesRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserReferenceTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserRoleRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.admin.AdminServiceImpl;
import com.mfm.api.service.admin.response.CustomerVO;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.mail.EmailContent;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReferrerServiceImpl implements ReferrerService {

    private static Logger logger = LogManager.getLogger(ReferrerServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private CustomersRepository customersRepository;

    @Autowired
    private ReferrerRepository referrerRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserStatusRepository userStatusRepository;

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    private MailNotification mailNotification;

    @Autowired
    private EmailContent emailContent;

    @Autowired
    private UserReferenceTypeRepository userReferenceTypeRepository;

    @Autowired
    private AdminServiceImpl adminService;
    @Autowired
    private AppAmbassadorPlusEligibleMembersRepository appAmbassadorPlusEligibleMembersRepository;
    @Autowired
    private LeadersWallCategoriesRepository leadersWallCategoriesRepository;


    @Override
    @Cacheable(value = "customer-rep.customers", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getMyCustomers(String auth) {
        logger.info("method ::: getMyCustomers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrers = referrerRepository.findByUser(user);
            if (referrers != null) {
                List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
                List<CustomerVO> customerVOList = new ArrayList<>();
                List<Customers> customersList = customersRepository.findAllByReferrers(referrers);
                if (!(customersList.isEmpty())) {
                    customersList.forEach(customersObj -> {
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setSponsorReferrerCode(referrers.getReferrerCode());
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(customersObj.getUser().getUserRegistrationId());
                        if (appAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(adminService.countTeamSize(appAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(adminService.countDirectAAP(appAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(customersObj.getUser());
                            if (referrerObj != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        Referrers referrerObj = referrerRepository.findByUser(customersObj.getUser());
                        if (referrerObj != null) {
                            List<User> userObjList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                            List<User> userObjListCopy = userObjList;
                            userObjList = userObjList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            customerVO.setAppAmbassadorCount(userObjList.size());
                            long p2pYEACount = userObjListCopy.stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        } else {
                            customerVO.setAppAmbassadorCount(0);
                            customerVO.setP2PYEACount(String.valueOf(0));
                        }


                        Referrers isReferrer = referrerRepository.findByUser(customersObj.getUser());
                        if (isReferrer != null) {
                            customerVO.setReferrerCode(isReferrer.getReferrerCode().toString());
                        }
                        Referrers customersReferrer = customersObj.getReferrers();
                        if (customersReferrer != null) {
                            customerVO.setReferrerName(customersReferrer.getUser().getFirstName() + (customersReferrer.getUser().getLastName()
                                    != null ? " " + customersReferrer.getUser().getLastName() : ""));
                        }
                        customerVO.setP2PYEA(customersObj.getUser().getIsP2PYEA());
                        customerVO.setUserId(customersObj.getUser().getUserRegistrationId());
                        customerVO.setName(customersObj.getUser().getFirstName() + (customersObj.getUser().getLastName()
                                != null ? " " + customersObj.getUser().getLastName() : ""));
                        customerVO.setEmail(customersObj.getUser().getEmailAddress());
                        customerVO.setContactNumber(customersObj.getUser().getContactNumber() != null
                                ? customersObj.getUser().getContactNumber() : "");
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(customersObj.getUser().getCreationDate()));
                        customerVO.setStatus(customersObj.getUser().getUserStatus().getStatusDescription());
                        //customersObj.getUser().getReferenceType().getReferenceCode()
                        Integer roleCode = customersObj.getUser().getRole().getRoleCode();
                        customerVO.setUserType(roleCode == 3 ? "Customer" : "Customer-Rep");
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        if (customerVO.getUserType().equals("Customer-Rep")) {

                            List<User> userList = userRepository.findAllByReferrerCode(referrerRepository.findByUser(customersObj.getUser()).getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());

                            customerCount = String.valueOf(userList.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                            customerRepCount = String.valueOf(userList.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                            p2pDistributorCount = String.valueOf(userList.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                        }
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVO.setAccountType("PAID");
                        customerVOList.add(customerVO);
                    });
                }
                List<User> userList = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream()
                        .filter(user1 -> (user1.getRole().getRoleCode() == 1) || (user1.getReferenceType().getReferenceCode() == 6))
                        .collect(Collectors.toList());
                if (!userList.isEmpty()) {
                    userList.forEach(userObj -> {
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setSponsorReferrerCode(referrers.getReferrerCode());
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                        if (appAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(adminService.countTeamSize(appAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(adminService.countDirectAAP(appAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(userObj);
                            if (referrerObj != null) {
                                List<User> selectedUserList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                selectedUserList = selectedUserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(selectedUserList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        Referrers referrerObj = referrerRepository.findByUser(userObj);
                        if (referrerObj != null) {
                            List<User> userObjList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                            List<User> userObjListCopy = userObjList;
                            userObjList = userObjList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            customerVO.setAppAmbassadorCount(userObjList.size());
                            long p2pYEACount = userObjListCopy.stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        } else {
                            customerVO.setAppAmbassadorCount(0);
                            customerVO.setP2PYEACount(String.valueOf(0));
                        }
                        Referrers isReferrer = referrerRepository.findByUser(userObj);
                        if (isReferrer != null) {
                            customerVO.setReferrerCode(isReferrer.getReferrerCode().toString());
                        }
                        Long referrerCode = userObj.getReferrerCode();
                        if (referrerCode != null) {
                            Referrers userReferrer = referrerRepository.findByReferrerCode(referrerCode);
                            if (userReferrer != null) {
                                customerVO.setReferrerName(userReferrer.getUser().getFirstName() + (userReferrer.getUser().getLastName()
                                        != null ? " " + userReferrer.getUser().getLastName() : ""));
                            }
                        }
                        customerVO.setP2PYEA(userObj.getIsP2PYEA());
                        customerVO.setUserId(userObj.getUserRegistrationId());
                        customerVO.setName(userObj.getFirstName() + (userObj.getLastName()
                                != null ? " " + userObj.getLastName() : ""));
                        customerVO.setEmail(userObj.getEmailAddress());
                        customerVO.setContactNumber(userObj.getContactNumber() != null
                                ? userObj.getContactNumber() : "");
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(userObj.getCreationDate()));
                        customerVO.setStatus(userObj.getUserStatus().getStatusDescription());
                        String userType = "";
                        Integer referenceCode = userObj.getReferenceType().getReferenceCode();
                        if (referenceCode == 1 || referenceCode == 3)
                            userType = "Customer-Rep";
                        else
                            userType = "Customer";
                        if (referenceCode == 6)
                            userType = "P2P Distributor";
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        customerVO.setUserType(userType);
                        if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {

                            Referrers referrer = referrerRepository.findByUser(userObj);
                            if (referrer == null) {
                                customerCount = "N.A [ACTIVATION PENDING]";
                            } else {
                                List<User> users = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());


                                customerCount = String.valueOf(users.stream()
                                        .filter(user3 -> (user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3)).count());
                                customerRepCount = String.valueOf(users.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(users.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                            }

//                            List<User> userListObj = userRepository.findAllByReferrerCode(referrerRepository.findByUser(userObj).getReferrerCode());
//                            count = String.valueOf(userListObj.size());
                        }
                        customerVO.setUserType(userType);
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVO.setAccountType("PAID");
                        customerVOList.add(customerVO);
                    });
                }
                return new ResponseEntity<>(customerVOList, HttpStatus.OK);
            } else {
                return ResponseDomain.badRequest(messageProperties.getNotAuthorizedAsReferrer());
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> registerCustomer(String auth,
                                              CustomerRegistrationDTO customerRegistrationDTO,
                                              HttpServletRequest request) {
        logger.info("method ::: registerCustomer");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrersEO = referrerRepository.findByUser(user);
            if (referrersEO != null) {
                User userEO = userRepository.findByEmailAddressIgnoreCase(customerRegistrationDTO.getEmail());
                if (userEO != null) {
                    return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
                } else {
                    User userObj = customerRegistrationDTO.customerRegistrationDTOMapper(customerRegistrationDTO);
                    userObj.setIsP2PYEA(Boolean.FALSE);
                    userObj.setIsEligibleForPromotion(Boolean.FALSE);
                    userObj.setRole(userRoleRepository.findByRoleCode(1));
                    userObj.setUserStatus(userStatusRepository.findByStatusCode(4));
                    userObj.setReferenceType(userReferenceTypeRepository.findByReferenceCode(5));
                    userObj.setReferrerCode(referrersEO.getReferrerCode());
                    User user1 = userRepository.save(userObj);
                    String jwt = tokenProvider.accountActivationToken(user1);
                    user1.setAccountActivationKey(jwt);
                    userRepository.save(user1);
                    try {
                        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
                        url = url + "/customer/activate/account/" + jwt;
                        mailNotification.sendMailToCustomer(userObj, url, referrersEO.getReferrerCode().toString(), false);
                        return ResponseDomain.postResponse(messageProperties.getCustomerCreated());
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        logger.info("method ::: registerCustomer ::: error ::: " + ex.getMessage());
                        return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                    }
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getNotAuthorizedAsReferrer());
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }

    @Override
    public ResponseEntity<?> resendMailToCustomer(String auth, Long customerId, HttpServletRequest request) {
        logger.info("method ::: resendMailToCustomer");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrersEO = referrerRepository.findByUser(user);
            if (referrersEO != null) {
                User customerUserEO = userRepository.findByUserRegistrationId(customerId);
                if (customerUserEO != null) {
                    if (customerUserEO.getPassword() != null) {
                        return ResponseDomain.badRequest(messageProperties.getCustomerAccountActivated());
                    } else {
                        String jwt = tokenProvider.accountActivationToken(customerUserEO);
                        customerUserEO.setAccountActivationKey(jwt);
                        User userSaveStatusObj = userRepository.save(customerUserEO);
                        if (userSaveStatusObj != null) {
                            try {
                                String url = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
                                url = url + "/customer/activate/account/" + jwt;
                                mailNotification.sendMailToCustomer(customerUserEO, url, referrersEO.getReferrerCode().toString(), false);
                                return ResponseDomain.postResponse(messageProperties.getCustomerResendActivationMail());
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                                logger.info("method ::: resendMailToCustomer ::: error ::: " + ex.getMessage());
                                return ResponseDomain.internalServerError(ex.getMessage());
                            }
                        } else {
                            return ResponseDomain.internalServerError();
                        }
                    }
                } else {
                    return ResponseDomain.badRequest(messageProperties.getNotExist());
                }
            } else {
                return ResponseDomain.badRequest(messageProperties.getNotAuthorizedAsReferrer());
            }
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "money.flow.user.personalInfo", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> updateCustomerAccount(String auth, Long userId, String action, HttpServletRequest request) {
        logger.info("method ::: updateCustomerAccount");
        User userReferrer = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (userReferrer == null)
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        else {
            User userEO = userRepository.findByUserRegistrationId(userId);
            if (userEO == null)
                return ResponseDomain.badRequest(messageProperties.getNotExist());
            else {
                if (action.equalsIgnoreCase("activate")) {
                    if (userEO.getUserStatus().getStatusCode() != 1) {
                        userEO.setUserStatus(userStatusRepository.findByStatusCode(1));
                        userRepository.save(userEO);
                        return ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate());
                    } else
                        return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyActive());
                } else if (action.equalsIgnoreCase("deactivate")) {
                    if (userEO.getUserStatus().getStatusCode() != 2) {
                        userEO.setUserStatus(userStatusRepository.findByStatusCode(2));
                        userRepository.save(userEO);
                        return ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate());
                    } else
                        return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyInActive());
                } else if (action.equalsIgnoreCase("Archive")) {
                    if (userEO.getUserStatus().getStatusCode() != 3) {
                        Customers customer = customersRepository.findByUser(userEO);
                        if (customer != null)
                            customersRepository.delete(customer);
                        userEO.setUserStatus(userStatusRepository.findByStatusCode(3));
                        userEO.setRole(userRoleRepository.findByRoleCode(1));
                        Referrers referrer = referrerRepository.findByUser(userEO);
                        if (referrer != null) {
                            List<User> users = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
                            users.forEach(user1 -> {
                                user1.setReferrerCode(1000l);
                            });
                            userRepository.saveAll(users);
                            List<Customers> customersList = customersRepository.findAllByReferrers(referrer);
                            if (!customersList.isEmpty()) {
                                Referrers superAdminRef = referrerRepository.findByReferrerCode(1000l);
                                customersList.forEach(customers -> {
                                    customers.setReferrers(superAdminRef);
                                });
                            }
                            referrerRepository.delete(referrer);
                        }
                        userRepository.save(userEO);
                        return ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate());
                    } else
                        return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyArchived());
                } else {
                    return ResponseDomain.badRequest(messageProperties.getInvalidInput());
                }
            }
        }
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
    })
    public ResponseEntity<?> updateEmail(UpdateEmail updateEmail) {
        logger.info("method ::: updateEmail");
        return adminService.updateEmailId(updateEmail);
    }

    @Override
    public ResponseEntity<?> getTeamMembersByUserId(String auth, String userId) {
        logger.info("method ::: getTeamMembersByUserId");
        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(Long.parseLong(userId));
        List<AppAmbassadorPlusEligibleMembers> teamMembersList = new ArrayList<>();
        List<TeamMemberVo> teamMembersVOList = new ArrayList<>();
        if (appAmbassadorPlusEligibleMembers != null) {
            teamMembersList.addAll(adminService.getTeamMembers(appAmbassadorPlusEligibleMembers, 1));
        }
        Collections.sort(teamMembersList, new Comparator<AppAmbassadorPlusEligibleMembers>() {
            @Override
            public int compare(AppAmbassadorPlusEligibleMembers o1, AppAmbassadorPlusEligibleMembers o2) {
                return o1.getLevel().compareTo(o2.getLevel());
            }
        });
        teamMembersList.stream().forEach(teamMembers -> {
            User user = userRepository.findByUserRegistrationId(teamMembers.getUserRegistrationId());
            TeamMemberVo teamMemberVo = new TeamMemberVo();
            if (user != null) {
                teamMemberVo.setAppAmbassadorId(teamMembers.getAppAmbassadorId());
                teamMemberVo.setUserRegistrationId(teamMembers.getUserRegistrationId());
                teamMemberVo.setContactNumber(user.getContactNumber());
                teamMemberVo.setEmailAddress(user.getEmailAddress());
                teamMemberVo.setFirstName(teamMembers.getFirstName());
                teamMemberVo.setLastName(teamMembers.getLastName());
                teamMemberVo.setLevel(teamMembers.getLevel());
                if (user.getReferrerCode() != null) {
                    teamMemberVo.setSponsorCode(user.getReferrerCode().toString());
                    Referrers referrer = referrerRepository.findByReferrerCode(user.getReferrerCode());
                    if (referrer != null) {
                        User sponsor = referrer.getUser();
                        teamMemberVo.setSponsorName(sponsor.getFirstName() + " " + sponsor.getLastName());
                    }
                }
            }
            teamMembersVOList.add(teamMemberVo);
        });
        return new ResponseEntity<>(teamMembersVOList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllDirectIndirectUsers(String auth, String userId) {
        logger.info("method :::  getAllDirectIndirectUsers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(userId));
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            List<User> userList = adminService.getAllChildUsers(user, 1);
            Collections.sort(userList, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getLevel().compareTo(o2.getLevel());
                }
            });
            List<CustomerVO> customerVOList = new ArrayList<>();
            userList.forEach(userObj -> {
                CustomerVO customerVO = new CustomerVO();
                User sponsor = referrerRepository.findByReferrerCode(userObj.getReferrerCode()).getUser();
                customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                customerVO.setLevel(userObj.getLevel());
                customerVO.setReferrerCode(userObj.getReferrerCode().toString());
                customerVO.setP2PYEA(userObj.getIsP2PYEA());
                customerVO.setUserId(userObj.getUserRegistrationId());
                customerVO.setName(userObj.getFirstName() + (userObj.getLastName()
                        != null ? " " + userObj.getLastName() : ""));
                customerVO.setEmail(userObj.getEmailAddress());
                customerVO.setContactNumber(userObj.getContactNumber() != null
                        ? userObj.getContactNumber() : "");
                customerVO.setJoiningDate(CustomDateConverter.dateToString(userObj.getCreationDate()));
                customerVO.setStatus(userObj.getUserStatus().getStatusDescription());
                customerVO.setAccountType(adminService.setUserAccountType(userObj.getReferenceType().getReferenceCode()));
                Integer referenceCode = userObj.getReferenceType().getReferenceCode();
                List<User> aAuserList = userRepository.findAllByReferrerCode(userObj.getReferrerCode());
                aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                customerVO.setAppAmbassadorCount(aAuserList.size());
//                String userType = "";
//                if (referenceCode == 1 || referenceCode == 3)
//                    userType = "Customer-Rep";
//                else
//                    userType = "Customer";
//                if (referenceCode == 6)
//                    userType = "P2P Distributor";

                String customerCount = "N.A";
                String customerRepCount = "N.A";
                String p2pDistributorCount = "N.A";
//                customerVO.setUserType(userType);
                Integer roleCode = userObj.getRole().getRoleCode();
                customerVO.setUserType((roleCode == 3 ? "Customer" : (userObj.getReferenceType().getReferenceCode() == 6 ? "P2P Distributor" : "Customer-Rep")));
                if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {
                    Referrers referrer = referrerRepository.findByUser(userObj);
                    if (referrer == null) {
                        customerCount = "N.A [ACTIVATION PENDING]";
                    } else {
                        List<User> userListObj = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                        ;
                        customerCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                        customerRepCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                        p2pDistributorCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                    }
                }
                AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                if (p2pappAmbassadorPlusEligibleMembers != null) {
                    customerVO.setIsAppAmbassador(true);
                    customerVO.setTeamSizeCount(String.valueOf(adminService.countTeamSize(p2pappAmbassadorPlusEligibleMembers)));
                    customerVO.setDirectAAPCount(String.valueOf(adminService.countDirectAAP(p2pappAmbassadorPlusEligibleMembers)));
                    Referrers referrerObj = referrerRepository.findByUser(userObj);
                    if (referrerObj != null) {
                        List<User> referredUserList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                        referredUserList = referredUserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(referredUserList.size(), Sort.by("leadersWallCategoriesId"));
                        if (leadersWallCategories.size() > 0) {
                            customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                        } else {
                            customerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        customerVO.setRecognitionCategory("N.A");
                    }
                } else {
                    customerVO.setIsAppAmbassador(false);
                    customerVO.setTeamSizeCount("N.A");
                    customerVO.setRecognitionCategory("N.A");
                }
//                customerVO.setUserType(userType);
                customerVO.setCustomerCount(customerCount);
                customerVO.setCustomerRepCount(customerRepCount);
                customerVO.setP2pDistributorCount(p2pDistributorCount);
                customerVO.setP2PYEACount(String.valueOf(0));
                Referrers referrerObj = referrerRepository.findByUser(userObj);
                if (referrerObj != null) {
                    long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                    customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                }
                customerVO.setDirectAAPCount(String.valueOf(0));
                customerVO.setTeamSizeCount(String.valueOf(0));
                customerVO.setIsAppAmbassador(false);
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    customerVO.setIsAppAmbassador(true);
                    customerVO.setTeamSizeCount(String.valueOf(adminService.countTeamSize(appAmbassadorPlusEligibleMembers)));
                    customerVO.setDirectAAPCount(String.valueOf(adminService.countDirectAAP(appAmbassadorPlusEligibleMembers)));
                }
                customerVOList.add(customerVO);
            });
            return new ResponseEntity<>(customerVOList, HttpStatus.OK);
        }
        return ResponseDomain.badRequest("User id does not exist");
    }
}
