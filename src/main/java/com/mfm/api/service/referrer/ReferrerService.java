package com.mfm.api.service.referrer;

import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.UpdateEmail;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface ReferrerService {

    ResponseEntity<?> getMyCustomers(String auth);

    ResponseEntity<?> registerCustomer(String auth, CustomerRegistrationDTO customerRegistrationDTO, HttpServletRequest request);

    ResponseEntity<?> resendMailToCustomer(String auth, Long customerId, HttpServletRequest request);

    ResponseEntity<?> updateCustomerAccount(String auth, Long id, String action, HttpServletRequest request);

    ResponseEntity<?> updateEmail(UpdateEmail updateEmail);

    ResponseEntity<?> getTeamMembersByUserId(String auth, String userId);

    ResponseEntity<?> getAllDirectIndirectUsers(String auth, String userId);
}


