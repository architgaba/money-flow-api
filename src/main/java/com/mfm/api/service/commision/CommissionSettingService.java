package com.mfm.api.service.commision;

import com.mfm.api.domain.commision.CommissionSetting;
import org.springframework.http.ResponseEntity;

public interface CommissionSettingService {

    ResponseEntity<?> getCommissionSetting(String auth);

    ResponseEntity<?> updateCommissionSetting(String auth, Long commissionSettingId, CommissionSetting commissionSetting);

    CommissionSetting getCommissionSettingByScheduler();

    ResponseEntity<?> getCommissionRecord(String auth, int month, int year);

    ResponseEntity<?> getCommissionRecordByReferrerId(String auth, Long commissionRecordId, String month, String year);

}
