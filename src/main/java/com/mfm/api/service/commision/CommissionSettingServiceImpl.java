package com.mfm.api.service.commision;

import com.mfm.api.configuration.scheduler.SchedulerTaskServiceConfig;
import com.mfm.api.domain.commision.CommissionPerCustomer;
import com.mfm.api.domain.commision.CommissionRecord;
import com.mfm.api.domain.commision.CommissionSetting;
import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.comission.ReferrerReports;
import com.mfm.api.repository.commision.CommissionPerCustomerRepository;
import com.mfm.api.repository.commision.CommissionRecordRepository;
import com.mfm.api.repository.commision.CommissionSettingRepository;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.payment.subscription.SubscriptionRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommissionSettingServiceImpl implements CommissionSettingService {

    private static Logger logger = LogManager.getLogger(CommissionSettingServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private CommissionSettingRepository commissionSettingRepository;

    @Autowired
    private SchedulerTaskServiceConfig SchedulerTaskServiceConfig;

    @Autowired
    private ReferrerRepository referrerRepository;

    @Autowired
    private CustomersRepository customersRepository;

    @Autowired
    private CommissionRecordRepository commissionRecordRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private CommissionPerCustomerRepository commissionPerCustomerRepository;

    /**
     * @param auth
     * @return
     */
    @Override
    public ResponseEntity<?> getCommissionSetting(String auth) {
        logger.info("method ::: getCommissionSetting");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Optional<CommissionSetting> commissionSetting = commissionSettingRepository.findAll().stream().findFirst();
            return new ResponseEntity<>(commissionSetting.get() != null ? commissionSetting.get() : new
                    CommissionSetting(), HttpStatus.OK);
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }

    /**
     * @param auth
     * @param commissionSettingId
     * @param commissionSetting
     * @return
     */
    @Override
    public ResponseEntity<?> updateCommissionSetting(String auth, Long commissionSettingId, CommissionSetting
            commissionSetting) {
        logger.info("method ::: updateCommissionSetting");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null && user.getRole().getRoleCode() == 5) {
            Optional<CommissionSetting> commissionSettingEO = commissionSettingRepository.findById(commissionSettingId);
            if (commissionSettingEO.isPresent()) {
                commissionSettingEO.get().setSchedulerRunningDay(commissionSetting.getSchedulerRunningDay());
                commissionSettingEO.get().setCommission1stLevelAmount(commissionSetting.getCommission1stLevelAmount());
                commissionSettingEO.get().setCommission2ndLevelAmount(commissionSetting.getCommission2ndLevelAmount());
                commissionSetting = commissionSettingRepository.save(commissionSettingEO.get());
                if (commissionSetting.getCommissionSettingId() != null) {
                    return ResponseDomain.putResponse(messageProperties.getCommissionSettingUpdate());
                }
            }
            return null;
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist() + " or Not Authorized");
        }
    }

    /**
     * @return
     */
    @Override
    public CommissionSetting getCommissionSettingByScheduler() {
        logger.info("method ::: getCommissionSettingByScheduler");
        Optional<CommissionSetting> commissionSetting = commissionSettingRepository.findAll().stream().findFirst();
        if (commissionSetting.isPresent()) {
            return commissionSetting.get();
        } else {
            return new CommissionSetting();
        }
    }

    /**
     * @param auth
     * @param month
     * @param year
     * @return
     */
    @Override
    public ResponseEntity<?> getCommissionRecord(String auth, int month, int year) {
        logger.info("method ::: getCommissionRecord");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if ((user != null && user.getRole().getRoleCode() == 2) && (user.getRole().getRoleCode() != 5 || user.getRole
                ().getRoleCode() != 4)) {

            Optional<CommissionSetting> commissionSettingEO = commissionSettingRepository.findAll().stream()
                    .findFirst();
            Referrers referrersEO = referrerRepository.findByUser(user);
            List<JSONObject> returnCustomerReportList = getCommissionRecordByCustomerReps(referrersEO, month, year);

            return new ResponseEntity<>(returnCustomerReportList, HttpStatus.OK);
        } else if (user != null && user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
            logger.info("method ::: getCommissionRecord ::: Block ::: ROLE_SUPER_ADMIN, ROLE_ADMIN");
            Optional<CommissionSetting> commissionSettingEO = commissionSettingRepository.findAll().stream()
                    .findFirst();

            List<ReferrerReports> returnReferrerReportsList = getCommissionRecordByAdmin(month, year);
            return new ResponseEntity<>(returnReferrerReportsList, HttpStatus.OK);
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist() + " or Not Authorized");
        }
    }


    /**
     * @param auth
     * @param commissionRecordId
     * @return
     */
    @Override
    public ResponseEntity<?> getCommissionRecordByReferrerId(String auth, Long commissionRecordId, String month, String year) {
        logger.info("method ::: getCommissionRecordByReferrerId");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        LocalDate filterDateFrom = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month) - 1, 1);
//        Referrers referrersEO = referrerRepository.findById(referrerId).get();
        List<JSONObject> customerReportList = new ArrayList<>();
//        final List<CommissionRecord> commissionRecordList = commissionRecordRepository
//                .findAllByReferrersOrderByCommissionCalculateFromDateDesc(referrersEO).stream().filter
//                        (commissionRecord ->
//                                commissionRecord.getCommissionCalculateFromDate().compareTo(filterDateFrom) == 0)
//                .collect(Collectors
//                        .toList());
        Optional<CommissionRecord> commissionRecordList = commissionRecordRepository.findById(commissionRecordId);
        if (commissionRecordList.isPresent()) {
            commissionPerCustomerRepository.findByCommissionRecord(commissionRecordList.get()).forEach
                    (commissionPerCustomer -> {
                        JSONObject customerReport = new JSONObject();
                        try {
                            List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                                    .findAllByUserOrderBySubscriptionIdDesc(commissionPerCustomer.getUser()).stream()
                                    .filter(subscriptionDetails ->
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                                    .getPlan()
                                                    .getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList());

                            customerReport.put("name", commissionPerCustomer.getUser().getFirstName() + " " +
                                    (commissionPerCustomer.getUser().getLastName() != null ? commissionPerCustomer
                                            .getUser().getLastName() : ""));
                            customerReport.put("userType", commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                                    6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "Self Referral" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");
                            customerReport.put("planType", ((subscriptionDetailsList.size() == 0) ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval()));
                            customerReport.put("p2PYEA", commissionPerCustomer.getUser().getIsP2PYEA());
                        } catch (Exception e) {
                            logger.error(e);
                            customerReport.put("p2PYEA", commissionPerCustomer.getIsP2PYEA());
                            customerReport.put("userType", commissionPerCustomer.getUserType());
                            customerReport.put("name", commissionPerCustomer.getName());
                            customerReport.put("planType", commissionPerCustomer.getPlanType());
                        }
                        customerReport.put("startPeriod", commissionRecordList.get().getCommissionCalculateFromDate());
                        customerReport.put("finishPeriod", commissionRecordList.get().getCommissionCalculateToDate());

//                        customerReport.put("level", commissionPerCustomer.getSecondLevelCommission() == 0 ? "Level I" :
//                                "Level II");
                        customerReport.put("commissionFromLevel1", commissionPerCustomer.getFirstLevelCommission());
                        customerReport.put("commissionFromLevel2", commissionPerCustomer.getSecondLevelCommission());

                        customerReport.put("amount", commissionPerCustomer.getCommissionAmount());
                        customerReportList.add(customerReport);

                    });
        }
        return new ResponseEntity<>(customerReportList, HttpStatus.OK);
    }


    public List<JSONObject> getCommissionRecordByCustomerReps(Referrers referrersEO, int month, int
            year) {
        logger.info("method ::: getCommissionRecordByCustomerReps");

        LocalDate filterDateFrom = LocalDate.of(year, month, 1);
        LocalDate filterDateTo = LocalDate.of(year, month, filterDateFrom.lengthOfMonth());

        List<CommissionRecord> commissionRecordList = commissionRecordRepository
                .findAllByReferrersOrderByCommissionCalculateFromDateAsc(referrersEO).stream()
                .filter(commissionRecord -> commissionRecord.getCommissionCalculateFromDate().compareTo
                        (filterDateFrom) == 0).collect(Collectors.toList());
        List<JSONObject> customerReportList = new ArrayList<>();
        if (!commissionRecordList.isEmpty()) {
            commissionRecordList.forEach(commissionRecord -> {
                List<CommissionPerCustomer> commissionPerCustomerList = commissionPerCustomerRepository
                        .findByCommissionRecord(commissionRecord);

                commissionPerCustomerList.forEach(commissionPerCustomer -> {
                    JSONObject customerReport = new JSONObject();
                    try {
                        List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                                .findAllByUserOrderBySubscriptionIdDesc(commissionPerCustomer.getUser())
                                .stream().filter(subscriptionDetails ->
                                        subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                                .getPlan
                                                        ().getPlanType().equalsIgnoreCase("Customer")).collect(Collectors
                                        .toList());
                        customerReport.put("userType", commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                                6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "SelfCommission" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");
                        customerReport.put("name", commissionPerCustomer.getUser().getFirstName() + " " +
                                (commissionPerCustomer.getUser().getLastName() != null ?
                                        commissionPerCustomer.getUser().getLastName() : ""));
                        customerReport.put("planType", (subscriptionDetailsList.isEmpty() ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval()));
                        customerReport.put("p2PYEA", commissionPerCustomer.getUser().getIsP2PYEA());
                    } catch (Exception e) {
                        logger.error(e);
                        customerReport.put("p2PYEA", commissionPerCustomer.getIsP2PYEA());
                        customerReport.put("userType", commissionPerCustomer.getUserType());
                        customerReport.put("name", commissionPerCustomer.getName());
                        customerReport.put("planType", commissionPerCustomer.getPlanType());
                    }


                    customerReport.put("startPeriod", commissionRecord.getCommissionCalculateFromDate().toString());
                    customerReport.put("finishPeriod", commissionRecord.getCommissionCalculateToDate().toString());

//                    customerReport.put("userType", commissionPerCustomer.getUser().getRole()
//                            .getRoleCode() == 2 ? "Customer Rep" : "Customer");
                    customerReport.put("commissionFromLevel1", commissionPerCustomer.getFirstLevelCommission());
                    customerReport.put("commissionFromLevel2", commissionPerCustomer.getSecondLevelCommission());

                    customerReport.put("amount", commissionPerCustomer.getCommissionAmount());
                    customerReportList.add(customerReport);
                });
            });

        } else {
            List<User> customerList = userRepository.findAllByReferrerCode(referrersEO.getReferrerCode());
            customerList.forEach(customer -> {
//                Referrers referrer = referrerRepository.findByUser(customer);
                JSONObject customerReport = new JSONObject();
                try {
                    List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                            .findAllByUserOrderBySubscriptionIdDesc(customer)
                            .stream().filter(subscriptionDetails ->
                                    subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                            .getPlan
                                                    ().getPlanType().equalsIgnoreCase("Customer")).collect(Collectors
                                    .toList());
                    customerReport.put("planType", (subscriptionDetailsList.isEmpty() ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval()));
                    customerReport.put("name", customer.getFirstName() + " " + (customer.getLastName() != null ? customer.getLastName() : ""));
                    customerReport.put("userType", customer.getReferenceType().getReferenceCode() ==
                            6 ? "P2P Distributor" : customer.getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");
                } catch (Exception e) {
                    logger.error(e);
                }


                customerReport.put("startPeriod", filterDateFrom.toString());
                customerReport.put("finishPeriod", filterDateTo.toString());

//                customerReport.put("userType", customer.getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");
                customerReport.put("commissionFromLevel1", 0);
                customerReport.put("commissionFromLevel2", 0);

                customerReport.put("amount", 0);
                customerReportList.add(customerReport);
            });
        }
        return customerReportList;
    }

    public List<ReferrerReports> getCommissionRecordByAdmin(int month, int year) {
        logger.info("method ::: getCommissionRecordByAdmin");
        List<ReferrerReports> referrerReportsList = new ArrayList<>();
        LocalDate filterDateFrom = LocalDate.of(year, month, 1);
        LocalDate filterDateTo = LocalDate.of(year, month, filterDateFrom.lengthOfMonth());

        List<CommissionRecord> commissionRecords = commissionRecordRepository.findAllByCommissionCalculateFromDateAndCommissionCalculateToDate(filterDateFrom, filterDateTo);

//        List<Referrers> referrersListEO = referrerRepository.findAll().stream().filter(referrers ->
//                referrers.getUser().getUserStatus().getStatusDescription().equalsIgnoreCase("Active")
//        ).collect(Collectors.toList());


        if (commissionRecords.isEmpty()) {
            return referrerReportsList;
        } else {
            final int[] monthlyUserCount = {0};
            final int[] yearlyUserCount = {0};
            commissionRecords.forEach(commissionRecord -> {
                Referrers referrers = commissionRecord.getReferrers();


//                List<CommissionRecord> commissionRecordList = commissionRecordRepository
//                        .findAllByReferrersOrderByCommissionCalculateFromDateAsc(referrers).stream()
//                        .filter(commissionRecord -> commissionRecord.getCommissionCalculateFromDate().compareTo
//                                (filterDateFrom) == 0).collect(Collectors.toList());

                ReferrerReports referrerReports = new ReferrerReports();
                try {

                    if (referrers.getUser().getUserStatus().getStatusCode() != 1) {
                        return;
                    }
                    referrerReports.setP2PYEA(referrers.getUser().getIsP2PYEA());
                    referrerReports.setReferrerId(referrers.getReferrerId());
                    switch (referrers.getUser().getReferenceType().getReferenceCode()) {
                        case 6:
                            referrerReports.setUserType("P2P Distributor");
                            break;
                        case 7:
                            referrerReports.setUserType("Admin");
                            break;
                        default:
                            if (referrers.getUser().getRole().getRoleCode() == 5)
                                referrerReports.setUserType("Super Admin");
                            else
                                referrerReports.setUserType("Customer Rep");
                            break;
                    }
                } catch (Exception e) {
                    logger.error(e);
                    referrerReports.setP2PYEA(commissionRecord.getIsP2PYEA());
                    referrerReports.setReferrerId(commissionRecord.getReferrerId());
                    referrerReports.setUserType(commissionRecord.getUserType());
                }

                List<CommissionPerCustomer> commissionPerCustomers = new ArrayList<>();
                monthlyUserCount[0] = 0;
                yearlyUserCount[0] = 0;


                final int[] totalCommission = {0};
                final int[] commissionFromLevel1 = {0};
                final int[] commissionFromLevel2 = {0};
                if (commissionRecord != null) {
                    commissionPerCustomers = commissionPerCustomerRepository
                            .findByCommissionRecord(commissionRecord);
                    commissionPerCustomers.forEach(commissionPerCustomer -> {

                        totalCommission[0] += commissionPerCustomer.getCommissionAmount();

                        commissionFromLevel1[0] += commissionPerCustomer.getFirstLevelCommission();

                        commissionFromLevel2[0] += commissionPerCustomer.getSecondLevelCommission();
                        try {
                            if (commissionRecord.getMonthlyUserCount() == null || commissionRecord.getYearlyUserCount() == null) {
                                if (commissionPerCustomer.getUser().getRole().getRoleCode() == 3) {
                                    List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                                            .findAllByUserOrderBySubscriptionIdDesc(commissionPerCustomer.getUser())
                                            .stream().filter(subscriptionDetails -> subscriptionDetails.getPlan().getPlanType()
                                                    .equalsIgnoreCase("Customer")).collect(Collectors.toList());

                                    monthlyUserCount[0] += subscriptionDetailsList.stream().filter(subscriptionDetails ->
                                            (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                                                    subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                            (Collectors.toList()).size();

                                    yearlyUserCount[0] += subscriptionDetailsList.stream().filter(subscriptionDetails ->
                                            (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Yearly") &&
                                                    subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                            (Collectors.toList()).size();
                                    referrerReports.setMonthlyCustomerCount(monthlyUserCount[0]);
                                    referrerReports.setYearlyCustomerCount(yearlyUserCount[0]);
                                }
                            } else {
                                referrerReports.setMonthlyCustomerCount(commissionRecord.getMonthlyUserCount());
                                referrerReports.setYearlyCustomerCount(commissionRecord.getYearlyUserCount());
                            }
                        } catch (Exception e) {
                            logger.error(e);
                        }
                    });
                    try {
                        referrerReports.setReferrerName(referrers.getUser().getFirstName() + " " + (referrers.getUser()
                                .getLastName() != null ? referrers.getUser().getLastName() : ""));
                        referrerReports.setStatus(referrers.getUser().getUserStatus().getStatusDescription());
                    } catch (Exception e) {
                        logger.error(e);
                        referrerReports.setReferrerName(commissionRecord.getReferrerName());
                        referrerReports.setStatus(commissionRecord.getReferrerStatus());
                    }
                    referrerReports.setCommissionRecordId(commissionRecord.getCommissionRecordId());

                    referrerReports.setStartPeriod(commissionRecord.getCommissionCalculateFromDate()
                            .toString());
                    referrerReports.setEndPeriod(commissionRecord.getCommissionCalculateToDate().toString());
                    referrerReports.setTotalCommissions(totalCommission[0]);
                    referrerReports.setCommissionFromLevel1(commissionFromLevel1[0]);
                    referrerReports.setCommissionFromLevel2(commissionFromLevel2[0]);
                    referrerReportsList.add(referrerReports);
                } else {
                    try {
                        referrerReports.setReferrerName(referrers.getUser().getFirstName() + " " + (referrers.getUser()
                                .getLastName() != null ? referrers.getUser().getLastName() : ""));
                        referrerReports.setStatus(referrers.getUser().getUserStatus().getStatusDescription());
                    } catch (Exception e) {
                        logger.error(e);
                        referrerReports.setReferrerName(commissionRecord.getReferrerName());
                        referrerReports.setStatus(commissionRecord.getReferrerStatus());
                    }
                    referrerReports.setMonthlyCustomerCount(monthlyUserCount[0]);
                    referrerReports.setYearlyCustomerCount(yearlyUserCount[0]);
                    referrerReports.setStartPeriod(filterDateFrom.toString());
                    referrerReports.setEndPeriod(filterDateTo.toString());
                    referrerReports.setTotalCommissions(totalCommission[0]);
                    referrerReports.setCommissionFromLevel1(commissionFromLevel1[0]);
                    referrerReports.setCommissionFromLevel2(commissionFromLevel2[0]);
                    referrerReportsList.add(referrerReports);
                    logger.info("Commission Record currently not Available");
                }
            });

        }
        return referrerReportsList;
    }
}
