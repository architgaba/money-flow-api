package com.mfm.api.service.admin.response;

import lombok.Data;

@Data
public class CustomerVO {
    private Long userId;
    private String name;
    private String email;
    private String contactNumber;
    private String joiningDate;
    private String referrerCode;
    private String referrerName;
    private String status;
    private String accountType;
    private String userType;
    private String customerCount;
    private String customerRepCount;
    private String p2pDistributorCount;
    private Boolean isAppAmbassador;
    private Integer appAmbassadorCount;
    private String teamSizeCount;
    private String recognitionCategory;
    private boolean isP2PYEA;
    private String p2PYEACount;
    private String directAAPCount;
    private Integer level;
    private Long sponsorReferrerCode;


    public CustomerVO() {
    }
}
