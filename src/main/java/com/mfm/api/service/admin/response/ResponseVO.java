package com.mfm.api.service.admin.response;

import lombok.Data;

import java.util.List;

@Data
public class ResponseVO {
    private List<ReferrerVO> referrerVOS;
}
