package com.mfm.api.service.admin;

import com.mfm.api.domain.commision.AppAmbassadorBonuses;
import com.mfm.api.domain.commision.AppAmbassadorPlusEligibleMembers;
import com.mfm.api.domain.commision.CommissionPerCustomer;
import com.mfm.api.domain.commision.CommissionRecord;
import com.mfm.api.domain.customer.Customers;
import com.mfm.api.domain.leaderswall.LeadersWallCategories;
import com.mfm.api.domain.moneyflowmanager.contactmanager.ContactManager;
import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.payment.transaction.Transaction;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.information.UserBillingAddress;
import com.mfm.api.domain.user.information.UserPhysicalAddress;
import com.mfm.api.domain.user.registration.ContactEnquiries;
import com.mfm.api.domain.user.registration.GuestUser;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.bonus.BonusBreakDownVo;
import com.mfm.api.dto.bonus.BonusVo;
import com.mfm.api.dto.request.ChangeReferralCodeDTO;
import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.TeamMemberVo;
import com.mfm.api.dto.user.UpdateEmail;
import com.mfm.api.repository.commision.AppAmbassadorBonusesRepository;
import com.mfm.api.repository.commision.AppAmbassadorPlusEligibleMembersRepository;
import com.mfm.api.repository.commision.CommissionPerCustomerRepository;
import com.mfm.api.repository.commision.CommissionRecordRepository;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.leaderswall.LeadersWallCategoriesRepository;
import com.mfm.api.repository.moneyflowmanager.contactmanager.ContactManagerRepository;
import com.mfm.api.repository.payment.subscription.SubscriptionRepository;
import com.mfm.api.repository.payment.transaction.TransactionRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.information.UserBillingAddressRepository;
import com.mfm.api.repository.user.information.UserPhysicalAddressRepository;
import com.mfm.api.repository.user.registration.*;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.admin.response.CustomerVO;
import com.mfm.api.service.admin.response.ReferrerVO;
import com.mfm.api.service.admin.response.ResponseVO;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.mail.EmailContent;
import com.mfm.api.util.mail.MailNotification;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import com.stripe.exception.StripeException;
import com.stripe.model.Subscription;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("admin")
@CacheConfig(cacheNames = "admin-cache")
public class AdminServiceImpl implements AdminService {

    private static Logger logger = LogManager.getLogger(AdminServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private ReferrerRepository referrerRepository;
    @Autowired
    private CommissionRecordRepository commissionRecordRepository;
    @Autowired
    private CommissionPerCustomerRepository commissionPerCustomerRepository;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private UserStatusRepository userStatusRepository;
    @Autowired
    private MailNotification mailNotification;
    @Autowired
    private UserReferenceTypeRepository referenceTypeRepository;
    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    private EmailContent emailContent;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRoleRepository roleRepository;
    @Autowired
    private GuestUserRepository guestUserRepository;
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ContactEnquiriesRepository contactEnquiriesRepository;
    @Autowired
    private UserPhysicalAddressRepository userPhysicalAddressRepository;
    @Autowired
    private UserBillingAddressRepository userBillingAddressRepository;
    @Autowired
    private ContactManagerRepository contactManagerRepository;
    @Autowired
    private AppAmbassadorPlusEligibleMembersRepository appAmbassadorPlusEligibleMembersRepository;
    @Autowired
    private AppAmbassadorBonusesRepository appAmbassadorBonusesRepository;
    @Autowired
    private LeadersWallCategoriesRepository leadersWallCategoriesRepository;

    /**
     * List OF Referrers
     * With Their Respective Customers
     *
     * @param auth
     * @return
     */
    @Override
    @Cacheable(value = "admin.customer-reps", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> allReferrers(String auth) {
        logger.info("method :::  allReferrers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
            ResponseVO responseVO = new ResponseVO();
            List<ReferrerVO> referrerVOS = new ArrayList<>();
            List<User> referredUsers = userRepository.findAll().stream()
                    .filter(user1 -> (user1.getRole().getRoleCode() == 1 && (user1.getUserStatus().getStatusCode
                            () == 3 || user1.getUserStatus().getStatusCode() == 4 || user1.getUserStatus()
                            .getStatusCode() == 5) && (user1.getReferenceType().getReferenceCode() == 1 || user1.getReferenceType()
                            .getReferenceCode() == 3 || user1.getReferenceType().getReferenceCode() == 6/* || user1.getReferenceType().getReferenceCode() == 5*/))).collect(Collectors.toList());

            List<Referrers> referrersList = referrerRepository.findAll().stream()
                    .filter(referrers -> (!referrers.getReferrerType().equalsIgnoreCase("ADMIN")
                            && !referrers.getReferrerType().equalsIgnoreCase("SUPER_ADMIN"))).collect(Collectors
                            .toList());
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            if (!referredUsers.isEmpty()) {
                referredUsers.forEach(user1 -> {
                    ReferrerVO referrerVO = new ReferrerVO();
                    referrerVO.setDirectAAPCount(String.valueOf(0));
                    AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user1.getUserRegistrationId());
                    if (appAmbassadorPlusEligibleMembers != null) {
                        referrerVO.setIsAppAmbassador(true);
                        referrerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                        referrerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                        Referrers referrerObj = referrerRepository.findByUser(user1);
                        if (referrerObj != null) {
                            List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                            userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                            if (leadersWallCategories.size() > 0) {
                                referrerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                            } else {
                                referrerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            referrerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        referrerVO.setIsAppAmbassador(false);
                        referrerVO.setTeamSizeCount("N.A");
                        referrerVO.setRecognitionCategory("N.A");
                    }
                    referrerVO.setP2PYEACount(String.valueOf(0));
                    Referrers referrerObj = referrerRepository.findByUser(user1);
                    if (referrerObj != null) {
                        long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                        referrerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                    }
                    referrerVO.setP2PYEA(user1.getIsP2PYEA());
                    referrerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                    referrerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                    referrerVO.setEmail(user1.getEmailAddress());
                    referrerVO.setUserId(user1.getUserRegistrationId());
                    referrerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                    referrerVO.setStatus(user1.getUserStatus().getStatusDescription());
                    referrerVO.setReferrerCode("");
                    referrerVO.setCustomerCount(0l);
                    referrerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));
                    referrerVOS.add(referrerVO);
                });
            }
            if (!referrersList.isEmpty()) {
                referrersList.forEach(referrers -> {
                    User user1 = referrers.getUser();
                    ReferrerVO referrerVO = new ReferrerVO();
                    referrerVO.setDirectAAPCount(String.valueOf(0));
                    AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user1.getUserRegistrationId());
                    if (appAmbassadorPlusEligibleMembers != null) {
                        referrerVO.setIsAppAmbassador(true);
                        referrerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                        referrerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                        if (referrers != null) {
                            List<User> userList = userRepository.findAllByReferrerCode(referrers.getReferrerCode());
                            userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                            if (leadersWallCategories.size() > 0) {
                                referrerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                            } else {
                                referrerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            referrerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        referrerVO.setIsAppAmbassador(false);
                        referrerVO.setTeamSizeCount("N.A");
                        referrerVO.setRecognitionCategory("N.A");
                    }
                    referrerVO.setP2PYEACount(String.valueOf(0));
                    Referrers referrerObject = referrerRepository.findByUser(user1);
                    if (referrerObject != null) {
                        long p2pYEACount = userRepository.findAllByReferrerCode(referrerObject.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                        referrerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                    }
                    referrerVO.setP2PYEA(user1.getIsP2PYEA());
                    referrerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                    referrerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                    referrerVO.setEmail(user1.getEmailAddress());
                    referrerVO.setUserId(user1.getUserRegistrationId());
                    referrerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                    referrerVO.setStatus(user1.getUserStatus().getStatusDescription());
                    referrerVO.setReferrerCode(referrers.getReferrerCode() != null ? referrers.getReferrerCode()
                            .toString() : "");
                    referrerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));

                    List<Customers> customersList = customersRepository.findAllByReferrers(referrers);
                    List<User> p2pDistributerList = new ArrayList<>();
                    if (referrers.getUser().getReferenceType().getReferenceCode() == 6 || referrers.getUser().getReferenceType().getReferenceCode() == 5 || referrers.getUser().getReferenceType().getReferenceCode() == 1) {
                        p2pDistributerList = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream().filter(user2 -> user2.getReferenceType().getReferenceCode() == 6).collect(Collectors.toList());
                    }
                    List<CustomerVO> customerVOS = new ArrayList<>();

                    if (!p2pDistributerList.isEmpty()) {
                        p2pDistributerList.forEach(p2pDistributer -> {
//                            User user2 = p2pDistributer.getUser();
                            CustomerVO customerVO = new CustomerVO();
                            User sponsor = referrerRepository.findByReferrerCode(p2pDistributer.getReferrerCode()).getUser();
                            customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                            customerVO.setDirectAAPCount(String.valueOf(0));
                            AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(p2pDistributer.getUserRegistrationId());
                            if (p2pappAmbassadorPlusEligibleMembers != null) {
                                customerVO.setIsAppAmbassador(true);
                                customerVO.setTeamSizeCount(String.valueOf(countTeamSize(p2pappAmbassadorPlusEligibleMembers)));
                                customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(p2pappAmbassadorPlusEligibleMembers)));
                                Referrers referrerObj = referrerRepository.findByUser(p2pDistributer);
                                if (referrerObj != null) {
                                    List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                    userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                            .collect(Collectors.toList());
                                    List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                    if (leadersWallCategories.size() > 0) {
                                        customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                    } else {
                                        customerVO.setRecognitionCategory("N.A");
                                    }
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setIsAppAmbassador(false);
                                customerVO.setTeamSizeCount("N.A");
                                customerVO.setRecognitionCategory("N.A");
                            }
                            customerVO.setP2PYEACount(String.valueOf(0));
                            Referrers referrerObj = referrerRepository.findByUser(p2pDistributer);
                            if (referrerObj != null) {
                                long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                                customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                            }
                            List<User> aAuserList = userRepository.findAllByReferrerCode(p2pDistributer.getReferrerCode());
                            aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            customerVO.setAppAmbassadorCount(aAuserList.size());
                            customerVO.setP2PYEA(p2pDistributer.getIsP2PYEA());
                            customerVO.setName(p2pDistributer.getFirstName() + (p2pDistributer.getLastName() != null ? " " + p2pDistributer.getLastName() : ""));
                            customerVO.setContactNumber(p2pDistributer.getContactNumber() != null ? p2pDistributer.getContactNumber() : "");
                            customerVO.setEmail(p2pDistributer.getEmailAddress());
                            customerVO.setUserId(p2pDistributer.getUserRegistrationId());
                            customerVO.setJoiningDate(CustomDateConverter.dateToString(p2pDistributer.getCreationDate()));
                            customerVO.setStatus(p2pDistributer.getUserStatus().getStatusDescription());
                            customerVO.setAccountType(setUserAccountType(p2pDistributer.getReferenceType().getReferenceCode()));
                            Integer roleCode = p2pDistributer.getRole().getRoleCode();
                            customerVO.setUserType((roleCode == 3 ? "Customer" : (p2pDistributer.getReferenceType().getReferenceCode() == 6 ? "P2P Distributor" : "Customer-Rep")));
                            String customerCount = "N.A";
                            String customerRepCount = "N.A";
                            String p2pDistributorCount = "N.A";
                            if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {
                                Referrers referrer = referrerRepository.findByUser(p2pDistributer);
                                if (referrer == null) {
                                    customerCount = "N.A [ACTIVATION PENDING]";

                                    customerVO.setReferrerCode("");
                                } else {
                                    customerVO.setReferrerCode(referrer.getReferrerCode().toString());
                                    List<User> userList = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user2 -> user2.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                    customerCount = String.valueOf(userList.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                    customerRepCount = String.valueOf(userList.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                    p2pDistributorCount = String.valueOf(userList.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());

                                }

                            }

                            customerVO.setCustomerCount(customerCount);
                            customerVO.setCustomerRepCount(customerRepCount);
                            customerVO.setP2pDistributorCount(p2pDistributorCount);
                            customerVOS.add(customerVO);
                        });
                    }
                    if (!customersList.isEmpty()) {
                        customersList.forEach(customers -> {
                            User user2 = customers.getUser();
                            CustomerVO customerVO = new CustomerVO();
                            User sponsor = referrerRepository.findByReferrerCode(customers.getUser().getReferrerCode()).getUser();
                            customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                            customerVO.setDirectAAPCount(String.valueOf(0));
                            AppAmbassadorPlusEligibleMembers customerAppAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user2.getUserRegistrationId());
                            if (customerAppAmbassadorPlusEligibleMembers != null) {
                                customerVO.setIsAppAmbassador(true);
                                customerVO.setTeamSizeCount(String.valueOf(countTeamSize(customerAppAmbassadorPlusEligibleMembers)));
                                customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(customerAppAmbassadorPlusEligibleMembers)));
                                Referrers referrerObj = referrerRepository.findByUser(customers.getUser());
                                if (referrerObj != null) {
                                    List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                    userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                            .collect(Collectors.toList());
                                    List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                    if (leadersWallCategories.size() > 0) {
                                        customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                    } else {
                                        customerVO.setRecognitionCategory("N.A");
                                    }
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setIsAppAmbassador(false);
                                customerVO.setTeamSizeCount("N.A");
                                customerVO.setRecognitionCategory("N.A");
                            }
                            customerVO.setP2PYEACount(String.valueOf(0));
                            Referrers referrerObj = referrerRepository.findByUser(user2);
                            if (referrerObj != null) {
                                long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                                customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                            }
                            List<User> aAuserList = userRepository.findAllByReferrerCode(customers.getUser().getReferrerCode());
                            aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            customerVO.setAppAmbassadorCount(aAuserList.size());
                            customerVO.setP2PYEA(user2.getIsP2PYEA());
                            customerVO.setName(user2.getFirstName() + (user2.getLastName() != null ? " " + user2.getLastName() : ""));
                            customerVO.setContactNumber(user2.getContactNumber() != null ? user2.getContactNumber() : "");
                            customerVO.setEmail(user2.getEmailAddress());
                            customerVO.setUserId(user2.getUserRegistrationId());
                            customerVO.setJoiningDate(CustomDateConverter.dateToString(user2.getCreationDate()));
                            customerVO.setStatus(user2.getUserStatus().getStatusDescription());
                            customerVO.setAccountType(setUserAccountType(user2.getReferenceType().getReferenceCode()));
                            customerVO.setReferrerCode("");
                            Integer roleCode = customers.getUser().getRole().getRoleCode();
                            customerVO.setUserType(roleCode == 3 ? "Customer" : "Customer-Rep");
                            String customerCount = "N.A";
                            String customerRepCount = "N.A";
                            String p2pDistributorCount = "N.A";
                            if (customerVO.getUserType().equals("Customer-Rep")) {
                                Referrers repositoryOfUser = referrerRepository.findByUser(user2);
                                if (repositoryOfUser != null) {
                                    Long referrerCode = repositoryOfUser.getReferrerCode();
                                    customerVO.setReferrerCode(referrerCode.toString());
                                    List<User> userListObj = userRepository.findAllByReferrerCode(referrerCode).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                    customerCount = String.valueOf(userListObj.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                    customerRepCount = String.valueOf(userListObj.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                    p2pDistributorCount = String.valueOf(userListObj.stream()
                                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());


                                    List<User> userList = userRepository.findAllByReferrerCode(referrerRepository.findByUser(customers.getUser()).getReferrerCode());
                                }
                            }
                            customerVO.setCustomerCount(customerCount);
                            customerVO.setCustomerRepCount(customerRepCount);
                            customerVO.setP2pDistributorCount(p2pDistributorCount);
                            customerVOS.add(customerVO);
                        });
                    }

                    List<User> referredCustomersList = userRepository.findAllByReferrerCode(referrers
                            .getReferrerCode()).stream()
                            .filter(user2 -> (user2.getRole().getRoleCode() == 1 && (user2.getUserStatus()
                                    .getStatusCode() == 4 || user2.getUserStatus().getStatusCode() == 5 ||
                                    user2.getUserStatus().getStatusCode() == 3)
                                    && (user2.getReferenceType().getReferenceCode() == 5 ||
                                    user2.getReferenceType().getReferenceCode() == 4)))
                            .filter(user2 -> customerVOS.stream().filter(customerVO -> customerVO.getUserId().equals(user2.getUserRegistrationId())).count() == 0)
                            .collect(Collectors.toList
                                    ());
                    ;
                    if (!referredCustomersList.isEmpty()) {
                        referredCustomersList.forEach(user2 -> {
                            CustomerVO customerVO = new CustomerVO();
                            User sponsor = referrerRepository.findByReferrerCode(user2.getReferrerCode()).getUser();
                            customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                            customerVO.setDirectAAPCount(String.valueOf(0));
                            AppAmbassadorPlusEligibleMembers referrerAppAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user2.getUserRegistrationId());
                            if (referrerAppAmbassadorPlusEligibleMembers != null) {
                                customerVO.setIsAppAmbassador(true);
                                customerVO.setTeamSizeCount(String.valueOf(countTeamSize(referrerAppAmbassadorPlusEligibleMembers)));
                                customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(referrerAppAmbassadorPlusEligibleMembers)));
                                Referrers referrerObj = referrerRepository.findByUser(user2);
                                if (referrerObj != null) {
                                    List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                    userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                            .collect(Collectors.toList());
                                    List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                    if (leadersWallCategories.size() > 0) {
                                        customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                    } else {
                                        customerVO.setRecognitionCategory("N.A");
                                    }
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setIsAppAmbassador(false);
                                customerVO.setTeamSizeCount("N.A");
                                customerVO.setRecognitionCategory("N.A");
                            }
                            customerVO.setP2PYEACount(String.valueOf(0));
                            Referrers referrerObj = referrerRepository.findByUser(user2);
                            if (referrerObj != null) {
                                long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                                customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                            }
                            List<User> aAuserList = userRepository.findAllByReferrerCode(user2.getReferrerCode());
                            aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                    .collect(Collectors.toList());
                            customerVO.setAppAmbassadorCount(aAuserList.size());
                            customerVO.setP2PYEA(user2.getIsP2PYEA());
                            customerVO.setName(user2.getFirstName() + (user2.getLastName() != null ? " " + user2.getLastName() : ""));
                            customerVO.setContactNumber(user2.getContactNumber() != null ? user2.getContactNumber() : "");
                            customerVO.setEmail(user2.getEmailAddress());
                            customerVO.setUserId(user2.getUserRegistrationId());
                            customerVO.setJoiningDate(CustomDateConverter.dateToString(user2.getCreationDate()));
                            customerVO.setStatus(user2.getUserStatus().getStatusDescription());
                            customerVO.setAccountType(setUserAccountType(user2.getReferenceType().getReferenceCode()));
                            customerVO.setReferrerCode("");
                            String userType = "";
                            Integer referenceCode = user2.getReferenceType().getReferenceCode();
                            if (referenceCode == 1 || referenceCode == 3)
                                userType = "Customer-Rep";
                            else
                                userType = "Customer";
                            String customerCount = "N.A";
                            String customerRepCount = "N.A";
                            String p2pDistributorCount = "N.A";
                            customerVO.setUserType(userType);
                            if (customerVO.getUserType().equals("Customer-Rep")) {
                                Long referrerCode = referrerRepository.findByUser(user2).getReferrerCode();
                                customerVO.setReferrerCode(referrerCode.toString());
                                List<User> userListObj = userRepository.findAllByReferrerCode(referrerCode).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());

                                customerCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                customerRepCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                            }
                            customerVO.setUserType(userType);
                            customerVO.setCustomerCount(customerCount);
                            customerVO.setCustomerRepCount(customerRepCount);
                            customerVO.setP2pDistributorCount(p2pDistributorCount);
                            customerVOS.add(customerVO);
                        });
                    }
                    List<User> users = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());


                    long customerCount = users.stream()
                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count();
                    long customerRepCount = users.stream()
                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count();
                    long p2pDistributorCount = users.stream()
                            .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count();

                    referrerVO.setCustomerCount(customerCount);
                    referrerVO.setCustomerRepCount(customerRepCount);
                    referrerVO.setP2pDistributorCount(p2pDistributorCount);
                    referrerVO.setCustomers(customerVOS);
                    referrerVOS.add(referrerVO);
                });
            }
            responseVO.setReferrerVOS(referrerVOS);
            return new ResponseEntity<>(referrerVOS, HttpStatus.OK);
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        }
    }

    /**
     * Adding Customers
     *
     * @param auth
     * @param customerRegistrationDTO
     * @param request
     * @return
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> addCustomer(String auth, CustomerRegistrationDTO customerRegistrationDTO,
                                         HttpServletRequest request) {
        logger.info("method ::: addCustomer");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrers = referrerRepository.findByUser(user);
            if (referrers != null && (referrers.getReferrerType().equalsIgnoreCase("ADMIN")
                    || referrers.getReferrerType().equalsIgnoreCase("SUPER_ADMIN"))) {
                User userEO = userRepository.findByEmailAddressIgnoreCase(customerRegistrationDTO.getEmail());
                if (userEO != null)
                    return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
                else {
                    User userObj = customerRegistrationDTO.customerRegistrationDTOMapper(customerRegistrationDTO);
                    String activationKey;
                    userObj.setIsP2PYEA(Boolean.FALSE);
                    userObj.setIsEligibleForPromotion(Boolean.FALSE);
                    userObj.setRole(userRoleRepository.findByRoleCode(1));
                    userObj.setUserStatus(userStatusRepository.findByStatusCode(4));
                    userObj.setReferrerCode(referrers.getReferrerCode());
                    userObj.setReferenceType(customerRegistrationDTO.getAccountType().equalsIgnoreCase("1") ? (referenceTypeRepository
                            .findByReferenceCode(2))
                            : referenceTypeRepository.findByReferenceCode(4));
                    userObj = userRepository.save(userObj);
                    activationKey = tokenProvider.accountActivationToken(userObj);
                    userObj.setAccountActivationKey(activationKey);
                    userRepository.save(userObj);
                    try {
                        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request
                                .getServerPort() + request.getContextPath());
                        url = url + "/customer/activate/account/" + activationKey;
                        mailNotification.sendMailToCustomer(userObj, url, referrers.getReferrerCode().toString(), !customerRegistrationDTO.getAccountType().equalsIgnoreCase("1"));
                        return ResponseDomain.postResponse(customerRegistrationDTO.getAccountType().equalsIgnoreCase("1") ?
                                messageProperties.getCustomerCreated() : messageProperties.getTestCustomer());
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        logger.error("method ::: addCustomer error ::: " + ex.getMessage());
                        return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                    }
                }
            } else
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> p2pDistributorSelfRegister(CustomerRegistrationDTO customerRegistrationDTO,
                                                        HttpServletRequest request) {
        logger.info("method ::: p2pDistributorSelfRegister");
        User user = userRepository.findByEmailAddressIgnoreCase(customerRegistrationDTO.getEmail());
        Referrers referrersObj = null;
        if (customerRegistrationDTO.getReferralCode() != null) {
            referrersObj = referrerRepository.findByReferrerCode(customerRegistrationDTO.getReferralCode());
            if (referrersObj == null)
                return ResponseDomain.badRequest("Invalid Referrer Code");
        }
        if (user == null) {
            User userObj = customerRegistrationDTO.customerRegistrationDTOMapper(customerRegistrationDTO);
            userObj.setIsP2PYEA(Boolean.FALSE);
            userObj.setIsEligibleForPromotion(Boolean.FALSE);
            userObj.setRole(userRoleRepository.findByRoleCode(1));
            userObj.setUserName(customerRegistrationDTO.getUserName());
            userObj.setPassword(passwordEncoder.encode(customerRegistrationDTO.getPassword()));
            userObj.setUserStatus(userStatusRepository.findByStatusCode(4));
            String responseMessage = "";
            userObj.setReferenceType(referenceTypeRepository.findByReferenceCode(6));
            userObj.setReferrerCode((customerRegistrationDTO.getReferralCode() == null) ? 1000 : customerRegistrationDTO.getReferralCode());
            responseMessage = messageProperties.getP2PDistributor();
            Referrers referrer = new Referrers();
            referrer.setUser(userObj);
            referrer.setReferrerType("REFERRER");
            List<Referrers> referrersListEO = referrerRepository.findAllByOrderByReferrerCodeAsc();
            Referrers referrersLastIndex = referrersListEO.isEmpty() ? null : referrersListEO.get(referrersListEO.size() - 1);
            referrer.setReferrerCode(referrersLastIndex == null ? 1000l : referrersLastIndex.getReferrerCode() + 1);
            userObj.setRole(roleRepository.findByRoleCode(2));
            userObj.setUserStatus(userStatusRepository.findByStatusCode(1));
            userRepository.save(userObj);
            referrerRepository.save(referrer);
            try {
                mailNotification.accountActivationMailToCustomerRep(userObj, "", userObj.getReferrerCode().toString(), "P2P Distributor");
                return ResponseDomain.postResponse(responseMessage);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                logger.error("method ::: addReferrer error ::: " + ex.getMessage());
                return ResponseDomain.internalServerError(ex.getLocalizedMessage());
            }
        }
        return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> addReferrer(String auth, CustomerRegistrationDTO customerRegistrationDTO,
                                         HttpServletRequest request) {
        logger.info("method ::: addReferrer");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (customerRegistrationDTO.getReferralCode() != null) {
            Referrers referrersObj = referrerRepository.findByReferrerCode(customerRegistrationDTO.getReferralCode());
            if (referrersObj == null)
                return ResponseDomain.badRequest("Invalid Referrer Code");
        }
        if (user != null) {
            Referrers referrers = referrerRepository.findByUser(user);
            if (referrers != null && (referrers.getReferrerType().equalsIgnoreCase("ADMIN")
                    || referrers.getReferrerType().equalsIgnoreCase("SUPER_ADMIN"))) {
                User userEO = userRepository.findByEmailAddressIgnoreCase(customerRegistrationDTO.getEmail());
                if (userEO != null)
                    return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
                else {
                    User userObj = customerRegistrationDTO.customerRegistrationDTOMapper(customerRegistrationDTO);
                    String activationKey;
                    userObj.setIsP2PYEA(Boolean.FALSE);
                    userObj.setIsEligibleForPromotion(Boolean.FALSE);
                    userObj.setRole(userRoleRepository.findByRoleCode(1));
                    userObj.setUserStatus(userStatusRepository.findByStatusCode(4));
                    userObj.setReferrerCode(referrers.getReferrerCode());
                    String responseMessage = "";
                    switch (customerRegistrationDTO.getAccountType()) {
                        case "1":
                            userObj.setReferenceType(referenceTypeRepository.findByReferenceCode(1));
                            responseMessage = messageProperties.getCustomerCreated();
                            break;
                        case "2":
                            userObj.setReferenceType(referenceTypeRepository.findByReferenceCode(3));
                            responseMessage = messageProperties.getTestCustomerRep();
                            break;
                        case "3":
                            userObj.setReferenceType(referenceTypeRepository.findByReferenceCode(6));
                            userObj.setReferrerCode((customerRegistrationDTO.getReferralCode() == null) ? 1000 : customerRegistrationDTO.getReferralCode());
                            responseMessage = messageProperties.getP2PDistributor();
                            break;
                        default:
                            break;
                    }
                    userObj = userRepository.save(userObj);
                    activationKey = tokenProvider.accountActivationToken(userObj);
                    userObj.setAccountActivationKey(activationKey);
                    userRepository.save(userObj);
                    try {
                        String url = (request.getScheme() + "://" + request.getServerName() + ":" + request
                                .getServerPort() + request.getContextPath());
                        url = url + "/customer/activate/account/" + activationKey;
                        switch (customerRegistrationDTO.getAccountType()) {
                            case "1":
                                mailNotification.accountActivationMailToCustomerRep(userObj, url, referrers.getReferrerCode().toString(), "Paid");
                                break;
                            case "2":
                                mailNotification.accountActivationMailToCustomerRep(userObj, url, referrers.getReferrerCode().toString(), "Free");
                                break;
                            case "3":
                                mailNotification.accountActivationMailToCustomerRep(userObj, url, referrers.getReferrerCode().toString(), "P2P Distributor");
                                break;
                            default:
                                break;
                        }
                        return ResponseDomain.postResponse(responseMessage);
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        logger.error("method ::: addReferrer error ::: " + ex.getMessage());
                        return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                    }
                }
            } else
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    /**
     * @param auth
     * @param userId
     * @param action
     * @return
     */
    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "admin.customers", allEntries = true),
                    @CacheEvict(value = "admin.customer-reps", allEntries = true),
                    @CacheEvict(value = "customer-rep.customers", allEntries = true),
                    @CacheEvict(value = "money.flow.user.role", allEntries = true),
                    @CacheEvict(value = "money.flow.user.personalInfo", allEntries = true),
                    @CacheEvict(value = "money.flow.user.role", allEntries = true),
                    @CacheEvict(value = "admin.users-details", allEntries = true),
                    @CacheEvict(value = "admin.all-customer", allEntries = true)
            }
    )
    public ResponseEntity<?> updateUserStatus(String auth, Long userId, String action, HttpServletRequest request) {
        User admin = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        User user = userRepository.findByUserRegistrationId(userId);
        String requestUrl = request.getRequestURL().toString();

        if (user == null)
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        else if ((admin.getRole().getRoleCode() != 4) && (admin.getRole().getRoleCode() != 5))
            return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        else {
            if (action.equalsIgnoreCase("activate")) {
                if (user.getUserStatus().getStatusCode() != 1) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    userRepository.save(user);
                    return (requestUrl.contains("/my-customer")) ? ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate())
                            : ResponseDomain.putResponse(messageProperties.getCustomerRepAccountUpdate());
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyActive());
            } else if (action.equalsIgnoreCase("deactivate")) {
                if (user.getUserStatus().getStatusCode() != 2) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(2));
                    userRepository.save(user);
                    return (requestUrl.contains("/my-customer")) ? ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate())
                            : ResponseDomain.putResponse(messageProperties.getCustomerRepAccountUpdate());
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyInActive());
            } else if (action.equalsIgnoreCase("Archive")) {
                if (user.getUserStatus().getStatusCode() != 3) {
                    if (requestUrl.contains("/my-customer")) {
                        Customers customer = customersRepository.findByUser(user);
                        if (customer != null) {
                            customersRepository.delete(customer);
                            List<SubscriptionDetails> userSubscriptions = subscriptionRepository
                                    .findAllByUserOrderBySubscriptionIdDesc(customer.getUser()).stream()
                                    .filter(subscriptionDetails -> subscriptionDetails.getStatus().equalsIgnoreCase("Active")).collect(Collectors.toList());
                            userSubscriptions.stream().map(subscriptionDetails -> {
                                Subscription sub = new Subscription();
                                try {
                                    if (subscriptionDetails.getStripeSubscriptionId().startsWith("sub")) {
                                        sub = Subscription.retrieve(subscriptionDetails
                                                .getStripeSubscriptionId());
                                        sub = sub.cancel(null);
                                    }
                                    subscriptionDetails.setStatus("Cancelled");
                                    subscriptionRepository.save(subscriptionDetails);
                                    return subscriptionDetails;
                                } catch (StripeException e) {
                                    logger.error(e.getMessage(), e);
                                    logger.error("method ::: updateUserStatus ::: error ::: " + e.getMessage());
                                    return null;
                                }
                            }).collect(Collectors.toList());
                        }
                        user.setUserStatus(userStatusRepository.findByStatusCode(3));
                        user.setRole(userRoleRepository.findByRoleCode(1));
                        user.setReferenceType(referenceTypeRepository.findByReferenceCode(5));
                        Referrers referrer1 = referrerRepository.findByUser(user);
                        if (referrer1 != null) {
                            List<User> users = userRepository.findAllByReferrerCode(referrer1.getReferrerCode());
                            if (!users.isEmpty()) {
                                users.forEach(user1 -> {
                                    user1.setReferrerCode(1000l);
                                });
                            }
                            userRepository.saveAll(users);
                            List<Customers> customersList = customersRepository.findAllByReferrers(referrer1);
                            if (!customersList.isEmpty()) {
                                Referrers superAdminRef = referrerRepository.findByReferrerCode(1000l);
                                customersList.forEach(customers -> {
                                    customers.setReferrers(superAdminRef);
                                });
                                customersRepository.saveAll(customersList);
                            }
                            referrerRepository.delete(referrer1);
                        }
                        userRepository.save(user);
                        return ResponseDomain.putResponse(messageProperties.getCustomerAccountUpdate());
                    } else {
                        Referrers referrer = referrerRepository.findByUser(user);
                        user.setRole(userRoleRepository.findByRoleCode(1));
                        user.setUserStatus(userStatusRepository.findByStatusCode(3));
                        user.setReferenceType(referenceTypeRepository.findByReferenceCode(5));
                        List<User> users = new ArrayList<>();
                        List<Customers> customersList;
                        if (referrer != null) {
                            users = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
                            if (!users.isEmpty()) {
                                users.forEach(user1 -> {
                                    user1.setReferrerCode(1000l);
                                });
                            }
                            customersList = customersRepository.findAllByReferrers(referrer);
                            if (!customersList.isEmpty()) {
                                Referrers superAdminRef = referrerRepository.findByReferrerCode(1000l);
                                customersList.forEach(customers -> {
                                    customers.setReferrers(superAdminRef);
                                });
                                customersRepository.saveAll(customersList);
                            }
                        }
                        users.add(user);
                        List<User> userList = userRepository.saveAll(users);
                        if (referrer != null) {
                            referrerRepository.delete(referrer);
                            List<SubscriptionDetails> userSubscriptions = subscriptionRepository
                                    .findAllByUserOrderBySubscriptionIdDesc(referrer.getUser()).stream().filter(subscriptionDetails ->
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active")).collect(Collectors.toList());
                            userSubscriptions.stream().map(subscriptionDetails -> {
                                Subscription sub = new Subscription();
                                try {
                                    if (subscriptionDetails.getStripeSubscriptionId().startsWith("sub")) {
                                        sub = Subscription.retrieve(subscriptionDetails
                                                .getStripeSubscriptionId());
                                        sub = sub.cancel(null);
                                    }
                                    subscriptionDetails.setStatus("Cancelled");
                                    return subscriptionDetails;
                                } catch (StripeException e) {
                                    logger.error(e.getMessage(), e);
                                    logger.error("method ::: updateUserStatus ::: error ::: " + e.getMessage());
                                    return null;
                                }
                            }).collect(Collectors.toList());
                            subscriptionRepository.saveAll(userSubscriptions);
                            Customers customer = customersRepository.findByUser(referrer.getUser());
                            if (customer != null)
                                customersRepository.delete(customer);
                        }
                        return ResponseDomain.putResponse(messageProperties.getCustomerRepAccountUpdate());
                    }
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyArchived());
            } else
                return ResponseDomain.badRequest(messageProperties.getInvalidInput());
        }
    }


    @Override
    @Cacheable(value = "admin.customers", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> getMyCustomers(String auth) {
        logger.info("method ::: getMyCustomers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            Referrers referrers = referrerRepository.findByUser(user);
            if (referrers != null) {
                List<CustomerVO> customerVOList = new ArrayList<>();
                List<Customers> customersList = customersRepository.findAllByReferrers(referrers);
                if (!(customersList.isEmpty())) {
                    customersList.forEach(customersObj -> {
                        CustomerVO customerVO = new CustomerVO();
                        User sponsor = referrerRepository.findByReferrerCode(customersObj.getUser().getReferrerCode()).getUser();
                        customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                        customerVO.setReferrerCode(customersObj.getUser().getReferrerCode().toString());
                        customerVO.setP2PYEA(customersObj.getUser().getIsP2PYEA());
                        customerVO.setUserId(customersObj.getUser().getUserRegistrationId());
                        customerVO.setName(customersObj.getUser().getFirstName() + (customersObj.getUser().getLastName()
                                != null ? " " + customersObj.getUser().getLastName() : ""));
                        customerVO.setEmail(customersObj.getUser().getEmailAddress());
                        customerVO.setContactNumber(customersObj.getUser().getContactNumber() != null
                                ? customersObj.getUser().getContactNumber() : "");
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(customersObj.getUser().getCreationDate()));
                        customerVO.setStatus(customersObj.getUser().getUserStatus().getStatusDescription());
                        customerVO.setAccountType(setUserAccountType(customersObj.getUser().getReferenceType().getReferenceCode()));
                        Integer roleCode = customersObj.getUser().getRole().getRoleCode();
                        customerVO.setUserType(roleCode == 3 ? "Customer" : "Customer-Rep");
                        List<User> aAuserList = userRepository.findAllByReferrerCode(customersObj.getUser().getReferrerCode());
                        aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        customerVO.setAppAmbassadorCount(aAuserList.size());
                        AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(customersObj.getUser().getUserRegistrationId());
                        if (p2pappAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(p2pappAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(p2pappAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(customersObj.getUser());
                            if (referrerObj != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        if (customerVO.getUserType().equals("Customer-Rep")) {
                            Referrers referrerOfUser = referrerRepository.findByUser(customersObj.getUser());
                            if (referrerOfUser != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerOfUser.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                customerCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                customerRepCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                            }
                        }
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVO.setP2PYEACount(String.valueOf(0));
                        Referrers referrerObj = referrerRepository.findByUser(customersObj.getUser());
                        if (referrerObj != null) {
                            long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        }
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        customerVO.setTeamSizeCount(String.valueOf(0));
                        customerVO.setIsAppAmbassador(false);
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(customersObj.getUser().getUserRegistrationId());
                        if (appAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                        }
                        customerVOList.add(customerVO);
                    });
                }
                List<User> userList = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream()
                        .filter(user1 -> ((user1.getReferenceType().getReferenceCode() == 6) || user1.getRole().getRoleCode() == 1 && (user1.getUserStatus().getStatusCode
                                () == 3 || user1.getUserStatus().getStatusCode() == 4 || user1.getUserStatus()
                                .getStatusCode() == 5)
                                && (user1.getReferenceType().getReferenceCode() == 2 || user1.getReferenceType()
                                .getReferenceCode() == 4 || user1.getReferenceType().getReferenceCode() == 5)))
                        .collect(Collectors.toList());
                if (!userList.isEmpty()) {
                    userList.forEach(userObj -> {
                        CustomerVO customerVO = new CustomerVO();
                        User sponsor = referrerRepository.findByReferrerCode(userObj.getReferrerCode()).getUser();
                        customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                        customerVO.setReferrerCode(userObj.getReferrerCode().toString());
                        customerVO.setP2PYEA(userObj.getIsP2PYEA());
                        customerVO.setUserId(userObj.getUserRegistrationId());
                        customerVO.setName(userObj.getFirstName() + (userObj.getLastName()
                                != null ? " " + userObj.getLastName() : ""));
                        customerVO.setEmail(userObj.getEmailAddress());
                        customerVO.setContactNumber(userObj.getContactNumber() != null
                                ? userObj.getContactNumber() : "");
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(userObj.getCreationDate()));
                        customerVO.setStatus(userObj.getUserStatus().getStatusDescription());
                        customerVO.setAccountType(setUserAccountType(userObj.getReferenceType().getReferenceCode()));
                        Integer referenceCode = userObj.getReferenceType().getReferenceCode();
//                        String userType = "";
//                        if (referenceCode == 1 || referenceCode == 3)
//                            userType = "Customer-Rep";
//                        else
//                            userType = "Customer";
//                        if (referenceCode == 6)
//                            userType = "P2P Distributor";
//
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
//                        customerVO.setUserType(userType);
                        Integer roleCode = userObj.getRole().getRoleCode();
                        List<User> aAuserList = userRepository.findAllByReferrerCode(userObj.getReferrerCode());
                        aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        customerVO.setAppAmbassadorCount(aAuserList.size());
                        customerVO.setUserType((roleCode == 3 ? "Customer" : (userObj.getReferenceType().getReferenceCode() == 6 ? "P2P Distributor" : "Customer-Rep")));
                        if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {
                            Referrers referrer = referrerRepository.findByUser(userObj);
                            if (referrer == null) {
                                customerCount = "N.A [ACTIVATION PENDING]";
                            } else {
                                List<User> userListObj = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                ;
                                customerCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                customerRepCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                            }
                        }
                        AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                        if (p2pappAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(p2pappAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(p2pappAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(userObj);
                            if (referrerObj != null) {
                                List<User> referredUserList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                referredUserList = referredUserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(referredUserList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
//                        customerVO.setUserType(userType);
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVO.setP2PYEACount(String.valueOf(0));
                        Referrers referrerObj = referrerRepository.findByUser(userObj);
                        if (referrerObj != null) {
                            long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        }
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        customerVO.setTeamSizeCount(String.valueOf(0));
                        customerVO.setIsAppAmbassador(false);
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                        if (appAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                        }
                        customerVOList.add(customerVO);
                    });
                }
                return new ResponseEntity<>(customerVOList, HttpStatus.OK);
            } else {
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }


    @Override
    public ResponseEntity<?> resendActivationMailToUser(String auth, Long userId, HttpServletRequest request) {
        logger.info("method ::: resendActivationMailToUser");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrersEO = referrerRepository.findByUser(user);
            if (referrersEO != null || user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
                User customerUserEO = userRepository.findByUserRegistrationId(userId);
                if (customerUserEO.getUserStatus().getStatusCode() == 4) {
                    String jwt = tokenProvider.accountActivationToken(customerUserEO);
                    customerUserEO.setAccountActivationKey(jwt);
                    customerUserEO = userRepository.save(customerUserEO);
                    if (customerUserEO != null) {
                        try {
                            String url = (request.getScheme() + "://" + request.getServerName() + ":" + request
                                    .getServerPort() + request.getContextPath());
                            if (customerUserEO.getUserName() == null && customerUserEO.getRole().getRoleCode() != 4) {
                                url = url + "/customer/activate/account/" + jwt;
                                mailNotification.sendMailToCustomer(customerUserEO, url, referrersEO.getReferrerCode
                                        ().toString(), customerUserEO.getReferenceType().getReferenceCode() == 4);
                            } else {
                                url = url + "user/activate/account" + jwt;
                                mailNotification.userAccountVerification(url, "registration", customerUserEO);
                            }
                            return ResponseDomain.postResponse(messageProperties.getCustomerResendActivationMail());
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                            logger.error("method ::: resendActivationMailToUser ::: Error ::: " + ex.getMessage());
                            return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                        }
                    } else
                        return ResponseDomain.internalServerError();
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyActive());
            } else
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @Cacheable(value = "super-admin.admins", key = "#root.methodName", sync = true)
    public ResponseEntity<?> getAllAdmins() {
        logger.info("method ::: getAllAdmins");
        List<User> adminList = userRepository.findAllByRole(userRoleRepository.findByRoleCode(4));
        List<CustomerVO> customerVOList = new ArrayList<>();
        if (adminList != null) {
            adminList.forEach(admin -> {
                CustomerVO customerVO = new CustomerVO();
                customerVO.setContactNumber(String.valueOf(admin.getContactNumber()));
                customerVO.setName(admin.getFirstName() + " " + (admin.getLastName() != null ? admin.getLastName() :
                        ""));
                customerVO.setEmail(admin.getEmailAddress());
                customerVO.setStatus(admin.getUserStatus().getStatusDescription());
                customerVO.setUserId(admin.getUserRegistrationId());
                customerVOList.add(customerVO);
            });
        }
        return new ResponseEntity<Object>(customerVOList, HttpStatus.OK);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "super-admin.admins", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true)
    })
    public ResponseEntity<?> addAdmin(String auth, CustomerRegistrationDTO customerRegistrationDTO, String url) {
        logger.info("method ::: addAdmin");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            if (user.getRole().getRoleCode() == 5) {
                Referrers referrers = referrerRepository.findByUser(user);
                User userEO = userRepository.findByEmailAddressIgnoreCase(customerRegistrationDTO.getEmail());
                if (userEO != null)
                    return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
                else {
                    User userObj = customerRegistrationDTO.customerRegistrationDTOMapper(customerRegistrationDTO);
                    userObj.setIsP2PYEA(Boolean.FALSE);
                    userObj.setIsEligibleForPromotion(Boolean.FALSE);
                    userObj.setRole(userRoleRepository.findByRoleCode(4));
                    userObj.setUserStatus(userStatusRepository.findByStatusCode(4));
//                    userObj.setReferrerCode(referrers.getReferrerCode());
                    userObj.setReferenceType(referenceTypeRepository.findByReferenceCode(7));
//                    userObj.setUserName(userObj.getFirstName());
//                    String password = UUID.randomUUID().toString().substring(0, 8);
//                    userObj.setPassword(passwordEncoder.encode(password));
                    userObj = userRepository.saveAndFlush(userObj);
                    String jwt = tokenProvider.accountActivationToken(userObj);
                    userObj.setAccountActivationKey(jwt);
                    url = url + "/customer/activate/account/" + jwt;
                    userRepository.save(userObj);
//                    Referrers referrer = new Referrers();
//                    referrer.setUser(userObj);
//                    referrer.setReferrerType("ADMIN");
//                    List<Referrers> referrersListEO = referrerRepository.findAll();
//                    Referrers referrersLastIndex = referrersListEO.isEmpty() ? null : referrersListEO.get
//                            (referrersListEO.size() - 1);
//                    referrer.setReferrerCode(referrersLastIndex == null ? 1000l : referrersLastIndex.getReferrerCode
//                            () + 1);
//                    referrerRepository.save(referrer);
                    try {
                        mailNotification.sendMailToNewlyCreateAdmin(userObj, url);
                        return ResponseDomain.postResponse(messageProperties.getAdminAdded());
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        logger.error("method :::  addAdmin ::: error ::: " + ex.getMessage());
                        return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                    }
                }
            } else
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "super-admin.admins", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
    })
    public ResponseEntity<?> updateAdmin(String auth, String id, String action) {
        logger.info("method ::: updateAdmin");
        User admin = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        User user = userRepository.findByUserRegistrationId(Long.valueOf(id));
        if (user == null)
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        else if (admin.getRole().getRoleCode() != 5)
            return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        else {
            if (action.equalsIgnoreCase("activate")) {
                if (user.getUserStatus().getStatusCode() != 1) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(1));
                    userRepository.save(user);
                    return ResponseDomain.putResponse(messageProperties.getAdminUpdate());
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyActive());
            } else if (action.equalsIgnoreCase("deactivate")) {
                if (user.getUserStatus().getStatusCode() != 2) {
                    user.setUserStatus(userStatusRepository.findByStatusCode(2));
                    userRepository.save(user);
                    return ResponseDomain.putResponse(messageProperties.getAdminUpdate());
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyInActive());
            } else if (action.equalsIgnoreCase("Archive")) {
                if (user.getUserStatus().getStatusCode() != 3) {
                    Referrers referrer = referrerRepository.findByUser(user);
                    user.setRole(userRoleRepository.findByRoleCode(1));
                    user.setUserStatus(userStatusRepository.findByStatusCode(3));
                    List<User> users = new ArrayList<>();
                    List<Customers> customersList;
                    if (referrer != null) {
                        users = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
                        if (!users.isEmpty()) {
                            users.forEach(user1 -> {
                                user1.setReferrerCode(1000l);
                            });
                        }
                        customersList = customersRepository.findAllByReferrers(referrer);
                        if (!customersList.isEmpty()) {
                            Referrers superAdminRef = referrerRepository.findByReferrerCode(1000l);
                            customersList.forEach(customers -> {
                                customers.setReferrers(superAdminRef);
                            });
                            customersRepository.saveAll(customersList);
                        }

                    }
                    users.add(user);
                    userRepository.saveAll(users);
                    if (referrer != null)
                        referrerRepository.delete(referrer);
                    return ResponseDomain.putResponse(messageProperties.getAdminUpdate());

                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyArchived());
            } else
                return ResponseDomain.badRequest(messageProperties.getInvalidInput());
        }
    }

    @Override
    @Cacheable(value = "money.flow.guest.users", key = "#root.methodName")
    public ResponseEntity<?> getAllGuestUsers() {
        logger.info("method ::: getAllGuestUsers");
        List<GuestUser> guestUserList = guestUserRepository.findAll();
        List<JSONObject> guestVOList = new ArrayList<>();
        if (guestUserList != null) {
            guestUserList.forEach(guest -> {
                JSONObject guestVo = new JSONObject();
                guestVo.put("name", guest.getName());
                guestVo.put("email", guest.getEmailId());
                guestVo.put("joiningDate", CustomDateConverter.dateToString(guest.getCreationDate()));
                guestVOList.add(guestVo);
            });
        }
        return new ResponseEntity<Object>(guestVOList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> report(String auth) {
        logger.info("method ::: report");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user == null) {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        } else {
            JSONObject jsonObjectList = new JSONObject();
            JSONObject jsonObjectCustomer = new JSONObject();
            JSONObject jsonObjectDistributors = new JSONObject();
            JSONObject jsonObjectGuestUser = new JSONObject();
            JSONObject jsonObjectAppAmbassadorPlusEligibleMembers = new JSONObject();
            JSONObject jsonObjectP2PYoungEntrepreneurs = new JSONObject();
            List<Customers> customersList = customersRepository.findAll().stream()
                    .filter(customers -> customers.getUser().getRole().getRoleCode() == 3)
                    .filter(customers -> customers.getUser().getUserStatus().getStatusCode() == 1l)
                    .collect(Collectors.toList());
            Map<String, Integer> customerMappedObject = checkCountingCustomerList(customersList);
            jsonObjectCustomer.put("today", customerMappedObject.get("currentDateCustomer"));
            jsonObjectCustomer.put("yesterday", customerMappedObject.get("previousDateCustomer"));
            jsonObjectCustomer.put("thisWeek", customerMappedObject.get("currentWeekCustomer"));
            jsonObjectCustomer.put("lastWeek", customerMappedObject.get("previousWeekCustomer"));
            jsonObjectCustomer.put("thisMonth", customerMappedObject.get("currentMonthCustomer"));
            jsonObjectCustomer.put("lastMonth", customerMappedObject.get("previousMonthCustomer"));
            jsonObjectCustomer.put("thisYear", customerMappedObject.get("currentYearCustomer"));
            jsonObjectCustomer.put("lastYear", customerMappedObject.get("previousYearCustomer"));
            jsonObjectCustomer.put("totalCustomers", customersList.size());


            List<Referrers> referrersList = referrerRepository.findAll().stream().filter(referrers ->
                    referrers.getReferrerType().equalsIgnoreCase("CUSTOMER_REP"))
                    .filter(referrers -> referrers.getUser().getUserStatus().getStatusCode() == 1l)
                    .filter(referrers -> !referrers.getUser().getIsP2PYEA())
                    .collect(Collectors.toList());

            List<SubscriptionDetails> activeMonthlySubscription = subscriptionRepository.findAll().stream().filter(subscriptionDetails ->
                    (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                            subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                    (Collectors.toList());
            HashMap<Long, SubscriptionDetails> subscriptionDetailsHashMap = new HashMap<>();
            activeMonthlySubscription.forEach(subscriptionDetails -> {
                if (subscriptionDetailsHashMap.get(subscriptionDetails.getUser().getUserRegistrationId()) == null) {
                    subscriptionDetailsHashMap.put(subscriptionDetails.getUser().getUserRegistrationId(), subscriptionDetails);
                }
            });
            referrersList = referrersList.stream().filter(referrers -> subscriptionDetailsHashMap.containsKey(referrers.getUser().getUserRegistrationId())).collect(Collectors.toList());


            Map<String, Integer> referrerMappedObject = checkCountingCustomerRapsList(referrersList);
            jsonObjectDistributors.put("today", referrerMappedObject.get("currentDateCustomerRaps"));
            jsonObjectDistributors.put("yesterday", referrerMappedObject.get("previousDateCustomerRaps"));
            jsonObjectDistributors.put("thisWeek", referrerMappedObject.get("currentWeekCustomerRaps"));
            jsonObjectDistributors.put("lastWeek", referrerMappedObject.get("previousWeekCustomerRaps"));
            jsonObjectDistributors.put("thisMonth", referrerMappedObject.get("currentMonthCustomerRaps"));
            jsonObjectDistributors.put("lastMonth", referrerMappedObject.get("previousMonthCustomerRaps"));
            jsonObjectDistributors.put("thisYear", referrerMappedObject.get("currentYearCustomerRaps"));
            jsonObjectDistributors.put("lastYear", referrerMappedObject.get("previousYearCustomerRaps"));
            jsonObjectDistributors.put("totalCustomersReps", referrersList.size());

            List<GuestUser> guestUserList = guestUserRepository.findAll();
            Map<String, Integer> guestUserMappedObject = checkCountingGuestList(guestUserList);
            jsonObjectGuestUser.put("today", guestUserMappedObject.get("currentDateGuestUser"));
            jsonObjectGuestUser.put("yesterday", guestUserMappedObject.get("previousDateGuestUser"));
            jsonObjectGuestUser.put("thisWeek", guestUserMappedObject.get("currentWeekGuestUser"));
            jsonObjectGuestUser.put("lastWeek", guestUserMappedObject.get("previousWeekGuestUser"));
            jsonObjectGuestUser.put("thisMonth", guestUserMappedObject.get("currentMonthGuestUser"));
            jsonObjectGuestUser.put("lastMonth", guestUserMappedObject.get("previousMonthGuestUser"));
            jsonObjectGuestUser.put("thisYear", guestUserMappedObject.get("currentYearGuestUser"));
            jsonObjectGuestUser.put("lastYear", guestUserMappedObject.get("previousYearGuestUser"));
            jsonObjectGuestUser.put("totalProspectUsers", guestUserList.size());

            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            Map<String, Integer> appAmbassadorPlusEligibleMembersMappedObject = checkCountingAppAmbassadorPlusEligibleMembersListList(appAmbassadorPlusEligibleMembersList);
            jsonObjectAppAmbassadorPlusEligibleMembers.put("today", appAmbassadorPlusEligibleMembersMappedObject.get("currentDateAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("yesterday", appAmbassadorPlusEligibleMembersMappedObject.get("previousDateAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("thisWeek", appAmbassadorPlusEligibleMembersMappedObject.get("currentWeekAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("lastWeek", appAmbassadorPlusEligibleMembersMappedObject.get("previousWeekAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("thisMonth", appAmbassadorPlusEligibleMembersMappedObject.get("currentMonthAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("lastMonth", appAmbassadorPlusEligibleMembersMappedObject.get("previousMonthAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("thisYear", appAmbassadorPlusEligibleMembersMappedObject.get("currentYearAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("lastYear", appAmbassadorPlusEligibleMembersMappedObject.get("previousYearAppAmbassadorPlusEligibleMembers"));
            jsonObjectAppAmbassadorPlusEligibleMembers.put("totalAppAmbassadorPlusEligibleMembers", appAmbassadorPlusEligibleMembersList.size());


            List<User> userList = userRepository.findAll().stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).collect(Collectors.toList());
            Map<String, Integer> P2PYoungEntrepreneursList = checkCountingP2PYoungEntrepreneurs(userList);
            jsonObjectP2PYoungEntrepreneurs.put("today", P2PYoungEntrepreneursList.get("currentDateP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("yesterday", P2PYoungEntrepreneursList.get("previousDateP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("thisWeek", P2PYoungEntrepreneursList.get("currentWeekP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("lastWeek", P2PYoungEntrepreneursList.get("previousWeekP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("thisMonth", P2PYoungEntrepreneursList.get("currentMonthP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("lastMonth", P2PYoungEntrepreneursList.get("previousMonthP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("thisYear", P2PYoungEntrepreneursList.get("currentYearP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("lastYear", P2PYoungEntrepreneursList.get("previousYearP2PYoungEntrepreneurs"));
            jsonObjectP2PYoungEntrepreneurs.put("totalP2PYoungEntrepreneurs", userList.size());


            Referrers referrers = referrerRepository.findByUser(user);

            LocalDate localDate = LocalDate.now();

            LocalDate localDatePrev = localDate.minusYears(1);
            List previousYearCommission = new ArrayList<>();
            for (int i = 1; i <= 12; i++) {
                LocalDate localDateMap = LocalDate.of(localDatePrev.getYear(), i, localDatePrev.withMonth(i).lengthOfMonth());
                CommissionRecord record = commissionRecordRepository.findByReferrersAndCommissionCalculateToDate(referrers, localDateMap);
                List<CommissionPerCustomer> commissionPerCustomers = commissionPerCustomerRepository.findAllByCommissionRecord(record);
                int commissionAmount = commissionPerCustomers.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();
                previousYearCommission.add(commissionAmount);
            }
            List currentYearCommission = new ArrayList<>();
            for (int i = 1; i < localDate.getMonthValue(); i++) {
                LocalDate localDateMap1 = LocalDate.of(localDate.getYear(), i, localDate.withMonth(i).lengthOfMonth());
                CommissionRecord record1 = commissionRecordRepository.findByReferrersAndCommissionCalculateToDate(referrers, localDateMap1);
                List<CommissionPerCustomer> commissionPerCustomers1 = commissionPerCustomerRepository.findAllByCommissionRecord(record1);
                int commissionAmount1 = commissionPerCustomers1.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();
                currentYearCommission.add(commissionAmount1);
            }

            JSONObject yearlyCommissionAmountBreakUp = new JSONObject();
            yearlyCommissionAmountBreakUp.put("previousYearMonthlyCommisssion", previousYearCommission);
            yearlyCommissionAmountBreakUp.put("thisYearMonthlyCommisssion", currentYearCommission);

            LocalDate localDatePrevFrom = LocalDate.of(localDatePrev.getYear(), 1, 1);
            LocalDate localDatePrevTo = LocalDate.of(localDatePrev.getYear(), 12, 31);
            List<CommissionRecord> records = commissionRecordRepository.findAllByReferrersAndCommissionCalculateToDateBetween(referrers, localDatePrevFrom, localDatePrevTo);
            List<CommissionPerCustomer> commissionPerCustomers = commissionPerCustomerRepository.findAllByCommissionRecordIn(records);
            int commissionAmount = commissionPerCustomers.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();

            LocalDate localDateFrom = LocalDate.of(localDate.getYear(), 1, 1);
            List<CommissionRecord> records1 = commissionRecordRepository.findAllByReferrersAndCommissionCalculateToDateBetween(referrers, localDateFrom, localDate);
            List<CommissionPerCustomer> commissionPerCustomers1 = commissionPerCustomerRepository.findAllByCommissionRecordIn(records1);
            int commissionAmount1 = commissionPerCustomers1.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();

            JSONObject yearlyCommission = new JSONObject();
            yearlyCommission.put("previousYearCommission", commissionAmount);
            yearlyCommission.put("thisYearCommission", commissionAmount1);

            LocalDate localDateNew = localDate.minusMonths(1);
            LocalDate localDateNew3 = localDateNew.plusDays(localDateNew.lengthOfMonth() - localDateNew.getDayOfMonth());
            CommissionRecord record = commissionRecordRepository.findByReferrersAndCommissionCalculateToDate(referrers, localDateNew3);
            List<CommissionPerCustomer> commissionPerCustomersList = commissionPerCustomerRepository.findAllByCommissionRecord(record);
            int previousMonthCommission = commissionPerCustomersList.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();

            LocalDate localDateNew1 = localDate.minusMonths(2);
            LocalDate localDateNew4 = localDateNew1.plusDays(localDateNew1.lengthOfMonth() - localDateNew1.getDayOfMonth());
            CommissionRecord record1 = commissionRecordRepository.findByReferrersAndCommissionCalculateToDate(referrers, localDateNew4);
            List<CommissionPerCustomer> commissionPerCustomers1List = commissionPerCustomerRepository.findAllByCommissionRecord(record1);
            int monthBeforePreviousMonthCommission = commissionPerCustomers1List.stream().mapToInt(CommissionPerCustomer::getCommissionAmount).sum();

            JSONObject monthlyCommission = new JSONObject();
            monthlyCommission.put("previousMonthCommission", previousMonthCommission);
            monthlyCommission.put("monthBeforePreviousMonthCommission", monthBeforePreviousMonthCommission);


            jsonObjectList.put("customers", jsonObjectCustomer);
            jsonObjectList.put("customerReps", jsonObjectDistributors);
            jsonObjectList.put("prospectUsers", jsonObjectGuestUser);
            jsonObjectList.put("appAmbassadorPlusEligibleMembers", jsonObjectAppAmbassadorPlusEligibleMembers);
            jsonObjectList.put("p2PYoungEntrepreneurs", jsonObjectP2PYoungEntrepreneurs);
            jsonObjectList.put("yearlyCommissionAmountBreakUp", yearlyCommissionAmountBreakUp);
            jsonObjectList.put("yearlyCommission", yearlyCommission);
            jsonObjectList.put("monthlyCommission", monthlyCommission);

            if (customersList.isEmpty())
                return new ResponseEntity<>(jsonObjectList, HttpStatus.OK);
            else {
                return new ResponseEntity<>(jsonObjectList, HttpStatus.OK);
            }
        }
    }


    public Map<String, Integer> checkCountingCustomerList(List<Customers> customersList) {
        logger.info("method ::: checkCountingCustomerList");
        Map<String, Integer> map = new HashMap<>();

        LocalDate currentDate = LocalDate.now();
        LocalDate previousDate = LocalDate.now().minusDays(1);
        LocalDate previousWeekEndDate = LocalDate.now().minusDays(6);
        LocalDate previousWeekStartDate = LocalDate.now().minusDays(12);
        Month currentMonth = LocalDate.now().getMonth();
        Month previousMonth = LocalDate.now().getMonth().minus(1);
        int currentYear = LocalDate.now().getYear();
        int previousYear = LocalDate.now().getYear() - 1;
        int currentDateCustomer = customersList.stream().filter(customers ->
                currentDate.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int previousDateCustomer = customersList.stream().filter(customers ->
                previousDate.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentWeekCustomer = customersList.stream().filter(customers ->
                ((customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (currentDate) ||
                        customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (currentDate))
                        && (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekEndDate) ||
                        customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekEndDate))
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int previousWeekCustomer = customersList.stream().filter(customers ->
                (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (previousWeekEndDate)
                        && (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekStartDate) ||
                        customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekStartDate))
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int currentMonthCustomer = customersList.stream().filter(customers ->
                currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int previousMonthCustomer = customersList.stream().filter(customers ->
                previousMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int currentYearCustomer = customersList.stream().filter(customers ->
                currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();


        int previousYearCustomer = customersList.stream().filter(customers ->
                previousYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();

        map.put("currentDateCustomer", currentDateCustomer);
        map.put("previousDateCustomer", previousDateCustomer);
        map.put("currentWeekCustomer", currentWeekCustomer);
        map.put("previousWeekCustomer", previousWeekCustomer);
        map.put("currentMonthCustomer", currentMonthCustomer);
        map.put("previousMonthCustomer", previousMonthCustomer);
        map.put("currentYearCustomer", currentYearCustomer);
        map.put("previousYearCustomer", previousYearCustomer);

        return map;
    }


    public Map<String, Integer> checkCountingCustomerRapsList(List<Referrers> referrersList) {
        logger.info("method ::: checkCountingCustomerRapsList");
        Map<String, Integer> map = new HashMap<>();

        LocalDate currentDate = LocalDate.now();
        LocalDate previousDate = LocalDate.now().minusDays(1);


        LocalDate previousWeekEndDate = LocalDate.now().minusDays(6);
        LocalDate previousWeekStartDate = LocalDate.now().minusDays(12);

        Month currentMonth = LocalDate.now().getMonth();
        Month previousMonth = LocalDate.now().getMonth().minus(1);

        int currentYear = LocalDate.now().getYear();
        int previousYear = LocalDate.now().getYear() - 1;


        int currentDateCustomerRaps = referrersList.stream().filter(customers ->
                currentDate.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int previousDateCustomerRaps = referrersList.stream().filter(customers ->
                previousDate.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentWeekCustomerRaps = referrersList.stream().filter(customers ->
                ((customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (currentDate) ||
                        customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (currentDate)) &&
                        (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter
                                (previousWeekEndDate) ||
                                customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                                        .isEqual(previousWeekEndDate))
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int previousWeekCustomerRaps = referrersList.stream().filter(customers ->
                (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (previousWeekEndDate)
                        && (customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekStartDate) ||
                        customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekStartDate))
                        && currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int currentMonthCustomerRaps = referrersList.stream().filter(customers ->
                currentMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int previousMonthCustomerRaps = referrersList.stream().filter(customers ->
                previousMonth.compareTo(customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();


        int currentYearCustomerRaps = referrersList.stream().filter(customers ->
                currentYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();


        int previousYearCustomerRaps = referrersList.stream().filter(customers ->
                previousYear == customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();

        map.put("currentDateCustomerRaps", currentDateCustomerRaps);
        map.put("previousDateCustomerRaps", previousDateCustomerRaps);

        map.put("currentWeekCustomerRaps", currentWeekCustomerRaps);
        map.put("previousWeekCustomerRaps", previousWeekCustomerRaps);

        map.put("currentMonthCustomerRaps", currentMonthCustomerRaps);
        map.put("previousMonthCustomerRaps", previousMonthCustomerRaps);

        map.put("currentYearCustomerRaps", currentYearCustomerRaps);
        map.put("previousYearCustomerRaps", previousYearCustomerRaps);

        return map;
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "super-admin.admins", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> updateEmailId(UpdateEmail updateEmail) {
        logger.info("method ::: updateEmailId");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(updateEmail.getUserId()));
        if (userRepository.existsByEmailAddress(updateEmail.getEmailId()))
            return ResponseDomain.badRequest(messageProperties.getEmailAlreadyExists());
        user.setEmailAddress(updateEmail.getEmailId());
        userRepository.save(user);
        return new ResponseEntity<>(messageProperties.getUpdateUserEmail(), HttpStatus.OK);
    }

    public Map<String, Integer> checkCountingGuestList(List<GuestUser> guestUserList) {
        logger.info("method ::: checkCountingGuestList");
        Map<String, Integer> map = new HashMap<>();

        LocalDate currentDate = LocalDate.now();
        LocalDate previousDate = LocalDate.now().minusDays(1);

        LocalDate previousWeekEndDate = LocalDate.now().minusDays(6);
        LocalDate previousWeekStartDate = LocalDate.now().minusDays(12);

        Month currentMonth = LocalDate.now().getMonth();
        Month previousMonth = LocalDate.now().getMonth().minus(1);

        int currentYear = LocalDate.now().getYear();
        int previousYear = LocalDate.now().getYear() - 1;

        int currentDateGuestUser = guestUserList.stream().filter(guestUser ->
                currentDate.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousDateGuestUser = guestUserList.stream().filter(guestUser ->
                previousDate.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentWeekGuestUser = guestUserList.stream().filter(guestUser ->
                ((guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (currentDate) ||
                        guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (currentDate)) &&
                        (guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter
                                (previousWeekEndDate) ||
                                guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                                        .isEqual(previousWeekEndDate))
                        && currentMonth.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int previousWeekGuestUser = guestUserList.stream().filter(guestUser ->
                (guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (previousWeekEndDate)
                        && (guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekStartDate) ||
                        guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekStartDate))
                        && currentMonth.compareTo(guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == guestUser.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int currentMonthGuestUser = guestUserList.stream().filter(guestUsers ->
                currentMonth.compareTo(guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousMonthGuestUser = guestUserList.stream().filter(guestUsers ->
                previousMonth.compareTo(guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentYearGuestUser = guestUserList.stream().filter(guestUsers ->
                currentYear == guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();


        int previousYearGuestUser = guestUserList.stream().filter(guestUsers ->
                previousYear == guestUsers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();

        map.put("currentDateGuestUser", currentDateGuestUser);
        map.put("previousDateGuestUser", previousDateGuestUser);

        map.put("currentWeekGuestUser", currentWeekGuestUser);
        map.put("previousWeekGuestUser", previousWeekGuestUser);

        map.put("currentMonthGuestUser", currentMonthGuestUser);
        map.put("previousMonthGuestUser", previousMonthGuestUser);

        map.put("currentYearGuestUser", currentYearGuestUser);
        map.put("previousYearGuestUser", previousYearGuestUser);

        return map;
    }

    public Map<String, Integer> checkCountingAppAmbassadorPlusEligibleMembersListList(List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusMembers) {
        logger.info("method ::: checkCountingAppAmbassadorPlusEligibleMembersListList");
        Map<String, Integer> map = new HashMap<>();

        LocalDate currentDate = LocalDate.now();
        LocalDate previousDate = LocalDate.now().minusDays(1);

        LocalDate previousWeekEndDate = LocalDate.now().minusDays(6);
        LocalDate previousWeekStartDate = LocalDate.now().minusDays(12);

        Month currentMonth = LocalDate.now().getMonth();
        Month previousMonth = LocalDate.now().getMonth().minus(1);

        int currentYear = LocalDate.now().getYear();
        int previousYear = LocalDate.now().getYear() - 1;

        int currentDateAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                currentDate.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousDateAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                previousDate.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentWeekAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                ((appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (currentDate) ||
                        appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (currentDate)) &&
                        (appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter
                                (previousWeekEndDate) ||
                                appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                                        .isEqual(previousWeekEndDate))
                        && currentMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int previousWeekAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                (appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (previousWeekEndDate)
                        && (appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekStartDate) ||
                        appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekStartDate))
                        && currentMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int currentMonthAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                currentMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousMonthAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                previousMonth.compareTo(appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentYearAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                currentYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();


        int previousYearAppAmbassadorPlusEligibleMembers = appAmbassadorPlusMembers.stream().filter(appAmbassadorPlusEligibleMembers ->
                previousYear == appAmbassadorPlusEligibleMembers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();

        map.put("currentDateAppAmbassadorPlusEligibleMembers", currentDateAppAmbassadorPlusEligibleMembers);
        map.put("previousDateAppAmbassadorPlusEligibleMembers", previousDateAppAmbassadorPlusEligibleMembers);

        map.put("currentWeekAppAmbassadorPlusEligibleMembers", currentWeekAppAmbassadorPlusEligibleMembers);
        map.put("previousWeekAppAmbassadorPlusEligibleMembers", previousWeekAppAmbassadorPlusEligibleMembers);

        map.put("currentMonthAppAmbassadorPlusEligibleMembers", currentMonthAppAmbassadorPlusEligibleMembers);
        map.put("previousMonthAppAmbassadorPlusEligibleMembers", previousMonthAppAmbassadorPlusEligibleMembers);

        map.put("currentYearAppAmbassadorPlusEligibleMembers", currentYearAppAmbassadorPlusEligibleMembers);
        map.put("previousYearAppAmbassadorPlusEligibleMembers", previousYearAppAmbassadorPlusEligibleMembers);

        return map;
    }


    public Map<String, Integer> checkCountingP2PYoungEntrepreneurs(List<User> userList) {
        logger.info("method ::: checkCountingP2PYoungEntrepreneurs");
        Map<String, Integer> map = new HashMap<>();

        LocalDate currentDate = LocalDate.now();
        LocalDate previousDate = LocalDate.now().minusDays(1);

        LocalDate previousWeekEndDate = LocalDate.now().minusDays(6);
        LocalDate previousWeekStartDate = LocalDate.now().minusDays(12);

        Month currentMonth = LocalDate.now().getMonth();
        Month previousMonth = LocalDate.now().getMonth().minus(1);

        int currentYear = LocalDate.now().getYear();
        int previousYear = LocalDate.now().getYear() - 1;

        int currentDateP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                currentDate.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousDateP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                previousDate.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()) == 0
                        && currentMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentWeekP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                ((P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (currentDate) ||
                        P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (currentDate)) &&
                        (P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter
                                (previousWeekEndDate) ||
                                P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                                        .isEqual(previousWeekEndDate))
                        && currentMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int previousWeekP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                (P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore
                        (previousWeekEndDate)
                        && (P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .isAfter(previousWeekStartDate) ||
                        P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual
                                (previousWeekStartDate))
                        && currentMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault
                        ()).toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear()))
                .collect(Collectors.toList()).size();

        int currentMonthP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                currentMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int previousMonthP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                previousMonth.compareTo(P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getMonth()) == 0
                        && currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate().getYear())
                .collect(Collectors.toList()).size();

        int currentYearP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                currentYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();


        int previousYearP2PYoungEntrepreneurs = userList.stream().filter(P2PYoungEntrepreneurs ->
                previousYear == P2PYoungEntrepreneurs.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                        .getYear())
                .collect(Collectors.toList()).size();

        map.put("currentDateP2PYoungEntrepreneurs", currentDateP2PYoungEntrepreneurs);
        map.put("previousDateP2PYoungEntrepreneurs", previousDateP2PYoungEntrepreneurs);

        map.put("currentWeekP2PYoungEntrepreneurs", currentWeekP2PYoungEntrepreneurs);
        map.put("previousWeekP2PYoungEntrepreneurs", previousWeekP2PYoungEntrepreneurs);

        map.put("currentMonthP2PYoungEntrepreneurs", currentMonthP2PYoungEntrepreneurs);
        map.put("previousMonthP2PYoungEntrepreneurs", previousMonthP2PYoungEntrepreneurs);

        map.put("currentYearP2PYoungEntrepreneurs", currentYearP2PYoungEntrepreneurs);
        map.put("previousYearP2PYoungEntrepreneurs", previousYearP2PYoungEntrepreneurs);

        return map;
    }

    public static String setUserAccountType(Integer refCode) {
        logger.info("method ::: setUserAccountType");
        switch (refCode) {
            case 3:
                return ("FREE");
            case 4:
                return ("FREE");
            case 6:
                return ("P2P Distributor");
            default:
                return "PAID";
        }
    }

    @Override
    public ResponseEntity<?> saveContactEnquiry(ContactEnquiries contactEnquiries) {
        logger.info("method ::: saveContactEnquiry");
        contactEnquiries.setDate(LocalDate.now());
        contactEnquiriesRepository.save(contactEnquiries);
        return ResponseDomain.postResponse("We appreciate you contacting us.One of our respresentatives will be getting back to you shortly.");
    }

    @Override
    public ResponseEntity<?> getContactEnquiry() {
        logger.info("method ::: getContactEnquiry");
        List<ContactEnquiries> contactEnquiriesList = contactEnquiriesRepository.findAll();
        return new ResponseEntity<>(contactEnquiriesList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> resendActivationMailToAdmin(String auth, Long userId, HttpServletRequest request) {
        logger.info("method ::: resendActivationMailToAdmin");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Referrers referrersEO = referrerRepository.findByUser(user);
            if (referrersEO != null || user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
                User customerUserEO = userRepository.findByUserRegistrationId(userId);
                if (customerUserEO.getUserStatus().getStatusCode() == 4) {
                    String jwt = tokenProvider.accountActivationToken(customerUserEO);
                    customerUserEO.setAccountActivationKey(jwt);
                    customerUserEO = userRepository.save(customerUserEO);
                    if (customerUserEO != null) {
                        try {
                            String url = (request.getScheme() + "://" + request.getServerName() + ":" + request
                                    .getServerPort() + request.getContextPath());
                            url = url + "/customer/activate/account/" + jwt;
                            mailNotification.sendMailToNewlyCreateAdmin(customerUserEO, url);
                            return ResponseDomain.postResponse("Mail Re-sent Successfully");
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                            logger.error("method ::: resendActivationMailToAdmin ::: Error ::: " + ex.getMessage());
                            return ResponseDomain.internalServerError(ex.getLocalizedMessage());
                        }
                    } else
                        return ResponseDomain.internalServerError();
                } else
                    return ResponseDomain.badRequest(messageProperties.getCustomerAlreadyActive());
            } else
                return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.customers", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true),
            @CacheEvict(value = "super-admin.admins", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true)
    })
    public ResponseEntity<?> deleteUser(String auth, String userId) {
        logger.info("method ::: deleteUser");
        Optional<User> user = userRepository.findById(Long.parseLong(userId));
        if (user.isPresent()) {
            UserPhysicalAddress physicalAddress = userPhysicalAddressRepository.findByUser(user.get());
            if (physicalAddress != null) {
                userPhysicalAddressRepository.delete(physicalAddress);
            }
            List<UserBillingAddress> billingAddressList = userBillingAddressRepository.findAllByUser(user.get());
            if (billingAddressList.size() > 0) {
                billingAddressList.forEach(billingAddress -> {
                    userBillingAddressRepository.delete(billingAddress);
                });
            }
            List<Transaction> transactionList = transactionRepository.findAllByUser(user.get());
            if (transactionList.size() > 0) {
                transactionList.forEach(transaction -> {
                    transactionRepository.delete(transaction);
                });
            }
            List<SubscriptionDetails> userSubscriptions = subscriptionRepository
                    .findAllByUserOrderBySubscriptionIdDesc(user.get()).stream().collect(Collectors.toList());
            userSubscriptions.stream().map(subscriptionDetails -> {
                Subscription sub = new Subscription();
                try {
                    if (subscriptionDetails.getStripeSubscriptionId().startsWith("sub") && (subscriptionDetails.getStatus().equalsIgnoreCase("Active"))) {
                        sub = Subscription.retrieve(subscriptionDetails
                                .getStripeSubscriptionId());
                        sub = sub.cancel(null);
                    }
                    subscriptionRepository.delete(subscriptionDetails);
                    return subscriptionDetails;
                } catch (StripeException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("method ::: updateUserStatus ::: error ::: " + e.getMessage());
                    return null;
                }
            }).collect(Collectors.toList());
            Referrers referrer1 = referrerRepository.findByUser(user.get());
            if (referrer1 != null) {
                List<User> users = userRepository.findAllByReferrerCode(referrer1.getReferrerCode());
                if (!users.isEmpty()) {
                    users.forEach(user1 -> {
                        user1.setReferrerCode(1000l);
                    });
                }
                userRepository.saveAll(users);
                List<Customers> customersList = customersRepository.findAllByReferrers(referrer1);
                if (!customersList.isEmpty()) {
                    Referrers superAdminRef = referrerRepository.findByReferrerCode(1000l);
                    customersList.forEach(customers -> {
                        customers.setReferrers(superAdminRef);
                    });
                    customersRepository.saveAll(customersList);
                }
            }
            List<ContactManager> contactManagerList = contactManagerRepository.findAllByUser(user.get());
            contactManagerRepository.deleteAll(contactManagerList);
            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.get().getUserRegistrationId());
            if (appAmbassadorPlusEligibleMembers != null) {
                appAmbassadorPlusEligibleMembersRepository.delete(appAmbassadorPlusEligibleMembers);
            }
            CommonUtilFunctions.unsubscribeUserP2PAcademy(user.get().getEmailAddress(), "DELETE");
//            if (referrer1 != null) {
//            List<CommissionRecord> comissinoRecordList = commissionRecordRepository.findAllByReferrers(referrer1);
//            comissinoRecordList.forEach(commissionRecord -> {
//                List<CommissionPerCustomer> calculatedCommissionsList = commissionPerCustomerRepository.findAllByCommissionRecord(commissionRecord);
//                if (calculatedCommissionsList.size() > 0) {
//                    calculatedCommissionsList.forEach(commissionPerCustomer -> {
//                        commissionPerCustomerRepository.delete(commissionPerCustomer);
//                    });
//                }
//                commissionRecordRepository.delete(commissionRecord);
//            });
//            } else {
//            List<CommissionPerCustomer> commissionPerCustomersList = commissionPerCustomerRepository.findAllByUser(user.get());
//            if (commissionPerCustomersList.size() > 0) {
//                commissionPerCustomersList.forEach(commissionPerCustomer -> {
//                    commissionPerCustomerRepository.delete(commissionPerCustomer);
//                });
//            }
//            }

            userRepository.delete(user.get());
            return ResponseDomain.postResponse("User Deleted Successfully");
        }
        return ResponseDomain.badRequest(messageProperties.getNotExist());
    }

    public List<JSONObject> userDeatils(List<User> userList) {
        logger.info("method ::: userDeatils");
        List<JSONObject> userDetailsList = new ArrayList<>();
        userList.forEach(user -> {
            JSONObject userDetails = new JSONObject();
            String accountType = "";
            Long userReferrerCode = user.getReferrerCode();
            String userReferrerName = "N.A";
            String referrerCode = "N.A";
            String planType = "N.A";
            userDetails.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
            userDetails.put("emailId", user.getEmailAddress());
            userDetails.put("contactNumber", user.getContactNumber());
            userDetails.put("creationDate", CustomDateConverter.convertDateToString(user.getCreationDate()));
            userDetails.put("userStatus", user.getUserStatus().getStatusDescription());
            Integer referenceCode = user.getReferenceType().getReferenceCode();
            if (referenceCode == 3 || referenceCode == 4 || referenceCode == 6 || referenceCode == 7) {
                accountType = user.getReferenceType().getDescription();
            } else
                accountType = user.getRole().getRoleDescription();
            userDetails.put("accountType", accountType.replace("_", " ").replace("ROLE", "").trim());
            switch (userDetails.get("accountType").toString()) {
                case "CUSTOMER REP":
                    userDetails.put("accountType", "APP AMBASSADOR");
                    break;
                case "P2P DISTRIBUTOR":
                    userDetails.put("accountType", "LEGACY MEMBER");
                    break;
            }
            if (user.getIsP2PYEA()) {
                userDetails.put("accountType", "P2P YEA");
            }
            userDetails.put("p2PYEACount", String.valueOf(0));
            Referrers referrer = referrerRepository.findByUser(user);
            List<User> referredUserList = new ArrayList<>();
            if (referrer != null) {
                referredUserList = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
                referrerCode = referrer.getReferrerCode().toString();
                long p2pYEACount = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                userDetails.put("p2PYEACount", String.valueOf(p2pYEACount));
            }
            userDetails.put("referrerCode", referrerCode);
            List<User> referredUserListCopy = referredUserList;
            int customerCount = referredUserList.stream().filter(user1 -> user1.getRole().getRoleCode() == 3).collect(Collectors.toList()).size();
            long referrerCount = referredUserListCopy.stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count();
            int p2pDistributerCount = referredUserList.stream().filter(user1 -> user1.getReferenceType().getReferenceCode() == 6).collect(Collectors.toList()).size();
            userDetails.put("customerCount", customerCount);
            userDetails.put("referrerCount", referrerCount);
            userDetails.put("p2pDistributerCount", p2pDistributerCount);
            if (userReferrerCode != null) {
                User userReferrer = referrerRepository.findByReferrerCode(userReferrerCode).getUser();
                userReferrerName = userReferrer.getFirstName() + (userReferrer.getLastName() != null ? (" " + userReferrer.getLastName()) : "");
            }
            userDetails.put("userReferrerName", userReferrerName);
            List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository.findAllByUserOrderBySubscriptionIdDesc(user).stream()
                    .filter(subscriptionDetails -> subscriptionDetails.getStatus().equalsIgnoreCase("Active")).collect(Collectors.toList());
            ;
            if (!subscriptionDetailsList.isEmpty()) {
                planType = subscriptionDetailsList.get(0).getPlan().getPlanInterval();
            }
            userDetails.put("planType", planType);
            userDetails.put("userId", user.getUserRegistrationId());
            userDetails.put("userName", user.getUserName());
            userDetails.put("directAAPCount", String.valueOf(0));
            userDetails.put("teamSizeCount", String.valueOf(0));
            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
            if (appAmbassadorPlusEligibleMembers != null) {
                userDetails.put("teamSizeCount", String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                userDetails.put("directAAPCount", String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
            }
            userDetailsList.add(userDetails);
        });
        return userDetailsList;
    }

    @Override
    @Cacheable(value = "admin.users-details", key = "#root.methodName", sync = true)
    public ResponseEntity<?> getAllUsers() {
        logger.info("method ::: getAllUsers");
        List<User> userList = userRepository.findAll();
        List<JSONObject> userDetailsList = new ArrayList<>();
        userDetailsList.addAll(userDeatils(userList));
        return new ResponseEntity<>(userDetailsList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllCustomers(String userId) {
        logger.info("method ::: getAllCustomers");
        List<JSONObject> userDetailsList = new ArrayList<>();
        Optional<User> user = userRepository.findById(Long.parseLong(userId));
        if (user.isPresent()) {
            Referrers referrer = referrerRepository.findByUser(user.get());
            if (referrer != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
                userDetailsList.addAll(userDeatils(userList));
            } else {
                return ResponseDomain.badRequest("The selected user is not a Customer-Rep/P2P Distributor.");
            }
        } else
            return ResponseDomain.badRequest(messageProperties.getNotExist());

        return new ResponseEntity<>(userDetailsList, HttpStatus.OK);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.customer-reps", allEntries = true),
            @CacheEvict(value = "admin.users-details", allEntries = true),
            @CacheEvict(value = "admin.all-customer", allEntries = true),
            @CacheEvict(value = "customer-rep.customers", allEntries = true)
    })
    public ResponseEntity<?> changeReferrerCode(String auth, ChangeReferralCodeDTO changeReferralCodeDTO) {
        User tokenUser = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        Integer roleCode = tokenUser.getRole().getRoleCode();
        if (roleCode.equals(4) || roleCode.equals(5) || tokenUser.getIsEligibleForPromotion()) {
            Boolean isReferralCodeValid = referrerRepository.existsByReferrerCode(Long.parseLong(changeReferralCodeDTO.getNewReferralCode()));
            if (isReferralCodeValid) {
                Optional<User> user = userRepository.findById(Long.parseLong(changeReferralCodeDTO.getUserRegistrationId()));
                if (user.isPresent()) {
                    if (user.get().getReferrerCode().toString().equals(changeReferralCodeDTO.getCurrentReferralCode())) {
                        Referrers newReferrer = referrerRepository.findByReferrerCode(Long.parseLong(changeReferralCodeDTO.getNewReferralCode()));
                        user.get().setReferrerCode(Long.parseLong(changeReferralCodeDTO.getNewReferralCode()));
                        userRepository.save(user.get());
                        Customers customer = customersRepository.findByUser(user.get());
                        if (customer != null) {
                            customer.setReferrers(newReferrer);
                            customersRepository.save(customer);
                        }
                        return ResponseDomain.postResponse("Referral code updated successfully.");
                    }
                    return ResponseDomain.badRequest("Current referral code did not matched");
                }
                return ResponseDomain.badRequest(messageProperties.getNotExist());
            }
            return ResponseDomain.badRequest("Referral Code does not exist");
        }
        return ResponseDomain.badRequest("You are not authorized to change referral code.");
    }

    @Override
    @Cacheable(value = "admin.all-customer", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> allCustomers(String auth) {
        logger.info("method :::  allCustomers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user.getRole().getRoleCode() == 4 || user.getRole().getRoleCode() == 5) {
            Set<CustomerVO> customerVOList = new HashSet<>();
            List<Customers> customersList = customersRepository.findAll().stream().filter(customers -> !customers.getUser().getRole().getRoleCode().equals(2)).collect(Collectors.toList());
            if (!customersList.isEmpty()) {
                customersList.forEach(customer -> {
                    User user1 = customer.getUser();
                    User referrer = referrerRepository.findByReferrerCode(user1.getReferrerCode()).getUser();
                    CustomerVO customerVO = new CustomerVO();
                    customerVO.setP2PYEA(user1.getIsP2PYEA());
                    customerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                    customerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                    customerVO.setEmail(user1.getEmailAddress());
                    customerVO.setUserId(user1.getUserRegistrationId());
                    customerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                    customerVO.setStatus(user1.getUserStatus().getStatusDescription());
                    customerVO.setReferrerCode(user1.getReferrerCode().toString());
                    customerVO.setReferrerName(referrer.getFirstName() + (referrer.getLastName() != null ? " " + referrer.getLastName() : ""));
                    customerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));
                    customerVO.setUserType(customerVO.getAccountType().equals("FREE") ? "Test" : "Customer");
                    customerVO.setCustomerCount("N.A");
                    customerVO.setCustomerRepCount("N.A");
                    customerVO.setP2pDistributorCount("N.A");
                    customerVO.setIsAppAmbassador(false);
                    customerVO.setAppAmbassadorCount(0);
                    customerVO.setTeamSizeCount(String.valueOf(0));
                    customerVO.setRecognitionCategory("N.A");
                    customerVO.setP2PYEACount(String.valueOf(0));
                    customerVO.setDirectAAPCount(String.valueOf(0));
                    customerVOList.add(customerVO);
                });
            }
            List<User> testUsers = userRepository.findAll().stream().filter(user1 -> (user1.getReferenceType().getReferenceCode().equals(4) || user1.getReferenceType().getReferenceCode().equals(5) || user1.getReferenceType().getReferenceCode().equals(2))).filter(customerVO -> customerVO.getReferrerCode() != null).filter(user1 -> !user1.getRole().getRoleCode().equals(2)).collect(Collectors.toList());
            testUsers.forEach(user1 -> {
                if (customerVOList.stream().filter(customerVO -> customerVO.getUserId().equals(user1.getUserRegistrationId())).count() == 0) {
                    Referrers referrerOfUser = referrerRepository.findByReferrerCode(user1.getReferrerCode());
                    if (referrerOfUser != null) {
                        User referrer = referrerOfUser.getUser();
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setP2PYEA(user1.getIsP2PYEA());
                        customerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                        customerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                        customerVO.setEmail(user1.getEmailAddress());
                        customerVO.setUserId(user1.getUserRegistrationId());
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                        customerVO.setStatus(user1.getUserStatus().getStatusDescription());
                        customerVO.setReferrerCode(user1.getReferrerCode().toString());
                        customerVO.setReferrerName(referrer.getFirstName() + (referrer.getLastName() != null ? " " + referrer.getLastName() : ""));
                        customerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));
                        customerVO.setUserType(customerVO.getAccountType().equals("FREE") ? "Test" : "Customer");
                        customerVO.setCustomerCount("N.A");
                        customerVO.setCustomerRepCount("N.A");
                        customerVO.setP2pDistributorCount("N.A");
                        customerVO.setIsAppAmbassador(false);
                        customerVO.setAppAmbassadorCount(0);
                        customerVO.setTeamSizeCount(String.valueOf(0));
                        customerVO.setRecognitionCategory("N.A");
                        customerVO.setP2PYEACount(String.valueOf(0));
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        customerVOList.add(customerVO);
                    }
                }
            });
            return new ResponseEntity<>(customerVOList, HttpStatus.OK);
        }
        return ResponseDomain.badRequest(messageProperties.getNotAuthorized());
    }

    @Override
    public ResponseEntity<?> allAppAmbassadorEligibleForBonuses() {
        logger.info("method :::  allAppAmbassadorEligibleForBonuses");
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        List<JSONObject> appAmbassadorPlusEligibleMembers = new ArrayList<>();
        appAmbassadorPlusEligibleMembersList.forEach(appAmbassador -> {
            JSONObject jsonObject = new JSONObject();
            User user = userRepository.findByUserRegistrationId(appAmbassador.getUserRegistrationId());
            jsonObject.put("p2PYEA", user.getIsP2PYEA());
            jsonObject.put("appAmbassadorId", appAmbassador.getAppAmbassadorId());
            jsonObject.put("userRegistrationId", appAmbassador.getUserRegistrationId());
            jsonObject.put("firstName", appAmbassador.getFirstName());
            jsonObject.put("lastName", appAmbassador.getLastName());
            jsonObject.put("email", user.getEmailAddress());
            jsonObject.put("contactNumber", user.getContactNumber());
            LocalDate creationDate = appAmbassador.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            jsonObject.put("creationDate", creationDate);
            Referrers referrerObj = referrerRepository.findByUser(user);
            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersObj = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
            if (appAmbassadorPlusEligibleMembersObj != null) {
                jsonObject.put("teamSizeCount", String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembersObj)));
            } else {
                jsonObject.put("teamSizeCount", "N.A");
            }
            jsonObject.put("p2PYEACount", "0");
            if (referrerObj != null) {
                long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                jsonObject.put("p2PYEACount", String.valueOf(p2pYEACount));
            }

            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                jsonObject.put("appAmbassadorCount", userList.size());
                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                if (leadersWallCategories.size() > 0) {
                    jsonObject.put("recognitionCategory", leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                } else {
                    jsonObject.put("recognitionCategory", "N.A");
                }
            } else {
                jsonObject.put("appAmbassadorCount", 0);
                jsonObject.put("recognitionCategory", "N.A");
            }

            try {
                User referrer = referrerRepository.findByReferrerCode(user.getReferrerCode()).getUser();
                jsonObject.put("referrerName", referrer.getFirstName() + (referrer.getLastName()
                        != null ? " " + referrer.getLastName() : ""));
            } catch (Exception e) {
                jsonObject.put("referrerName", "P2P Connection");
            }
            appAmbassadorPlusEligibleMembers.add(jsonObject);
        });
        return new ResponseEntity<>(appAmbassadorPlusEligibleMembers, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<?> allAppAmbassadorEligibleForBonusesTokenFree() {
        logger.info("method :::  allAppAmbassadorEligibleForBonusesTokenFree");
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        List<JSONObject> appAmbassadorPlusEligibleMembers = new ArrayList<>();
        appAmbassadorPlusEligibleMembersList.forEach(appAmbassador -> {
            JSONObject jsonObject = new JSONObject();
            User user = userRepository.findByUserRegistrationId(appAmbassador.getUserRegistrationId());
            jsonObject.put("p2PYEA", user.getIsP2PYEA());
            jsonObject.put("email", user.getEmailAddress());
            Referrers referrerObj = referrerRepository.findByUser(user);
            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                jsonObject.put("appAmbassadorCount", userList.size());
            } else {
                jsonObject.put("appAmbassadorCount", 0);
            }
            appAmbassadorPlusEligibleMembers.add(jsonObject);
        });
        return new ResponseEntity<>(appAmbassadorPlusEligibleMembers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deleteAppAmbassadorEligibleForBonuses(String auth, String userRegistrationId) {
        logger.info("method :::  deleteAppAmbassadorEligibleForBonuses");
        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMember = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(Long.parseLong(userRegistrationId));
        if (appAmbassadorPlusEligibleMember != null) {
            appAmbassadorPlusEligibleMembersRepository.delete(appAmbassadorPlusEligibleMember);
            Optional<User> userOptional = userRepository.findById(appAmbassadorPlusEligibleMember.getUserRegistrationId());
            if (userOptional.isPresent()) {
                CommonUtilFunctions.unsubscribeUserP2PAcademy(userOptional.get().getEmailAddress(), "INACTIVE");
            }
            return ResponseDomain.deleteResponse("User Successfully removed from being eligible for bonus.");
        }
        return ResponseDomain.badRequest("User is already not eligible for bonus.");
    }

    @Override
    public ResponseEntity<?> addAsAppAmbassadorEligibleForBonuses(String auth, String userRegistrationId) {
        logger.info("method :::  addAsAppAmbassadorEligibleForBonuses");
        Optional<User> userOptional = userRepository.findById(Long.parseLong(userRegistrationId));
        if (userOptional.isPresent()) {
            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMember = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(Long.parseLong(userRegistrationId));
            if (appAmbassadorPlusEligibleMember == null) {
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = new AppAmbassadorPlusEligibleMembers();
                appAmbassadorPlusEligibleMembers.setUserRegistrationId(userOptional.get().getUserRegistrationId());
                appAmbassadorPlusEligibleMembers.setFirstName(userOptional.get().getFirstName());
                appAmbassadorPlusEligibleMembers.setLastName(userOptional.get().getLastName());
                appAmbassadorPlusEligibleMembersRepository.save(appAmbassadorPlusEligibleMembers);
                CommonUtilFunctions.subscribeUserP2PAcademy(userOptional.get());
                Long referrerCode = userOptional.get().getReferrerCode();
                Referrers referrersEligibleForBonus = referrerRepository.findByReferrerCode(referrerCode);
                AppAmbassadorPlusEligibleMembers sponsorAppAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
                if (sponsorAppAmbassadorPlusEligibleMembers != null || referrerCode == 1000) {
                    AppAmbassadorBonuses appAmbassadorBonuses = new AppAmbassadorBonuses();
                    appAmbassadorBonuses.setFirstName(referrersEligibleForBonus.getUser().getFirstName());
                    appAmbassadorBonuses.setLastName(referrersEligibleForBonus.getUser().getLastName());
                    appAmbassadorBonuses.setBonusEarnedFrom(userOptional.get().getFirstName() + (userOptional.get().getLastName() != null ? " " + userOptional.get().getLastName() : ""));
                    if (referrersEligibleForBonus.getUser().getIsEligibleForPromotion()) {
                        appAmbassadorBonuses.setBonuseAmount(20f);
                    } else {
                        appAmbassadorBonuses.setBonuseAmount(50f);
                    }
                    appAmbassadorBonuses.setDisbursementDate(LocalDate.now());
                    appAmbassadorBonuses.setUserRegistrationId(referrersEligibleForBonus.getUser().getUserRegistrationId());
                    appAmbassadorBonusesRepository.save(appAmbassadorBonuses);
                }
                return ResponseDomain.postResponse("User Successfully marked eligible for bonus.");
            }
            return ResponseDomain.badRequest("User already marked eligible for bonus.");
        }
        return ResponseDomain.badRequest("Invalid User.");
    }

    @Override
    public ResponseEntity<?> allP2pDistributor(String auth) {
        logger.info("method :::  allP2pDistributor");
        List<User> userList = userRepository.findAll();
        List<User> p2pDistributors = userList.stream().filter(user -> user.getReferenceType().getReferenceCode().equals(6)).collect(Collectors.toList());
        List<JSONObject> p2pDistributorList = new ArrayList<>();
        p2pDistributors.stream().forEach(p2pDistributor -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userRegistrationId", p2pDistributor.getUserRegistrationId());
            jsonObject.put("name", p2pDistributor.getFirstName() + (p2pDistributor.getLastName() != null ? " " + p2pDistributor.getLastName() : ""));
            p2pDistributorList.add(jsonObject);
        });
        return new ResponseEntity<>(p2pDistributorList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> allBonus(String auth, String fromDate, String toDate) {
        logger.info("method :::  allBonus");
        if (fromDate != null && toDate != null) {
            LocalDate fromDateObj = CustomDateConverter.stringToLocalDate(fromDate);
            LocalDate toDateObj = CustomDateConverter.stringToLocalDate(toDate);
            if (fromDateObj.isAfter(toDateObj)) {
                return ResponseDomain.badRequest("from date cannot exceed to date");
            }
            List<BonusVo> bonusVoList = new ArrayList<>();
            Set<Long> appAmbassadorList = appAmbassadorBonusesRepository.findAll().stream().map(bonus -> bonus.getUserRegistrationId()).collect(Collectors.toSet());
            appAmbassadorList.stream().forEach(appAmbassador -> {
                List<AppAmbassadorBonuses> appAmbassadorBonusList = appAmbassadorBonusesRepository.findAllByUserRegistrationIdAndDisbursementDateBetween(appAmbassador, fromDateObj, toDateObj);
                if (appAmbassadorBonusList.size() > 0) {
                    BonusVo bonusVo = new BonusVo();
                    bonusVo.setCustomerCount(0l);
                    User sponsor = userRepository.findByUserRegistrationId(appAmbassador);
                    if (sponsor != null) {
                        Referrers referrer = referrerRepository.findByUser(sponsor);
                        if (referrer != null) {
                            List<User> users = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                            long customerCount = users.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count();
                            bonusVo.setCustomerCount(customerCount);
                        }
                    }
                    double sum = appAmbassadorBonusList.stream().mapToDouble(bonus -> bonus.getBonuseAmount()).sum();
                    DecimalFormat df = new DecimalFormat("0.00");
                    df.setMaximumFractionDigits(2);
                    bonusVo.setBonuseAmount(df.format(sum));
                    bonusVo.setUserRegistrationId(appAmbassador);
                    bonusVo.setName(appAmbassadorBonusList.get(0).getFirstName() + (appAmbassadorBonusList.get(0).getLastName() != null ? " " + appAmbassadorBonusList.get(0).getLastName() : ""));
                    List<BonusBreakDownVo> bonusBreakDownVos = new ArrayList<>();
                    appAmbassadorBonusList.forEach(bonus -> {
                        BonusBreakDownVo bonusBreakDownVo = new BonusBreakDownVo();
                        bonusBreakDownVo.setAppAmbassadorBonuseId(bonus.getAppAmbassadorBonuseId());
                        bonusBreakDownVo.setBonuseAmount(bonus.getBonuseAmount());
                        bonusBreakDownVo.setBonusEarnedFrom(bonus.getBonusEarnedFrom());
                        bonusBreakDownVo.setDisbursementDate(bonus.getDisbursementDate());
                        bonusBreakDownVos.add(bonusBreakDownVo);
                    });
                    bonusVo.setBonusBreakDownVoList(bonusBreakDownVos);
                    bonusVoList.add(bonusVo);
                }
            });
            return new ResponseEntity<>(bonusVoList, HttpStatus.OK);
        }
        return ResponseDomain.badRequest("Selected Date Range cannot be empty.");
    }

    @Override
    public ResponseEntity<?> bonusByUserRegistrationId(String auth, String fromDate, String toDate) {
        logger.info("method :::  bonusByUserRegistrationId");
        User userOptional = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (userOptional != null) {
            if (fromDate != null && toDate != null) {
                LocalDate fromDateObj = CustomDateConverter.stringToLocalDate(fromDate);
                LocalDate toDateObj = CustomDateConverter.stringToLocalDate(toDate);
                if (fromDateObj.isAfter(toDateObj)) {
                    return ResponseDomain.badRequest("from date cannot exceed to date");
                }
                List<AppAmbassadorBonuses> bonusesBetweenFromDateAndToDate = appAmbassadorBonusesRepository.findAllByUserRegistrationIdAndDisbursementDateBetween(userOptional.getUserRegistrationId(), fromDateObj, toDateObj);
                return new ResponseEntity<>(bonusesBetweenFromDateAndToDate, HttpStatus.OK);
            }
            List<AppAmbassadorBonuses> appAmbassadorBonusesList = appAmbassadorBonusesRepository.findAllByUserRegistrationId(userOptional.getUserRegistrationId());
            return new ResponseEntity<>(appAmbassadorBonusesList, HttpStatus.OK);
        }
        return ResponseDomain.badRequest("Invalid User.");
    }

    @Override
    public ResponseEntity<?> getTeamMembersByUserId(String auth, String userId) {
        logger.info("method ::: getTeamMembersByUserId");
        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(Long.parseLong(userId));
        List<AppAmbassadorPlusEligibleMembers> teamMembersList = new ArrayList<>();
        List<TeamMemberVo> teamMembersVOList = new ArrayList<>();
        if (appAmbassadorPlusEligibleMembers != null) {
            teamMembersList.addAll(getTeamMembers(appAmbassadorPlusEligibleMembers, 1));
        }
        Collections.sort(teamMembersList, new Comparator<AppAmbassadorPlusEligibleMembers>() {
            @Override
            public int compare(AppAmbassadorPlusEligibleMembers o1, AppAmbassadorPlusEligibleMembers o2) {
                return o1.getLevel().compareTo(o2.getLevel());
            }
        });
        teamMembersList.stream().forEach(teamMembers -> {
            User user = userRepository.findByUserRegistrationId(teamMembers.getUserRegistrationId());
            TeamMemberVo teamMemberVo = new TeamMemberVo();
            if (user != null) {
                teamMemberVo.setAppAmbassadorId(teamMembers.getAppAmbassadorId());
                teamMemberVo.setUserRegistrationId(teamMembers.getUserRegistrationId());
                teamMemberVo.setContactNumber(user.getContactNumber());
                teamMemberVo.setEmailAddress(user.getEmailAddress());
                teamMemberVo.setFirstName(teamMembers.getFirstName());
                teamMemberVo.setLastName(teamMembers.getLastName());
                teamMemberVo.setLevel(teamMembers.getLevel());
                if (user.getReferrerCode() != null) {
                    teamMemberVo.setSponsorCode(user.getReferrerCode().toString());
                    Referrers referrer = referrerRepository.findByReferrerCode(user.getReferrerCode());
                    if (referrer != null) {
                        User sponsor = referrer.getUser();
                        teamMemberVo.setSponsorName(sponsor.getFirstName() + " " + sponsor.getLastName());
                    }
                }
            }
            teamMembersVOList.add(teamMemberVo);
        });
        return new ResponseEntity<>(teamMembersVOList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> allAppAmbassador() {
        logger.info("method :::  allAppAmbassador");
        ResponseVO responseVO = new ResponseVO();
        List<ReferrerVO> referrerVOS = new ArrayList<>();
        List<User> referredUsers = userRepository.findAll().stream()
                .filter(user1 -> (user1.getRole().getRoleCode() == 1 && (user1.getUserStatus().getStatusCode
                        () == 3 || user1.getUserStatus().getStatusCode() == 4 || user1.getUserStatus()
                        .getStatusCode() == 5) && (user1.getReferenceType().getReferenceCode() == 1 || user1.getReferenceType()
                        .getReferenceCode() == 3 || user1.getReferenceType().getReferenceCode() == 6/* || user1.getReferenceType().getReferenceCode() == 5*/))).collect(Collectors.toList());

        List<Referrers> referrersList = referrerRepository.findAll().stream()
                .filter(referrers -> (!referrers.getReferrerType().equalsIgnoreCase("ADMIN")
                        && !referrers.getReferrerType().equalsIgnoreCase("SUPER_ADMIN"))).collect(Collectors
                        .toList());
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        if (!referredUsers.isEmpty()) {
            referredUsers.forEach(user1 -> {
                ReferrerVO referrerVO = new ReferrerVO();
                referrerVO.setDirectAAPCount(String.valueOf(0));
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user1.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    referrerVO.setIsAppAmbassador(true);
                    referrerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                    referrerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                    Referrers referrerObj = referrerRepository.findByUser(user1);
                    if (referrerObj != null) {
                        List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                        userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                        if (leadersWallCategories.size() > 0) {
                            referrerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                        } else {
                            referrerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        referrerVO.setRecognitionCategory("N.A");
                    }
                } else {
                    referrerVO.setIsAppAmbassador(false);
                    referrerVO.setTeamSizeCount("N.A");
                    referrerVO.setRecognitionCategory("N.A");
                }
                referrerVO.setP2PYEACount(String.valueOf(0));
                Referrers referrerObj = referrerRepository.findByUser(user1);
                if (referrerObj != null) {
                    long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                    referrerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                }
                referrerVO.setP2PYEA(user1.getIsP2PYEA());
                referrerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                referrerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                referrerVO.setEmail(user1.getEmailAddress());
                referrerVO.setUserId(user1.getUserRegistrationId());
                referrerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                referrerVO.setStatus(user1.getUserStatus().getStatusDescription());
                referrerVO.setReferrerCode("");
                referrerVO.setCustomerCount(0l);
                referrerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));
                referrerVOS.add(referrerVO);
            });
        }
        if (!referrersList.isEmpty()) {
            referrersList.forEach(referrers -> {
                User user1 = referrers.getUser();
                ReferrerVO referrerVO = new ReferrerVO();
                referrerVO.setDirectAAPCount(String.valueOf(0));
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user1.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    referrerVO.setIsAppAmbassador(true);
                    referrerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                    referrerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                    if (referrers != null) {
                        List<User> userList = userRepository.findAllByReferrerCode(referrers.getReferrerCode());
                        userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                        if (leadersWallCategories.size() > 0) {
                            referrerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                        } else {
                            referrerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        referrerVO.setRecognitionCategory("N.A");
                    }
                } else {
                    referrerVO.setIsAppAmbassador(false);
                    referrerVO.setTeamSizeCount("N.A");
                    referrerVO.setRecognitionCategory("N.A");
                }
                referrerVO.setP2PYEACount(String.valueOf(0));
                Referrers referrerObject = referrerRepository.findByUser(user1);
                if (referrerObject != null) {
                    long p2pYEACount = userRepository.findAllByReferrerCode(referrerObject.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                    referrerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                }
                referrerVO.setP2PYEA(user1.getIsP2PYEA());
                referrerVO.setName(user1.getFirstName() + (user1.getLastName() != null ? " " + user1.getLastName() : ""));
                referrerVO.setContactNumber(user1.getContactNumber() != null ? user1.getContactNumber() : "");
                referrerVO.setEmail(user1.getEmailAddress());
                referrerVO.setUserId(user1.getUserRegistrationId());
                referrerVO.setJoiningDate(CustomDateConverter.dateToString(user1.getCreationDate()));
                referrerVO.setStatus(user1.getUserStatus().getStatusDescription());
                referrerVO.setReferrerCode(referrers.getReferrerCode() != null ? referrers.getReferrerCode()
                        .toString() : "");
                referrerVO.setAccountType(setUserAccountType(user1.getReferenceType().getReferenceCode()));

                List<Customers> customersList = customersRepository.findAllByReferrers(referrers);
                List<User> p2pDistributerList = new ArrayList<>();
                if (referrers.getUser().getReferenceType().getReferenceCode() == 6 || referrers.getUser().getReferenceType().getReferenceCode() == 5 || referrers.getUser().getReferenceType().getReferenceCode() == 1) {
                    p2pDistributerList = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream().filter(user2 -> user2.getReferenceType().getReferenceCode() == 6).collect(Collectors.toList());
                }
                List<CustomerVO> customerVOS = new ArrayList<>();

                if (!p2pDistributerList.isEmpty()) {
                    p2pDistributerList.forEach(p2pDistributer -> {
//                            User user2 = p2pDistributer.getUser();
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(p2pDistributer.getUserRegistrationId());
                        if (p2pappAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(p2pDistributer);
                            if (referrerObj != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        customerVO.setP2PYEACount(String.valueOf(0));
                        Referrers referrerObj = referrerRepository.findByUser(p2pDistributer);
                        if (referrerObj != null) {
                            long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        }
                        customerVO.setP2PYEA(p2pDistributer.getIsP2PYEA());
                        customerVO.setName(p2pDistributer.getFirstName() + (p2pDistributer.getLastName() != null ? " " + p2pDistributer.getLastName() : ""));
                        customerVO.setContactNumber(p2pDistributer.getContactNumber() != null ? p2pDistributer.getContactNumber() : "");
                        customerVO.setEmail(p2pDistributer.getEmailAddress());
                        customerVO.setUserId(p2pDistributer.getUserRegistrationId());
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(p2pDistributer.getCreationDate()));
                        customerVO.setStatus(p2pDistributer.getUserStatus().getStatusDescription());
                        customerVO.setAccountType(setUserAccountType(p2pDistributer.getReferenceType().getReferenceCode()));
                        Integer roleCode = p2pDistributer.getRole().getRoleCode();
                        customerVO.setUserType((roleCode == 3 ? "Customer" : (p2pDistributer.getReferenceType().getReferenceCode() == 6 ? "P2P Distributor" : "Customer-Rep")));
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {
                            Referrers referrer = referrerRepository.findByUser(p2pDistributer);
                            if (referrer == null) {
                                customerCount = "N.A [ACTIVATION PENDING]";

                                customerVO.setReferrerCode("");
                            } else {
                                customerVO.setReferrerCode(referrer.getReferrerCode().toString());
                                List<User> userList = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user2 -> user2.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                customerCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                customerRepCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(userList.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());

                            }

                        }

                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVOS.add(customerVO);
                    });
                }
                if (!customersList.isEmpty()) {
                    customersList.forEach(customers -> {
                        User user2 = customers.getUser();
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        AppAmbassadorPlusEligibleMembers customerAppAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user2.getUserRegistrationId());
                        if (customerAppAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(customerAppAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(customerAppAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(customers.getUser());
                            if (referrerObj != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        customerVO.setP2PYEACount(String.valueOf(0));
                        Referrers referrerObj = referrerRepository.findByUser(user2);
                        if (referrerObj != null) {
                            long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        }
                        customerVO.setP2PYEA(user2.getIsP2PYEA());
                        customerVO.setName(user2.getFirstName() + (user2.getLastName() != null ? " " + user2.getLastName() : ""));
                        customerVO.setContactNumber(user2.getContactNumber() != null ? user2.getContactNumber() : "");
                        customerVO.setEmail(user2.getEmailAddress());
                        customerVO.setUserId(user2.getUserRegistrationId());
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(user2.getCreationDate()));
                        customerVO.setStatus(user2.getUserStatus().getStatusDescription());
                        customerVO.setAccountType(setUserAccountType(user2.getReferenceType().getReferenceCode()));
                        customerVO.setReferrerCode("");
                        Integer roleCode = customers.getUser().getRole().getRoleCode();
                        customerVO.setUserType(roleCode == 3 ? "Customer" : "Customer-Rep");
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        if (customerVO.getUserType().equals("Customer-Rep")) {
                            Referrers repositoryOfUser = referrerRepository.findByUser(user2);
                            if (repositoryOfUser != null) {
                                Long referrerCode = repositoryOfUser.getReferrerCode();
                                customerVO.setReferrerCode(referrerCode.toString());
                                List<User> userListObj = userRepository.findAllByReferrerCode(referrerCode).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                                customerCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                                customerRepCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                                p2pDistributorCount = String.valueOf(userListObj.stream()
                                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());


                                List<User> userList = userRepository.findAllByReferrerCode(referrerRepository.findByUser(customers.getUser()).getReferrerCode());
                            }
                        }
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVOS.add(customerVO);
                    });
                }

                List<User> referredCustomersList = userRepository.findAllByReferrerCode(referrers
                        .getReferrerCode()).stream()
                        .filter(user2 -> (user2.getRole().getRoleCode() == 1 && (user2.getUserStatus()
                                .getStatusCode() == 4 || user2.getUserStatus().getStatusCode() == 5 ||
                                user2.getUserStatus().getStatusCode() == 3)
                                && (user2.getReferenceType().getReferenceCode() == 5 ||
                                user2.getReferenceType().getReferenceCode() == 4)))
                        .filter(user2 -> customerVOS.stream().filter(customerVO -> customerVO.getUserId().equals(user2.getUserRegistrationId())).count() == 0)
                        .collect(Collectors.toList
                                ());
                ;
                if (!referredCustomersList.isEmpty()) {
                    referredCustomersList.forEach(user2 -> {
                        CustomerVO customerVO = new CustomerVO();
                        customerVO.setDirectAAPCount(String.valueOf(0));
                        AppAmbassadorPlusEligibleMembers referrerAppAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user2.getUserRegistrationId());
                        if (referrerAppAmbassadorPlusEligibleMembers != null) {
                            customerVO.setIsAppAmbassador(true);
                            customerVO.setTeamSizeCount(String.valueOf(countTeamSize(referrerAppAmbassadorPlusEligibleMembers)));
                            customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(referrerAppAmbassadorPlusEligibleMembers)));
                            Referrers referrerObj = referrerRepository.findByUser(user2);
                            if (referrerObj != null) {
                                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                        .collect(Collectors.toList());
                                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
                                if (leadersWallCategories.size() > 0) {
                                    customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                                } else {
                                    customerVO.setRecognitionCategory("N.A");
                                }
                            } else {
                                customerVO.setRecognitionCategory("N.A");
                            }
                        } else {
                            customerVO.setIsAppAmbassador(false);
                            customerVO.setTeamSizeCount("N.A");
                            customerVO.setRecognitionCategory("N.A");
                        }
                        customerVO.setP2PYEACount(String.valueOf(0));
                        Referrers referrerObj = referrerRepository.findByUser(user2);
                        if (referrerObj != null) {
                            long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                            customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                        }
                        customerVO.setP2PYEA(user2.getIsP2PYEA());
                        customerVO.setName(user2.getFirstName() + (user2.getLastName() != null ? " " + user2.getLastName() : ""));
                        customerVO.setContactNumber(user2.getContactNumber() != null ? user2.getContactNumber() : "");
                        customerVO.setEmail(user2.getEmailAddress());
                        customerVO.setUserId(user2.getUserRegistrationId());
                        customerVO.setJoiningDate(CustomDateConverter.dateToString(user2.getCreationDate()));
                        customerVO.setStatus(user2.getUserStatus().getStatusDescription());
                        customerVO.setAccountType(setUserAccountType(user2.getReferenceType().getReferenceCode()));
                        customerVO.setReferrerCode("");
                        String userType = "";
                        Integer referenceCode = user2.getReferenceType().getReferenceCode();
                        if (referenceCode == 1 || referenceCode == 3)
                            userType = "Customer-Rep";
                        else
                            userType = "Customer";
                        String customerCount = "N.A";
                        String customerRepCount = "N.A";
                        String p2pDistributorCount = "N.A";
                        customerVO.setUserType(userType);
                        if (customerVO.getUserType().equals("Customer-Rep")) {
                            Long referrerCode = referrerRepository.findByUser(user2).getReferrerCode();
                            customerVO.setReferrerCode(referrerCode.toString());
                            List<User> userListObj = userRepository.findAllByReferrerCode(referrerCode).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());

                            customerCount = String.valueOf(userListObj.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                            customerRepCount = String.valueOf(userListObj.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                            p2pDistributorCount = String.valueOf(userListObj.stream()
                                    .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                        }
                        customerVO.setUserType(userType);
                        customerVO.setCustomerCount(customerCount);
                        customerVO.setCustomerRepCount(customerRepCount);
                        customerVO.setP2pDistributorCount(p2pDistributorCount);
                        customerVOS.add(customerVO);
                    });
                }
                List<User> users = userRepository.findAllByReferrerCode(referrers.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());


                long customerCount = users.stream()
                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count();
                long customerRepCount = users.stream()
                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count();
                long p2pDistributorCount = users.stream()
                        .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count();

                referrerVO.setCustomerCount(customerCount);
                referrerVO.setCustomerRepCount(customerRepCount);
                referrerVO.setP2pDistributorCount(p2pDistributorCount);
                referrerVO.setCustomers(customerVOS);
                referrerVOS.add(referrerVO);
            });
        }
        responseVO.setReferrerVOS(referrerVOS);
        return new ResponseEntity<>(referrerVOS, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<?> allAppAmbassadorTokenFree() {
        logger.info("method :::  allAppAmbassadorTokenFree");
        List<Referrers> referrersList = referrerRepository.findAll();
        List<JSONObject> appAmbassadorList = new ArrayList<>();
        if (!referrersList.isEmpty()) {
            referrersList.forEach(referrer -> {
                User user = referrer.getUser();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", user.getEmailAddress());
                jsonObject.put("p2PYEA", user.getIsP2PYEA());
                List<User> userList = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user2 -> user2.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                long customerCount = userList.stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count();
                jsonObject.put("customerCount", customerCount);
                appAmbassadorList.add(jsonObject);
            });
        }
        return new ResponseEntity<Object>(appAmbassadorList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllDirectIndirectUsers(String auth, String userId) {
        logger.info("method :::  getAllDirectIndirectUsers");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(userId));
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            List<User> userList = getAllChildUsers(user, 1);
            Collections.sort(userList, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getLevel().compareTo(o2.getLevel());
                }
            });
            List<CustomerVO> customerVOList = new ArrayList<>();
            userList.forEach(userObj -> {
                CustomerVO customerVO = new CustomerVO();
                User sponsor = referrerRepository.findByReferrerCode(userObj.getReferrerCode()).getUser();
                customerVO.setReferrerName(sponsor.getFirstName() + (sponsor.getLastName() != null ? " " + sponsor.getLastName() : ""));
                customerVO.setLevel(userObj.getLevel());
                customerVO.setReferrerCode(userObj.getReferrerCode().toString());
                customerVO.setP2PYEA(userObj.getIsP2PYEA());
                customerVO.setUserId(userObj.getUserRegistrationId());
                customerVO.setName(userObj.getFirstName() + (userObj.getLastName()
                        != null ? " " + userObj.getLastName() : ""));
                customerVO.setEmail(userObj.getEmailAddress());
                customerVO.setContactNumber(userObj.getContactNumber() != null
                        ? userObj.getContactNumber() : "");
                customerVO.setJoiningDate(CustomDateConverter.dateToString(userObj.getCreationDate()));
                customerVO.setStatus(userObj.getUserStatus().getStatusDescription());
                customerVO.setAccountType(setUserAccountType(userObj.getReferenceType().getReferenceCode()));
                Integer referenceCode = userObj.getReferenceType().getReferenceCode();
                List<User> aAuserList = userRepository.findAllByReferrerCode(userObj.getReferrerCode());
                aAuserList = aAuserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                customerVO.setAppAmbassadorCount(aAuserList.size());
//                String userType = "";
//                if (roleCode == 2)
//                    userType = "Customer-Rep";
//                else
//                    userType = "Customer";
//                if (referenceCode == 6)
//                    userType = "P2P Distributor";

                String customerCount = "N.A";
                String customerRepCount = "N.A";
                String p2pDistributorCount = "N.A";
                Integer roleCode = userObj.getRole().getRoleCode();
                customerVO.setUserType((roleCode == 3 ? "Customer" : (userObj.getReferenceType().getReferenceCode() == 6 ? "P2P Distributor" : "Customer-Rep")));

//                customerVO.setUserType(userType);
                if (customerVO.getUserType().equals("Customer-Rep") || customerVO.getUserType().equals("P2P Distributor")) {
                    Referrers referrer = referrerRepository.findByUser(userObj);
                    if (referrer == null) {
                        customerCount = "N.A [ACTIVATION PENDING]";
                    } else {
                        List<User> userListObj = userRepository.findAllByReferrerCode(referrer.getReferrerCode()).stream().filter(user3 -> user3.getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
                        ;
                        customerCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 3).count());
                        customerRepCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getRole().getRoleCode() == 2 && user3.getReferenceType().getReferenceCode() != 6).count());
                        p2pDistributorCount = String.valueOf(userListObj.stream()
                                .filter(user3 -> user3.getUserStatus().getStatusCode() == 1 && user3.getReferenceType().getReferenceCode() == 6).count());
                    }
                }
                AppAmbassadorPlusEligibleMembers p2pappAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                if (p2pappAmbassadorPlusEligibleMembers != null) {
                    customerVO.setIsAppAmbassador(true);
                    customerVO.setTeamSizeCount(String.valueOf(countTeamSize(p2pappAmbassadorPlusEligibleMembers)));
                    customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(p2pappAmbassadorPlusEligibleMembers)));
                    Referrers referrerObj = referrerRepository.findByUser(userObj);
                    if (referrerObj != null) {
                        List<User> referredUserList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                        referredUserList = referredUserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                                .collect(Collectors.toList());
                        List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(referredUserList.size(), Sort.by("leadersWallCategoriesId"));
                        if (leadersWallCategories.size() > 0) {
                            customerVO.setRecognitionCategory(leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                        } else {
                            customerVO.setRecognitionCategory("N.A");
                        }
                    } else {
                        customerVO.setRecognitionCategory("N.A");
                    }
                } else {
                    customerVO.setIsAppAmbassador(false);
                    customerVO.setTeamSizeCount("N.A");
                    customerVO.setRecognitionCategory("N.A");
                }
//                customerVO.setUserType(userType);
                customerVO.setCustomerCount(customerCount);
                customerVO.setCustomerRepCount(customerRepCount);
                customerVO.setP2pDistributorCount(p2pDistributorCount);
                customerVO.setP2PYEACount(String.valueOf(0));
                Referrers referrerObj = referrerRepository.findByUser(userObj);
                if (referrerObj != null) {
                    long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                    customerVO.setP2PYEACount(String.valueOf(p2pYEACount));
                }
                customerVO.setDirectAAPCount(String.valueOf(0));
                customerVO.setTeamSizeCount(String.valueOf(0));
                customerVO.setIsAppAmbassador(false);
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userObj.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    customerVO.setIsAppAmbassador(true);
                    customerVO.setTeamSizeCount(String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembers)));
                    customerVO.setDirectAAPCount(String.valueOf(countDirectAAP(appAmbassadorPlusEligibleMembers)));
                }
                customerVOList.add(customerVO);
            });
            return new ResponseEntity<>(customerVOList, HttpStatus.OK);
        }
        return ResponseDomain.badRequest("User id does not exist");
    }

    @Override
    public ResponseEntity<?> makeEligibleforPromotion(String auth, List<String> userIdList) {
        logger.info("method :::  makeEligibleforPromotion");
        userIdList.stream().forEach(userId -> {
            User user = userRepository.findByUserRegistrationId(Long.parseLong(userId));
            if (user != null) {
                user.setIsEligibleForPromotion(Boolean.TRUE);
                userRepository.save(user);
            }
        });
        return ResponseDomain.postResponse("Users marked eligible for promotion");
    }

    @Override
    public ResponseEntity<?> makeIneligibleforPromotion(String auth, List<String> userIdList) {
        logger.info("method :::  makeIneligibleforPromotion");
        userIdList.stream().forEach(userId -> {
            User user = userRepository.findByUserRegistrationId(Long.parseLong(userId));
            if (user != null) {
                user.setIsEligibleForPromotion(Boolean.FALSE);
                userRepository.save(user);
            }
        });
        return ResponseDomain.postResponse("Users marked Ineligible for promotion");
    }

    @Override
    public ResponseEntity<?> getUsersEligibleForPromotion(String auth) {
        logger.info("method :::  getUsersEligibleforPromotion");
        List<User> userList = userRepository.findAll().stream().filter(user -> user.getIsEligibleForPromotion()).collect(Collectors.toList());
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        List<JSONObject> usersEligibleForPromotionList = new ArrayList<>();
        userList.forEach(user -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("p2PYEA", user.getIsP2PYEA());
            jsonObject.put("userRegistrationId", user.getUserRegistrationId());
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("lastName", user.getLastName());
            jsonObject.put("email", user.getEmailAddress());
            jsonObject.put("contactNumber", user.getContactNumber());
            LocalDate creationDate = user.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            jsonObject.put("creationDate", creationDate);
            Referrers referrerObj = referrerRepository.findByUser(user);
            AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembersObj = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
            if (appAmbassadorPlusEligibleMembersObj != null) {
                jsonObject.put("teamSizeCount", String.valueOf(countTeamSize(appAmbassadorPlusEligibleMembersObj)));
            } else {
                jsonObject.put("teamSizeCount", "N.A");
            }
            jsonObject.put("p2PYEACount", "0");
            if (referrerObj != null) {
                long p2pYEACount = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode()).stream().filter(p2pYEA -> p2pYEA.getIsP2PYEA()).count();
                jsonObject.put("p2PYEACount", String.valueOf(p2pYEACount));
            }

            if (referrerObj != null) {
                List<User> referredUserList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                referredUserList = referredUserList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                jsonObject.put("appAmbassadorPlusCount", referredUserList.size());
                List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(referredUserList.size(), Sort.by("leadersWallCategoriesId"));
                if (leadersWallCategories.size() > 0) {
                    jsonObject.put("recognitionCategory", leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                } else {
                    jsonObject.put("recognitionCategory", "N.A");
                }
            } else {
                jsonObject.put("appAmbassadorCount", 0);
                jsonObject.put("recognitionCategory", "N.A");
            }

            try {
                User referrer = referrerRepository.findByReferrerCode(user.getReferrerCode()).getUser();
                jsonObject.put("referrerName", referrer.getFirstName() + (referrer.getLastName()
                        != null ? " " + referrer.getLastName() : ""));
            } catch (Exception e) {
                jsonObject.put("referrerName", "P2P Connection");
            }
            usersEligibleForPromotionList.add(jsonObject);
        });
        return new ResponseEntity<>(usersEligibleForPromotionList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllUsersWithGroup(String auth) {
        logger.info("method :::  getAllUsersWithGroup");
        List<Referrers> referrersList = referrerRepository.findAll().stream().filter(referrers -> !referrers.getUser().getIsEligibleForPromotion()).collect(Collectors.toList());
        List<JSONObject> userGroupList = new ArrayList<>();
        List<JSONObject> sortedUserGroupList = new ArrayList<>();
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        referrersList.stream().forEach(referrer -> {
            User user = referrer.getUser();
            JSONObject jsonObject = new JSONObject();
            if (user.getReferenceType().getReferenceCode() == 6) {
                jsonObject.put("type", "Legacy Member");
                jsonObject.put("userId", user.getUserRegistrationId());
                jsonObject.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
                userGroupList.add(jsonObject);
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    jsonObject = new JSONObject();
                    jsonObject.put("type", "App Ambassador Plus");
                    jsonObject.put("userId", user.getUserRegistrationId());
                    jsonObject.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
                    userGroupList.add(jsonObject);
                }
            } else {
                AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(user.getUserRegistrationId());
                if (appAmbassadorPlusEligibleMembers != null) {
                    jsonObject = new JSONObject();
                    jsonObject.put("type", "App Ambassador Plus");
                    jsonObject.put("userId", user.getUserRegistrationId());
                    jsonObject.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
                    userGroupList.add(jsonObject);
                } else {
                    jsonObject = new JSONObject();
                    jsonObject.put("type", "App Ambassadors");
                    jsonObject.put("userId", user.getUserRegistrationId());
                    jsonObject.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
                    userGroupList.add(jsonObject);
                }
            }

            List<User> userList = userRepository.findAllByReferrerCode(referrer.getReferrerCode());
            userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                    .collect(Collectors.toList());
            List<LeadersWallCategories> leadersWallCategories = leadersWallCategoriesRepository.findByAapRequirementLessThanEqual(userList.size(), Sort.by("leadersWallCategoriesId"));
            if (leadersWallCategories.size() > 0) {
                jsonObject = new JSONObject();
                jsonObject.put("type", leadersWallCategories.get(leadersWallCategories.size() - 1).getCategoryName());
                jsonObject.put("userId", user.getUserRegistrationId());
                jsonObject.put("name", user.getFirstName() + (user.getLastName() != null ? (" " + user.getLastName()) : ""));
                userGroupList.add(jsonObject);
            }
        });
        List<JSONObject> platinumUsers = userGroupList.stream().filter(user -> user.get("type").equals("Platinum")).collect(Collectors.toList());
        List<JSONObject> diamondUsers = userGroupList.stream().filter(user -> user.get("type").equals("Diamond")).collect(Collectors.toList());
        List<JSONObject> bronzeUsers = userGroupList.stream().filter(user -> user.get("type").equals("Bronze")).collect(Collectors.toList());
        List<JSONObject> legacyMemberplatinumUsers = userGroupList.stream().filter(user -> user.get("type").equals("Legacy Member")).collect(Collectors.toList());
        List<JSONObject> appAmbassadorPlusUsers = userGroupList.stream().filter(user -> user.get("type").equals("App Ambassador Plus")).collect(Collectors.toList());
        List<JSONObject> appAmbassadorsUsers = userGroupList.stream().filter(user -> user.get("type").equals("App Ambassadors")).collect(Collectors.toList());
        sortedUserGroupList.addAll(platinumUsers);
        sortedUserGroupList.addAll(diamondUsers);
        sortedUserGroupList.addAll(bronzeUsers);
        sortedUserGroupList.addAll(legacyMemberplatinumUsers);
        sortedUserGroupList.addAll(appAmbassadorPlusUsers);
        sortedUserGroupList.addAll(appAmbassadorsUsers);
        return new ResponseEntity<>(sortedUserGroupList, HttpStatus.OK);
    }

    public int countTeamSize(AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers) {
//        logger.info("method :::  countTeamSize");
        int count = 0;
        User user = userRepository.findByUserRegistrationId(appAmbassadorPlusEligibleMembers.getUserRegistrationId());
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            Referrers referrerObj = referrerRepository.findByUser(user);
            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                if (userList.size() > 0) {
                    for (int i = 0; i < userList.size(); i++) {
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMember = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userList.get(i).getUserRegistrationId());
                        count += countTeamSize(appAmbassadorPlusEligibleMember);
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public int countDirectAAP(AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMembers) {
//        logger.info("method :::  countDirectAAP");
        int count = 0;
        User user = userRepository.findByUserRegistrationId(appAmbassadorPlusEligibleMembers.getUserRegistrationId());
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            Referrers referrerObj = referrerRepository.findByUser(user);
            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                count = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId()))).collect(Collectors.toList()).size();
            }
        }
        return count;
    }

    public List<AppAmbassadorPlusEligibleMembers> getTeamMembers(AppAmbassadorPlusEligibleMembers
                                                                         appAmbassadorPlusEligibleMembers, int level) {
//        logger.info("method :::  getTeamMembers");
        List<AppAmbassadorPlusEligibleMembers> teamMembers = new ArrayList<>();
        User user = userRepository.findByUserRegistrationId(appAmbassadorPlusEligibleMembers.getUserRegistrationId());
        if (user != null) {
            List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
            Referrers referrerObj = referrerRepository.findByUser(user);
            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                userList = userList.stream().filter(o1 -> !appAmbassadorPlusEligibleMembersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                if (userList.size() > 0) {
                    for (int i = 0; i < userList.size(); i++) {
                        AppAmbassadorPlusEligibleMembers appAmbassadorPlusEligibleMember = appAmbassadorPlusEligibleMembersRepository.findByUserRegistrationId(userList.get(i).getUserRegistrationId());
                        teamMembers.addAll(getTeamMembers(appAmbassadorPlusEligibleMember, level + 1));
                        appAmbassadorPlusEligibleMember.setLevel(level);
                        teamMembers.add(appAmbassadorPlusEligibleMember);
                    }
                }
            }
        }
        return teamMembers;
    }

    public List<User> getAllChildUsers(User user, int level) {
//        logger.info("method :::  getAllChildUsers");
        List<User> childUsersList = new ArrayList<>();
        User currentUser = userRepository.findByUserRegistrationId(user.getUserRegistrationId());
        if (currentUser != null) {
            List<User> allUsersList = userRepository.findAll().stream().collect(Collectors.toList());
            Referrers referrerObj = referrerRepository.findByUser(currentUser);
            if (referrerObj != null) {
                List<User> userList = userRepository.findAllByReferrerCode(referrerObj.getReferrerCode());
                userList = userList.stream().filter(o1 -> !allUsersList.stream().noneMatch(o2 -> o2.getUserRegistrationId().equals(o1.getUserRegistrationId())))
                        .collect(Collectors.toList());
                if (userList.size() > 0) {
                    for (int i = 0; i < userList.size(); i++) {
                        User user1 = userRepository.findByUserRegistrationId(userList.get(i).getUserRegistrationId());
                        childUsersList.addAll(getAllChildUsers(user1, level + 1));
                        user1.setLevel(level);
                        childUsersList.add(user1);
                    }
                }
            }
        }
        return childUsersList;
    }
}

