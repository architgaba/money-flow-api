package com.mfm.api.service.admin;

import com.mfm.api.domain.user.registration.ContactEnquiries;
import com.mfm.api.dto.request.ChangeReferralCodeDTO;
import com.mfm.api.dto.request.CustomerRegistrationDTO;
import com.mfm.api.dto.user.UpdateEmail;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AdminService {

    ResponseEntity<?> allReferrers(String auth);

    ResponseEntity<?> addCustomer(String auth, CustomerRegistrationDTO customerRegistrationDTO, HttpServletRequest request);

    ResponseEntity<?> addReferrer(String auth, CustomerRegistrationDTO customerRegistrationDTO, HttpServletRequest request);

    ResponseEntity<?> p2pDistributorSelfRegister(CustomerRegistrationDTO customerRegistrationDTO, HttpServletRequest request);

    ResponseEntity<?> updateUserStatus(String auth, Long userId, String action, HttpServletRequest request);

    ResponseEntity<?> getMyCustomers(String auth);

    ResponseEntity<?> resendActivationMailToUser(String auth, Long userId, HttpServletRequest request);

    ResponseEntity<?> getAllAdmins();

    ResponseEntity<?> addAdmin(String auth, CustomerRegistrationDTO customerRegistrationDTO, String url);

    ResponseEntity<?> updateAdmin(String auth, String id, String action);

    ResponseEntity<?> getAllGuestUsers();

    ResponseEntity<?> report(String auth);

    ResponseEntity<?> updateEmailId(UpdateEmail updateEmail);

    ResponseEntity<?> saveContactEnquiry(ContactEnquiries contactEnquiries);

    ResponseEntity<?> getContactEnquiry();

    ResponseEntity<?> resendActivationMailToAdmin(String auth, Long userId, HttpServletRequest request);

    ResponseEntity<?> deleteUser(String auth, String userId);

    ResponseEntity<?> getAllUsers();

    ResponseEntity<?> getAllCustomers(String userId);

    ResponseEntity<?> changeReferrerCode(String auth, ChangeReferralCodeDTO changeReferralCodeDTO);

    ResponseEntity<?> allCustomers(String auth);

    ResponseEntity<?> allAppAmbassadorEligibleForBonuses();

    ResponseEntity<?> allAppAmbassadorEligibleForBonusesTokenFree();

    ResponseEntity<?> deleteAppAmbassadorEligibleForBonuses(String auth, String userRegistrationId);

    ResponseEntity<?> addAsAppAmbassadorEligibleForBonuses(String auth, String userRegistrationId);

    ResponseEntity<?> allP2pDistributor(String auth);

    ResponseEntity<?> allBonus(String auth, String fromDate, String toDate);

    ResponseEntity<?> bonusByUserRegistrationId(String auth, String fromDate, String toDate);

    ResponseEntity<?> getTeamMembersByUserId(String auth, String userId);

    ResponseEntity<?> allAppAmbassador();

    ResponseEntity<?> allAppAmbassadorTokenFree();

    ResponseEntity<?> getAllDirectIndirectUsers(String auth, String userId);

    ResponseEntity<?> makeEligibleforPromotion(String auth, List<String> userIdList);

    ResponseEntity<?> getAllUsersWithGroup(String auth);

    ResponseEntity<?> makeIneligibleforPromotion(String auth, List<String> userIdList);

    ResponseEntity<?> getUsersEligibleForPromotion(String auth);
}












