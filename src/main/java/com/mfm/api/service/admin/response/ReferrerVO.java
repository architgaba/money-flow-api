package com.mfm.api.service.admin.response;

import lombok.Data;

import java.util.List;

@Data
public class ReferrerVO {
    private Long userId;
    private String name;
    private String email;
    private String contactNumber;
    private String joiningDate;
    private String status;
    private String referrerCode;
    private String accountType;
    private List<CustomerVO> customers;
    private Long customerCount;
    private Long customerRepCount;
    private Long p2pDistributorCount;
    private Boolean isAppAmbassador;
    private String teamSizeCount;
    private String recognitionCategory;
    private boolean isP2PYEA;
    private String p2PYEACount;
    private String directAAPCount;
}
