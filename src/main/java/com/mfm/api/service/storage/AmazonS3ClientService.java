package com.mfm.api.service.storage;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface AmazonS3ClientService {

    ResponseEntity<?> saveIncome_Expense_File(MultipartFile file, Long moneyFlowExpenseId, String type);

    boolean deleteReceipt(String receiptKey);

    ResponseEntity<?> uploadImage(MultipartFile file);

    ResponseEntity<?> uploadLeadersWall(MultipartFile file);

}
