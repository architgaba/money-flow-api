package com.mfm.api.service.storage;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class AmazonS3ClientServiceImpl implements AmazonS3ClientService {

    private Logger logger = LogManager.getLogger(AmazonS3ClientServiceImpl.class);

    @Autowired
    private AmazonS3 s3client;

    @Value("${gkz.s3.bucket}")
    private String bucketName;


    @Value("${gkz.s3.url}")
    private String url;


    @Override
    public ResponseEntity<?> saveIncome_Expense_File(MultipartFile file, Long moneyFlowExpenseId, String type) {
        logger.info("method :::  saveIncome_Expense_File");
        S3Object s3Object = null;
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.getSize());
            PutObjectResult putObjectRequest = uploadWithPermission(file.getInputStream(), FilenameUtils.removeExtension(file.getOriginalFilename()) + "_" + type + "_" + moneyFlowExpenseId + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, FilenameUtils.removeExtension(file.getOriginalFilename()) + "_" + type + "_" + moneyFlowExpenseId + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
            s3Object = s3client.getObject(getObjectRequest);
            return new ResponseEntity<>(s3Object.getKey(), HttpStatus.OK);
        } catch (Exception ioe) {
            logger.error(ioe.getMessage(), ioe);
            logger.error("method ::: saveIncome_Expense_File ::: error ::: " + ioe.getMessage());
            return ResponseDomain.internalServerError("Something Wrong in save File successfully");
        } finally {
            if (s3Object != null) {
                try {
                    s3Object.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }


    private PutObjectResult uploadWithPermission(InputStream inputStream, String uploadKey) {
        logger.info("method :::  uploadWithPermission");
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uploadKey, inputStream, new ObjectMetadata());
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        PutObjectResult putObjectResult = s3client.putObject(putObjectRequest);
        IOUtils.closeQuietly(inputStream);
        return putObjectResult;
    }

    @Override
    public boolean deleteReceipt(String receiptKey) {
        logger.info("method :::  deleteReceipt");
        try {
            s3client.deleteObject(new DeleteObjectRequest(bucketName, receiptKey));
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: deleteReceipt ::: error ::: " + e.getMessage());
            return false;
        }
    }

    public ResponseEntity<?> uploadImage(MultipartFile file) {
        logger.info("method :::  uploadImage");
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.getSize());
        double randomNum = Math.random() * 1000;
        S3Object s3Object = null;
        try {
            PutObjectResult putObjectRequest = uploadWithPermission(file.getInputStream(), FilenameUtils.removeExtension(file.getOriginalFilename()) + randomNum + FilenameUtils.getExtension(file.getOriginalFilename()));
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, FilenameUtils.removeExtension(file.getOriginalFilename()) + randomNum + FilenameUtils.getExtension(file.getOriginalFilename()));
            s3Object = s3client.getObject(getObjectRequest);
            return ResponseDomain.postResponse(this.url + s3Object.getKey());
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: uploadImage ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError("Something Wrong in save File successfully");
        } finally {
            if (s3Object != null) {
                try {
                    s3Object.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }


    }

    public ResponseEntity<?> uploadLeadersWall(MultipartFile file) {
        logger.info("method :::  uploadImage");
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.getSize());
//        double randomNum = Math.random() * 1000;
        S3Object s3Object = null;
        try {
            PutObjectResult putObjectRequest = uploadWithPermission(file.getInputStream(), file.getOriginalFilename());
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, file.getOriginalFilename());
            s3Object = s3client.getObject(getObjectRequest);
            return new ResponseEntity<>(this.url + s3Object.getKey(), HttpStatus.OK);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: uploadImage ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError("Something Wrong in save File successfully");
        } finally {
            if (s3Object != null) {
                try {
                    s3Object.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }


    }

}

