package com.mfm.api.service.moneyflowmanager.budget;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.budget.Budget;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.budget.BudgetRepository;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Budget Service Class
 * For a Money Flow Account
 */
@Service
@CacheConfig(cacheNames = "money-flow-budget-cache")
public class BudgetServiceImpl implements BudgetService {

    private static Logger logger = LogManager.getLogger(BudgetServiceImpl.class);

    @Autowired
    private BudgetRepository budgetRepository;
    @Autowired
    private MoneyFlowAccountRepository moneyFlowAccountRepository;
    @Autowired
    private MessageProperties messageProperties;

    /**
     * Creating a Budget
     *
     * @param budget
     * @return Response Message with Status Code
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.budget", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true)
    })
    public ResponseEntity<?> createBudget(Budget budget, Long moneyFlowAccountId) {
        logger.info("method ::: createBudget");
        try {
            MoneyFlowAccount moneyFlowAccount = new MoneyFlowAccount();
            moneyFlowAccount.setMoneyFlowAccountId(moneyFlowAccountId);
            budget.setMoneyFlowAccount(moneyFlowAccount);
            budget.setStartDate(CustomDateConverter.stringToLocalDate(budget.getFromDate()));
            budget.setEndDate(CustomDateConverter.stringToLocalDate(budget.getToDate()));
            budget.setAmount(CommonUtilFunctions.roundOf(budget.getAmount()));
            budgetRepository.save(budget);
            return ResponseDomain.postResponse(messageProperties.getMfmBudgetSave());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: createBudget ::: error " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Fetching Budgets
     *
     * @param moneyFlowAccountId
     * @return List Of Budgets
     */
    @Override
    @Cacheable(value = "money.flow.budget", key = "#root.methodName.concat(#moneyFlowAccountId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchBudgets(Long moneyFlowAccountId, String fromDate, String toDate) {
        logger.info("method ::: createBudget");
        try {
            MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(moneyFlowAccountId);
            if (moneyFlowAccount != null) {
                List<Budget> budgetList = budgetRepository.findAllByMoneyFlowAccount(moneyFlowAccount);
                if (fromDate != null && toDate != null) {
                    budgetList = budgetList.stream().filter(budget -> CommonUtilFunctions.dateRangeCheck(fromDate, toDate, budget.getStartDate(), budget.getEndDate()))
                            .map(obj -> new Budget(obj.getMoneyFlowBudgetId(), CustomDateConverter.localDateToString(obj.getStartDate()), CustomDateConverter.localDateToString(obj.getEndDate()), obj.getAmount()))
                            .collect(Collectors.toList());
                } else {
                    budgetList = budgetList.stream()
                            .map(obj -> new Budget(obj.getMoneyFlowBudgetId(), CustomDateConverter.localDateToString(obj.getStartDate()), CustomDateConverter.localDateToString(obj.getEndDate()), obj.getAmount()))
                            .collect(Collectors.toList());
                }
                return new ResponseEntity<>(budgetList, HttpStatus.OK);
            } else {
                return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method ::: createBudget ::: error " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Deleting a Budget
     *
     * @param moneyFlowBudgetId
     * @return
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.budget", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true)
    })
    public ResponseEntity<?> deleteBudget(Long moneyFlowBudgetId) {
        logger.info("method ::: moneyFlowBudgetId");
        Optional<Budget> budget = budgetRepository.findById(moneyFlowBudgetId);
        if (budget.isPresent()) {
            budgetRepository.delete(budget.get());
            return ResponseDomain.deleteResponse(messageProperties.getMfmBudgetDelete());
        } else
            return ResponseDomain.badRequest(messageProperties.getMfmBudgetNotExist());
    }

    /**
     * Updating a Budget
     *
     * @param moneyFlowBudgetId
     * @param budget
     * @return
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.budget", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> updateBudget(Long moneyFlowBudgetId, Budget budget) {
        logger.info("method ::: updateBudget");
        Optional<Budget> budgetEO = budgetRepository.findById(moneyFlowBudgetId);
        if (budgetEO.isPresent()) {
            budgetEO.get().setStartDate(CustomDateConverter.stringToLocalDate(budget.getFromDate()));
            budgetEO.get().setEndDate(CustomDateConverter.stringToLocalDate(budget.getToDate()));
            budgetEO.get().setAmount(CommonUtilFunctions.roundOf(budget.getAmount()));
            budgetRepository.save(budgetEO.get());
            return ResponseDomain.putResponse(messageProperties.getMfmBudgetUpdate());
        } else {
            return ResponseDomain.badRequest(messageProperties.getMfmBudgetNotExist());
        }
    }
}
