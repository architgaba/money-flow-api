package com.mfm.api.service.moneyflowmanager.expense;

import com.mfm.api.domain.moneyflowmanager.expense.ExpenseType;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface MoneyFlowExpenseService {

    ResponseEntity<?> createExpenseType(String auth, ExpenseType expenseType);

    ResponseEntity<?> fetchExpenseTypes(String auth);

    ResponseEntity<?> createExpense(Income_Expense_Dto income, String flowId,MultipartFile file);

    ResponseEntity<?> fetchAllExpenseOfAMoneyFlowAccount(String flowId, String fromDate, String toDate);

    ResponseEntity<?> updateExpense(Long moneyFlowExpenseId, Income_Expense_Dto expenseDto,MultipartFile file);

    ResponseEntity<?> deleteExpense(Long moneyFlowExpenseId);
}
