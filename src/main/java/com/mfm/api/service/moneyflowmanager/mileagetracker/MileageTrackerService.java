package com.mfm.api.service.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import org.springframework.http.ResponseEntity;

public interface MileageTrackerService {

    ResponseEntity<?> addVehicle(Vehicle vehicle, Long flowId);

    ResponseEntity<?> vehicleList(Long flowId);

    ResponseEntity<?> updateVehicle(Long vehicleId, Vehicle vehicle);

    ResponseEntity<?> deleteVehicle(Long vehicleId);

    ResponseEntity<?> addMileage(Long flowId, Mileage mileage);

    ResponseEntity<?> mileageList(Long flowId, String date, String toDate);

    ResponseEntity<?> updateMileage(Long mileageId, Long newFlowId, Mileage mileage);

    ResponseEntity<?> deleteMileage(Long mileageId);

    ResponseEntity<?> makeList();

    ResponseEntity<?> mergeMileage(Long tripId1, Long tripId2);

    ResponseEntity<?> getFirstFlowAndVehicle(String authKey);
}
