package com.mfm.api.service.moneyflowmanager.mileagetracker;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.mileagetracker.MileageRepository;
import com.mfm.api.repository.moneyflowmanager.mileagetracker.VehicleMakeRepository;
import com.mfm.api.repository.moneyflowmanager.mileagetracker.VehicleRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("mileageTracker")
@CacheConfig(cacheNames = "money-flow-mileageTracker-cache")
public class MileageTrackerServiceImpl implements MileageTrackerService {

    private static Logger logger = LogManager.getLogger(MileageTrackerServiceImpl.class);

    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private MoneyFlowAccountRepository moneyFlowAccountRepository;
    @Autowired
    private MileageRepository mileageRepository;
    @Autowired
    private VehicleMakeRepository vehicleMakeRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserRepository userRepository;

    /**
     * Adding A Vehicle
     * TO A Money Flow Account
     *
     * @param vehicle
     * @return
     */
    @Override
    @CacheEvict(value = "money.flow.mileageTracker.vehicle", allEntries = true)
    public ResponseEntity<?> addVehicle(Vehicle vehicle, Long flowId) {
        logger.info("method :::  addVehicle");
        if (vehicle != null && flowId != null) {
            MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(flowId);
            if (moneyFlowAccount == null)
                return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
            vehicle.setMoneyFlowAccount(moneyFlowAccount);
            vehicleRepository.save(vehicle);
            return ResponseDomain.postResponse(messageProperties.getVehicleSave());
        } else
            return ResponseDomain.badRequest(messageProperties.getInvalidInput());
    }

    /**
     * Fetching List Of Vehicles
     *
     * @param flowId
     * @return
     */
    @Override
    @Cacheable(value = "money.flow.mileageTracker.vehicle", key = "#root.methodName.concat(#flowId)", sync = true)
    public ResponseEntity<?> vehicleList(Long flowId) {
        logger.info("method :::  vehicleList");
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(flowId);
        if (moneyFlowAccount != null) {
            List<Vehicle> vehicles = vehicleRepository.findByMoneyFlowAccount(moneyFlowAccount);
            return new ResponseEntity(vehicles, HttpStatus.OK);
        } else
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.mileageTracker.vehicle", allEntries = true),
            @CacheEvict(value = "money.flow.mileageTracker.trips", allEntries = true)})
    public ResponseEntity<?> updateVehicle(Long vehicleId, Vehicle vehicle) {
        logger.info("method :::  updateVehicle");
        Optional<Vehicle> vehicle1 = vehicleRepository.findById(vehicleId);
        if (vehicle1.isPresent()) {
            vehicle1.get().setMake(vehicle.getMake());
            vehicle1.get().setModel(vehicle.getModel());
            vehicle1.get().setPlate(vehicle.getPlate());
            vehicle1.get().setOdoReading(vehicle.getOdoReading());
            vehicle1.get().setYear(vehicle.getYear());
            vehicleRepository.save(vehicle1.get());
            List<Mileage> mileageList = mileageRepository.findAllByVehicle(vehicle1.get());
            mileageList.forEach(mileage -> {
                mileage.setVehicleName(vehicle1.get().getYear() + " " + vehicle1.get().getMake() + " " + vehicle1.get().getModel());
                mileageRepository.save(mileage);
            });
            return ResponseDomain.putResponse(messageProperties.getVehicleUpdate());
        } else
            return ResponseDomain.badRequest(messageProperties.getVehicleNotExists());
    }

    @Override
    @CacheEvict(value = "money.flow.mileageTracker.vehicle", allEntries = true)
    public ResponseEntity<?> deleteVehicle(Long vehicleId) {
        logger.info("method :::  deleteVehicle");
        Optional<Vehicle> vehicle = vehicleRepository.findById(vehicleId);
        if (vehicle.isPresent()) {
            List<Vehicle> vehicleList = vehicleRepository.findByMoneyFlowAccount(vehicle.get().getMoneyFlowAccount());
            if (vehicleList.size() > 1) {
                vehicleRepository.delete(vehicle.get());
            } else {
                return ResponseDomain.badRequest("You cannot delete this Vehicle as it is your Default Vehicle");
            }
            return ResponseDomain.deleteResponse(messageProperties.getVehicleDelete());
        } else
            return ResponseDomain.badRequest(messageProperties.getVehicleNotExists());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.mileageTracker.trips", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> addMileage(Long flowId, Mileage mileage) {
        logger.info("method :::  addVehicle");
        if (mileage != null && flowId != null) {
            MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(flowId);
            Optional<Vehicle> vehicle = vehicleRepository.findById(mileage.getVehicleId());
            if (moneyFlowAccount == null || !vehicle.isPresent())
                return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
            mileage.setMoneyFlowAccount(moneyFlowAccount);
            mileage.setVehicle(vehicle.get());
            mileage.setDate(CustomDateConverter.stringToLocalDate(mileage.getTripDate()));
            mileage.setStartingTime(LocalTime.parse(mileage.getStartTime()));
            mileage.setEndingTime(LocalTime.parse(mileage.getEndTime()));
            mileageRepository.save(mileage);
            return ResponseDomain.postResponse(messageProperties.getMileageSave());
        } else
            return ResponseDomain.badRequest(messageProperties.getInvalidInput());
    }

    @Override
    @Cacheable(value = "money.flow.mileageTracker.trips", key = "#root.methodName.concat(#flowId).concat(#date!=null?#date:'')", sync = true)
    public ResponseEntity<?> mileageList(Long flowId, String date, String toDate) {
        logger.info("method :::  vehicleList");
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(flowId);
        if (moneyFlowAccount != null) {
            JSONObject jsonObject = new JSONObject();
          /*  List<MileageResponseObj> manualTrips = new ArrayList<>();
            List<MileageResponseObj> automaticTrips = new ArrayList<>();*/
            List<Mileage> mileageList1 = new ArrayList<>();
            List<Mileage> mileageList = mileageRepository.findByMoneyFlowAccountAndDateOrderByDateDescStartingTimeAsc(moneyFlowAccount, CustomDateConverter.stringToLocalDate(date));
            if (toDate != null)
                mileageList1 = mileageRepository.findByDateBeforeAndDateAfterAndMoneyFlowAccountOrderByDateDescStartingTimeAsc(CustomDateConverter.stringToLocalDate(toDate).plusDays(1), CustomDateConverter.stringToLocalDate(date).minusDays(1), moneyFlowAccount);
            List<Mileage> manualMileageList = new ArrayList<>();
            List<Mileage> automaticMileageList = new ArrayList<>();
           /* List<Mileage> manualMileageList = mileageList.stream().filter(mileage -> mileage.isManual()).collect(Collectors.toList());
            List<Mileage> automaticMileageList = mileageList.stream().filter(mileage -> !(mileage.isManual())).collect(Collectors.toList());*/
            if (!mileageList.isEmpty() && toDate == null) {
                manualMileageList = mileageList.stream().filter(mileage -> mileage.isManual()).map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle().getId(), mileage.getVehicle(), mileage.getVehicle().getYear() + " " + mileage.getVehicle().getMake() + " " + mileage.getVehicle().getModel(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                        mileage.getStartingTime(), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                        mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount(), mileage.isMerged())).collect(Collectors.toList());
                automaticMileageList = mileageList.stream().filter(mileage -> !(mileage.isManual())).map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle().getId(), mileage.getVehicle(), mileage.getVehicle().getYear() + " " + mileage.getVehicle().getMake() + " " + mileage.getVehicle().getModel(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                        mileage.getStartingTime(), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                        mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount(), mileage.isMerged())).collect(Collectors.toList());
            }

            if (!mileageList1.isEmpty() && toDate != null) {
                manualMileageList = mileageList1.stream().filter(mileage -> mileage.isManual()).map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle().getId(), mileage.getVehicle(), mileage.getVehicle().getYear() + " " + mileage.getVehicle().getMake() + " " + mileage.getVehicle().getModel(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                        mileage.getStartingTime(), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                        mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount(), mileage.isMerged())).collect(Collectors.toList());
                automaticMileageList = mileageList1.stream().filter(mileage -> !(mileage.isManual())).map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle().getId(), mileage.getVehicle(), mileage.getVehicle().getYear() + " " + mileage.getVehicle().getMake() + " " + mileage.getVehicle().getModel(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                        mileage.getStartingTime(), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                        mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount(), mileage.isMerged())).collect(Collectors.toList());
            }

      /*      if(!manualMileageList.isEmpty()) {
                List<String> manualVehicleList = manualMileageList.stream().map(mileage -> mileage.getVehicle()).distinct().collect(Collectors.toList());
                final Integer[] counter = new Integer[]{0};
                for (int i = 0; i < manualVehicleList.size(); i++) {
                    MileageResponseObj manualMileage = new MileageResponseObj();
                    manualMileage.setVehicleName(manualVehicleList.get(i));
                    manualMileage.setTrips(manualMileageList.stream().filter(mileage -> mileage.getVehicle().equals(manualVehicleList.get(counter[0])))
                            .map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                                    mileage.getStartingTime(  ), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                                    mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount())).collect(Collectors.toList()));
                    manualMileage.setTolls(CommonUtilFunctions.roundOf((float) manualMileage.getTrips().stream().mapToDouble(mileage -> mileage.getTolls()).sum()));
                    manualMileage.setParking(CommonUtilFunctions.roundOf((float) manualMileage.getTrips().stream().mapToDouble(mileage -> mileage.getParking()).sum()));
                    manualMileage.setTripCost(CommonUtilFunctions.roundOf((float) manualMileage.getTrips().stream().mapToDouble(mileage -> mileage.getTripCost()).sum()));
                    manualTrips.add(manualMileage);
                    counter[0]++;
                }
                counter[0]=0;
            }

            if(!automaticMileageList.isEmpty()) {
                List<String> vehicleList = automaticMileageList.stream().map(mileage -> mileage.getVehicle()).distinct().collect(Collectors.toList());
                final Integer[] counter = new Integer[]{0};
                for (int i = 0; i < vehicleList.size(); i++) {
                    MileageResponseObj automaticMileage = new MileageResponseObj();
                    automaticMileage.setVehicleName(vehicleList.get(i));
                    automaticMileage.setTrips(manualMileageList.stream().filter(mileage -> mileage.getVehicle().equals(vehicleList.get(counter[0])))
                            .map(mileage -> new Mileage(mileage.getId(), mileage.getVehicle(), mileage.getTripType(), mileage.getDate(), mileage.getFromAddress(), mileage.getToAddress(),
                                    mileage.getStartingTime(), mileage.getEndingTime(), mileage.getMileageRate(), mileage.getMilesDriven(), mileage.getTripCost(),
                                    mileage.getTolls(), mileage.getParking(), mileage.getTotalExpense(), mileage.isManual(), mileage.getMoneyFlowAccount())).collect(Collectors.toList()));
                    automaticMileage.setTolls(CommonUtilFunctions.roundOf((float) automaticMileage.getTrips().stream().mapToDouble(mileage -> mileage.getTolls()).sum()));
                    automaticMileage.setParking(CommonUtilFunctions.roundOf((float) automaticMileage.getTrips().stream().mapToDouble(mileage -> mileage.getParking()).sum()));
                    automaticMileage.setTripCost(CommonUtilFunctions.roundOf((float) automaticMileage.getTrips().stream().mapToDouble(mileage -> mileage.getTripCost()).sum()));
                    automaticTrips.add(automaticMileage);
                    counter[0]++;
                }
            }

            jsonObject.put("manualTrips", manualTrips);
            jsonObject.put("automaticTrips", automaticTrips);
            jsonObject.put("parking", CommonUtilFunctions.roundOf((float)(manualTrips.stream().mapToDouble(value -> value.getParking()).sum() + automaticTrips.stream().mapToDouble(value -> value.getParking()).sum())));
            jsonObject.put("tolls", CommonUtilFunctions.roundOf((float)(manualTrips.stream().mapToDouble(value -> value.getTolls()).sum() + automaticTrips.stream().mapToDouble(value -> value.getTolls()).sum())));
            jsonObject.put("tripCost", CommonUtilFunctions.roundOf((float)(manualTrips.stream().mapToDouble(value -> value.getTripCost()).sum() + automaticTrips.stream().mapToDouble(value -> value.getTripCost()).sum())));
            jsonObject.put("totalExpense", CommonUtilFunctions.roundOf((float)(manualTrips.stream().mapToDouble(value -> value.getTrips().stream().mapToDouble(value1 -> value1.getTotalExpense()).sum()).sum()
                    + automaticTrips.stream().mapToDouble(value -> value.getTripCost()).sum())));*/
            jsonObject.put("manualTrips", manualMileageList);
            jsonObject.put("automaticTrips", automaticMileageList);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } else
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.mileageTracker.trips", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> updateMileage(Long mileageId, Long newFlowId, Mileage mileage) {
        logger.info("method :::  vehicleList");
        Optional<Mileage> mileage1 = mileageRepository.findById(mileageId);
        Optional<MoneyFlowAccount> moneyFlowAccount = moneyFlowAccountRepository.findById(newFlowId);
        if (mileage1.isPresent() && moneyFlowAccount.isPresent()) {
            mileage1.get().setMoneyFlowAccount(moneyFlowAccount.get());
            mileage1.get().setVehicle(vehicleRepository.findById(mileage.getVehicleId()).get());
            mileage1.get().setTripType(mileage.getTripType());
            mileage1.get().setDate(CustomDateConverter.stringToLocalDate(mileage.getTripDate()));
            mileage1.get().setFromAddress(mileage.getFromAddress());
            mileage1.get().setToAddress(mileage.getToAddress());
            mileage1.get().setStartingTime(LocalTime.parse(mileage.getStartTime()));
            mileage1.get().setEndingTime(LocalTime.parse(mileage.getEndTime()));
            mileage1.get().setMileageRate(mileage.getMileageRate());
            mileage1.get().setMilesDriven(mileage.getMilesDriven());
            mileage1.get().setTripCost(mileage.getTripCost());
            mileage1.get().setTolls(mileage.getTolls());
            mileage1.get().setParking(mileage.getParking());
            mileage1.get().setTotalExpense(mileage.getTotalExpense());
            mileage.setTotalExpense(mileage.getTotalExpense());
            mileageRepository.save(mileage1.get());
            return ResponseDomain.putResponse(messageProperties.getMileageUpdate());
        } else
            return ResponseDomain.badRequest(messageProperties.getMileageNotExists());
    }


    @Override
    @Cacheable(value = "money.flow.mileageTracker.vehicle.makes", key = "#root.methodName")
    public ResponseEntity<?> makeList() {
        return new ResponseEntity(vehicleMakeRepository.findAllByOrderByNameAsc(), HttpStatus.OK);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.mileageTracker.trips", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true)
    })
    public ResponseEntity<?> deleteMileage(Long mileageId) {
        logger.info("method :::  deleteMileage");
        Optional<Mileage> mileage = mileageRepository.findById(mileageId);
        if (mileage.isPresent()) {
            mileageRepository.delete(mileage.get());
            return ResponseDomain.deleteResponse(messageProperties.getMileageDelete());
        } else
            return ResponseDomain.badRequest(messageProperties.getMileageNotExists());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.mileageTracker.trips", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true)
    })
    public ResponseEntity<?> mergeMileage(Long tripId1, Long tripId2) {
        logger.info("method :::  deleteMileage");
        Optional<Mileage> trip1 = mileageRepository.findById(tripId1);
        Optional<Mileage> trip2 = mileageRepository.findById(tripId2);
        if (trip1.isPresent() && trip2.isPresent()) {
            trip1.get().setEndingTime(trip2.get().getEndingTime());
            trip1.get().setToAddress(trip2.get().getToAddress());
            trip1.get().setMilesDriven(trip1.get().getMilesDriven() + trip2.get().getMilesDriven());
            trip1.get().setTripCost(trip1.get().getTripCost() + trip2.get().getTripCost());
            trip1.get().setParking(trip1.get().getParking() + trip2.get().getParking());
            trip1.get().setTolls(trip1.get().getTolls() + trip2.get().getTolls());
            trip1.get().setTotalExpense(trip1.get().getTotalExpense() + trip2.get().getTotalExpense());
            trip1.get().setMerged(true);
            mileageRepository.delete(trip2.get());
            mileageRepository.save(trip1.get());
            return ResponseDomain.putResponse(messageProperties.getMergeTrips());
        } else
            return ResponseDomain.badRequest(messageProperties.getSelectedTripsInvalid());
    }

    @Override
    public ResponseEntity<?> getFirstFlowAndVehicle(String authKey) {
        logger.info("method :::  getFirstFlowAndVehicle");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(authKey)));
        List<MoneyFlowAccount> userList = moneyFlowAccountRepository.findAllByUserOrderByMoneyFlowAccountId(user);
        if (userList.size() > 0) {
            MoneyFlowAccount moneyFlowAccount = userList.get(0);
            List<Vehicle> vehicleList = vehicleRepository.findAllByMoneyFlowAccountOrderById(moneyFlowAccount);
            Vehicle vehicle = vehicleList.get(0);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("moneyFlowAccountId", moneyFlowAccount.getMoneyFlowAccountId());
            jsonObject.put("vehicleId", vehicle.getId());
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
        return ResponseDomain.badRequest("No Flow Exist For You");
    }
}


