package com.mfm.api.service.moneyflowmanager.contactmanager;

import com.mfm.api.domain.moneyflowmanager.contactmanager.ContactManager;
import org.springframework.http.ResponseEntity;

public interface ContactManagerService {
    ResponseEntity<?> addContact(String auth, ContactManager contactManager);

    ResponseEntity<?> fetchAllContactByUser(String auth);

    ResponseEntity<?> deleteContactById(String auth, Long contactId);

    ResponseEntity<?> updateContact(String auth, Long contactId, String status, ContactManager contactManager);
}
