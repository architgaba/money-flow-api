package com.mfm.api.service.moneyflowmanager.report;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.budget.Budget;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Mileage;
import com.mfm.api.dto.moneyflowmanager.report.MonthWiseTotals;
import com.mfm.api.dto.moneyflowmanager.report.MonthWiseVO;
import com.mfm.api.dto.moneyflowmanager.report.ReportVO;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.budget.BudgetRepository;
import com.mfm.api.repository.moneyflowmanager.expense.ExpenseRepository;
import com.mfm.api.repository.moneyflowmanager.income.IncomeRepository;
import com.mfm.api.repository.moneyflowmanager.mileagetracker.MileageRepository;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("reports")
@CacheConfig(cacheNames = "money-flow-report-cache")
public class ReportServiceImpl implements ReportService {

    private static Logger logger = LogManager.getLogger(ReportServiceImpl.class);

    @Autowired
    private IncomeRepository incomeRepository;
    @Autowired
    private ExpenseRepository expenseRepository;
    @Autowired
    private MoneyFlowAccountRepository moneyFlowAccountRepository;
    @Autowired
    private BudgetRepository budgetRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private MileageRepository mileageRepository;

    /**
     * Profit/Loss Report Generation
     * For A Mentioned Year
     *
     * @param moneyFlowAccountId
     * @param year
     * @return
     */
    @Override
    @Cacheable(value = "money.flow.report-profit-loss", key = "#root.methodName.concat(#moneyFlowAccountId).concat(#year!=null?#year:'')", sync = true)
    public ResponseEntity<?> profitLossReport(Long moneyFlowAccountId, Integer year) {
        logger.info("method :::  profitLossReport");
        try {
            final LocalDate currentDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), 01);
            MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(moneyFlowAccountId);
            if (moneyFlowAccount != null) {
                List<Income> incomeList = incomeRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(income -> !income.isUnassigned())
                        .collect(Collectors.toList());
                List<String> incomeTypes = incomeList.stream().map(income -> income.getIncomeType()).distinct().collect(Collectors.toList());
                List<Expense> expenseList = expenseRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(expense -> !expense.isUnassigned())
                        .collect(Collectors.toList());
                List<String> expenseTypes = expenseList.stream().map(expense -> expense.getExpenseType()).distinct().collect(Collectors.toList());
                List<Mileage> mileageList = mileageRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream()
                        .filter(mileage -> mileage.getDate().getYear() == year).collect(Collectors.toList());
                List<Budget> budgetList = budgetRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().
                        filter(budget -> budget.getStartDate().getYear() == year).collect(Collectors.toList());

                JSONObject object = new JSONObject();
                List<ReportVO> incomeVO = new ArrayList<>();
                List<ReportVO> expenseVO = new ArrayList<>();
                final int[] monthPosition = new int[]{1};
                final int[] typePosition = new int[]{0};

                if (!incomeList.isEmpty()) {
                    for (int j = 0; j < incomeTypes.size(); j++) {
                        ArrayList<MonthWiseTotals> typeTotalMonthWise = new ArrayList<>();
                        ReportVO incomes = new ReportVO();
                        incomes.setTypeName(incomeTypes.get(j));
                        incomes.setTotal(CommonUtilFunctions.roundOf((float) incomeList.stream().filter(income -> income.getIncomeType().equals(incomeTypes.get(typePosition[0])))
                                .mapToDouble(income -> income.getAmount()).sum()));
                        for (int i = 0; i < 12; i++) {
                            LocalDate fromDate = LocalDate.of(year, i + 1, 01);
                            LocalDate toDate = fromDate.withDayOfMonth(
                                    fromDate.getMonth().length(fromDate.isLeapYear()));
                            String stringFromDate = fromDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                            String stringToDate = toDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                            MonthWiseTotals typeTotalMonthWise1 = new MonthWiseTotals();
                            typeTotalMonthWise1.setAmount(CommonUtilFunctions.roundOf((float) incomeList.stream()
                                    .filter(income -> income.getIncomeType().equals(incomeTypes.get(typePosition[0])) && CommonUtilFunctions.filterRecurringIncome(stringFromDate, stringToDate, income) > 0 && fromDate.compareTo(currentDate) <= 0)
                                    .mapToDouble(income -> CommonUtilFunctions.filterRecurringIncome(stringFromDate, stringToDate, income) * income.getAmount()).sum()));
                            typeTotalMonthWise1.setMonthName(new DateFormatSymbols().getMonths()[i]);
                            monthPosition[0]++;
                            typeTotalMonthWise.add(typeTotalMonthWise1);
                        }
                        incomes.setAmount(typeTotalMonthWise);
                        incomeVO.add(incomes);
                        typePosition[0]++;
                        monthPosition[0] = 1;
                    }
                    typePosition[0] = 0;
                    monthPosition[0] = 1;
                }

                if (!expenseList.isEmpty()) {
                    for (int j = 0; j < expenseTypes.size(); j++) {
                        ArrayList<MonthWiseTotals> typeTotalMonthWise = new ArrayList<>();
                        ReportVO expenses = new ReportVO();
                        expenses.setTypeName(expenseTypes.get(j));
                        expenses.setTotal(CommonUtilFunctions.roundOf((float) expenseList.stream().filter(exp -> exp.getExpenseType().equals(expenseTypes.get(typePosition[0])))
                                .mapToDouble(expense -> expense.getAmount()).sum()));
                        for (int i = 0; i < 12; i++) {
                            LocalDate fromDate = LocalDate.of(year, i + 1, 01);
                            LocalDate toDate = fromDate.withDayOfMonth(
                                    fromDate.getMonth().length(fromDate.isLeapYear()));
                            String stringFromDate = fromDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                            String stringToDate = toDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                            MonthWiseTotals typeTotalMonthWise1 = new MonthWiseTotals();
                            typeTotalMonthWise1.setAmount(CommonUtilFunctions.roundOf((float) expenseList.stream()
                                    .filter(expense -> expense.getExpenseType().equals(expenseTypes.get(typePosition[0])) && CommonUtilFunctions.filterRecurringExpense(stringFromDate, stringToDate, expense) > 0 && fromDate.compareTo(currentDate) <= 0)
                                    .mapToDouble(expense -> CommonUtilFunctions.filterRecurringExpense(stringFromDate, stringToDate, expense) * expense.getAmount()).sum()));
                            typeTotalMonthWise1.setMonthName(new DateFormatSymbols().getMonths()[i]);
                            monthPosition[0]++;
                            typeTotalMonthWise.add(typeTotalMonthWise1);
                        }
                        expenses.setAmount(typeTotalMonthWise);
                        expenseVO.add(expenses);
                        typePosition[0]++;
                        monthPosition[0] = 1;
                    }
                    typePosition[0] = 0;
                    monthPosition[0] = 1;
                }

                if (!mileageList.isEmpty()) {
                    ArrayList<MonthWiseTotals> mileageTotals = new ArrayList<>();
                    ReportVO mileages = new ReportVO();
                    mileages.setTypeName("Mileage Expense");
                    mileages.setTotal(CommonUtilFunctions.roundOf((float) mileageList.stream().mapToDouble(mileage -> mileage.getTotalExpense()).sum()));
                    for (int i = 0; i < 12; i++) {
                        MonthWiseTotals monthWiseTotals = new MonthWiseTotals();
                        monthWiseTotals.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream()
                                .filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).mapToDouble(mileage -> mileage.getTotalExpense()).sum()));
                        monthWiseTotals.setMonthName(new DateFormatSymbols().getMonths()[i]);
                        monthPosition[0]++;
                        mileageTotals.add(monthWiseTotals);
                    }
                    mileages.setAmount(mileageTotals);
                    expenseVO.add(mileages);
                    monthPosition[0] = 1;
                }

                MonthWiseVO incomeSum = new MonthWiseVO();
                MonthWiseVO expenseSum = new MonthWiseVO();
                MonthWiseVO budgetSum = new MonthWiseVO();
                MonthWiseVO profitLossSum = new MonthWiseVO();
                MonthWiseVO deviationSum = new MonthWiseVO();


                if (!incomeList.isEmpty() || !expenseList.isEmpty() || !mileageList.isEmpty() || !budgetList.isEmpty()) {
                    ArrayList<MonthWiseTotals> monthWiseIncomeTotal = new ArrayList<>();
                    ArrayList<MonthWiseTotals> monthWiseExpenseTotal = new ArrayList<>();
                    ArrayList<MonthWiseTotals> monthWiseBudgetTotal = new ArrayList<>();
                    ArrayList<MonthWiseTotals> monthWiseProfitLossTotal = new ArrayList<>();
                    ArrayList<MonthWiseTotals> monthWiseDeviation = new ArrayList<>();
                    for (int i = 0; i < 12; i++) {
                        LocalDate fromDate = LocalDate.of(year, i + 1, 01);
                        LocalDate toDate = fromDate.withDayOfMonth(
                                fromDate.getMonth().length(fromDate.isLeapYear()));
                        String stringFromDate = fromDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                        String stringToDate = toDate.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
                        String monthName = new DateFormatSymbols().getMonths()[i];
                        MonthWiseTotals incomes = new MonthWiseTotals();
                        incomes.setAmount(CommonUtilFunctions.roundOf((float) incomeList.stream().filter(income -> CommonUtilFunctions.filterRecurringIncome(stringFromDate, stringToDate, income) > 0 && fromDate.compareTo(currentDate) <= 0)
                                .mapToDouble(income -> CommonUtilFunctions.filterRecurringIncome(stringFromDate, stringToDate, income) * income.getAmount()).sum()));
                        incomes.setMonthName(monthName);
                        MonthWiseTotals expenses = new MonthWiseTotals();
                        expenses.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).mapToDouble(mileage -> mileage.getTotalExpense()).sum()
                                + (float) expenseList.stream().filter(expense -> CommonUtilFunctions.filterRecurringExpense(stringFromDate, stringToDate, expense) > 0 && fromDate.compareTo(currentDate) <= 0).mapToDouble(expense -> CommonUtilFunctions.filterRecurringExpense(stringFromDate, stringToDate, expense) * expense.getAmount()).sum()));
                        expenses.setMonthName(monthName);
                        MonthWiseTotals budgetTotal = new MonthWiseTotals();
                        budgetTotal.setAmount(CommonUtilFunctions.roundOf((float) budgetList.stream().filter(budget -> budget.getStartDate().getMonth().getValue() == monthPosition[0])
                                .mapToDouble(budget -> budget.getAmount()).sum()));
                        budgetTotal.setMonthName(monthName);
                        MonthWiseTotals profitLossTotal = new MonthWiseTotals();
                        profitLossTotal.setAmount(CommonUtilFunctions.roundOf(incomes.getAmount() - expenses.getAmount()));
                        profitLossTotal.setMonthName(monthName);
                        MonthWiseTotals deviation = new MonthWiseTotals();
                        deviation.setMonthName(monthName);
                        deviation.setAmount((expenses.getAmount() == 0 || budgetTotal.getAmount() == 0) ? 0 : CommonUtilFunctions.roundOf(((expenses.getAmount() / budgetTotal.getAmount()) * 100)));
                        monthWiseIncomeTotal.add(incomes);
                        monthWiseExpenseTotal.add(expenses);
                        monthWiseBudgetTotal.add(budgetTotal);
                        monthWiseProfitLossTotal.add(profitLossTotal);
                        monthWiseDeviation.add(deviation);
                        if (i == 11) {
                            incomeSum.setAmount(monthWiseIncomeTotal);
                            incomeSum.setTotal(CommonUtilFunctions.roundOf((float) monthWiseIncomeTotal.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                            expenseSum.setAmount(monthWiseExpenseTotal);
                            expenseSum.setTotal(CommonUtilFunctions.roundOf((float) monthWiseExpenseTotal.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                            budgetSum.setAmount(monthWiseBudgetTotal);
                            budgetSum.setTotal(CommonUtilFunctions.roundOf((float) monthWiseBudgetTotal.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                            profitLossSum.setAmount(monthWiseProfitLossTotal);
                            profitLossSum.setTotal(CommonUtilFunctions.roundOf((float) monthWiseProfitLossTotal.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                            deviationSum.setAmount(monthWiseDeviation);
                            deviationSum.setTotal(CommonUtilFunctions.roundOf((float) monthWiseDeviation.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                        }
                        monthPosition[0]++;
                    }

                }
                object.put("income", incomeVO);
                object.put("expense", expenseVO);
                object.put("incomeTotal", incomeVO.isEmpty() ? null : incomeSum);
                object.put("expenseTotal", (expenseVO.isEmpty() && mileageList.isEmpty()) ? null : expenseSum);
                object.put("budgetTotal", budgetList.isEmpty() ? null : budgetSum);
                object.put("profitLossTotal", (incomeList.isEmpty() && expenseList.isEmpty()) ? null : profitLossSum);
                object.put("deviationTotal", (expenseList.isEmpty() && budgetList.isEmpty()) ? null : deviationSum);
                return new ResponseEntity<>(object, HttpStatus.OK);
            } else
                return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  profitLossReport ::: Error ::: " + e.getMessage());
            e.printStackTrace();
            return ResponseDomain.internalServerError(e.getMessage());
        }
    }


    @Override
    @Cacheable(value = "money.flow.report-mileage", key = "#root.methodName.concat(#moneyFlowAccountId).concat(#vehicle).concat(#year)", sync = true)
    public ResponseEntity<?> mileageReport(Long moneyFlowAccountId, Integer year, String vehicle) {
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(moneyFlowAccountId);
        if (moneyFlowAccount != null) {
            List<Mileage> mileageList = mileageRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().
                    filter(mileage -> (mileage.getDate().getYear() == year && mileage.getVehicle().getId().equals(Long.valueOf(vehicle)))).collect(Collectors.toList());
            List<String> tripTypes = mileageList.stream().map(mileage -> mileage.getTripType()).distinct().collect(Collectors.toList());

            JSONObject responseObj = new JSONObject();
            List<ReportVO> businessTrips = new ArrayList<>();
            List<ReportVO> personalTrips = new ArrayList<>();
            List<ReportVO> commuteTrips = new ArrayList<>();
            final int[] monthPosition = new int[]{1};
            final int[] typePosition = new int[]{0};

            if (!mileageList.isEmpty() && !tripTypes.isEmpty()) {
                for (int j = 0; j < tripTypes.size(); j++) {
                    ArrayList<MonthWiseTotals> mileageTrip1 = new ArrayList<>();
                    ArrayList<MonthWiseTotals> mileageTrip2 = new ArrayList<>();
                    ArrayList<MonthWiseTotals> mileageTrip3 = new ArrayList<>();
                    ArrayList<MonthWiseTotals> mileageTrip4 = new ArrayList<>();
                    ReportVO trip1 = new ReportVO();
                    ReportVO trip2 = new ReportVO();
                    ReportVO trip3 = new ReportVO();
                    ReportVO trip4 = new ReportVO();
                    trip1.setTypeName("Mileage (miles)");
                    trip1.setTotal(CommonUtilFunctions.roundOf((float) (mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                            .mapToDouble(mileage -> mileage.getMilesDriven())).sum()));
                    trip2.setTypeName("Standard Mileage Rate($)");
                    trip2.setTotal(standardMileageRate(mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0]))).collect(Collectors.toList())));
                    trip3.setTypeName("Parking($)");
                    trip3.setTotal(CommonUtilFunctions.roundOf((float) (mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                            .mapToDouble(mileage -> mileage.getParking())).sum()));
                    trip4.setTypeName("Tolls($)");
                    trip4.setTotal(CommonUtilFunctions.roundOf((float) (mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                            .mapToDouble(mileage -> mileage.getTolls())).sum()));
                    for (int i = 0; i < 12; i++) {
                        MonthWiseTotals trip1Data = new MonthWiseTotals();
                        MonthWiseTotals trip2Data = new MonthWiseTotals();
                        MonthWiseTotals trip3Data = new MonthWiseTotals();
                        MonthWiseTotals trip4Data = new MonthWiseTotals();
                        trip1Data.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                                .filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).mapToDouble(mileage -> mileage.getMilesDriven()).sum()));
                        trip1Data.setMonthName(new DateFormatSymbols().getMonths()[i]);
                        trip2Data.setAmount(standardMileageRate(mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                                .filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).collect(Collectors.toList())));
                        trip2Data.setMonthName(new DateFormatSymbols().getMonths()[i]);
                        trip3Data.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                                .filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).mapToDouble(mileage -> mileage.getParking()).sum()));
                        trip3Data.setMonthName(new DateFormatSymbols().getMonths()[i]);
                        trip4Data.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> mileage.getTripType().equals(tripTypes.get(typePosition[0])))
                                .filter(mileage -> mileage.getDate().getMonth().getValue() == monthPosition[0]).mapToDouble(mileage -> mileage.getTolls()).sum()));
                        trip4Data.setMonthName(new DateFormatSymbols().getMonths()[i]);
                        monthPosition[0]++;
                        mileageTrip1.add(trip1Data);
                        mileageTrip2.add(trip2Data);
                        mileageTrip3.add(trip3Data);
                        mileageTrip4.add(trip4Data);
                    }
                    trip1.setAmount(mileageTrip1);
                    trip2.setAmount(mileageTrip2);
                    trip3.setAmount(mileageTrip3);
                    trip4.setAmount(mileageTrip4);
                    if (tripTypes.get(typePosition[0]).equalsIgnoreCase("Business")) {
                        businessTrips.add(trip1);
                        businessTrips.add(trip2);
                        businessTrips.add(trip3);
                        businessTrips.add(trip4);
                    } else if (tripTypes.get(typePosition[0]).equalsIgnoreCase("Personal")) {
                        personalTrips.add(trip1);
                        personalTrips.add(trip2);
                        personalTrips.add(trip3);
                        personalTrips.add(trip4);
                    } else if (tripTypes.get(typePosition[0]).equalsIgnoreCase("Commute")) {
                        commuteTrips.add(trip1);
                        commuteTrips.add(trip2);
                        commuteTrips.add(trip3);
                        commuteTrips.add(trip4);
                    }
                    typePosition[0]++;
                    monthPosition[0] = 1;
                }
            }

            MonthWiseVO businessMileagesTotal = new MonthWiseVO();
            MonthWiseVO personalMileagesTotal = new MonthWiseVO();
            MonthWiseVO commuteMileagesTotal = new MonthWiseVO();

            if (!mileageList.isEmpty()) {

                ArrayList<MonthWiseTotals> businessMonthWiseSum = new ArrayList<>();
                ArrayList<MonthWiseTotals> personalMonthWiseSum = new ArrayList<>();
                ArrayList<MonthWiseTotals> commuteMonthWiseSum = new ArrayList<>();

                for (int i = 0; i < 12; i++) {
                    String monthName = new DateFormatSymbols().getMonths()[i];
                    MonthWiseTotals businessTrip = new MonthWiseTotals();
                    businessTrip.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> (mileage.getTripType().equalsIgnoreCase("Business") && mileage.getDate().getMonth().getValue() == monthPosition[0]))
                            .mapToDouble(mileage -> mileage.getTotalExpense()).sum()));
                    businessTrip.setMonthName(monthName);
                    MonthWiseTotals personalTrip = new MonthWiseTotals();
                    personalTrip.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> (mileage.getTripType().equalsIgnoreCase("Personal") && mileage.getDate().getMonth().getValue() == monthPosition[0]))
                            .mapToDouble(mileage -> mileage.getTotalExpense()).sum()));
                    personalTrip.setMonthName(monthName);
                    MonthWiseTotals commuteTrip = new MonthWiseTotals();
                    commuteTrip.setAmount(CommonUtilFunctions.roundOf((float) mileageList.stream().filter(mileage -> (mileage.getTripType().equalsIgnoreCase("Commute") && mileage.getDate().getMonth().getValue() == monthPosition[0]))
                            .mapToDouble(mileage -> mileage.getTotalExpense()).sum()));
                    commuteTrip.setMonthName(monthName);

                    businessMonthWiseSum.add(businessTrip);
                    personalMonthWiseSum.add(personalTrip);
                    commuteMonthWiseSum.add(commuteTrip);

                    if (i == 11) {
                        businessMileagesTotal.setAmount(businessMonthWiseSum);
                        businessMileagesTotal.setTotal(CommonUtilFunctions.roundOf((float) businessMonthWiseSum.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                        personalMileagesTotal.setAmount(personalMonthWiseSum);
                        personalMileagesTotal.setTotal(CommonUtilFunctions.roundOf((float) personalMonthWiseSum.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                        commuteMileagesTotal.setAmount(commuteMonthWiseSum);
                        commuteMileagesTotal.setTotal(CommonUtilFunctions.roundOf((float) commuteMonthWiseSum.stream().mapToDouble(MonthWiseTotals::getAmount).sum()));
                    }
                    monthPosition[0]++;
                }

            }

            responseObj.put("business", businessTrips);
            responseObj.put("personal", personalTrips);
            responseObj.put("commute", commuteTrips);
            responseObj.put("businessTotal", businessTrips.isEmpty() ? null : businessMileagesTotal);
            responseObj.put("personalTotal", personalTrips.isEmpty() ? null : personalMileagesTotal);
            responseObj.put("commuteTotal", commuteTrips.isEmpty() ? null : commuteMileagesTotal);
            return new ResponseEntity<>(responseObj, HttpStatus.OK);
        } else
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
    }

    private float standardMileageRate(List<Mileage> mileages) {
        if (mileages.isEmpty())
            return 0.0f;
        else
            return mileages.stream().findFirst().get().getMileageRate();
    }
}
