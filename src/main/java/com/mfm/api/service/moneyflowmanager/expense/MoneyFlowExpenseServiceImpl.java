package com.mfm.api.service.moneyflowmanager.expense;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.account.RecurrenceInterval;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import com.mfm.api.domain.moneyflowmanager.expense.ExpenseType;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.expense.ExpenseRepository;
import com.mfm.api.repository.moneyflowmanager.expense.ExpenseTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.storage.AmazonS3ClientService;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.response.AppProperties;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Expense Service Class
 * For a Money Flow Account
 */
@Service
@CacheConfig(cacheNames = "money-flow-expense-cache")
public class MoneyFlowExpenseServiceImpl implements MoneyFlowExpenseService {
    private static Logger logger = LogManager.getLogger(MoneyFlowExpenseServiceImpl.class);
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private ExpenseTypeRepository expenseTypeRepository;
    @Autowired
    private ExpenseRepository expenseRepository;
    @Autowired
    private MoneyFlowAccountRepository flowAccountRepository;
    @Autowired
    private AmazonS3ClientService amazonS3ClientService;
    @Value("${gkz.s3.url}")
    private String url;

    /**
     * Creating a Expense Type
     *
     * @param auth
     * @param expenseType
     * @return Response Message With Status Code
     */
    @Override
    @CacheEvict(value = "money.flow.expense.types", allEntries = true)
    public ResponseEntity<?> createExpenseType(String auth, ExpenseType expenseType) {
        logger.info("method :::  createExpenseType");
        String userId = tokenProvider.getUserId(auth);
        if (expenseType.getName() == null) {
            return ResponseDomain.badRequest(messageProperties.getInvalidInput());
        } else {
            expenseType.setUser(userRepository.findByUserRegistrationId(Long.parseLong(userId)));
            expenseType.setCustom(true);
            expenseTypeRepository.save(expenseType);
            return ResponseDomain.postResponse(messageProperties.getMfmExpenseTypeSave());
        }
    }

    /**
     * Fetching Expense Types
     * of a User
     *
     * @param auth
     * @return List Of Expense Types
     */
    @Override
    @Cacheable(value = "money.flow.expense.types", key = "#root.methodName.concat(#auth)", sync = true)
    public ResponseEntity<?> fetchExpenseTypes(String auth) {
        logger.info("method :::  fetchExpenseTypes");
        Long userId = Long.parseLong(tokenProvider.getUserId(auth));
        List<ExpenseType> expenseTypes = expenseTypeRepository.findAll().stream().filter
                (expenseType -> expenseType.getUser() == null || expenseType.getUser().getUserRegistrationId().equals(userId)).collect(Collectors.toList());
        return new ResponseEntity<>(expenseTypes, HttpStatus.OK);
    }

    /**
     * Creating a Expense
     * For a Money Flow Account
     *
     * @param expenseDto
     * @param flowId
     * @return Response Message With Status Code
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.expense", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> createExpense(Income_Expense_Dto expenseDto, String flowId, MultipartFile file) {
        logger.info("method :::  createExpense");
        Expense expense = expenseDto.DtoToExpense(expenseDto);
        try {
            if (expenseDto.isRecurring() && expenseDto.getRecurrenceDate() == null) {
                return ResponseDomain.badRequest(messageProperties.getRecurrenceDateNotSelected());
            } else {
                expense.setEntryDate(CustomDateConverter.stringToLocalDate(expenseDto.getDate()));
                MoneyFlowAccount moneyFlowAccount = new MoneyFlowAccount();
                moneyFlowAccount.setMoneyFlowAccountId(Long.parseLong(flowId));
                expense.setMoneyFlowAccount(moneyFlowAccount);
                if (expense.isRecurring()) {
                    expense.setRecurringDate(CustomDateConverter.stringToLocalDate(expenseDto.getRecurrenceDate()));
                    try {
                        expense.setRecurrenceInterval(RecurrenceInterval.valueOf(expenseDto.getRecurrenceInterval()));
                    } catch (Exception e) {
                        return ResponseDomain.badRequest("Please select a valid recurrence interval.");
                    }
                }

                Expense expenseEO = expenseRepository.save(expense);
                expenseRepository.flush();
                expenseEO.getMoneyFlowExpenseId();
                if (file != null && expenseEO != null) {
                    ResponseEntity entity = amazonS3ClientService.saveIncome_Expense_File(file, expenseEO.getMoneyFlowExpenseId(), "EXP");
                    if (entity.getStatusCode().value() == 200) {
                        expenseEO.setReceiptKey(entity.getBody().toString());
                        expenseRepository.save(expenseEO);
                    }
                }
                return ResponseDomain.postResponse(messageProperties.getMfmExpenseSave());
            }
        } catch (DateTimeParseException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  createExpense ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getInvalidDateFormat());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  createExpense ::: Error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Fetching ALl the Expenses
     * For a Money FlowAccount
     *
     * @param flowId
     * @return List Of Expenses
     */
    @Override
    @Cacheable(value = "money.flow.expense", key = "#root.methodName.concat(#flowId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchAllExpenseOfAMoneyFlowAccount(String flowId, String fromDate, String toDate) {
        logger.info("method :::  fetchAllExpenseOfAMoneyFlowAccount");
        MoneyFlowAccount flowAccount = flowAccountRepository.findByMoneyFlowAccountId(Long.parseLong(flowId));
        if (flowAccount != null) {
            List<Expense> expenseList = expenseRepository.findAllByMoneyFlowAccount(flowAccount);
            if (fromDate != null && toDate != null) {
                expenseList = expenseList.stream().filter(expense -> !expense.isUnassigned() && CommonUtilFunctions.dateRangeCheck(fromDate, toDate, expense.getEntryDate()))
                        .map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "", obj.getMoneyFlowAccount(),
                                obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval())).collect(Collectors.toList());
            } else {
                expenseList = expenseList.stream().filter(expense -> !expense.isUnassigned())
                        .map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "", obj.getMoneyFlowAccount(),
                                obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval())).collect(Collectors.toList());
            }
            return new ResponseEntity<>(expenseList, HttpStatus.OK);

        } else {
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
        }
    }

    /**
     * Updating an Expense
     *
     * @param expenseId
     * @param expenseDto
     * @return Response Message with Status Code
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.expense", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> updateExpense(Long expenseId, Income_Expense_Dto expenseDto, MultipartFile file) {
        logger.info("method :::  updateExpense");
        try {
            boolean fileDeleteStatus;
            Expense expense = expenseRepository.findById(expenseId).get();
            if (file != null) {
                fileDeleteStatus = expense.getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(expense.getReceiptKey()) : false;
                ResponseEntity entity = amazonS3ClientService.saveIncome_Expense_File(file, expense.getMoneyFlowExpenseId(), "EXP");
                if (entity.getStatusCode().value() == 200) {
                    expense.setReceiptKey(entity.getBody().toString());
                    expenseRepository.save(expense);
                }
            }
            expense.setAmount(expenseDto.getAmount());
            expense.setExpenseType(expenseDto.getType());
            expense.setSource(expenseDto.getSource());
            expense.setNotes(expenseDto.getNotes());
            expense.setRecurring(expenseDto.isRecurring());
            expense.setEntryDate(CustomDateConverter.stringToLocalDate(expenseDto.getDate()));
            if (expense.isRecurring()) {
                expense.setRecurringDate(CustomDateConverter.stringToLocalDate(expenseDto.getRecurrenceDate()));
                try {
                    expense.setRecurrenceInterval(RecurrenceInterval.valueOf(expenseDto.getRecurrenceInterval()));
                } catch (Exception e) {
                    return ResponseDomain.badRequest("Please select a valid recurrence interval.");
                }
            } else {
                expense.setRecurringDate(null);
                expense.setRecurrenceInterval(null);
            }
            expenseRepository.save(expense);
            return ResponseDomain.putResponse(messageProperties.getMfmExpenseUpdate());
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage(), e);
            logger.error("updateExpense ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getMfmExpenseNotExists());
        } catch (DateTimeParseException e) {
            logger.error("updateExpense ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getInvalidDateFormat());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("updateExpense ::: Error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Deleting Expense of
     * a Money Flow Account
     *
     * @param expenseId
     * @return status
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.expense", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> deleteExpense(Long expenseId) {
        logger.info("method :::  deleteExpense");
        try {
            boolean fileDelete;
            Optional<Expense> expenseEO = expenseRepository.findById(expenseId);
            if (expenseEO.isPresent()) {
                fileDelete = expenseEO.get().getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(expenseEO.get().getReceiptKey()) : false;
                expenseRepository.deleteById(expenseId);
                return ResponseDomain.deleteResponse(messageProperties.getMfmExpenseDelete());
            } else {
                return ResponseDomain.badRequest(messageProperties.getMfmExpenseNotExists());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("deleteExpense ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }
}
