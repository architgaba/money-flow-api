package com.mfm.api.service.moneyflowmanager.report;

import org.springframework.http.ResponseEntity;

public interface ReportService {

    ResponseEntity<?> profitLossReport(Long moneyFlowAccountId, Integer year);

    ResponseEntity<?> mileageReport(Long moneyFlowAccountId, Integer year, String vehicle);
}
