package com.mfm.api.service.moneyflowmanager.budget;

import com.mfm.api.domain.moneyflowmanager.budget.Budget;
import org.springframework.http.ResponseEntity;

public interface BudgetService {

    ResponseEntity<?> createBudget(Budget budget, Long moneyFlowAccountId);

    ResponseEntity<?> fetchBudgets(Long moneyFlowAccountId, String fromDate, String toDate);

    ResponseEntity<?> deleteBudget(Long moneyFlowBudgetId);

    ResponseEntity<?> updateBudget(Long moneyFlowBudgetId,Budget budget);
}
