package com.mfm.api.service.moneyflowmanager.income;

import com.mfm.api.domain.moneyflowmanager.income.IncomeType;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface MoneyFlowIncomeService {

    ResponseEntity<?> createIncome(Income_Expense_Dto income, String flowId, MultipartFile file);
    ResponseEntity<?> createIncomeType(String authKey, IncomeType incomeType);
    ResponseEntity<?> fetchAllIncomeTypes(String authKey);
    ResponseEntity<?> fetchAllIncomeOfAMoneyFlowAccount(String flowId, String fromDate, String toDate);
    ResponseEntity<?> updateIncome(Long incomeId, Income_Expense_Dto income_expense_dto,MultipartFile file);
    ResponseEntity<?> deleteIncome(Long incomeId);
}
