package com.mfm.api.service.moneyflowmanager.contactmanager;

import com.mfm.api.domain.moneyflowmanager.contactmanager.ContactManager;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.moneyflowmanager.contactmanager.ContactManagerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactManagerServiceImpl implements ContactManagerService {

    private static Logger logger = LogManager.getLogger(ContactManagerServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private ContactManagerRepository contactManagerRepository;

    @Override
    public ResponseEntity<?> addContact(String auth, ContactManager contactManager) {
        logger.info("method :::  addContact");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            boolean status = contactManagerRepository.existsByEmailAddress(contactManager.getEmailAddress());
            if (status)
                return ResponseDomain.badRequest(messageProperties.getContactManagerExists());
            else {
                contactManager.setUser(user);
                contactManager = contactManagerRepository.save(contactManager);
                if (contactManager != null)
                    return ResponseDomain.postResponse(messageProperties.getContactManagerSave());
                else
                    return ResponseDomain.internalServerError();
            }
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }

    }

    @Override
    public ResponseEntity<?> fetchAllContactByUser(String auth) {
        logger.info(" method :::  fetchAllContactByUser");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<ContactManager> contactManagerList = contactManagerRepository.findAllByUser(user);
            if (!contactManagerList.isEmpty())
                return new ResponseEntity<>(contactManagerList, HttpStatus.OK);
            else
                return new ResponseEntity<>(contactManagerList, HttpStatus.OK);
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }


    @Override
    public ResponseEntity<?> deleteContactById(String auth, Long contactId) {
        logger.info(" method :::  fetchAllContactByUser");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Optional<ContactManager> contactManager = contactManagerRepository.findById(contactId);
            if (contactManager.isPresent()) {
                contactManagerRepository.delete(contactManager.get());
                return ResponseDomain.deleteResponse(messageProperties.getContactManagerDelete());
            } else
                return ResponseDomain.badRequest(messageProperties.getContactManagerNotExists());
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }

    @Override
    public ResponseEntity<?> updateContact(String auth, Long contactId, String status, ContactManager contactManager) {
        logger.info(" method :::  updateContact");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Optional<ContactManager> contactManagerEO = contactManagerRepository.findById(contactId);
            if (contactManagerEO.isPresent()) {
                if (status != null) {
                    if (status.equalsIgnoreCase("ACTIVE")) {
                        if (contactManagerEO.get().isStatus())
                            return ResponseDomain.badRequest(messageProperties.getContactManagerActivated());
                        else {
                            contactManagerEO.get().setStatus(true);
                            contactManagerRepository.save(contactManagerEO.get());
                            return ResponseDomain.putResponse(messageProperties.getContactManagerActivatedSuccess());
                        }
                    } else if (status.equalsIgnoreCase("INACTIVE")) {
                        if (!contactManagerEO.get().isStatus())
                            return ResponseDomain.badRequest(messageProperties.getContactManagerInactivated());
                        else {
                            contactManagerEO.get().setStatus(false);
                            contactManagerRepository.save(contactManagerEO.get());
                            return ResponseDomain.putResponse(messageProperties.getContactManagerInactivatedSuccess());
                        }
                    } else {
                        return ResponseDomain.badRequest(messageProperties.getInvalidInput());
                    }
                } else {
                    contactManagerEO.get().setContactType(contactManager.getContactType());
                    contactManagerEO.get().setStatus(contactManager.isStatus());
                    contactManagerEO.get().setName(contactManager.getName());
                    contactManagerEO.get().setContactNumber(contactManager.getContactNumber());
                    contactManagerEO.get().setEmailAddress(contactManager.getEmailAddress());
                    contactManagerRepository.save(contactManagerEO.get());
                    return ResponseDomain.putResponse(messageProperties.getContactManagerUpdate());
                }
            } else
                return ResponseDomain.badRequest(messageProperties.getContactManagerNotExists());
        } else {
            return ResponseDomain.badRequest(messageProperties.getNotExist());
        }
    }
}
