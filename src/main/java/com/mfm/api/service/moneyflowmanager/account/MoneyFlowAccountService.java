package com.mfm.api.service.moneyflowmanager.account;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import org.springframework.http.ResponseEntity;

public interface MoneyFlowAccountService {
    ResponseEntity<?> getMoneyFlowAccountList(String auth , String fromDate , String toDate);
    ResponseEntity<?> saveMoneyFlowAccount(String auth, MoneyFlowAccount moneyFlowAccount);
    ResponseEntity<?> updateMoneyFlowAccount(String auth, Long moneyFlowAccountId, MoneyFlowAccount moneyFlowAccount);
    ResponseEntity<?> deleteMoneyFlowAccount(String auth, Long moneyFlowAccountId);
    ResponseEntity<?> fetchRecurringEntries(String flowId, String fromDate , String toDate);
    ResponseEntity<?> fetchDashboardData(Long flowId,String fromDate, String toDate);
    ResponseEntity<?> fetchUnassignedReceipts(Long moneyFlowId, String fromDate, String toDate);
    ResponseEntity<?> deleteReceipts(Long id, String type);
}
