package com.mfm.api.service.moneyflowmanager.income;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.account.RecurrenceInterval;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import com.mfm.api.domain.moneyflowmanager.income.IncomeType;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.dto.moneyflowmanager.Income_Expense_Dto;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.income.IncomeRepository;
import com.mfm.api.repository.moneyflowmanager.income.IncomeTypeRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.moneyflowmanager.account.MoneyFlowAccountService;
import com.mfm.api.service.storage.AmazonS3ClientService;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * MoneyFlowIncome Service
 * of a Money Flow Account
 */
@Service
@CacheConfig(cacheNames = "money-flow-income-cache")
public class MoneyFlowIncomeServiceImpl implements MoneyFlowIncomeService {

    private static Logger logger = LogManager.getLogger(MoneyFlowIncomeServiceImpl.class);
    @Autowired
    private IncomeTypeRepository incomeTypeRepository;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageProperties messageProperties;
    @Autowired
    private IncomeRepository incomeRepository;
    @Autowired
    private MoneyFlowAccountRepository flowAccountRepository;
    @Autowired
    private Income_Expense_Dto incomeDto;
    @Autowired
    private AmazonS3ClientService amazonS3ClientService;
    @Autowired
    private MoneyFlowAccountService moneyFlowAccountService;

    // TODO Need To Remove
    @Value("${gkz.s3.url}")
    private String url;

    /**
     * Creating an Income
     *
     * @param dto
     * @param flowId
     * @return Response Message with Status Code
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.income", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> createIncome(Income_Expense_Dto dto, String flowId, MultipartFile file) {
        logger.info(" method :::  createIncome");
        Income income = dto.DtoToIncome(dto);
        try {
            if (dto.isRecurring() && dto.getRecurrenceDate() == null) {
                return ResponseDomain.badRequest(messageProperties.getRecurrenceDateNotSelected());
            } else {
                income.setEntryDate(CustomDateConverter.stringToLocalDate(dto.getDate()));
                MoneyFlowAccount moneyFlowAccount = new MoneyFlowAccount();
                moneyFlowAccount.setMoneyFlowAccountId(Long.parseLong(flowId));
                income.setMoneyFlowAccount(moneyFlowAccount);
                if (income.isRecurring()) {
                    try {
                        income.setRecurrenceInterval(RecurrenceInterval.valueOf(dto.getRecurrenceInterval()));
                    } catch (Exception e) {
                        return ResponseDomain.badRequest("Please select a valid recurrence interval.");
                    }

                    income.setRecurringDate(CustomDateConverter.stringToLocalDate(dto.getRecurrenceDate()));
                }

                Income incomeEO = incomeRepository.save(income);
                incomeEO.getMoneyFlowIncomeId();
                if (file != null && incomeEO != null) {
                    ResponseEntity entity = amazonS3ClientService.saveIncome_Expense_File(file, income.getMoneyFlowIncomeId(), "INC");
                    if (entity.getStatusCode().value() == 200) {
                        incomeEO.setReceiptKey(entity.getBody().toString());
                        incomeRepository.save(incomeEO);
                    }
                }
                return ResponseDomain.postResponse(messageProperties.getMfmIncomeSave());
            }
        } catch (DateTimeParseException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  createIncome ::: error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getInvalidDateFormat());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  createIncome ::: error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * List of All Incomes
     * of a Money Flow Account
     *
     * @param flowId
     * @return List of Incomes
     */
    @Override
    @Cacheable(value = "money.flow.income", key = "#root.methodName.concat(#flowId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchAllIncomeOfAMoneyFlowAccount(String flowId, String fromDate, String toDate) {
        logger.info(" method :::  fetchAllIncomeOfAMoneyFlowAccount");
        MoneyFlowAccount flowAccount = flowAccountRepository.findByMoneyFlowAccountId(Long.parseLong(flowId));
        if (flowAccount != null) {
            List<Income> incomeList = incomeRepository.findAllByMoneyFlowAccount(flowAccount);
            if (fromDate != null && toDate != null) {
                incomeList = incomeList.stream().filter(income -> !income.isUnassigned() && CommonUtilFunctions.dateRangeCheck(fromDate, toDate, income.getEntryDate()))
                        .map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList());
            } else
                incomeList = incomeList.stream().filter(income -> !income.isUnassigned())
                        .map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList());
            return new ResponseEntity<>(incomeList, HttpStatus.OK);
        } else {
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
        }
    }

    /**
     * Creating a Income Type
     *
     * @param authKey
     * @param incomeType
     * @return Response Message with Status Code
     */
    @Override
    @CacheEvict(value = "money.flow.income.types", allEntries = true)
    public ResponseEntity<?> createIncomeType(String authKey, IncomeType incomeType) {
        logger.info(" method :::  createIncomeType");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(authKey)));
        try {
            incomeType.setUser(user);
            incomeType.setCustom(true);
            incomeTypeRepository.save(incomeType);
            return ResponseDomain.postResponse(messageProperties.getMfmIncomeTypeSave());

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  createIncomeType ::: Error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Getting All Income Types
     *
     * @param authKey
     * @return List of Income Types
     */
    @Override
    @Cacheable(value = "money.flow.income.types", key = "#root.methodName.concat(#authKey)", sync = true)
    public ResponseEntity<?> fetchAllIncomeTypes(String authKey) {
        logger.info(" method :::  createIncomeType");
        Long userId = Long.parseLong(tokenProvider.getUserId(authKey));
        List<IncomeType> list = incomeTypeRepository.findAll().stream().filter(expenseType -> expenseType.getUser() == null || expenseType.getUser().getUserRegistrationId() == userId).collect(Collectors.toList());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * Updating an Income
     *
     * @param incomeId
     * @param incomeDto
     * @return Response Message with Status Code
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.income", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true),
    })
    public ResponseEntity<?> updateIncome(Long incomeId, Income_Expense_Dto incomeDto, MultipartFile file) {
        logger.info(" method :::  updateIncome");
        try {
            boolean fileDeleteStatus;
            Income income = incomeRepository.findById(incomeId).get();
            if (file != null) {
                fileDeleteStatus = income.getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(income.getReceiptKey()) : false;
                ResponseEntity entity = amazonS3ClientService.saveIncome_Expense_File(file, income.getMoneyFlowIncomeId(), "INC");
                if (entity.getStatusCode().value() == 200) {
                    income.setReceiptKey(entity.getBody().toString());
                    incomeRepository.save(income);
                }
            }
            income.setAmount(incomeDto.getAmount());
            income.setIncomeType(incomeDto.getType());
            income.setSource(incomeDto.getSource());
            income.setNotes(incomeDto.getNotes());
            income.setRecurring(incomeDto.isRecurring());
            income.setEntryDate(CustomDateConverter.stringToLocalDate(incomeDto.getDate()));
            if (income.isRecurring()) {
                try {
                    income.setRecurrenceInterval(RecurrenceInterval.valueOf(incomeDto.getRecurrenceInterval()));
                } catch (Exception e) {
                    return ResponseDomain.badRequest("Please select a valid recurrence interval.");
                }

                income.setRecurringDate(CustomDateConverter.stringToLocalDate(incomeDto.getRecurrenceDate()));
            } else {
                income.setRecurringDate(null);
                income.setRecurrenceInterval(null);
            }
            incomeRepository.save(income);
            return ResponseDomain.putResponse(messageProperties.getMfmIncomeUpdate());
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  updateIncome ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest();
        } catch (DateTimeParseException e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  updateIncome ::: Error ::: " + e.getMessage());
            return ResponseDomain.badRequest(messageProperties.getInvalidDateFormat());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  updateIncome ::: Error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

    /**
     * Deleting Income of
     * a Money Flow Account
     *
     * @param incomeId
     * @return status
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.income", allEntries = true),
            @CacheEvict(value = "money.flow.dashboard", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true),
            @CacheEvict(value = "money.flow.report-profit-loss", allEntries = true),
            @CacheEvict(value = "money.flow.report-mileage", allEntries = true)
    })
    public ResponseEntity<?> deleteIncome(Long incomeId) {
        logger.info(" method :::  deleteIncome");
        try {
            boolean fileDelete;
            Optional<Income> income = incomeRepository.findById(incomeId);
            if (income.isPresent()) {
                fileDelete = income.get().getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(income.get().getReceiptKey()) : false;
                incomeRepository.deleteById(incomeId);
                return ResponseDomain.deleteResponse(messageProperties.getMfmIncomeDelete());
            } else {
                return ResponseDomain.badRequest(messageProperties.getMfmIncomeNotExists());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("method :::  updateIncome ::: Error ::: " + e.getMessage());
            return ResponseDomain.internalServerError();
        }
    }

}