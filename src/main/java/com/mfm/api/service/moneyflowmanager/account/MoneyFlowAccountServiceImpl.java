package com.mfm.api.service.moneyflowmanager.account;

import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccount;
import com.mfm.api.domain.moneyflowmanager.account.MoneyFlowAccountType;
import com.mfm.api.domain.moneyflowmanager.expense.Expense;
import com.mfm.api.domain.moneyflowmanager.income.Income;
import com.mfm.api.domain.moneyflowmanager.mileagetracker.Vehicle;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.moneyflowmanager.accout.MoneyFlowAccountRepository;
import com.mfm.api.repository.moneyflowmanager.budget.BudgetRepository;
import com.mfm.api.repository.moneyflowmanager.expense.ExpenseRepository;
import com.mfm.api.repository.moneyflowmanager.income.IncomeRepository;
import com.mfm.api.repository.moneyflowmanager.mileagetracker.VehicleRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.security.jwt.TokenProvider;
import com.mfm.api.service.storage.AmazonS3ClientService;
import com.mfm.api.util.converter.CustomDateConverter;
import com.mfm.api.util.function.CommonUtilFunctions;
import com.mfm.api.util.response.MessageProperties;
import com.mfm.api.util.response.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "money-flow-account-cache")
public class MoneyFlowAccountServiceImpl implements MoneyFlowAccountService {

    private static Logger logger = LogManager.getLogger(MoneyFlowAccountServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MoneyFlowAccountRepository moneyFlowAccountRepository;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Value("${gkz.s3.url}")
    private String url;

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Value("${default.vehicle.make}")
    private String vehicleMake;

    @Value("${default.vehicle.model}")
    private String vehicleModel;

    @Value("${default.vehicle.odoReading}")
    private Long vehicleOdoReading;

    @Value("${default.vehicle.plate}")
    private String vehiclePlate;

    @Value("${default.vehicle.year}")
    private int vehicleYear;

    @Override
    @Cacheable(value = "money.flow.list", key = "#root.methodName.concat(#auth).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> getMoneyFlowAccountList(String auth, String fromDate, String toDate) {
        logger.info("method :::  getMoneyFlowAccountList");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            List<JSONObject> jsonObjectArrayListVO = new ArrayList<>();
            List<MoneyFlowAccount> moneyFlowAccountList = moneyFlowAccountRepository.findAllByUser(user);
            if (!moneyFlowAccountList.isEmpty()) {
                if (fromDate != null && toDate != null)
                    moneyFlowAccountList = moneyFlowAccountList.stream().filter(moneyFlowAccount -> CommonUtilFunctions.dateRangeCheck(fromDate, toDate, CustomDateConverter.stringToLocalDate(CustomDateConverter.convertDateToString(moneyFlowAccount.getCreationDate()))))
                            .collect(Collectors.toList());
                moneyFlowAccountList.forEach(moneyFlowAccount -> {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("moneyFlowAccountId", moneyFlowAccount.getMoneyFlowAccountId());
                    jsonObject.put("moneyFlowName", moneyFlowAccount.getMoneyFlowName());
                    jsonObject.put("moneyFlowType", moneyFlowAccount.getMoneyFlowType());
                    jsonObject.put("creationDate", CustomDateConverter.convertDateToString(moneyFlowAccount.getCreationDate()));
                    jsonObject.put("lastModifiedDate", CustomDateConverter.convertDateToString(moneyFlowAccount.getLastModifiedDate()));
                    jsonObjectArrayListVO.add(jsonObject);
                });
                return new ResponseEntity<>(jsonObjectArrayListVO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(moneyFlowAccountList, HttpStatus.OK);
            }
        } else {
            return ResponseDomain.responseNotFound(messageProperties.getNotExist());
        }
    }

    @Override
    @CacheEvict(value = "money.flow.list", allEntries = true)
    public ResponseEntity<?> saveMoneyFlowAccount(String auth, MoneyFlowAccount moneyFlowAccount) {
        logger.info("method :::  saveMoneyFlowAccount");
        if (moneyFlowAccount != null) {
            User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
            if (user != null) {
                moneyFlowAccount.setUser(user);
                if (moneyFlowAccount.getMoneyFlowType().equalsIgnoreCase(MoneyFlowAccountType.HOME.toString()))
                    moneyFlowAccount.setMoneyFlowType(MoneyFlowAccountType.HOME.toString());
                else if (moneyFlowAccount.getMoneyFlowType().equalsIgnoreCase(MoneyFlowAccountType.BUSINESS.toString()))
                    moneyFlowAccount.setMoneyFlowType(MoneyFlowAccountType.BUSINESS.toString());
                else
                    return ResponseDomain.badRequest(messageProperties.getMfmTypeNotSelected());
                MoneyFlowAccount savedMoneyFlowAccount = null;
                try {
                    savedMoneyFlowAccount = moneyFlowAccountRepository.save(moneyFlowAccount);
                } catch (Exception e) {
                    logger.info(e.getMessage());
                    return ResponseDomain.badRequest("Money Flow Account Already Exist");
                }
                Vehicle newVehicle = new Vehicle();
                newVehicle.setMake(vehicleMake);
                newVehicle.setModel(vehicleModel);
                newVehicle.setOdoReading(vehicleOdoReading);
                newVehicle.setPlate(vehiclePlate);
                newVehicle.setYear(vehicleYear);
                newVehicle.setMoneyFlowAccount(savedMoneyFlowAccount);
                vehicleRepository.save(newVehicle);
                return ResponseDomain.postResponse(messageProperties.getMfmAccountSave());
            } else {
                return ResponseDomain.postResponse(messageProperties.getNotExist());
            }
        } else {
            return ResponseDomain.postResponse(messageProperties.getInvalidInput());
        }
    }

    @Override
    @CacheEvict(value = "money.flow.list", allEntries = true)
    public ResponseEntity<?> updateMoneyFlowAccount(String auth, Long moneyFlowAccountId, MoneyFlowAccount moneyFlowAccount) {
        logger.info("method :::  updateMoneyFlowAccount");
        if (moneyFlowAccount != null) {
            User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
            if (user != null) {
                Optional<MoneyFlowAccount> moneyFlowAccountEO = moneyFlowAccountRepository.findById(moneyFlowAccountId);
                if (moneyFlowAccountEO.isPresent()) {
                    moneyFlowAccountEO.get().setMoneyFlowName(moneyFlowAccount.getMoneyFlowName());
                    moneyFlowAccountEO.get().setUser(user);
                    if (moneyFlowAccount.getMoneyFlowType().equalsIgnoreCase(MoneyFlowAccountType.HOME.toString()))
                        moneyFlowAccountEO.get().setMoneyFlowType(MoneyFlowAccountType.HOME.toString());
                    else if (moneyFlowAccount.getMoneyFlowType().equalsIgnoreCase(MoneyFlowAccountType.BUSINESS.toString()))
                        moneyFlowAccountEO.get().setMoneyFlowType(MoneyFlowAccountType.BUSINESS.toString());
                    else
                        return ResponseDomain.postResponse(messageProperties.getMfmTypeNotSelected());
                    moneyFlowAccountRepository.save(moneyFlowAccountEO.get());
                    return ResponseDomain.putResponse(messageProperties.getMfmAccountUpdate());
                } else {
                    return ResponseDomain.putResponse(messageProperties.getMfmAccountNotExist());
                }
            } else {
                return ResponseDomain.putResponse(messageProperties.getNotExist());
            }
        } else {
            return ResponseDomain.putResponse(messageProperties.getInvalidInput());
        }
    }


    @Override
    @CacheEvict(value = "money.flow.list", allEntries = true)
    public ResponseEntity<?> deleteMoneyFlowAccount(String auth, Long moneyFlowAccountId) {
        logger.info("method :::  deleteMoneyFlowAccount");
        User user = userRepository.findByUserRegistrationId(Long.parseLong(tokenProvider.getUserId(auth)));
        if (user != null) {
            Optional<MoneyFlowAccount> moneyFlowAccountEO = moneyFlowAccountRepository.findById(moneyFlowAccountId);
            if (moneyFlowAccountEO.isPresent()) {
                moneyFlowAccountRepository.delete(moneyFlowAccountEO.get());
                return ResponseDomain.deleteResponse(messageProperties.getMfmAccountDelete());
            } else {
                return ResponseDomain.putResponse(messageProperties.getMfmAccountNotExist());
            }
        } else {
            return ResponseDomain.deleteResponse(messageProperties.getNotExist());
        }
    }

    /**
     * Recurring Entries
     * of a Money Flow Account
     *
     * @param flowId
     * @return List of Recurring Entries
     */
    @Override
    @Cacheable(value = "money.flow.recurring-entries", key = "#root.methodName.concat(#flowId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchRecurringEntries(String flowId, String fromDate, String toDate) {
        logger.info("method :::  deleteMoneyFlowAccount");
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(Long.parseLong(flowId));
        List response;
        if (moneyFlowAccount == null)
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
        else {
            List<Income> incomeList = incomeRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream()
                    .filter(income -> income.isRecurring() && !income.isUnassigned()).collect(Collectors.toList());
            List<Expense> expenseList = expenseRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream()
                    .filter(expense -> expense.isRecurring() && !expense.isUnassigned()).collect(Collectors.toList());
            if (fromDate != null && toDate != null) {
                response = new ArrayList(incomeList.stream().filter((income -> CommonUtilFunctions.filterRecurringIncome(fromDate, toDate, income) > 0))
                        .map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
                response.addAll(expenseList.stream().filter((expense -> CommonUtilFunctions.filterRecurringExpense(fromDate, toDate, expense) > 0))
                        .map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
            } else {
                response = new ArrayList(incomeList.stream().map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                        , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                        CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
                response.addAll(expenseList.stream().map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                        , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                        obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
            }
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

    /**
     * Dashboard Data
     *
     * @param flowId
     * @param fromDate
     * @param toDate
     * @return
     */
    // TODO Need to Complete
    @Override
    @Cacheable(value = "money.flow.dashboard", key = "#root.methodName.concat(#flowId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchDashboardData(Long flowId, String fromDate, String toDate) {
        logger.info("method :::  fetchDashboardData");
        JSONObject object = new JSONObject();
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(flowId);
        if (moneyFlowAccount != null) {
            try {
                List<Income> incomeList = incomeRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(income -> !income.isUnassigned()).collect(Collectors.toList());

                float incomeSum = (float) incomeList.stream().filter(income -> CommonUtilFunctions.filterRecurringIncome(fromDate, toDate, income) > 0).mapToDouble(income -> CommonUtilFunctions.filterRecurringIncome(fromDate, toDate, income) * income.getAmount()).sum();

            /*    float incomeRecurSum = incomeRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(income -> income.isRecurring())
                        .mapToDouble(income -> recurringAmountSum(fromDate, toDate, income.getRecurringDate(), income.getEntryDate(), income.getAmount())).sum();*/

                float expenseSum = (float) expenseRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(expense -> (CommonUtilFunctions.filterRecurringExpense(fromDate, toDate, expense) > 0) && !expense.isUnassigned())
                        .mapToDouble(expense -> CommonUtilFunctions.filterRecurringExpense(fromDate, toDate, expense) * expense.getAmount()).sum();

                float budgetSum = (float) budgetRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream().filter(budget -> CommonUtilFunctions.dateRangeCheck(fromDate, toDate, budget.getStartDate(), budget.getEndDate()))
                        .mapToDouble(budget -> budget.getAmount()).sum();

                object.put("income", incomeSum);
                object.put("expense", expenseSum);
                /*object.put("rec", incomeRecurSum);*/
                object.put("profit_loss", CommonUtilFunctions.roundOf(incomeSum - expenseSum));
                object.put("budget", CommonUtilFunctions.roundOf(budgetSum));
                return new ResponseEntity<>(object, HttpStatus.OK);
            } catch (HttpServerErrorException.InternalServerError error) {
                return ResponseDomain.internalServerError();
            }
        } else
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
    }

    /**
     * Fetching Un-Assigned Receipts
     *
     * @param moneyFlowId
     * @return
     */
    @Override
    @Cacheable(value = "money.flow.unassigned-receipts", key = "#root.methodName.concat(#moneyFlowId).concat(#fromDate!=null?#fromDate:'').concat(#toDate!=null?#toDate:'')", sync = true)
    public ResponseEntity<?> fetchUnassignedReceipts(Long moneyFlowId, String fromDate, String toDate) {
        logger.info("method :::  deleteMoneyFlowAccount");
        MoneyFlowAccount moneyFlowAccount = moneyFlowAccountRepository.findByMoneyFlowAccountId(moneyFlowId);
        List response;
        if (moneyFlowAccount == null)
            return ResponseDomain.badRequest(messageProperties.getMfmAccountNotExist());
        else {
            List<Income> incomeList = incomeRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream()
                    .filter(income -> income.isUnassigned()).collect(Collectors.toList());
            List<Expense> expenseList = expenseRepository.findAllByMoneyFlowAccount(moneyFlowAccount).stream()
                    .filter(expense -> expense.isUnassigned()).collect(Collectors.toList());
            if (fromDate != null && toDate != null) {
                response = new ArrayList(incomeList.stream().filter((income -> CommonUtilFunctions.dateRangeCheck(fromDate, toDate, income.getEntryDate())))
                        .map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
                response.addAll(expenseList.stream().filter((expense -> CommonUtilFunctions.dateRangeCheck(fromDate, toDate, expense.getEntryDate())))
                        .map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                                , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                                obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
            } else {
                response = new ArrayList(incomeList.stream().map(obj -> new Income(obj.getMoneyFlowIncomeId(), obj.getIncomeType(), "INCOME", obj.getAmount()
                        , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                        CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
                response.addAll(expenseList.stream().map(obj -> new Expense(obj.getMoneyFlowExpenseId(), obj.getExpenseType(), "EXPENSE", obj.getAmount()
                        , obj.getSource() != null ? obj.getSource() : "", obj.getNotes() != null ? obj.getNotes() : "", obj.isRecurring(), CustomDateConverter.localDateToString(obj.getEntryDate()), obj.getRecurringDate() != null ? CustomDateConverter.localDateToString(obj.getRecurringDate()) : "",
                        obj.getMoneyFlowAccount(), obj.getReceiptKey() != null ? url + obj.getReceiptKey() : "", obj.isUnassigned(), obj.getRecurrenceInterval()))
                        .collect(Collectors.toList()));
            }
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

    /**
     * Deleting Receipts
     *
     * @param id
     * @return
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "money.flow.income", allEntries = true),
            @CacheEvict(value = "money.flow.expense", allEntries = true),
            @CacheEvict(value = "money.flow.recurring-entries", allEntries = true),
            @CacheEvict(value = "money.flow.unassigned-receipts", allEntries = true)
    })
    public ResponseEntity<?> deleteReceipts(Long id, String type) {
        logger.info("method :::  deleteReceipts");
        Boolean fileDelete = null;
        if (type.equalsIgnoreCase("income")) {
            Optional<Income> income = incomeRepository.findById(id);
            if (income.isPresent()) {
                fileDelete = income.get().getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(income.get().getReceiptKey()) : false;
                income.get().setReceiptKey(null);
                incomeRepository.save(income.get());
            }
        } else if (type.equalsIgnoreCase("expense")) {
            Optional<Expense> expense = expenseRepository.findById(id);
            if (expense.isPresent()) {
                fileDelete = expense.get().getReceiptKey() != null ? amazonS3ClientService.deleteReceipt(expense.get().getReceiptKey()) : false;
                expense.get().setReceiptKey(null);
                expenseRepository.save(expense.get());
            }
        }
        return fileDelete ? ResponseDomain.successResponse(messageProperties.getReceiptDeleted()) : ResponseDomain.badRequest(messageProperties.getReceiptNotFound());
    }

    /**
     * Recurring Amount Calculation
     *
     * @param fromDate
     * @param toDate
     * @param recurrenceDate
     * @param incomeDate
     * @param amount
     * @return
     */
    // TODO Need To Include Recurring Sum
    public long recurringAmountSum(String fromDate, String toDate, LocalDate recurrenceDate, LocalDate incomeDate, Long amount) {
        LocalDate fromDateLocal = CustomDateConverter.stringToLocalDate(fromDate);
        LocalDate toDateLocal = CustomDateConverter.stringToLocalDate(toDate);
        Period period = Period.between(fromDateLocal, toDateLocal);
        int count = period.getMonths() + period.getYears() * 12;
        long totalSum = 0l;

        if (recurrenceDate.isBefore(fromDateLocal) || recurrenceDate.isEqual(fromDateLocal)) {
            if (recurrenceDate.getDayOfMonth() <= toDateLocal.getDayOfMonth()) {
                count = count != 0 ? count + 1 : 1;
                totalSum = totalSum + (amount * count);
                if (CommonUtilFunctions.dateRangeCheck(fromDate, toDate, incomeDate))
                    totalSum = totalSum - amount;
                return totalSum;
            } else {
//                count = count - 1 > 1 ? count : 1;
                if (count == 0 && !(recurrenceDate.getDayOfMonth() >= fromDateLocal.getDayOfMonth() && recurrenceDate.getDayOfMonth() <= toDateLocal.getDayOfMonth()))
                    count = 0;
                else if (recurrenceDate.getDayOfMonth() <= toDateLocal.getDayOfMonth())
                    count = count - 1;
                totalSum = totalSum + (amount * count);
                if (CommonUtilFunctions.dateRangeCheck(fromDate, toDate, incomeDate))
                    totalSum = totalSum - amount;
                return totalSum;
            }
        } else if (recurrenceDate.isBefore(toDateLocal) || recurrenceDate.isEqual(toDateLocal)) {
            count = (toDateLocal.getYear() - recurrenceDate.getYear()) * 12 + toDateLocal.getMonth().getValue() - recurrenceDate.getMonth().getValue();
            if (recurrenceDate.getDayOfMonth() <= toDateLocal.getDayOfMonth())
                totalSum = totalSum + (amount * (count != 0 ? count : 1));
            else
                totalSum = totalSum + (amount * (count - 1 > 0 ? count : 1));
            return totalSum;
        }
        return 0;
    }

}
