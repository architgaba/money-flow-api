package com.mfm.api.service.scheduler;


import com.mfm.api.domain.commision.*;
import com.mfm.api.domain.payment.subcription.SubscriptionDetails;
import com.mfm.api.domain.referrer.Referrers;
import com.mfm.api.domain.user.registration.User;
import com.mfm.api.repository.commision.AppAmbassadorBonusesRepository;
import com.mfm.api.repository.commision.AppAmbassadorPlusEligibleMembersRepository;
import com.mfm.api.repository.commision.CommissionPerCustomerRepository;
import com.mfm.api.repository.commision.CommissionRecordRepository;
import com.mfm.api.repository.customer.CustomersRepository;
import com.mfm.api.repository.payment.subscription.SubscriptionRepository;
import com.mfm.api.repository.referrer.ReferrerRepository;
import com.mfm.api.repository.user.registration.UserRepository;
import com.mfm.api.repository.user.registration.UserStatusRepository;
import com.mfm.api.service.admin.AdminServiceImpl;
import com.mfm.api.service.commision.CommissionSettingService;
import com.mfm.api.util.mail.MailNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SchedulerTaskServiceImpl implements SchedulerTaskService {

    private static Logger logger = LogManager.getLogger(SchedulerTaskServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserStatusRepository userStatusRepository;

    @Autowired
    private CommissionSettingService commissionSettingService;


    @Autowired
    private CommissionRecordRepository commissionRecordRepository;


    @Autowired
    private ReferrerRepository referrerRepository;

    @Autowired
    private CustomersRepository customersRepository;

    @Autowired
    private CommissionPerCustomerRepository commissionPerCustomerRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private AdminServiceImpl adminService;

    @Autowired
    private AppAmbassadorBonusesRepository appAmbassadorBonusesRepository;

    @Autowired
    private MailNotification mailNotification;

    @Autowired
    private AppAmbassadorPlusEligibleMembersRepository appAmbassadorPlusEligibleMembersRepository;

    static long testCustomersForReferrerId1124[] = {742, 733, 734, 736, 738, 737, 735, 739, 740, 743, 757, 745, 753, 752, 744, 741, 747, 750, 749, 748, 746, 755, 751, 754, 756, 760, 758, 759, 10, 11};


    @Override
    @Caching(evict = {
            @CacheEvict(value = "admin.users-details", allEntries = true)
    })
    public void triggerSchedulerUserTestToArchived() {
        logger.info("method :::  triggerSchedulerUserTestToArchived");
        List<User> userList = userRepository.findAll();
        userList = userList.stream().filter(user -> ((user.getUserStatus().getStatusCode().equals(4) || user.getUserStatus().getStatusCode().equals(5)) && (user.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(LocalDate.now())))).collect(Collectors.toList());
        userList.forEach(user -> {
            adminService.deleteUser("", user.getUserRegistrationId().toString());
        });
//        LocalDate currentDate = LocalDate.now();
//        List<User> userList = new ArrayList<>();
//        for (long userId : testCustomersForReferrerId1124) {
//            User user2 = userRepository.findByUserRegistrationId(Long.valueOf(userId));
//            if (user2 != null)
//                userList.add(user2);
//        }
//        List<User> userListEO = userRepository.findAll().stream().filter(user ->
//                (user.getReferenceType().getReferenceCode() == 3 || user.getReferenceType().getReferenceCode() == 4) &&
//                        (user.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().plusDays(7).getDayOfMonth()
////                                .getDayOfMonth()
//                                == currentDate.getDayOfMonth()))
//                .collect(Collectors.toList());
//        if (userListEO.isEmpty()) {
//            logger.warn("Scheduler can't find any User");
//        } else {
//            userListEO.removeAll(userList);
//            userListEO.stream().map(user -> {
//                user.setUserStatus(userStatusRepository.findByStatusCode(7));
//                return user;
//            }).collect(Collectors.toList());
//            try {
//                userRepository.saveAll(userListEO);
//                logger.info("Users successfully Archived");
//            } catch (Exception ex) {
//                logger.warn("Scheduler Failed to Archived Users");
//            }
//
//
//        }
    }


    @Override
    public void triggerSchedulerCalculateCommission() {
        logger.info("method :::  triggerSchedulerCalculateCommission");
        CommissionSetting commissionSettingEO = commissionSettingService.getCommissionSettingByScheduler();
        LocalDate currentDate = LocalDate.now();
        if (currentDate.getDayOfMonth() == commissionSettingEO.getSchedulerRunningDay()) {
            calculateCommission(commissionSettingEO);
        } else {
            logger.info("Today is not Scheduler Running date " + "Today's Date :: " + currentDate + " " +
                    "And Scheduler Running Date :: " + commissionSettingEO.getSchedulerRunningDay());
        }
    }

    public void calculateCommission(CommissionSetting commissionSetting) {
        LocalDate previousMonth = LocalDate.now().minusMonths(1);
        LocalDate commissionCalculateFromDate = previousMonth.withDayOfMonth(1);
        LocalDate commissionCalculateToDate = previousMonth.with(TemporalAdjusters.lastDayOfMonth());
        List<CommissionRecord> existingCommissionRecords = commissionRecordRepository.findAllByCommissionCalculateFromDateAndCommissionCalculateToDate(commissionCalculateFromDate, commissionCalculateToDate);
        if (existingCommissionRecords.size() > 0) {
            return;
        }
        calculateCommissionForReferrerCode1124(commissionSetting);
        logger.info("method ::: calculateCommission");
        List<Referrers> referrersList = referrerRepository.findAll().stream().filter(referrers -> !(referrers.getReferrerCode() == 1124))
                .filter(referrers -> referrers.getUser().getUserStatus().getStatusCode() == 1).collect(Collectors.toList());
//        List<Referrers> referrersList = referrerRepository.findAll().stream().filter(referrers -> !referrers.getReferrerCode().equals(1124)).collect(Collectors.toList());
        final int[] monthlyUserCount = {0};
        final int[] yearlyUserCount = {0};
        if (!referrersList.isEmpty()) {
            LocalDate currentDate = LocalDate.now();

            referrersList.forEach(referrers -> {
//                List<Customers> customersList = customersRepository.findAllByReferrers(referrers)
//                        .stream()
//                        .filter(customers -> (customers.getUser().getUserStatus().getStatusCode() == 1))
//                        .collect(Collectors.toList());
                monthlyUserCount[0] = 0;
                yearlyUserCount[0] = 0;
                List<CommissionPerCustomer> commissionPerCustomerList = new ArrayList<>();
                Long savedCommissionRecordId = null;
                List<User> userList = userRepository.findAllByReferrerCode(referrers.getReferrerCode())
                        .stream()
                        .filter(user -> (user.getUserStatus().getStatusCode() == 1) && (user.getReferenceType().getReferenceCode() != 7) && ((user.getReferenceType().getReferenceCode() != 4) && (user.getReferenceType().getReferenceCode() != 3)))
                        .filter(distributer -> {
                            LocalDate date = distributer.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            if ((date.getMonth() == LocalDate.now().getMonth()) && (date.getYear() == LocalDate.now().getYear()))
                                return false;
                            else
                                return true;
                        }).collect(Collectors.toList());


                List<CommissionRecord> commissionRecordList = commissionRecordRepository
                        .findAllByReferrersOrderByCommissionCalculateFromDateDesc(referrers).stream
                                ().filter(commissionRecord -> {
                                    if ((commissionRecord.getCommissionCalculateFromDate().getMonth().compareTo(commissionCalculateFromDate.getMonth()) == 0) && (commissionRecord.getCommissionCalculateFromDate().getYear() == commissionCalculateFromDate.getYear())) {
                                        return true;
                                    } else
                                        return false;
                                }
                        ).collect(Collectors.toList());

                if (commissionRecordList.size() != 0 || userList.isEmpty()) {
                    logger.info("commission already calculated for this month ie., " + commissionCalculateFromDate.getMonth() + " No Record Available for the Customer Rep " + referrers);
//                    return;
                } else {
                    logger.info("Enter to Calculate 1st Level Commissions :::  " + userList);
                    CommissionRecord commissionRecord = new CommissionRecord();
                    commissionRecord.setIsP2PYEA(referrers.getUser().getIsP2PYEA());
                    commissionRecord.setReferrers(referrers);
                    commissionRecord.setCommissionCalculateToDate(commissionCalculateToDate);
                    commissionRecord.setCommissionCalculateFromDate(commissionCalculateFromDate);
                    commissionRecord.setReferrerName(referrers.getUser().getFirstName() + (referrers.getUser().getLastName()
                            != null ? " " + referrers.getUser().getLastName() : ""));
                    commissionRecord.setReferrerStatus(referrers.getUser().getUserStatus().getStatusDescription());
                    commissionRecord.setReferrerId(referrers.getReferrerId());
                    commissionRecord.setMonthlyUserCount(0);
                    commissionRecord.setYearlyUserCount(0);
                    switch (referrers.getUser().getReferenceType().getReferenceCode()) {
                        case 6:
                            commissionRecord.setUserType("P2P Distributor");
                            break;
                        case 7:
                            commissionRecord.setUserType("Admin");
                            break;
                        default:
                            if (referrers.getUser().getRole().getRoleCode() == 5)
                                commissionRecord.setUserType("Super Admin");
                            else
                                commissionRecord.setUserType("Customer Rep");
                            break;
                    }
                    final CommissionRecord savedCommissionRecord = commissionRecordRepository.save
                            (commissionRecord);
                    savedCommissionRecordId = savedCommissionRecord.getCommissionRecordId();
                    final Integer[] comissions = new Integer[]{0, 0};
                    userList.stream().forEach(user -> {
                        comissions[0] = 0;
                        comissions[1] = 0;
                        CommissionPerCustomer commissionPerCustomer = new CommissionPerCustomer();
                        comissions[0] = comissions[0] + ((user.getReferenceType().getReferenceCode() != 6) ? commissionSetting.getCommission1stLevelAmount() : 0);
                        if (user.getRole().getRoleCode() == 2) {
                            Referrers referrers1 = referrerRepository.findByUser(user);
                            comissions[1] = comissions[1] + userRepository.findAllByReferrerCode(referrers1.getReferrerCode()).stream().filter(customers -> {
                                LocalDate date = customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                                if ((date.getMonth() == LocalDate.now().getMonth()) && (date.getYear() == LocalDate.now().getYear()))
                                    return false;
                                else
                                    return true;
                            }).filter(prospectUser -> (prospectUser.getUserStatus().getStatusCode() == 1) && (prospectUser.getReferenceType().getReferenceCode() != 7) && (user.getReferenceType().getReferenceCode() != 4) && (user.getReferenceType().getReferenceCode() != 3))
                                    .filter(p2pdistributer -> p2pdistributer.getReferenceType().getReferenceCode() != 6).collect(Collectors.toList()).size()
                                    * commissionSetting.getCommission2ndLevelAmount();
                        }
                        commissionPerCustomer.setName(user.getFirstName() + (user.getLastName()
                                != null ? " " + user.getLastName() : ""));


                        List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                                .findAllByUserOrderBySubscriptionIdDesc(user).stream()
                                .filter(subscriptionDetails ->
                                        subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                                .getPlan()
                                                .getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList());
                        commissionPerCustomer.setPlanType((subscriptionDetailsList.size() == 0) ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval());
                        commissionPerCustomer.setIsP2PYEA(user.getIsP2PYEA());
                        commissionPerCustomer.setUser(user);

                        commissionPerCustomer.setFirstLevelCommission(comissions[0]);
                        commissionPerCustomer.setSecondLevelCommission(comissions[1]);
                        commissionPerCustomer.setCommissionAmount(comissions[0] + comissions[1]);
                        commissionPerCustomer.setCommissionRecord(savedCommissionRecord);
                        commissionPerCustomer.setUserType(commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                                6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "SelfCommission" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");

                        commissionPerCustomer = commissionPerCustomerRepository.save(commissionPerCustomer);
                        commissionPerCustomerList.add(commissionPerCustomer);
                        if (user.getRole().getRoleCode() == 3) {
                            List<SubscriptionDetails> referrerSubscriptionDetailsList = subscriptionRepository
                                    .findAllByUserOrderBySubscriptionIdDesc(user)
                                    .stream().filter(subscriptionDetails -> subscriptionDetails.getPlan().getPlanType()
                                            .equalsIgnoreCase("Customer")).collect(Collectors.toList());

                            monthlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                    (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                    (Collectors.toList()).size();

                            yearlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                    (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Yearly") &&
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                    (Collectors.toList()).size();
                            commissionRecord.setMonthlyUserCount(monthlyUserCount[0]);
                            commissionRecord.setYearlyUserCount(yearlyUserCount[0]);
                        }

                    });
                    commissionRecordRepository.save(commissionRecord);
                    monthlyUserCount[0] = 0;
                    yearlyUserCount[0] = 0;
                }
                if (referrers.getUser().getReferenceType().getReferenceCode() == 6) {
                    CommissionRecord commissionRecord = null;
                    if (savedCommissionRecordId != null) {
                        commissionRecord = commissionRecordRepository.findById(savedCommissionRecordId).get();
                    } else {
                        commissionRecord = new CommissionRecord();
                        commissionRecord.setIsP2PYEA(referrers.getUser().getIsP2PYEA());
                        commissionRecord.setReferrers(referrers);
                        commissionRecord.setCommissionCalculateToDate(commissionCalculateToDate);
                        commissionRecord.setCommissionCalculateFromDate(commissionCalculateFromDate);
                        commissionRecord.setReferrerName(referrers.getUser().getFirstName() + (referrers.getUser().getLastName()
                                != null ? " " + referrers.getUser().getLastName() : ""));

                        commissionRecord.setReferrerStatus(referrers.getUser().getUserStatus().getStatusDescription());

                        commissionRecord.setReferrerId(referrers.getReferrerId());
                        commissionRecord.setMonthlyUserCount(0);
                        commissionRecord.setYearlyUserCount(0);
                        userList.stream().forEach(user -> {
                            if (user.getRole().getRoleCode() == 3) {
                                List<SubscriptionDetails> referrerSubscriptionDetailsList = subscriptionRepository
                                        .findAllByUserOrderBySubscriptionIdDesc(user)
                                        .stream().filter(subscriptionDetails -> subscriptionDetails.getPlan().getPlanType()
                                                .equalsIgnoreCase("Customer")).collect(Collectors.toList());

                                monthlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                        (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                                                subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                        (Collectors.toList()).size();

                                yearlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                        (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Yearly") &&
                                                subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                        (Collectors.toList()).size();
                            }
                        });
                        commissionRecord.setMonthlyUserCount(monthlyUserCount[0]);
                        commissionRecord.setYearlyUserCount(yearlyUserCount[0]);
                        switch (referrers.getUser().getReferenceType().getReferenceCode()) {
                            case 6:
                                commissionRecord.setUserType("P2P Distributor");
                                break;
                            case 7:
                                commissionRecord.setUserType("Admin");
                                break;
                            default:
                                if (referrers.getUser().getRole().getRoleCode() == 5)
                                    commissionRecord.setUserType("Super Admin");
                                else
                                    commissionRecord.setUserType("Customer Rep");
                                break;
                        }
                        commissionRecord = commissionRecordRepository.save(commissionRecord);
                    }
                    CommissionPerCustomer commissionPerCustomer = new CommissionPerCustomer();

                    commissionPerCustomer.setName(referrers.getUser().getFirstName() + (referrers.getUser().getLastName()
                            != null ? " " + referrers.getUser().getLastName() : ""));

                    List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                            .findAllByUserOrderBySubscriptionIdDesc(referrers.getUser()).stream()
                            .filter(subscriptionDetails ->
                                    subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                            .getPlan()
                                            .getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList());
                    commissionPerCustomer.setPlanType((subscriptionDetailsList.size() == 0) ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval());

                    commissionPerCustomer.setUser(referrers.getUser());
                    commissionPerCustomer.setIsP2PYEA(referrers.getUser().getIsP2PYEA());
                    commissionPerCustomer.setFirstLevelCommission(5);
                    commissionPerCustomer.setSecondLevelCommission(0);
                    commissionPerCustomer.setCommissionAmount(5);
                    commissionPerCustomer.setCommissionRecord(commissionRecord);
                    commissionPerCustomer.setUserType(commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                            6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "SelfCommission" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");

                    commissionPerCustomerRepository.save(commissionPerCustomer);
                    commissionPerCustomerList.add(commissionPerCustomer);
                }
            });
        } else {
            logger.info("Size of the List of the Referrer are :: " + referrersList.size() + " Or List is Empty");
        }
    }


    public void calculateCommissionForReferrerCode1124(CommissionSetting commissionSetting) {
        logger.info("method ::: calculateCommissionForReferrerCode1124");
        Referrers referrer1124 = referrerRepository.findByReferrerCode(Long.valueOf(1124));
        LocalDate previousMonth = LocalDate.now().minusMonths(1);
        LocalDate commissionCalculateFromDate = previousMonth.withDayOfMonth(1);
        LocalDate commissionCalculateToDate = previousMonth.with(TemporalAdjusters.lastDayOfMonth());
        if (referrer1124 != null) {
            LocalDate currentDate = LocalDate.now();

            List<CommissionPerCustomer> commissionPerCustomerList = new ArrayList<>();
            Long savedCommissionRecordId = null;
            List<User> userList = userRepository.findAllByReferrerCode(referrer1124.getReferrerCode())
                    .stream()
                    .filter(user -> (user.getUserStatus().getStatusCode() == 1) && (user.getReferenceType().getReferenceCode() != 7))
                    .filter(distributer -> {
                        LocalDate date = distributer.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        if ((date.getMonth() == LocalDate.now().getMonth()) && (date.getYear() == LocalDate.now().getYear()))
                            return false;
                        else
                            return true;
                    }).collect(Collectors.toList());


            List<CommissionRecord> commissionRecordList = commissionRecordRepository
                    .findAllByReferrersOrderByCommissionCalculateFromDateDesc(referrer1124).stream
                            ().filter(commissionRecord -> {
                                if ((commissionRecord.getCommissionCalculateFromDate().getMonth().compareTo(commissionCalculateFromDate.getMonth()) == 0) && (commissionRecord.getCommissionCalculateFromDate().getYear() == commissionCalculateFromDate.getYear())) {
                                    return true;
                                } else
                                    return false;
                            }
                    ).collect(Collectors.toList());

            if (commissionRecordList.size() != 0 || userList.isEmpty()) {
                logger.info("commission already calculated for this month ie., " + commissionCalculateFromDate.getMonth() + " No Record Available for the Customer Rep " + referrer1124);
                return;
            } else {
                final int[] monthlyUserCount = {0};
                final int[] yearlyUserCount = {0};
                logger.info("Enter to Calculate 1st Level Commissions :::  " + userList);
                CommissionRecord commissionRecord = new CommissionRecord();
                commissionRecord.setIsP2PYEA(referrer1124.getUser().getIsP2PYEA());
                commissionRecord.setReferrers(referrer1124);
                commissionRecord.setCommissionCalculateToDate(commissionCalculateToDate);
                commissionRecord.setCommissionCalculateFromDate(commissionCalculateFromDate);
                commissionRecord.setReferrerName(referrer1124.getUser().getFirstName() + (referrer1124.getUser().getLastName()
                        != null ? " " + referrer1124.getUser().getLastName() : ""));

                commissionRecord.setReferrerStatus(referrer1124.getUser().getUserStatus().getStatusDescription());
                commissionRecord.setReferrerId(referrer1124.getReferrerId());
                commissionRecord.setMonthlyUserCount(0);
                commissionRecord.setYearlyUserCount(0);
                switch (referrer1124.getUser().getReferenceType().getReferenceCode()) {
                    case 6:
                        commissionRecord.setUserType("P2P Distributor");
                        break;
                    case 7:
                        commissionRecord.setUserType("Admin");
                        break;
                    default:
                        if (referrer1124.getUser().getRole().getRoleCode() == 5)
                            commissionRecord.setUserType("Super Admin");
                        else
                            commissionRecord.setUserType("Customer Rep");
                        break;
                }
                final CommissionRecord savedCommissionRecord = commissionRecordRepository.save
                        (commissionRecord);
                savedCommissionRecordId = savedCommissionRecord.getCommissionRecordId();
                final Integer[] comissions = new Integer[]{0, 0};
                userList.stream().forEach(user -> {
                    comissions[0] = 0;
                    comissions[1] = 0;
                    CommissionPerCustomer commissionPerCustomer = new CommissionPerCustomer();
                    comissions[0] = comissions[0] + ((user.getReferenceType().getReferenceCode() != 6) ? commissionSetting.getCommission1stLevelAmount() : 0);
                    if (user.getRole().getRoleCode() == 2) {
                        Referrers referrers1 = referrerRepository.findByUser(user);
                        comissions[1] = comissions[1] + userRepository.findAllByReferrerCode(referrers1.getReferrerCode()).stream().filter(customers -> {
                            LocalDate date = customers.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            if ((date.getMonth() == LocalDate.now().getMonth()) && (date.getYear() == LocalDate.now().getYear()))
                                return false;
                            else
                                return true;
                        }).filter(prospectUser -> (prospectUser.getUserStatus().getStatusCode() == 1) && (prospectUser.getReferenceType().getReferenceCode() != 7))
                                .filter(p2pdistributer -> p2pdistributer.getReferenceType().getReferenceCode() != 6).collect(Collectors.toList()).size()
                                * commissionSetting.getCommission2ndLevelAmount();
                    }


                    commissionPerCustomer.setName(user.getFirstName() + (user.getLastName()
                            != null ? " " + user.getLastName() : ""));

                    List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                            .findAllByUserOrderBySubscriptionIdDesc(user).stream()
                            .filter(subscriptionDetails ->
                                    subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                            .getPlan()
                                            .getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList());
                    commissionPerCustomer.setPlanType((subscriptionDetailsList.size() == 0) ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval());

                    commissionPerCustomer.setUser(user);
                    commissionPerCustomer.setIsP2PYEA(user.getIsP2PYEA());
                    commissionPerCustomer.setFirstLevelCommission(comissions[0]);
                    commissionPerCustomer.setSecondLevelCommission(comissions[1]);
                    commissionPerCustomer.setCommissionAmount(comissions[0] + comissions[1]);
                    commissionPerCustomer.setCommissionRecord(savedCommissionRecord);
                    commissionPerCustomer.setUserType(commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                            6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "SelfCommission" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");

                    commissionPerCustomer = commissionPerCustomerRepository.save(commissionPerCustomer);
                    commissionPerCustomerList.add(commissionPerCustomer);
                    if (commissionPerCustomer.getUser().getRole().getRoleCode() == 3) {
                        List<SubscriptionDetails> referrerSubscriptionDetailsList = subscriptionRepository
                                .findAllByUserOrderBySubscriptionIdDesc(user)
                                .stream().filter(subscriptionDetails -> subscriptionDetails.getPlan().getPlanType()
                                        .equalsIgnoreCase("Customer")).collect(Collectors.toList());

                        monthlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                                        subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                (Collectors.toList()).size();

                        yearlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Yearly") &&
                                        subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                (Collectors.toList()).size();
                    }
                    commissionRecord.setMonthlyUserCount(monthlyUserCount[0]);
                    commissionRecord.setYearlyUserCount(yearlyUserCount[0]);
                });
                commissionRecordRepository.save(commissionRecord);
                monthlyUserCount[0] = 0;
                yearlyUserCount[0] = 0;
            }
            if (referrer1124.getUser().getReferenceType().getReferenceCode() == 6) {
                CommissionRecord commissionRecord = null;
                if (savedCommissionRecordId != null) {
                    commissionRecord = commissionRecordRepository.findById(savedCommissionRecordId).get();
                } else {
                    final int[] monthlyUserCount = {0};
                    final int[] yearlyUserCount = {0};
                    commissionRecord = new CommissionRecord();
                    commissionRecord.setIsP2PYEA(referrer1124.getUser().getIsP2PYEA());
                    commissionRecord.setReferrers(referrer1124);
                    commissionRecord.setCommissionCalculateToDate(commissionCalculateToDate);
                    commissionRecord.setCommissionCalculateFromDate(commissionCalculateFromDate);
                    commissionRecord.setReferrerName(referrer1124.getUser().getFirstName() + (referrer1124.getUser().getLastName()
                            != null ? " " + referrer1124.getUser().getLastName() : ""));

                    commissionRecord.setReferrerStatus(referrer1124.getUser().getUserStatus().getStatusDescription());

                    commissionRecord.setReferrerId(referrer1124.getReferrerId());
                    commissionRecord.setMonthlyUserCount(0);
                    commissionRecord.setYearlyUserCount(0);
                    userList.stream().forEach(user -> {
                        if (user.getRole().getRoleCode() == 3) {
                            List<SubscriptionDetails> referrerSubscriptionDetailsList = subscriptionRepository
                                    .findAllByUserOrderBySubscriptionIdDesc(user)
                                    .stream().filter(subscriptionDetails -> subscriptionDetails.getPlan().getPlanType()
                                            .equalsIgnoreCase("Customer")).collect(Collectors.toList());

                            monthlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                    (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Monthly") &&
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                    (Collectors.toList()).size();

                            yearlyUserCount[0] += referrerSubscriptionDetailsList.stream().filter(subscriptionDetails ->
                                    (subscriptionDetails.getPlan().getPlanInterval().equalsIgnoreCase("Yearly") &&
                                            subscriptionDetails.getStatus().equalsIgnoreCase("Active"))).collect
                                    (Collectors.toList()).size();
                        }
                    });
                    commissionRecord.setMonthlyUserCount(monthlyUserCount[0]);
                    commissionRecord.setYearlyUserCount(yearlyUserCount[0]);
                    switch (referrer1124.getUser().getReferenceType().getReferenceCode()) {
                        case 6:
                            commissionRecord.setUserType("P2P Distributor");
                            break;
                        case 7:
                            commissionRecord.setUserType("Admin");
                            break;
                        default:
                            if (referrer1124.getUser().getRole().getRoleCode() == 5)
                                commissionRecord.setUserType("Super Admin");
                            else
                                commissionRecord.setUserType("Customer Rep");
                            break;
                    }
                    commissionRecord = commissionRecordRepository.save(commissionRecord);
                }
                CommissionPerCustomer commissionPerCustomer = new CommissionPerCustomer();

                commissionPerCustomer.setName(referrer1124.getUser().getFirstName() + (referrer1124.getUser().getLastName()
                        != null ? " " + referrer1124.getUser().getLastName() : ""));

                List<SubscriptionDetails> subscriptionDetailsList = subscriptionRepository
                        .findAllByUserOrderBySubscriptionIdDesc(referrer1124.getUser()).stream()
                        .filter(subscriptionDetails ->
                                subscriptionDetails.getStatus().equalsIgnoreCase("Active") && subscriptionDetails
                                        .getPlan()
                                        .getPlanType().equalsIgnoreCase("Customer")).collect(Collectors.toList());
                commissionPerCustomer.setPlanType((subscriptionDetailsList.size() == 0) ? "N.A" : subscriptionDetailsList.get(0).getPlan().getPlanInterval());

                commissionPerCustomer.setUser(referrer1124.getUser());
                commissionPerCustomer.setIsP2PYEA(referrer1124.getUser().getIsP2PYEA());
                commissionPerCustomer.setFirstLevelCommission(5);
                commissionPerCustomer.setSecondLevelCommission(0);
                commissionPerCustomer.setCommissionAmount(5);
                commissionPerCustomer.setCommissionRecord(commissionRecord);
                commissionPerCustomer.setUserType(commissionPerCustomer.getUser().getReferenceType().getReferenceCode() ==
                        6 ? (commissionPerCustomer.getFirstLevelCommission() == 5 ? "SelfCommission" : "P2P Distributor") : commissionPerCustomer.getUser().getRole().getRoleCode() == 2 ? "Customer Rep" : "Customer");

                commissionPerCustomerRepository.save(commissionPerCustomer);
                commissionPerCustomerList.add(commissionPerCustomer);
            }
        } else {
            logger.info("Size of the List of the Referrer are :: " + referrer1124 + " Or List is Empty");
        }
    }

    @Override
    public void bonusEmailScheduler() {
        logger.info("method :::  bonusEmailScheduler");
        LocalDate date = LocalDate.now();
        DayOfWeek day = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        if (day.equals(DayOfWeek.FRIDAY)) {
            Map<User, String> userList = new HashMap<>();
            LocalDate previousMonday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)).minusDays(7);
            LocalDate previousSunday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
            Set<Long> appAmbassadorList = appAmbassadorBonusesRepository.findAll().stream().map(bonus -> bonus.getUserRegistrationId()).collect(Collectors.toSet());
            appAmbassadorList.stream().forEach(appAmbassador -> {
                List<AppAmbassadorBonuses> appAmbassadorBonusList = appAmbassadorBonusesRepository.findAllByUserRegistrationIdAndDisbursementDateBetween(appAmbassador, previousMonday, previousSunday);
                if (appAmbassadorBonusList.size() > 0) {
                    try {
                        double sum = appAmbassadorBonusList.stream().mapToDouble(bonus -> bonus.getBonuseAmount()).sum();
                        User user = userRepository.findByUserRegistrationId(appAmbassador);
                        if (user != null) {
                            DecimalFormat df = new DecimalFormat("0.00");
                            df.setMaximumFractionDigits(2);
                            userList.put(user, df.format(sum));
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            });
            Set<Map.Entry<User, String>> entries = userList.entrySet();
            Iterator<Map.Entry<User, String>> iterator = entries.iterator();
            while (iterator.hasNext()) {
                Map.Entry<User, String> next = iterator.next();
                mailNotification.sendBonusMail(next.getKey(), next.getValue());
            }
        }

    }

    @Override
    public void commissionEmailScheduler() {
        logger.info("method :::  commissionEmailScheduler");
        LocalDate previousMonth = LocalDate.now().minusMonths(1);
        LocalDate commissionCalculateFromDate = previousMonth.withDayOfMonth(1);
        LocalDate commissionCalculateToDate = previousMonth.with(TemporalAdjusters.lastDayOfMonth());
        Map<User, String> userList = new HashMap<>();
        List<CommissionRecord> commissionRecordList = commissionRecordRepository.findAllByCommissionCalculateFromDateAndCommissionCalculateToDate(commissionCalculateFromDate, commissionCalculateToDate);
        commissionRecordList.stream().forEach(commissionRecord -> {
            List<CommissionPerCustomer> commissionPerCustomerList = commissionPerCustomerRepository.findAllByCommissionRecord(commissionRecord);
            if (commissionPerCustomerList.size() > 0) {
                try {
                    double commission = commissionPerCustomerList.stream().mapToDouble(commissionPerCustomer -> commissionPerCustomer.getCommissionAmount()).sum();
                    if (commission >= 15) {
                        DecimalFormat df = new DecimalFormat("0.00");
                        df.setMaximumFractionDigits(2);
                        if (commissionRecord.getReferrers() != null) {
                            userList.put(commissionRecord.getReferrers().getUser(), df.format(commission));
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });
        Set<Map.Entry<User, String>> entries = userList.entrySet();
        Iterator<Map.Entry<User, String>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<User, String> next = iterator.next();
            mailNotification.sendCommissionMail(next.getKey(), next.getValue());
        }
    }

    @Override
    public void complianceWednesdayScheduler() {
        logger.info("method :::  commissionEmailScheduler");
        List<AppAmbassadorPlusEligibleMembers> appAmbassadorPlusEligibleMembersList = appAmbassadorPlusEligibleMembersRepository.findAll();
        Map<String, User> userMap = new HashMap<>();
        appAmbassadorPlusEligibleMembersList.forEach(appAmbassadorPlusEligibleMembers -> {
            User user = userRepository.findByUserRegistrationId(appAmbassadorPlusEligibleMembers.getUserRegistrationId());
            if (user != null) {
                userMap.put(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""), user);
            }
        });
        Set<Map.Entry<String, User>> entries = userMap.entrySet();
        Iterator<Map.Entry<String, User>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, User> next = iterator.next();
            mailNotification.sendComplianceWednesdayMailToAAP(next.getKey(), next.getValue());
        }
    }

    @Override
    public void compliance18Scheduler() {
        logger.info("method :::  compliance18Scheduler");
        List<Referrers> referrersList = referrerRepository.findAll();
        Map<String, User> userMap = new HashMap<>();
        referrersList.forEach(referrers -> {
            User user = userRepository.findByUserRegistrationId(referrers.getUser().getUserRegistrationId());
            if (user != null) {
                userMap.put(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""), user);
            }
        });
        Set<Map.Entry<String, User>> entries = userMap.entrySet();
        Iterator<Map.Entry<String, User>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, User> next = iterator.next();
            mailNotification.sendCompliance18MailToAA(next.getKey(), next.getValue());
        }

    }
}
