package com.mfm.api.service.scheduler;

public interface SchedulerTaskService {
    void triggerSchedulerUserTestToArchived();

    void triggerSchedulerCalculateCommission();

    void bonusEmailScheduler();

    void commissionEmailScheduler();

    void complianceWednesdayScheduler();

    void compliance18Scheduler();
}


