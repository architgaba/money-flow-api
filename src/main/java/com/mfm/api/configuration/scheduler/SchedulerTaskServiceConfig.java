package com.mfm.api.configuration.scheduler;


import com.mfm.api.service.scheduler.SchedulerTaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class SchedulerTaskServiceConfig {


    private static Logger logger = LogManager.getLogger(SchedulerTaskServiceConfig.class);

    @Autowired
    private SchedulerTaskService schedulerTaskService;

    /**
     * This method trigger By Scheduler to convert Test User status to User Status="Archived"
     * Running Time: Daily
     * Cron timing to run Scheduler 10 5 0 * * ?
     */
    //@Scheduled(cron = "20 * * * * ?")
//    @Scheduled(cron = "0 * * ? * *")
    @Scheduled(cron = "0 0 1 * * ?")
    public void triggerSchedulerUserTestToArchived() {
        logger.info("method ::: triggerSchedulerUserTestToArchived");
        schedulerTaskService.triggerSchedulerUserTestToArchived();
    }

    /**
     * This method trigger By Scheduler to calculate commission for active and subscribed Customer
     * Running Time: Daily
     * Cron timing to run Scheduler 10 5 0 * * ?
     */

//    @Scheduled(cron = "0 * * ? * *")
    @Scheduled(cron = "0 0 1 1 * ?")
    public void triggerSchedulerCalculateCommission() {
        logger.info("method ::: triggerSchedulerCalculateCommission");
        schedulerTaskService.triggerSchedulerCalculateCommission();
    }


    @Scheduled(cron = "0 0 7 * * ?")
    public void bonusEmailScheduler() {
        logger.info("method ::: bonusEmailScheduler");
        schedulerTaskService.bonusEmailScheduler();
    }


    @Scheduled(cron = "0 0 7 20 * ?")
    public void commissionEmailScheduler() {
        logger.info("method ::: commissionEmailScheduler");
        schedulerTaskService.commissionEmailScheduler();
    }

    @Scheduled(cron = "0 0 9 ? * WED")
    public void complianceWednesdayScheduler() {
        logger.info("method ::: complianceWednesdayScheduler");
        schedulerTaskService.complianceWednesdayScheduler();
    }

    @Scheduled(cron = "0 0 8 18 * ?")
    public void compliance18Scheduler() {
        logger.info("method ::: compliance18Scheduler");
        schedulerTaskService.compliance18Scheduler();
    }


//    Cron expression “0 24 23 * * ? *” means the job will fire at time 23:24:00 every day.

}
