package com.mfm.api.configuration.globalexception;

import com.mfm.api.util.response.ResponseDomain;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.security.Security;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);


    /**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        logger.info("Exception Trace ::: " + ex.getMessage() + "Parameters ::: " + ex.getParameterName());
        String error = ex.getParameterName() + "Kindly Contact System Administrator";
        return buildResponseEntity(new ApiError(BAD_REQUEST, error, ex));
    }


    /**
     * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is invalid as well.
     *
     * @param ex      HttpMediaTypeNotSupportedException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append("Media type is not supported. Supported media types are ");
        logger.info("Exception Trace ::: " + ex.getMessage() + "Parameters ::: " + ex.getContentType());
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
        return buildResponseEntity(new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, builder.substring(0, builder
                .length() - 2), ex));
    }

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage("Validation error!Kindly Contact System Administrator");
        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());

        logger.info("Exception Trace ::: " + ex.getMessage() + "Fields Error ::: "
                + ex.getBindingResult().getFieldErrors() + " Global Error ::: " + ex.getBindingResult().getGlobalErrors());

        return buildResponseEntity(apiError);
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage("Validation error! Kindly Contact System Administrator");
        apiError.addValidationErrors(ex.getConstraintViolations());
        logger.info("Exception Trace ::: " + ex.getMessage() + " Violation Error ::: " + ex.getConstraintViolations());
        return buildResponseEntity(apiError);
    }

    /**
     * Handles EntityNotFoundException. Created to encapsulate errors with more detail than
     * javax.persistence.EntityNotFoundException.
     *
     * @param ex the EntityNotFoundException
     * @return the ApiError object
     */
    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(
            EntityNotFoundException ex) {
        ApiError apiError = new ApiError(NOT_FOUND);
        apiError.setMessage(ex.getMessage());
        logger.info("Exception Trace ::: " + ex.getMessage());
        return buildResponseEntity(apiError);
    }

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param ex      HttpMessageNotReadableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders
            headers, HttpStatus status, WebRequest request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.info("{} to {}", servletWebRequest.getHttpMethod(), servletWebRequest.getRequest().getServletPath());
        String error = "Kindly Contact System Administrator";
        logger.info("Exception Trace ::: " + ex.getMessage() + " Some Errors may be ::: " +
                servletWebRequest.getHttpMethod() + " " + servletWebRequest.getRequest().getServletPath());
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
    }

    /**
     * Handle HttpMessageNotWritableException.
     *
     * @param ex      HttpMessageNotWritableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders
            headers, HttpStatus status, WebRequest request) {
        String error = "Error writing JSON output";
        logger.info("Exception Trace ::: " + ex.getMessage());
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, error, ex));
    }

    /*  *//**
     * Handle javax.persistence.EntityNotFoundException
     *//*
    @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(javax.persistence.EntityNotFoundException ex) {
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, ex));
    }*/

    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     *
     * @param ex the DataIntegrityViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex,
                                                                  WebRequest request) {
        if (ex.getCause() instanceof ConstraintViolationException) {
            logger.info("Exception Trace ::: " + ex.getMessage());
            return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, "Kindly Contact System Administrator", ex
                    .getCause()));

        }
        logger.info("Exception Trace ::: " + ex.getMessage());
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex));
    }

    /**
     * Handle Exception, handle generic Exception.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex
                .getName(), ex.getValue(), ex.getRequiredType().getSimpleName()));
        apiError.setDebugMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    /**
     * Handles can't create Transaction Exception.
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(CannotCreateTransactionException.class)
    protected ResponseEntity<Object> handleCannotCreateTransactionException(CannotCreateTransactionException ex,
                                                                            WebRequest request) {
        if (ex.getCause() instanceof CannotCreateTransactionException) {
            logger.info("Exception Trace ::: " + ex.getMessage());
            return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, "Kindly Contact System Administrator", ex
                    .getCause()));
        }
        logger.info("Exception Trace ::: " + ex.getMessage());
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex));
    }


    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        logger.info("Exception Trace ::: " + apiError.getStatus());
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    public ResponseEntity<?> handle(org.springframework.web.multipart.MultipartException exception) {
        logger.error("handle->MultipartException" + exception.getMessage(), exception);
        // general exception
        if (exception.getCause() instanceof IOException && exception.getCause().getMessage().startsWith("The temporary upload location")) {
            String pathToRecreate = exception.getMessage().substring(exception.getMessage().indexOf("[") + 1, exception.getMessage().indexOf("]"));
            Set<PosixFilePermission> perms = new HashSet<>();
            // add permission as rw-r--r-- 644
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.GROUP_WRITE);
            perms.add(PosixFilePermission.GROUP_EXECUTE);
            FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
            try {
                Files.createDirectories(FileSystems.getDefault().getPath(pathToRecreate), fileAttributes);
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
                return ResponseDomain.internalServerError("Unable to recreate deleted temp directories. Please check  " + pathToRecreate);
            }
            return ResponseDomain.internalServerError("Recovered from temporary error by recreating temporary directory. Please try to upload logo again.");
        }
        return ResponseDomain.internalServerError("Unable to process this request.");
    }


    @ExceptionHandler(UnknownHostException.class)
    protected ResponseEntity<Object> UnknownHostException(UnknownHostException ex,
                                                          WebRequest request) {
        if (ex.getCause() instanceof UnknownHostException) {
            System.out.println(Security.getProperty("networkaddress.cache.ttl"));
            System.out.println(System.getProperty("networkaddress.cache.ttl"));
            System.out.println(Security.getProperty("networkaddress.cache.negative.ttl"));
            System.out.println(System.getProperty("networkaddress.cache.negative.ttl"));
            System.out.println(System.getProperty("networkaddress.cache.ttl"));
            return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Kindly Contact System Administrator", ex
                    .getCause()));
        }
        logger.info("Exception Trace ::: " + ex.getMessage());
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex));
    }
}

