package com.mfm.api.configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AmazonS3Config {
    @Value("${gkz.aws.access_key_id}")
    private String awsId;

    @Value("${gkz.aws.secret_access_key}")
    private String awsKey;

    @Value("${gkz.s3.region}")
    private String region;

    @Value("${gkz.s3.url}")
    private String url;


    @Bean
    public AmazonS3 s3client() {

        ClientConfiguration config = new ClientConfiguration();
        config.setMaxConnections(500);
        config.setMaxErrorRetry(15);

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsId, awsKey);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.fromName(region))
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withClientConfiguration(config)
                .build();
        return s3Client;
    }
}
