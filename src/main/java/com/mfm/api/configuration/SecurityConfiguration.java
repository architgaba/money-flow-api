package com.mfm.api.configuration;

import com.mfm.api.security.jwt.JwtConfigurer;
import com.mfm.api.security.jwt.TokenProvider;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.actuate.context.ShutdownEndpoint;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.SessionManagementFilter;

import javax.annotation.PostConstruct;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private SimpleCORSFilter corsFilter;
    @Autowired
    private AuthenticationManagerBuilder authenticationManagerBuilder;
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    @Autowired
    private TokenProvider tokenProvider;

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return authenticationManager();
    }

    @PostConstruct
    public void init() {
        try {
            authenticationManagerBuilder
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
              /*  .headers()
                .cacheControl().disable().and()*/
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .requestMatchers(EndpointRequest.to(ShutdownEndpoint.class))
                .hasRole("SUPER_ADMIN")
                .requestMatchers(EndpointRequest.toAnyEndpoint())
                .permitAll()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations())
                .permitAll()
                .antMatchers("/user/authenticate").permitAll()
                .antMatchers("/user/countries").permitAll()
                .antMatchers(HttpMethod.POST, "/marketing/guest-users").permitAll()
                .antMatchers("/user/activate/account/**").permitAll()
                .antMatchers(HttpMethod.GET, "/customer/activate/account/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/customer/activate/account/**").permitAll()
                .antMatchers(HttpMethod.POST, "/admin/contact-enquiries").permitAll()
                .antMatchers("/user/reset-password").permitAll()
                .antMatchers("/payment/**").permitAll()
                .antMatchers("/user/reset-password/**").permitAll()
                .antMatchers("/user/forgot-password/**").permitAll()
                .antMatchers("/subscription/log-order-transaction").permitAll()
                .antMatchers("/subscription/log-invoice-transaction").permitAll()
                .antMatchers(HttpMethod.POST, "/admin/add-p2p-distributor/self-register").permitAll()
//                .antMatchers("/subscription/log-subscription").permitAll()
                .antMatchers("/user/register").permitAll()
                .antMatchers("/leaders-wall/list").permitAll()
                .antMatchers("/reset-password/**").permitAll()
                .antMatchers("/admin/app-ambassador/token-free").permitAll()
                .antMatchers("/admin/app-ambassador/eligible-for-bonuses/token-free").permitAll()


                .antMatchers("/swagger-ui.html", "/v2/api-docs", "/webjars/**", "/swagger-resources/**", "/swagger-ui.html#!/", "/configuration/**", "/configuration/ui").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(corsFilter, SessionManagementFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .httpBasic().and()
                .apply(securityConfigurerAdapter());
    }

    private JwtConfigurer securityConfigurerAdapter() {
        return new JwtConfigurer(tokenProvider);
    }


}


