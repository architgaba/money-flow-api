package com.mfm.api.configuration;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        String userName;
        try{
            userName= SecurityContextHolder.getContext().getAuthentication().getName();
        }
        catch (NullPointerException e){
            userName="P2P Team";
        }
        return Optional.ofNullable(userName);
    }
}

